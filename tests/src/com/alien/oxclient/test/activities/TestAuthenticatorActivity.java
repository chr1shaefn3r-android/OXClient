/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.activities;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.OXConstants;
import com.alien.oxclient.OXUtilsImpl;
import com.alien.oxclient.R;
import com.alien.oxclient.activities.AuthenticatorActivity;
import com.alien.oxclient.test.action.mocks.OXActionLoginMock;
import com.alien.oxclient.test.mocks.OXAccountManagerMockImpl;

public class TestAuthenticatorActivity extends
		OXActionBasedActivityInstrumentationTestCase2<AuthenticatorActivity> {

	private OXAccountManagerMockImpl mAccountManager;
	private OXActionLoginMock mOXActionLoginMockImpl;
	private EditText mServer;
	private EditText mUsername;
	private EditText mPassword;
	private Button mLogin;

	public TestAuthenticatorActivity(String name) {
		super(AuthenticatorActivity.class);
		setName(name);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		// Set up dependencies
		mAccountManager = new OXAccountManagerMockImpl();
		OXApplication.setAccountManager(mAccountManager);
		mOXActionLoginMockImpl = new OXActionLoginMock();
		mOXActionDependencyManagerMockImpl
				.setOXActionLoginMockImpl(mOXActionLoginMockImpl);
	}

	/**
	 * Confirm that all needed Views are actually shown
	 */
	public void testLoginOfferedWithNewAccount() {
		startActivity();

		OXApplication.setUtils(new OXUtilsImpl(mActivity));

		mServer = (EditText) mActivity
				.findViewById(R.id.authenticatoractivity_editText_server);
		mUsername = (EditText) mActivity
				.findViewById(R.id.authenticatoractivity_editText_username);
		mPassword = (EditText) mActivity
				.findViewById(R.id.authenticatoractivity_editText_password);
		mLogin = (Button) mActivity
				.findViewById(R.id.authenticatoractivity_button_ok);

		// Check for all views to be: empty
		// Server
		assertNotNull(mServer);
		assertEquals(View.VISIBLE, mServer.getVisibility());
		assertTrue(TextUtils.isEmpty(mServer.getText()));
		// Username
		assertNotNull(mUsername);
		assertEquals(View.VISIBLE, mUsername.getVisibility());
		assertTrue(TextUtils.isEmpty(mUsername.getText()));
		// Password
		assertNotNull(mPassword);
		assertEquals(View.VISIBLE, mPassword.getVisibility());
		assertTrue(TextUtils.isEmpty(mPassword.getText()));
		// Login
		assertNotNull(mLogin);
		assertEquals(View.VISIBLE, mLogin.getVisibility());
	}

	public void testLoginOfferedWithAccountUpdate() throws InterruptedException {
		mOXActionLoginMockImpl
				.setAuthtoken("sessionID=a235fae104f34833a8df8a72cd9d899b;JSESSIONID=0e9ebe31e50c6db5ec3fba9134e61d19.APP1;"
						+ "open-xchange-secret-YtGl3FOv6xh4AmQSDhGYFw=b6eba18fb1dc5098bf9cbcb6cf281c25;");
		String expectUsername = "JohnDoe";
		final String newPassword = "superSecret";
		String server = mAccountManager.getAccountServer();
		Intent intent = new Intent();
		intent.putExtra(OXConstants.PARAM_USERNAME, expectUsername);
		intent.putExtra(OXConstants.PARAM_CONFIRMCREDENTIALS, true);
		setActivityIntent(intent);

		startActivity();

		OXApplication.setUtils(new OXUtilsImpl(mActivity));

		mServer = (EditText) mActivity
				.findViewById(R.id.authenticatoractivity_editText_server);
		mUsername = (EditText) mActivity
				.findViewById(R.id.authenticatoractivity_editText_username);
		final EditText mPassword = (EditText) mActivity
				.findViewById(R.id.authenticatoractivity_editText_password);
		final Button mLogin = (Button) mActivity
				.findViewById(R.id.authenticatoractivity_button_ok);

		// Server
		assertNotNull(mServer);
		assertEquals(View.VISIBLE, mServer.getVisibility());
		assertEquals(server, mServer.getText().toString());
		// Username
		assertNotNull(mUsername);
		assertEquals(View.VISIBLE, mUsername.getVisibility());
		assertEquals(expectUsername, mUsername.getText().toString());
		// Password
		assertNotNull(mPassword);
		assertEquals(View.VISIBLE, mPassword.getVisibility());
		// Loginbutton
		assertNotNull(mLogin);
		assertEquals(View.VISIBLE, mLogin.getVisibility());

		mActivity.runOnUiThread(new Runnable() {
			public void run() {
				// Set a new password
				mPassword.setText(newPassword);
				// Perform the click
				assertTrue(mLogin.performClick());
			}
		});

		// Wait for the Activity to finish login
		super.waitForFirstRoundtripToFinish();

		// Check if the new password is in the account now
		assertEquals(server, mAccountManager.getAccountServer());
		assertEquals(newPassword, mAccountManager.getPassword(null));
		// assertTrue(mActivity.isFinishing()); // For whatever reason this
		// fails when all tests a run ...
	}
}
