/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.activities;

import android.content.Intent;
import android.test.ActivityUnitTestCase;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.OXClient;
import com.alien.oxclient.mockinterfaces.OXPreferences;
import com.alien.oxclient.mockinterfaces.OXUtils;
import com.alien.oxclient.test.mocks.OXAccountManagerMockImpl;
import com.alien.oxclient.test.mocks.OXPreferencesMockImpl;
import com.alien.oxclient.test.mocks.OXUtilsMockImpl;

public class TestOXClient extends ActivityUnitTestCase<OXClient> {

	private Intent mStartIntent;

	public TestOXClient(String name) {
		super(OXClient.class);
		OXApplication.setAccountManager(new OXAccountManagerMockImpl());
		OXUtils utils = new OXUtilsMockImpl();
		OXApplication.setUtils(utils);
		OXPreferences prefs = new OXPreferencesMockImpl();
		// Force the situation:
		// The queried version is less then the saved version
		prefs.setVersionCode(-4);
		prefs.setInformationenDialogSeen(true);
		OXApplication.setPrefs(prefs);
		setName(name);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mStartIntent = new Intent(Intent.ACTION_MAIN);
	}

	public void testOnCreate() {
		startActivity(mStartIntent, null, null);
		assertNotNull(getActivity());
	}

	public void testOnResume() {
		OXClient oxC = startActivity(mStartIntent, null, null);
		getInstrumentation().callActivityOnResume(oxC);
		assertNotNull(getActivity());
	}
}
