package com.alien.oxclient.test.activities;

import android.app.Activity;
import android.test.ActivityInstrumentationTestCase2;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.activities.OnUnitTestHooks;
import com.alien.oxclient.test.mocks.OXActionDependencyManagerMockImpl;

public class OXActionBasedActivityInstrumentationTestCase2<T extends Activity>
		extends ActivityInstrumentationTestCase2<T> {

	protected OXActionDependencyManagerMockImpl mOXActionDependencyManagerMockImpl;
	protected Activity mActivity;
	protected OnUnitTestHooks mUnitTestHooks;

	public OXActionBasedActivityInstrumentationTestCase2(Class<T> activityClass) {
		super(activityClass);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mOXActionDependencyManagerMockImpl = new OXActionDependencyManagerMockImpl();
		OXApplication
				.setActionDependencyManager(mOXActionDependencyManagerMockImpl);
	}

	protected void startActivity() {
		mActivity = getActivity();
		mUnitTestHooks = (OnUnitTestHooks) mActivity;
	}

	/**
	 * Wait until FirstRoundtripFinished but maximum "maximum"
	 */
	protected void waitForFirstRoundtripToFinish() {
		long started = System.currentTimeMillis();
		int maximum = 10000;
		while (mUnitTestHooks.isFirstRoundtripFinished()) {
			if((System.currentTimeMillis() - started) > maximum) fail("That took tooo long! break up!");
		}
	}
}
