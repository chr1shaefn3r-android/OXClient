/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.activities;

import android.content.Intent;
import android.test.ActivityUnitTestCase;

import com.alien.oxclient.activities.AddConnection;

public class TestAddConnection extends ActivityUnitTestCase<AddConnection> {

	private Intent mStartIntent;

	public TestAddConnection(String name) {
		super(AddConnection.class);
		setName(name);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mStartIntent = new Intent(Intent.ACTION_MAIN);
	}

	public void testOnCreate() {
		startActivity(mStartIntent, null, null);
		assertNotNull(getActivity());
	}
}
