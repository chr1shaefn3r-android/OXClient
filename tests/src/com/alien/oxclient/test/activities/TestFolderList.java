package com.alien.oxclient.test.activities;

import java.util.ArrayList;

import android.widget.ListView;

import com.alien.oxclient.activities.FolderList;
import com.alien.oxclient.test.action.mocks.OXActionGetRootFolderListMock;

public class TestFolderList extends
		OXActionBasedActivityInstrumentationTestCase2<FolderList> {

	private ListView mListView;
	private OXActionGetRootFolderListMock mOXActionGetRootFolderListMockImpl;

	public TestFolderList() {
		super(FolderList.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mOXActionGetRootFolderListMockImpl = new OXActionGetRootFolderListMock();
		mOXActionDependencyManagerMockImpl
				.setOXActionGetRootFolderList(mOXActionGetRootFolderListMockImpl);

	}

	public void testListView() throws InterruptedException {
		ArrayList<String> result = new ArrayList<String>();
		result.add("This is a result!");
		mOXActionGetRootFolderListMockImpl.setResult(result);
		startActivity();
		super.waitForFirstRoundtripToFinish();
		assertEquals(result.size(), mListView.getCount());
	}

	// TODO: UnitTests fuer Fehlerfaelle schreiben: Was passiert bei einer
	// Exception? Wird das MessageView angezeigt? Etc.
	@Override
	protected void startActivity() {
		super.startActivity();
		mListView = (ListView) mActivity.findViewById(android.R.id.list);
	}
}
