/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.client.Session;
import com.alien.oxclient.test.mocks.OXAccountManagerMockImpl;
import com.alien.oxclient.test.mocks.OXNetworkMockImpl;

public class OXNetworkTestCase extends OXTestCase {

	protected OXAccountManagerMockImpl mOXAccountManagerMockImpl;
	protected OXNetworkMockImpl mOXNetworkMockImpl;
	protected Session mSession;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mOXAccountManagerMockImpl = new OXAccountManagerMockImpl();
		OXApplication.setAccountManager(mOXAccountManagerMockImpl);
		mOXNetworkMockImpl = new OXNetworkMockImpl();
		OXApplication.setNetwork(mOXNetworkMockImpl);
		mSession = new Session(mOXAccountManagerMockImpl.blockingGetAuthToken(
				null, false), mOXAccountManagerMockImpl.getAccountServer());
	}
}
