package com.alien.oxclient.test.mocks;

import java.util.Calendar;
import java.util.Date;

import com.alien.oxclient.mockinterfaces.OXCalendar;

public class OXCalendarMockImpl implements OXCalendar {

	private Calendar mCalendar;
	private int mYear;
	private int mMonth;
	private int mDay;
	private int mHourOfDay;
	private int mMinute;
	private int mSecond;

	public OXCalendarMockImpl() {
		this.mCalendar = Calendar.getInstance();
	}

	public OXCalendarMockImpl(int pYear, int pMonth, int pDay, int pHourOfDay,
			int pMinute, int pSecond) {
		this();
		this.mYear = pYear;
		this.mMonth = pMonth;
		this.mDay = pDay;
		this.mHourOfDay = pHourOfDay;
		this.mMinute = pMinute;
		this.mSecond = pSecond;
		mCalendar.set(pYear, pMonth, pDay, pHourOfDay, pMinute, pSecond);
	}

	public long getTimeInMillis() throws IllegalArgumentException {
		return mCalendar.getTimeInMillis();
	}

	public void setHourAndMinute(Date pDate) {
		mCalendar.set(Calendar.HOUR_OF_DAY, pDate.getHours());
		mCalendar.set(Calendar.MINUTE, pDate.getMinutes());
		mCalendar.set(Calendar.SECOND, 0);
	}

	public int get(int pField) {
		return mCalendar.get(pField);
	}

	public OXCalendar refresh() {
		return this;
	}

	public boolean after(OXCalendar pCalendar) throws IllegalArgumentException {
		return mCalendar.after(pCalendar.getCalendar());
	}

	public boolean before(OXCalendar pCalendar) throws IllegalArgumentException {
		return mCalendar.before(pCalendar.getCalendar());
	}

	public Calendar getCalendar() {
		return mCalendar;
	}

	public Object clone() {
		return new OXCalendarMockImpl(mYear, mMonth, mDay, mHourOfDay, mMinute,
				mSecond);
	};

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(mCalendar.get(Calendar.DATE) + ".");
		sb.append(mCalendar.get(Calendar.MONTH) + ".");
		sb.append(mCalendar.get(Calendar.YEAR) + "/");
		sb.append(mCalendar.get(Calendar.HOUR_OF_DAY) + ":");
		sb.append(mCalendar.get(Calendar.MINUTE) + ":");
		sb.append(mCalendar.get(Calendar.SECOND));
		return sb.toString();
	}
}
