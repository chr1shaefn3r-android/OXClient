package com.alien.oxclient.test.mocks;

import android.app.Notification;
import android.content.Context;

import com.alien.oxclient.mockinterfaces.OXNotificationManager;

public class OXNotificationManagerMockImpl implements OXNotificationManager {

	public OXNotificationManagerMockImpl(Context pContex) {
	}

	public void notify(int pId, Notification pNotification) {
	}
}
