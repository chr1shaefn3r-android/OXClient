package com.alien.oxclient.test.mocks;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.HttpContext;

import android.graphics.Bitmap;

import com.alien.oxclient.mockinterfaces.OXNetwork;

/**
 * This class should propably behave like the 'real' OX-Http-Api does. So check
 * http://oxpedia.org/wiki/index.php?title=HTTP_API
 * 
 */
// TODO: It should be adjustable whether the execute should fail due to a login
// problem (changed password on the server-side)
public class OXNetworkMockImpl implements OXNetwork {

	private boolean mTimedout = false;
	private String mResponse;
	private String mSecondResponse;
	private int mHttpStatusCode = -1;
	private List<Cookie> mCookieList = new ArrayList<Cookie>();
	private Bitmap mBitmap;
	private int mCallCounter = 0;

	@Override
	public String execute(HttpUriRequest pRequest, HttpContext pContext)
			throws IOException, HttpResponseException {
		if(mTimedout) {
			throw new ConnectTimeoutException("The connection to '"
					+ pRequest.getURI() + "' timed out.");
		} else if(mHttpStatusCode != -1) {
			throw new HttpResponseException(mHttpStatusCode, "Failed!");
		} else {
			if(pContext != null) {
				CookieStore cookieStore = new BasicCookieStore();
				for(Cookie cookie : mCookieList) {
					cookieStore.addCookie(cookie);
				}
				pContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
			}
			if(mCallCounter == 0) {
				this.mCallCounter += 1;
				return mResponse;
			} else {
				return mSecondResponse;
			}
		}
	}

	/**
	 * Set whether our mock-server should time out.
	 * 
	 * @param pTimedout
	 *            If true a {@link SocketTimeoutException} will be thrown.
	 */
	public void setTimedout(boolean pTimedout) {
		this.mTimedout = pTimedout;
	}

	public void addCookie(String pCookieName, String pCookieValue,
			String pServer) {
		Cookie cookie = new BasicClientCookie(pCookieName, pCookieValue);
		((BasicClientCookie) cookie).setDomain(pServer);
		mCookieList.add(cookie);
	}

	public void setResponse(String pContent) {
		this.mResponse = pContent;
		this.mCallCounter = 0;
	}

	public void setSecondResponse(String pSecondContent) {
		this.mSecondResponse = pSecondContent;
	}

	public void setFailingHttpStatusCode() {
		this.mHttpStatusCode = HttpStatus.SC_INTERNAL_SERVER_ERROR;
		this.mCallCounter = 0;
	}

	@Override
	public Bitmap executeToGetBitmap(HttpUriRequest pRequest,
			HttpContext pContext) throws IOException, HttpResponseException {
		return mBitmap;
	}

	public void setBitmap(Bitmap pBitmap) {
		this.mBitmap = pBitmap;
	}
}
