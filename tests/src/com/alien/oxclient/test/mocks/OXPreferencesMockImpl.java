/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.mocks;

import com.alien.oxclient.mockinterfaces.OXPreferences;
import com.alien.oxclient.preferences.ListPreferenceMultiSelect;

public class OXPreferencesMockImpl implements OXPreferences {

	private int mVersionCode = VERSIONCODE_DEFAULT;
	private String mLangauge = LANGUAGE_DEFAULT;
	private String mDefaultLanguage = DEFAULTLANGUAGE_DEFAULT;
	private long mLastSync = LAST_SYNC_DEFAULT;
	private String mSyncFrequency = SYNC_FREQUENCY_DEFAULT;
	private boolean mIsConstantSyncFrequency = CONSTANT_SYNC_FREQUENCY_DEFAULT;
	private String mMaintimeDays = MAINTIME_DAYS_DEFAULT;
	private String mMaintimeBegin = MAINTIME_BEGIN_DEFAULT;
	private String mMaintimeEnd = MAINTIME_END_DEFAULT;
	private String mMaintimeSyncFrequency = MAINTIME_SYNC_FREQUENCY_DEFAULT;
	private String mSecondaryimeSyncFrequency = SECONDARYTIME_SYNC_FREQUENCY_DEFAULT;
	private boolean mInformationenDialogSeen = INFORMATIONDIALOG_SEEN_DEFAULT;
	private boolean mSyncPicturesOnlyOnWifi = SYNC_PICTURES_ONLY_ON_WIFI_DEFAULT;

	public int getVersionCode() {
		return mVersionCode;
	}

	public void setVersionCode(int pVersionCode) {
		this.mVersionCode = pVersionCode;
	}

	public String getLanguage() {
		return mLangauge;
	}

	public String getDefaultLanguage() {
		return mDefaultLanguage;
	}

	public void setDefaultLanguage(String pDefaultLanguage) {
		this.mDefaultLanguage = pDefaultLanguage;
	}

	public String getFirstEmailMapping() {
		throw new UnsupportedOperationException();
	}

	public void setFirstEmailMapping(String pFirstEmailMapping) {
		throw new UnsupportedOperationException();
	}

	public String getSecondEmailMapping() {
		throw new UnsupportedOperationException();
	}

	public void setSecondEmailMapping(String pSecondEmailMapping) {
		throw new UnsupportedOperationException();
	}

	public String getThirdEmailMapping() {
		throw new UnsupportedOperationException();
	}

	public void setThirdEmailMapping(String pThirdEmailMapping) {
		throw new UnsupportedOperationException();
	}

	public long getLastSync() {
		return mLastSync;
	}

	public void setLastSync(long pLastSync) {
		this.mLastSync = pLastSync;
	}

	public boolean getIsConstantSyncFrequency() {
		return mIsConstantSyncFrequency;
	}

	public String[] getMaintimeDays() {
		return ListPreferenceMultiSelect.parseStoredValue(mMaintimeDays);
	}

	public void setMaintimeDays(String pDays) {
		this.mMaintimeDays = pDays;
	}

	public String getMaintimeBegin() {
		return mMaintimeBegin;
	}

	public String getMaintimeEnd() {
		return mMaintimeEnd;
	}

	public String getMaintimeSyncFrequency() {
		return mMaintimeSyncFrequency;
	}

	public String getSecondarytimeSyncFrequency() {
		return mSecondaryimeSyncFrequency;
	}

	public String getSyncFrequency() {
		return mSyncFrequency;
	}

	public boolean getInformationenDialogSeen() {
		return mInformationenDialogSeen;
	}

	public void setInformationenDialogSeen(boolean pInformationenDialogSeen) {
		this.mInformationenDialogSeen = pInformationenDialogSeen;
	}

	@Override
	public boolean getSyncPicturesOnlyOnWifi() {
		return mSyncPicturesOnlyOnWifi;
	}

	@Override
	public void setSyncPicturesOnlyOnWifi(boolean pSyncPicturesOnlyOnWifi) {
		this.mSyncPicturesOnlyOnWifi = pSyncPicturesOnlyOnWifi;
	}
}
