/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.mocks;

import android.accounts.Account;
import android.os.Bundle;

import com.alien.oxclient.mockinterfaces.OXAccountManager;

public class OXAccountManagerMockImpl implements OXAccountManager {

	private String mAccountServer = "example.com";
	private String mContactsFolder = "Kontakte";
	private String mContactsFolderId = "59";
	private String mPassword = "test123";
	private boolean mEncrypted = false;
	private String mAuthToken = "sessionID=a235fae104f34833a8df8a72cd9d899b;JSESSIONID=0e9ebe31e50c6db5ec3fba9134e61d19.APP1;"
			+ "open-xchange-secret-YtGl3FOv6xh4AmQSDhGYFw=b6eba18fb1dc5098bf9cbcb6cf281c25;";

	public String getAccountServer() {
		return mAccountServer;
	}

	public void setAccountServer(String pServer) {
		this.mAccountServer = pServer;
	}

	@Override
	public boolean getEncrypted() {
		return mEncrypted;
	}

	@Override
	public void setEncrypted(boolean pEncrypted) {
		this.mEncrypted = pEncrypted;
	}

	@Override
	public void setEncrypted(Account pAccount, boolean pEncrypted) {
		this.mEncrypted = pEncrypted;
	}

	public String getContactsFolder() {
		return mContactsFolder;
	}

	public void setContactsFolder(String pContactsFolder) {
		this.mContactsFolder = pContactsFolder;
	}

	public String getContactsFolderId() {
		return mContactsFolderId;
	}

	public void setContactsFolderId(String pContactsFolderId) {
		this.mContactsFolderId = pContactsFolderId;
	}

	public Account getOXAccount() {
		Account account = new Account("testuser", "com.openxchange.android");
		return account;
	}

	public void addAccountExplicitly(Account pAccount, String pPassword,
			Bundle pUserData) {
		throw new UnsupportedOperationException();
	}

	public void setAuthToken(String pAuthTokenType, String pAuthToken) {
		throw new UnsupportedOperationException();
	}

	public String getPassword(Account pAccount) {
		return mPassword;
	}

	public void setPassword(String pPassword) {
		this.mPassword = pPassword;
	}

	public String getUserData(Account pAccount, String pKey) {
		throw new UnsupportedOperationException();
	}

	public void invalidateAuthToken(String pAuthTokenType, String pAuthToken) {
		// it's a mock, just do nothing
	}

	public String blockingGetAuthToken(String pAuthTokenType,
			boolean pNotifyAuthFailure) {
		return mAuthToken;
	}

	public void setAuthToken(String pAuthToken) {
		this.mAuthToken = pAuthToken;
	}
}
