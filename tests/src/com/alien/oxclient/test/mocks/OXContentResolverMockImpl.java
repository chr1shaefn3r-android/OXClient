/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.mocks;

import java.util.ArrayList;

import android.accounts.Account;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.Bundle;
import android.os.RemoteException;

import com.alien.oxclient.mockinterfaces.OXContentResolver;

public class OXContentResolverMockImpl implements OXContentResolver {

	private Account mAccount;
	private String mAuthority;
	private Bundle mExtrasBundle;
	private long mFrequency;
	private Context mContext;

	public OXContentResolverMockImpl(Context pContext) {
		this.mContext = pContext;
	}

	public void addPeriodicSync(Account pAccount, String pAuthority,
			Bundle pExtrasBundle, long pFrequency) {
		this.mAccount = pAccount;
		this.mAuthority = pAuthority;
		this.mExtrasBundle = pExtrasBundle;
		this.mFrequency = pFrequency;
	}

	public Account getAccount() {
		return mAccount;
	}

	public String getAuthority() {
		return mAuthority;
	}

	public Bundle getExtrasBundle() {
		return mExtrasBundle;
	}

	public long getFrequency() {
		return mFrequency;
	}

	public ContentProviderResult[] applyBatch(String pAuthority,
			ArrayList<ContentProviderOperation> pOperations)
			throws OperationApplicationException, RemoteException {
		return mContext.getContentResolver()
				.applyBatch(pAuthority, pOperations);
	}
}
