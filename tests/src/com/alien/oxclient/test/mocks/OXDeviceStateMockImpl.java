package com.alien.oxclient.test.mocks;

import com.alien.oxclient.mockinterfaces.OXDeviceState;

public class OXDeviceStateMockImpl implements OXDeviceState {

	private boolean mIsOnWifi = false;

	@Override
	public boolean isOnWifi() {
		return mIsOnWifi;
	}

	public void setIsOnWifi(boolean pIsOnWifi) {
		this.mIsOnWifi = pIsOnWifi;
	}
}
