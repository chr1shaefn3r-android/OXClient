/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.mocks;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Button;
import android.widget.ImageButton;

import com.alien.oxclient.mockinterfaces.OXUtils;

public class OXUtilsMockImpl implements OXUtils {

	private boolean mOnlineStatus = true;
	private boolean mHasOXAccount = true;
	private boolean mIsIntentAvailable = true;

	public String constructColumns() {
		throw new UnsupportedOperationException();
	}

	public boolean isOnline() {
		return mOnlineStatus;
	}

	public boolean hasOXAccount() {
		return mHasOXAccount;
	}

	public boolean isHoneyCombOrBetter() {
		return(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB);
	}

	@Override
	public boolean isIcsOrBetter() {
		return(Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH);
	}

	@Override
	public boolean hasActionBar(Activity pActivity) {
		return(isIcsOrBetter() && pActivity.getActionBar() != null);
	}

	public String errorToString(JSONObject pJSON) {
		throw new UnsupportedOperationException();
	}

	public void toggleButton(Button pButton, boolean pState) {
		throw new UnsupportedOperationException();
	}

	public void toggleButton(ImageButton pButton, boolean pState) {
		throw new UnsupportedOperationException();
	}

	public String checkAndcorrectServerInput(String pServer) {
		throw new UnsupportedOperationException();
	}

	public boolean isIntentAvailable(String pAction) {
		return mIsIntentAvailable;
	}

	public boolean isIntentAvailable(Intent pIntent) {
		return mIsIntentAvailable;
	}

	public void setOnlineStatus(boolean pOnlineStatus) {
		this.mOnlineStatus = pOnlineStatus;
	}

	public String getStringFromName(Context pContext, String pName) {
		throw new UnsupportedOperationException();
	}
}
