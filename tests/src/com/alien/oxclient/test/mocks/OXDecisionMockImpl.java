package com.alien.oxclient.test.mocks;

import com.alien.oxclient.mockinterfaces.OXDecision;

public class OXDecisionMockImpl implements OXDecision {

	private boolean mShouldSync;

	@Override
	public boolean shouldSyncPictures() {
		return mShouldSync;
	}

	public void setShouldSyncPictures(boolean pShouldSyncPictures) {
		this.mShouldSync = pShouldSyncPictures;
	}
}
