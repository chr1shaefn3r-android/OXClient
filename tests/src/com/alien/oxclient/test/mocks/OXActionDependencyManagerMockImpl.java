package com.alien.oxclient.test.mocks;

import java.util.ArrayList;
import java.util.Date;

import android.graphics.Bitmap;

import com.alien.oxclient.action.OXAction;
import com.alien.oxclient.action.OXActionDependencyManager;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXFolder;
import com.alien.oxclient.test.action.mocks.OXActionCreateContactMockImpl;
import com.alien.oxclient.test.action.mocks.OXActionDeleteContactMock;
import com.alien.oxclient.test.action.mocks.OXActionGetAllContactIdsMock;
import com.alien.oxclient.test.action.mocks.OXActionGetAllContactsMock;
import com.alien.oxclient.test.action.mocks.OXActionGetContactFolderMock;
import com.alien.oxclient.test.action.mocks.OXActionGetContactMock;
import com.alien.oxclient.test.action.mocks.OXActionGetPictureIntoContactMock;
import com.alien.oxclient.test.action.mocks.OXActionGetRootFolderListMock;
import com.alien.oxclient.test.action.mocks.OXActionGetUpdatedContactsMock;
import com.alien.oxclient.test.action.mocks.OXActionLoginMock;
import com.alien.oxclient.test.action.mocks.OXActionUpdateContactMock;

public class OXActionDependencyManagerMockImpl implements
		OXActionDependencyManager {

	private OXActionDeleteContactMock mOXActionDeleteContactMockImpl;
	private OXActionUpdateContactMock mOXActionUpdateContactMockImpl;
	private OXActionCreateContactMockImpl mOXActionCreateContactMockImpl;
	private OXActionGetContactMock mOXActionGetContactMockImpl;
	private OXActionGetPictureIntoContactMock mOXActionGetPictureIntoContact;
	private OXActionGetAllContactsMock mOXActionGetAllContactsMockImpl;
	private OXActionGetAllContactIdsMock mOXActionGetAllContactIdsMockImpl;
	private OXActionGetUpdatedContactsMock mOXActionGetUpdatedContactsMockImpl;
	private OXActionGetContactFolderMock mOXActionGetContactFolderMockImpl;
	private OXActionGetRootFolderListMock mOXActionGetRootFolderListMockImpl;
	private OXActionLoginMock mOXActionLoginMockImpl;

	@Override
	public OXAction<String> getOXActionDeleteContact(OXContact pContact) {
		return mOXActionDeleteContactMockImpl;
	}

	public void setOXActionDeleteContact(OXActionDeleteContactMock pAction) {
		this.mOXActionDeleteContactMockImpl = pAction;
	}

	@Override
	public OXAction<String> getOXActionUpdateContact(OXContact pContact) {
		return mOXActionUpdateContactMockImpl;
	}

	public void setOXActionUpdateContact(OXActionUpdateContactMock pAction) {
		this.mOXActionUpdateContactMockImpl = pAction;
	}

	@Override
	public OXAction<String> getOXActionCreateContact(OXContact pContact) {
		return mOXActionCreateContactMockImpl;
	}

	public void setOXActionCreateContact(OXActionCreateContactMockImpl pAction) {
		this.mOXActionCreateContactMockImpl = pAction;
	}

	@Override
	public OXAction<OXContact> getOXActionGetContact(String pOXId) {
		return mOXActionGetContactMockImpl;
	}

	public void setOXActionGetContact(OXActionGetContactMock pAction) {
		this.mOXActionGetContactMockImpl = pAction;
	}

	@Override
	public OXAction<Bitmap> getOXActionGetPictureIntoContact(OXContact pContact) {
		return mOXActionGetPictureIntoContact;
	}

	public void setOXActionGetPictureIntoContact(
			OXActionGetPictureIntoContactMock pAction) {
		this.mOXActionGetPictureIntoContact = pAction;
	}

	@Override
	public OXAction<OXFolder> getOXActionGetContactFolder() {
		return mOXActionGetContactFolderMockImpl;
	}

	public void setOXActionGetContactFolder(OXActionGetContactFolderMock pAction) {
		this.mOXActionGetContactFolderMockImpl = pAction;
	}

	@Override
	public OXAction<ArrayList<String>> getOXActionGetRootFolderList() {
		return mOXActionGetRootFolderListMockImpl;
	}

	public void setOXActionGetRootFolderList(
			OXActionGetRootFolderListMock pAction) {
		this.mOXActionGetRootFolderListMockImpl = pAction;
	}

	@Override
	public OXAction<String> getOXActionLogin(String pHost, String pUsername,
			String pPassword) {
		return mOXActionLoginMockImpl;
	}

	public void setOXActionLoginMockImpl(OXActionLoginMock pAction) {
		this.mOXActionLoginMockImpl = pAction;
	}

	@Override
	public OXAction<ArrayList<OXContact>> getOXActionGetAllContacts() {
		return mOXActionGetAllContactsMockImpl;
	}

	public void setOXActionGetAllContactsMockImpl(
			OXActionGetAllContactsMock pAction) {
		this.mOXActionGetAllContactsMockImpl = pAction;
	}

	@Override
	public OXAction<ArrayList<String>> getOXActionGetAllContactIds() {
		return mOXActionGetAllContactIdsMockImpl;
	}

	public void setOXActionGetAllContactIdsMockImpl(
			OXActionGetAllContactIdsMock pAction) {
		this.mOXActionGetAllContactIdsMockImpl = pAction;
	}

	@Override
	public OXAction<ArrayList<OXContact>> getOXActionGetUpdatedContacts(
			Date pLastUpdated) {
		return mOXActionGetUpdatedContactsMockImpl;
	}

	public void setOXActionGetUpdatedContactsMockImpl(
			OXActionGetUpdatedContactsMock pAction) {
		this.mOXActionGetUpdatedContactsMockImpl = pAction;
	}
}
