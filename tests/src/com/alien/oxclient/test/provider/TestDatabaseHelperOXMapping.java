/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.provider;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.ActivityInstrumentationTestCase2;
import android.test.mock.MockContext;

import com.alien.oxclient.OXClient;
import com.alien.oxclient.provider.OXMappingDatabaseHelper;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class TestDatabaseHelperOXMapping extends
		ActivityInstrumentationTestCase2 {

	// private final String TAG = "TestDatabaseHelperOXMapping";

	private SQLiteDatabase mDb;
	private OXMappingDatabaseHelper mHelper;

	public TestDatabaseHelperOXMapping() {
		super("com.alien.oxclient", OXClient.class);
	}

	public TestDatabaseHelperOXMapping(Class pClass) {
		super(pClass);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		final SQLiteDatabase db = SQLiteDatabase.create(null);
		Context context = new MockContext() {
			@Override
			public SQLiteDatabase openOrCreateDatabase(String file, int mode,
					SQLiteDatabase.CursorFactory factory) {
				return db;
			};
		};
		mHelper = new OXMappingDatabaseHelper(context);
		mDb = mHelper.getWritableDatabase();
	}

	/**
	 * Simple method testing
	 */
	/** Method: onCreate **/
	public void testOnCreate() {
		assertTrue(mDb.isOpen());
		Cursor cursor = mHelper.query(null, null, null, null);
		assertNotNull(cursor);
		try {
			assertEquals(61, cursor.getCount());
		} finally {
			cursor.close();
		}
	}

	/** Method: onUpgrade **/
	public void testOnUpgrade() {
		mHelper.onUpgrade(mDb, 0, 0);
		Cursor cursor = mHelper.query(null, null, null, null);
		assertNotNull(cursor);
		try {
			assertEquals(61, cursor.getCount());
		} finally {
			cursor.close();
		}
	}

	/** Method: Query **/
	public void testQueryWithoutPicture() {
		Cursor cursor = mHelper.query(null, null, null, null);
		assertNotNull(cursor);
		try {
			assertEquals(61, cursor.getCount());
		} finally {
			cursor.close();
		}
		cursor.close();
	}

	public void testQueryWithPicture() {
		Cursor cursor = mHelper.query(null, null, null, null, true);
		assertNotNull(cursor);
		try {
			assertEquals(62, cursor.getCount());
		} finally {
			cursor.close();
		}
		cursor.close();
	}

	@Override
	protected void tearDown() throws Exception {
		mDb.close();
		mDb = null;
		mHelper.close();
		super.tearDown();
	}

	// private void dumpCursor(Cursor cursor) {
	// cursor.moveToPosition(-1);
	// String[] cols = cursor.getColumnNames();
	//
	// Log.i(TAG, "dumpCursor() count: " + cursor.getCount());
	// int index = 0;
	// while (cursor.moveToNext()) {
	// Log.i(TAG, index + " {");
	// for (int i = 0; i < cols.length; i++) {
	// Log.i(TAG, "    " + cols[i] + '=' + cursor.getString(i));
	// }
	// Log.i(TAG, "}");
	// index += 1;
	// }
	// cursor.moveToPosition(-1);
	// }
}
