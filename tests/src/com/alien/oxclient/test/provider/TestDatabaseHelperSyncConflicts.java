/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.provider;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.ActivityInstrumentationTestCase2;
import android.test.mock.MockContext;

import com.alien.oxclient.OXClient;
import com.alien.oxclient.provider.SyncConflicts;
import com.alien.oxclient.provider.SyncConflictsDatabaseHelper;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class TestDatabaseHelperSyncConflicts extends
		ActivityInstrumentationTestCase2 {

	// private final String TAG = "TestDatabaseHelperSyncConflicts";

	private SQLiteDatabase mDb;
	private SyncConflictsDatabaseHelper mHelper;

	public TestDatabaseHelperSyncConflicts() {
		super("com.alien.oxclient", OXClient.class);
	}

	public TestDatabaseHelperSyncConflicts(Class pClass) {
		super(pClass);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		final SQLiteDatabase db = SQLiteDatabase.create(null);
		Context context = new MockContext() {
			@Override
			public SQLiteDatabase openOrCreateDatabase(String file, int mode,
					SQLiteDatabase.CursorFactory factory) {
				return db;
			};
		};
		mHelper = new SyncConflictsDatabaseHelper(context);
		mDb = mHelper.getWritableDatabase();
		wipeData(mDb);
	}

	/**
	 * Simple method testing
	 */
	/** Method: onCreate **/
	public void testOnCreate() {
		// Noch nicht ganz sicher wie genau onCreate getestet werden muss
		// vor allen Dingen weil onCreate keinen Rueckgabewert hat ...
	}

	/** Method: onUpgrade **/
	public void testOnUpgrade() {
		mHelper.onUpgrade(mDb, 0, 0);
		Cursor cursor = mHelper.query(null, null, null, null);
		assertNotNull(cursor);
		try {
			assertEquals(0, cursor.getCount());
		} finally {
			cursor.close();
		}
	}

	/** Method: Query **/
	public void testQuery() {
		Cursor cursor = mHelper.query(null, null, null, null);
		assertNotNull(cursor);
		cursor.close();
	}

	/** Method: SyncConflictsConstructor **/
	public void testConstructor() {
		assertNotNull(new SyncConflicts());
	}

	@Override
	protected void tearDown() throws Exception {
		mDb.close();
		mDb = null;
		mHelper.close();
		super.tearDown();
	}

	public void wipeData(SQLiteDatabase db) {
		db.execSQL("DELETE FROM " + SyncConflicts.TABLE + ";");
	}
	// private void dumpCursor(Cursor cursor) {
	// cursor.moveToPosition(-1);
	// String[] cols = cursor.getColumnNames();
	//
	// Log.i(TAG, "dumpCursor() count: " + cursor.getCount());
	// int index = 0;
	// while (cursor.moveToNext()) {
	// Log.i(TAG, index + " {");
	// for (int i = 0; i < cols.length; i++) {
	// Log.i(TAG, "    " + cols[i] + '=' + cursor.getString(i));
	// }
	// Log.i(TAG, "}");
	// index += 1;
	// }
	// cursor.moveToPosition(-1);
	// }
}
