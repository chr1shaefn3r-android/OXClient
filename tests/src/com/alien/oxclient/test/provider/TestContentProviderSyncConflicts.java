/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.provider;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.test.ProviderTestCase2;

import com.alien.oxclient.provider.SyncConflicts;
import com.alien.oxclient.provider.SyncConflictsContentProvider;
import com.alien.oxclient.provider.SyncConflictsDatabaseHelper;

public class TestContentProviderSyncConflicts extends
		ProviderTestCase2<SyncConflictsContentProvider> {

	private SQLiteDatabase mDb;
	private ContentProvider mProvider;

	public TestContentProviderSyncConflicts() {
		super(SyncConflictsContentProvider.class, "com.alien.oxclient");
	}

	public TestContentProviderSyncConflicts(
			Class<SyncConflictsContentProvider> providerClass,
			String providerAuthority) {
		super(providerClass, providerAuthority);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mProvider = getProvider();
		SyncConflictsDatabaseHelper helper = (SyncConflictsDatabaseHelper) getProvider()
				.getDatabaseHelper();
		mDb = helper.getWritableDatabase();
		wipeData(mDb);
	}

	/**
	 * Simple method testing
	 */
	/** Method: Query **/
	public void testQueryWithMatchUri() {
		Cursor cursor = mProvider.query(SyncConflicts.CONTENT_URI, null, null,
				null, null);
		assertNotNull(cursor);
		try {
			assertEquals(0, cursor.getCount());
		} finally {
			cursor.close();
		}
	}

	public void testQueryWithSearchSuggestionsUri() {
		Cursor cursor = mProvider.query(
				Uri.parse("content://" + SyncConflictsContentProvider.AUTHORITY
						+ "/" + SearchManager.SUGGEST_URI_PATH_QUERY), null,
				null, new String[] { "test" }, null);
		assertNotNull(cursor);
		try {
			assertEquals(0, cursor.getCount());
		} finally {
			cursor.close();
		}
	}

	public void testQueryWithSearchSuggestionsUriAndNullSelectionAargs() {
		try {
			mProvider.query(
					Uri.parse("content://"
							+ SyncConflictsContentProvider.AUTHORITY + "/"
							+ SearchManager.SUGGEST_URI_PATH_QUERY), null,
					null, null, null);
			fail("Should have thrown IllegalArgumentException");
		} catch (IllegalArgumentException expected) {
		}
	}

	/** Method: Query with wrong Uri **/
	public void testQueryWithWrongUri() {
		try {
			mProvider.query(Uri.parse("wrong.uri"), null, null, null, null);
			fail("Should have raised an IllegalArgumentException");
		} catch (IllegalArgumentException expected) {
		}
	}

	/** Method: insert **/
	public void testInsert() {
		ContentValues insertValues = new ContentValues();
		insertValues.put(SyncConflicts.OXID, "0");
		insertValues.put(SyncConflicts.USERNAME, "John, Doe");
		insertValues.put(SyncConflicts.TIMESTAMP, "12:00 GMT");
		mProvider.insert(SyncConflicts.CONTENT_URI, insertValues);

		Cursor cursor = mProvider.query(SyncConflicts.CONTENT_URI, null, null,
				null, null);
		assertNotNull(cursor);
		try {
			// There should be a exactly one row now
			assertEquals(1, cursor.getCount());
			// Move Cursor to first position
			assertTrue(cursor.moveToFirst());
			// Check for OXID
			assertEquals("0",
					cursor.getString(cursor.getColumnIndex(SyncConflicts.OXID)));
			// Check for Username
			assertEquals("John, Doe", cursor.getString(cursor
					.getColumnIndex(SyncConflicts.USERNAME)));
			// Check for Timestamp
			assertEquals("12:00 GMT", cursor.getString(cursor
					.getColumnIndex(SyncConflicts.TIMESTAMP)));
		} finally {
			cursor.close();
		}
	}

	/** Method: insert with wrong uri **/
	public void testInsertWithWrongUri() {
		try {
			mProvider.insert(Uri.parse("wrong.uri"), new ContentValues());
			fail("Should have raised an IllegalArgumentException");
		} catch (IllegalArgumentException expected) {
		}
	}

	/** Method: insert with null value **/
	public void testInsertWithNull() {
		try {
			mProvider.insert(SyncConflicts.CONTENT_URI, null);
			fail("Should have raised an SQLException");
		} catch (SQLException expected) {
		}
	}

	/** Method: delete **/
	public void testDelete() {
		testInsert(); // To initialize
		mProvider.delete(SyncConflicts.CONTENT_URI,
				SyncConflicts.OXID + " = ?", new String[] { "0" });

		Cursor cursor = mProvider.query(SyncConflicts.CONTENT_URI, null, null,
				null, null);
		assertNotNull(cursor);
		try {
			assertEquals(0, cursor.getCount());
		} finally {
			cursor.close();
		}
	}

	/** Method: delete with wrong Uri **/
	public void testDeleteWithWrongUri() {
		testInsert(); // To initialize
		try {
			mProvider.delete(Uri.parse("wrong.uri"), null, null);
			fail("Should have raised an IllegalArgumentException");
		} catch (IllegalArgumentException expected) {
		}
	}

	/** Method: update **/
	public void testUpdate() {
		try {
			mProvider.update(SyncConflicts.CONTENT_URI, new ContentValues(),
					SyncConflicts.OXID + " = ?", new String[] { "0" });
			fail("Should have thrown UnsupportedOperationException!");
		} catch (UnsupportedOperationException expected) {
		}
	}

	/** Method: getType **/
	public void testGetTypeMatchUri() {
		assertEquals(SyncConflicts.CONTENT_TYPE,
				mProvider.getType(SyncConflicts.CONTENT_URI));
	}

	public void testGetTypeSearchSuggestionsUri() {
		assertEquals(
				SearchManager.SUGGEST_MIME_TYPE,
				mProvider.getType(Uri.parse("content://"
						+ SyncConflictsContentProvider.AUTHORITY + "/"
						+ SearchManager.SUGGEST_URI_PATH_QUERY)));
	}

	/** Method: getType with wrong Uri **/
	public void testGetTypeWithWrongUri() {
		try {
			mProvider.getType(Uri.parse("wrong.uri"));
			fail("Should have raised an IllegalArgumentException");
		} catch (IllegalArgumentException expected) {
		}
	}

	/** Method: SyncConflictsConstructor **/
	public void testConstructor() {
		assertNotNull(new SyncConflicts());
	}

	@Override
	protected void tearDown() throws Exception {
		mDb.close();
		mDb = null;
		getProvider().getDatabaseHelper().close();
		super.tearDown();
	}

	public void wipeData(SQLiteDatabase db) {
		db.execSQL("DELETE FROM " + SyncConflicts.TABLE + ";");
	}
}
