/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.test.ProviderTestCase2;

import com.alien.oxclient.provider.Folders;
import com.alien.oxclient.provider.FoldersContentProvider;
import com.alien.oxclient.provider.FoldersDatabaseHelper;

public class TestContentProviderFolders extends
		ProviderTestCase2<FoldersContentProvider> {

	// private final String TAG = "TestContentProviderFolders";

	private SQLiteDatabase mDb;
	private ContentProvider mProvider;

	public TestContentProviderFolders() {
		super(FoldersContentProvider.class, "com.alien.oxclient");
	}

	public TestContentProviderFolders(
			Class<FoldersContentProvider> providerClass,
			String providerAuthority) {
		super(providerClass, providerAuthority);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mProvider = getProvider();
		FoldersDatabaseHelper helper = (FoldersDatabaseHelper) getProvider()
				.getDatabaseHelper();
		mDb = helper.getWritableDatabase();
		wipeData(mDb);
	}

	/**
	 * Simple method testing
	 */
	/** Method: onCreate **/
	public void testOnCreate() {
		// Noch nicht ganz sicher wie genau onCreate getestet werden muss
		// vor allen Dingen weil in meiner Implementierung noch hardcodiert
		// "return true" steht ...
	}

	/** Method: onUpgrade **/
	public void testOnUpgrade() {
		FoldersDatabaseHelper helper = (FoldersDatabaseHelper) getProvider()
				.getDatabaseHelper();
		helper.onUpgrade(mDb, 0, 0);
		Cursor cursor = mProvider.query(Folders.CONTENT_URI, null, null, null,
				null);
		assertNotNull(cursor);
		try {
			assertEquals(0, cursor.getCount());
		} finally {
			cursor.close();
		}
	}

	/** Method: Query **/
	public void testQuery() {
		Cursor cursor = mProvider.query(Folders.CONTENT_URI, null, null, null,
				null);
		assertNotNull(cursor);
		try {
			assertEquals(0, cursor.getCount());
		} finally {
			cursor.close();
		}
	}

	/** Method: Query with wrong Uri **/
	public void testQueryWithWrongUri() {
		try {
			mProvider.query(Uri.parse("wrong.uri"), null, null, null, null);
			fail("Should have raised an IllegalArgumentException");
		} catch (IllegalArgumentException expected) {
		}
	}

	/** Method: insert **/
	public void testInsert() {
		ContentValues insertValues = new ContentValues();
		insertValues.put(Folders.FOLDER_ID, "0");
		insertValues.put(Folders.TITLE, "Testfolder");
		insertValues.put(Folders.SUBFOLDERS, "SubTestfolder");
		mProvider.insert(Folders.CONTENT_URI, insertValues);

		Cursor cursor = mProvider.query(Folders.CONTENT_URI, null, null, null,
				null);
		assertNotNull(cursor);
		try {
			// There should be a exactly one row now
			assertEquals(1, cursor.getCount());
			// Move Cursor to first position
			assertTrue(cursor.moveToFirst());
			// Check for Folder_Id
			assertEquals("0",
					cursor.getString(cursor.getColumnIndex(Folders.FOLDER_ID)));
			// Check for Title
			assertEquals("Testfolder",
					cursor.getString(cursor.getColumnIndex(Folders.TITLE)));
			// Check for Subfolders
			assertEquals("SubTestfolder",
					cursor.getString(cursor.getColumnIndex(Folders.SUBFOLDERS)));
		} finally {
			cursor.close();
		}
	}

	/** Method: insert with wrong uri **/
	public void testInsertWithWrongUri() {
		ContentValues insertValues = new ContentValues();
		insertValues.put(Folders.FOLDER_ID, "0");
		insertValues.put(Folders.TITLE, "Testfolder");
		insertValues.put(Folders.SUBFOLDERS, "SubTestfolder");
		try {
			mProvider.insert(Uri.parse("wrong.uri"), insertValues);
			fail("Should have raised an IllegalArgumentException");
		} catch (IllegalArgumentException expected) {
		}
	}

	/** Method: insert with null value **/
	public void testInsertWithNull() {
		try {
			mProvider.insert(Folders.CONTENT_URI, null);
			fail("Should have raised an SQLException");
		} catch (SQLException expected) {
		}
	}

	/** Method: delete **/
	public void testDelete() {
		testInsert(); // To initialize
		mProvider.delete(Folders.CONTENT_URI, Folders.FOLDER_ID + " = ?",
				new String[] { "0" });

		Cursor cursor = mProvider.query(Folders.CONTENT_URI, null, null, null,
				null);
		assertNotNull(cursor);
		try {
			assertEquals(0, cursor.getCount());
		} finally {
			cursor.close();
		}
	}

	/** Method: delete with wrong Uri **/
	public void testDeleteWithWrongUri() {
		testInsert(); // To initialize
		try {
			mProvider.delete(Uri.parse("wrong.uri"),
					Folders.FOLDER_ID + " = ?", new String[] { "0" });
			fail("Should have raised an IllegalArgumentException");
		} catch (IllegalArgumentException expected) {
		}
	}

	/** Method: update **/
	public void testUpdate() {
		testInsert(); // To initialize
		ContentValues updateValues = new ContentValues();
		updateValues.put(Folders.TITLE, "NewTestFolder");
		updateValues.put(Folders.SUBFOLDERS, "NewSubTestfolder");
		mProvider.update(Folders.CONTENT_URI, updateValues, Folders.FOLDER_ID
				+ " = ?", new String[] { "0" });

		Cursor cursor = mProvider.query(Folders.CONTENT_URI, null, null, null,
				null);
		assertNotNull(cursor);
		try {
			// There should be a exactly one row now
			assertEquals(1, cursor.getCount());
			// Move Cursor to first position
			assertTrue(cursor.moveToFirst());
			// Check for Folder_Id
			assertEquals("0",
					cursor.getString(cursor.getColumnIndex(Folders.FOLDER_ID)));
			// Check for Title
			assertEquals("NewTestFolder",
					cursor.getString(cursor.getColumnIndex(Folders.TITLE)));
			// Check for Subfolders
			assertEquals("NewSubTestfolder",
					cursor.getString(cursor.getColumnIndex(Folders.SUBFOLDERS)));
		} finally {
			cursor.close();
		}
	}

	/** Method: update with wrong Uri **/
	public void testUpdateWithWrongUri() {
		testInsert(); // To initialize
		ContentValues updateValues = new ContentValues();
		updateValues.put(Folders.TITLE, "NewTestFolder");
		updateValues.put(Folders.SUBFOLDERS, "NewSubTestfolder");
		try {
			mProvider.update(Uri.parse("wrong.uri"), updateValues,
					Folders.FOLDER_ID + " = ?", new String[] { "0" });
			fail("Should have raised an IllegalArgumentException");
		} catch (IllegalArgumentException expected) {
		}
	}

	/** Method: update with null value **/
	public void testUpdateWithNull() {
		testInsert(); // To initialize
		try {
			mProvider.update(Folders.CONTENT_URI, null, null, null);
			fail("Should have raised an IllegalArgumentException");
		} catch (IllegalArgumentException expected) {
		}
	}

	/** Method: getType **/
	public void testGetType() {
		assertEquals(Folders.CONTENT_TYPE,
				mProvider.getType(Folders.CONTENT_URI));
	}

	/** Method: getType with wrong Uri **/
	public void testGetTypeWithWrongUri() {
		try {
			mProvider.getType(Uri.parse("wrong.uri"));
			fail("Should have raised an IllegalArgumentException");
		} catch (IllegalArgumentException expected) {
		}
	}

	/** Method: FoldersConstructor **/
	public void testConstructor() {
		assertNotNull(new Folders());
	}

	@Override
	protected void tearDown() throws Exception {
		mDb.close();
		mDb = null;
		getProvider().getDatabaseHelper().close();
		super.tearDown();
	}

	public void wipeData(SQLiteDatabase db) {
		db.execSQL("DELETE FROM " + Folders.TABLE + ";");
	}
	// private void dumpCursor(Cursor cursor) {
	// cursor.moveToPosition(-1);
	// String[] cols = cursor.getColumnNames();
	//
	// Log.i(TAG, "dumpCursor() count: " + cursor.getCount());
	// int index = 0;
	// while (cursor.moveToNext()) {
	// Log.i(TAG, index + " {");
	// for (int i = 0; i < cols.length; i++) {
	// Log.i(TAG, "    " + cols[i] + '=' + cursor.getString(i));
	// }
	// Log.i(TAG, "}");
	// index += 1;
	// }
	// cursor.moveToPosition(-1);
	// }
}
