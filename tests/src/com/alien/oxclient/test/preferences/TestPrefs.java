/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.preferences;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.alien.oxclient.preferences.OXPreferencesImpl;
import com.alien.oxclient.test.OXTestCase;

public class TestPrefs extends OXTestCase {

	private RenamingMockContext mockContext;
	private OXPreferencesImpl mPrefs;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mockContext = new RenamingMockContext(getContext());
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(mockContext);
		SharedPreferences.Editor editor = sharedPrefs.edit();
		editor.clear();
		editor.commit();
	}

	public void testPreconditions() {
		mPrefs = new OXPreferencesImpl(mockContext);
		assertNotNull(mPrefs);
	}

	public void testVersionCode() {
		mPrefs = new OXPreferencesImpl(mockContext);
		assertEquals(OXPreferencesImpl.VERSIONCODE_DEFAULT,
				mPrefs.getVersionCode());
		int expected = 1;
		mPrefs.setVersionCode(expected);
		assertEquals(expected, mPrefs.getVersionCode());
	}

	public void testLanguage() {
		mPrefs = new OXPreferencesImpl(mockContext);
		assertEquals(OXPreferencesImpl.LANGUAGE_DEFAULT, mPrefs.getLanguage());
	}

	public void testDefaultLanguage() {
		mPrefs = new OXPreferencesImpl(mockContext);
		assertEquals(OXPreferencesImpl.DEFAULTLANGUAGE_DEFAULT,
				mPrefs.getDefaultLanguage());
		String expected = "de";
		mPrefs.setDefaultLanguage(expected);
		assertEquals(expected, mPrefs.getDefaultLanguage());
	}

	public void testFirstEmailMapping() {
		mPrefs = new OXPreferencesImpl(mockContext);
		assertEquals("mEmailTypeWork", mPrefs.getFirstEmailMapping());
		String expected = "mEmailTypeHome";
		mPrefs.setFirstEmailMapping(expected);
		assertEquals(expected, mPrefs.getFirstEmailMapping());
	}

	public void testSecondEmailMapping() {
		mPrefs = new OXPreferencesImpl(mockContext);
		assertEquals("mEmailTypeHome", mPrefs.getSecondEmailMapping());
		String expected = "mEmailTypeOther";
		mPrefs.setSecondEmailMapping(expected);
		assertEquals(expected, mPrefs.getSecondEmailMapping());
	}

	public void testThirdEmailMapping() {
		mPrefs = new OXPreferencesImpl(mockContext);
		assertEquals("mEmailTypeOther", mPrefs.getThirdEmailMapping());
		String expected = "mEmailTypeWork";
		mPrefs.setThirdEmailMapping(expected);
		assertEquals(expected, mPrefs.getThirdEmailMapping());
	}

	public void testLastSync() {
		mPrefs = new OXPreferencesImpl(mockContext);
		assertEquals(OXPreferencesImpl.LAST_SYNC_DEFAULT, mPrefs.getLastSync());
		Date date = new Date();
		long expected = date.getTime();
		mPrefs.setLastSync(expected);
		assertEquals(expected, mPrefs.getLastSync());
	}

	public void testConstantSyncFrequency() {
		mPrefs = new OXPreferencesImpl(mockContext);
		assertEquals(OXPreferencesImpl.CONSTANT_SYNC_FREQUENCY_DEFAULT,
				mPrefs.getIsConstantSyncFrequency());
	}

	public void testMaintimeDays() {
		mPrefs = new OXPreferencesImpl(mockContext);
		List<String> expectedValue = Arrays.asList(new String[] { "2", "3",
				"4", "5", "6", "7", "1" });
		List<String> savedValue = Arrays.asList(mPrefs.getMaintimeDays());
		assertEquals(expectedValue, savedValue);
	}

	public void testMaintimeBegin() {
		mPrefs = new OXPreferencesImpl(mockContext);
		assertEquals(OXPreferencesImpl.MAINTIME_BEGIN_DEFAULT,
				mPrefs.getMaintimeBegin());
	}

	public void testMaintimeEnd() {
		mPrefs = new OXPreferencesImpl(mockContext);
		assertEquals(OXPreferencesImpl.MAINTIME_END_DEFAULT,
				mPrefs.getMaintimeEnd());
	}

	public void testMaintimeSyncFrequenc() {
		mPrefs = new OXPreferencesImpl(mockContext);
		assertEquals(OXPreferencesImpl.MAINTIME_SYNC_FREQUENCY_DEFAULT,
				mPrefs.getMaintimeSyncFrequency());
	}

	public void testSecondarytimeSyncFrequenc() {
		mPrefs = new OXPreferencesImpl(mockContext);
		assertEquals(OXPreferencesImpl.SECONDARYTIME_SYNC_FREQUENCY_DEFAULT,
				mPrefs.getSecondarytimeSyncFrequency());
	}

	public void testSyncFrequency() {
		mPrefs = new OXPreferencesImpl(mockContext);
		assertEquals(OXPreferencesImpl.SYNC_FREQUENCY_DEFAULT,
				mPrefs.getSyncFrequency());
	}

	public void testInformationDialogSeen() {
		mPrefs = new OXPreferencesImpl(mockContext);
		assertEquals(OXPreferencesImpl.INFORMATIONDIALOG_SEEN_DEFAULT,
				mPrefs.getInformationenDialogSeen());
		boolean expected = true;
		mPrefs.setInformationenDialogSeen(expected);
		assertEquals(expected, mPrefs.getInformationenDialogSeen());
	}
}
