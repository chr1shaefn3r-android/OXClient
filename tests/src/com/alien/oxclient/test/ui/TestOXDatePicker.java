package com.alien.oxclient.test.ui;

import java.util.LinkedHashMap;

import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactBuilder;
import com.alien.oxclient.test.OXTestCase;
import com.alien.oxclient.ui.OXDatePicker;

public class TestOXDatePicker extends OXTestCase {

	public void testConstructor() {
		new OXDatePicker(getContext(), null, new OXContactBuilder()
				.addContext(getContext())
				.addContactInformations(new LinkedHashMap<String, String>())
				.getOXObject(), true, 0, new Object());
	}

	public void testChange() {
		String key = "key";
		LinkedHashMap<String, String> daten = new LinkedHashMap<String, String>();
		OXContact contact = new OXContactBuilder().addContext(getContext())
				.addContactInformations(daten).getOXObject();
		OXDatePicker oxDatePicker = new OXDatePicker(getContext(), key,
				contact, true, 0, new Object());
		oxDatePicker.performClick();
		assertEquals(contact.getContactInformation(key), "1900-01-01");
	}
}
