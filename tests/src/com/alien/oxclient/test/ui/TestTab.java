/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.ui;

import android.os.Bundle;
import android.widget.TabHost;

import com.alien.oxclient.test.OXTestCase;
import com.alien.oxclient.ui.Tab;

public class TestTab extends OXTestCase {

	private RenamingMockContext mockContext;
	private Tab mTab;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mockContext = new RenamingMockContext(getContext());
	}

	public void testPreconditions() {
		mTab = new Tab(mockContext, "label", Tab.class, 0, new Bundle());
		assertNotNull(mTab);
	}

	public void testAddTab() {
		mTab = new Tab(mockContext, "label", Tab.class,
				android.R.drawable.ic_delete, new Bundle());
		TabHost tabHost = new TabHost(mockContext);
		try {
			mTab.addTab(null, tabHost);
			fail("Should have thrown NullPointerException!");
		} catch (NullPointerException expected) {
		}
	}
}
