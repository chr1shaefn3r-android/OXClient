/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.ui;

import junit.framework.TestCase;
import android.os.Bundle;

import com.alien.oxclient.ui.TabInfo;

public class TestTabInfo extends TestCase {

	private TabInfo mTabInfo;

	public void testPreconditions() {
		mTabInfo = new TabInfo("tag", TabInfo.class, new Bundle());
		assertNotNull(mTabInfo);
	}

	public void testGetTag() {
		mTabInfo = new TabInfo("tag", TabInfo.class, new Bundle());
		assertEquals("tag", mTabInfo.getTag());
	}

	public void testGetFragment() {
		mTabInfo = new TabInfo("tag", TabInfo.class, new Bundle());
		assertNull(mTabInfo.getFragment());
	}
}
