package com.alien.oxclient.test.ui;

import java.util.LinkedHashMap;

import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactBuilder;
import com.alien.oxclient.test.OXTestCase;
import com.alien.oxclient.ui.OXEditText;

public class TestOXEditText extends OXTestCase {

	public void testConstructor() {
		new OXEditText(getContext(), null, new OXContactBuilder()
				.addContext(getContext())
				.addContactInformations(new LinkedHashMap<String, String>())
				.getOXObject(), 0, 0, new Object());
	}

	public void testChange() {
		String newValue = "value";
		String key = "key";
		LinkedHashMap<String, String> daten = new LinkedHashMap<String, String>();
		OXContact contact = new OXContactBuilder().addContext(getContext())
				.addContactInformations(daten).getOXObject();
		OXEditText oxEditText = new OXEditText(getContext(), key, contact, 0,
				0, new Object());
		oxEditText.setText(newValue);
		assertEquals(contact.getContactInformation(key), newValue);
	}
}
