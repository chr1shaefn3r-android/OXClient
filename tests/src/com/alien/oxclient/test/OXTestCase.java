/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test;

import java.util.Date;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentProviderResult;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.RawContacts;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;
import android.test.mock.MockContext;
import android.text.TextUtils;
import android.util.Log;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.mockinterfaces.OXAccountManager;
import com.alien.oxclient.test.mocks.OXAccountManagerMockImpl;
import com.alien.oxclient.test.mocks.OXCalendarMockImpl;
import com.alien.oxclient.test.mocks.OXDecisionMockImpl;
import com.alien.oxclient.test.mocks.OXDeviceStateMockImpl;
import com.alien.oxclient.test.mocks.OXPreferencesMockImpl;
import com.alien.oxclient.test.mocks.OXUtilsMockImpl;

public abstract class OXTestCase extends AndroidTestCase {
	protected JSONArray mJsonArray = new JSONArray();
	protected LinkedHashMap<String, String> mContactInformations = new LinkedHashMap<String, String>();
	protected JSONObject mJsonObject = new JSONObject();
	protected Date mDate = new Date(2013, 01, 02);
	protected OXAccountManagerMockImpl mOXAccountManagerMockImpl;
	protected OXPreferencesMockImpl mOXPreferencesMockImpl;
	protected OXUtilsMockImpl mOXUtilsMockImpl;
	protected OXDeviceStateMockImpl mOXDeviceStateMockImpl;
	protected OXDecisionMockImpl mOXDecisionMockImpl;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mOXAccountManagerMockImpl = new OXAccountManagerMockImpl();
		OXApplication.setAccountManager(mOXAccountManagerMockImpl);
		mOXPreferencesMockImpl = new OXPreferencesMockImpl();
		OXApplication.setPrefs(mOXPreferencesMockImpl);
		mOXUtilsMockImpl = new OXUtilsMockImpl();
		OXApplication.setUtils(mOXUtilsMockImpl);
		OXApplication.setCalendar(new OXCalendarMockImpl());
		mOXDeviceStateMockImpl = new OXDeviceStateMockImpl();
		OXApplication.setDeviceState(mOXDeviceStateMockImpl);
		mOXDecisionMockImpl = new OXDecisionMockImpl();
		OXApplication.setDecision(mOXDecisionMockImpl);
	}

	protected void checkForDirtyFlagSet(boolean pConditionToBeCheckedFor,
			String pRawContactId) {
		OXAccountManager accountManager = new OXAccountManagerMockImpl();
		Uri rawContactUri = RawContacts.CONTENT_URI
				.buildUpon()
				.appendQueryParameter(RawContacts.ACCOUNT_NAME,
						accountManager.getOXAccount().name)
				.appendQueryParameter(RawContacts.ACCOUNT_TYPE,
						accountManager.getOXAccount().type).build();
		Cursor cursor = getContext().getContentResolver().query(rawContactUri,
				new String[] { RawContacts.DIRTY }, RawContacts._ID + "=?",
				new String[] { pRawContactId }, null);
		if(cursor.moveToFirst()) if(pConditionToBeCheckedFor) assertTrue(cursor
				.getString(cursor.getColumnIndex(RawContacts.DIRTY)).compareTo(
						"1") == 0);
		else assertTrue(cursor.getString(
				cursor.getColumnIndex(RawContacts.DIRTY)).compareTo("0") == 0);
		else fail("No RawContact with id=" + pRawContactId + " found!");
	}

	protected void checkForSourceIdSet(boolean pConditionToBeCheckedFor,
			String pRawContactId) {
		OXAccountManager accountManager = new OXAccountManagerMockImpl();
		Uri rawContactUri = RawContacts.CONTENT_URI
				.buildUpon()
				.appendQueryParameter(RawContacts.ACCOUNT_NAME,
						accountManager.getOXAccount().name)
				.appendQueryParameter(RawContacts.ACCOUNT_TYPE,
						accountManager.getOXAccount().type).build();
		Cursor cursor = getContext().getContentResolver().query(rawContactUri,
				new String[] { RawContacts.SOURCE_ID }, RawContacts._ID + "=?",
				new String[] { pRawContactId }, null);
		if(cursor.moveToFirst()) if(pConditionToBeCheckedFor) assertFalse(TextUtils
				.isEmpty(cursor.getString(cursor
						.getColumnIndex(RawContacts.SOURCE_ID))));
		else assertTrue(TextUtils.isEmpty(cursor.getString(cursor
				.getColumnIndex(RawContacts.SOURCE_ID))));
		else fail("No RawContact with id=" + pRawContactId + " found!");
	}

	protected void checkForContactAmount(int pAmount) {
		OXAccountManager accountManager = new OXAccountManagerMockImpl();
		Uri rawContactUri = RawContacts.CONTENT_URI
				.buildUpon()
				.appendQueryParameter(RawContacts.ACCOUNT_NAME,
						accountManager.getOXAccount().name)
				.appendQueryParameter(RawContacts.ACCOUNT_TYPE,
						accountManager.getOXAccount().type).build();
		Cursor cursor = getContext().getContentResolver().query(rawContactUri,
				new String[] { RawContacts._ID }, null, null, RawContacts._ID);
		assertEquals(pAmount, cursor.getCount());
	}

	protected void checkForWorkDone(ContentProviderResult[] pResults) {
		for(ContentProviderResult result : pResults) {
			if(result.count != null) assertEquals("result.toString(): "
					+ result.toString(), Integer.valueOf(1), result.count);
		}
	}

	protected void dumpCursor(Cursor cursor, String TAG) {
		cursor.moveToPosition(-1);
		String[] cols = cursor.getColumnNames();
		Log.i(TAG, "dumpCursor() count: " + cursor.getCount());
		int index = 0;
		while (cursor.moveToNext()) {
			Log.i(TAG, index + " {");
			for(int i = 0; i < cols.length; i++) {
				Log.i(TAG, "    " + cols[i] + '=' + cursor.getString(i));
			}
			Log.i(TAG, "}");
			index += 1;
		}
		cursor.moveToPosition(-1);
	}

	protected static class RenamingMockContext extends
			RenamingDelegatingContext {
		private static final String PREFIX = "test.";

		public RenamingMockContext(Context context) {
			super(new DelegatedMockContext(context), PREFIX);
		}

		private static class DelegatedMockContext extends MockContext {
			private Context mDelegatedContext;

			public DelegatedMockContext(Context context) {
				mDelegatedContext = context;
			}

			@Override
			public String getPackageName() {
				return mDelegatedContext.getPackageName();
			}

			@Override
			public SharedPreferences getSharedPreferences(String name, int mode) {
				return mDelegatedContext.getSharedPreferences(PREFIX + name,
						mode);
			}

			@Override
			public Resources getResources() {
				return mDelegatedContext.getResources();
			}

			@Override
			public SQLiteDatabase openOrCreateDatabase(String file, int mode,
					CursorFactory factory) {
				return mDelegatedContext.openOrCreateDatabase(file, mode,
						factory);
			}

			@Override
			public SQLiteDatabase openOrCreateDatabase(String file, int mode,
					CursorFactory factory, DatabaseErrorHandler errorHandler) {
				return mDelegatedContext.openOrCreateDatabase(file, mode,
						factory, errorHandler);
			}

			@Override
			public boolean deleteDatabase(String name) {
				return mDelegatedContext.deleteDatabase(name);
			}
		}
	}

	protected void setupContactInformations(boolean pWithDates)
			throws JSONException {
		// Kontrolliste & JSONArray bauen
		mJsonArray = new JSONArray();
		mContactInformations = new LinkedHashMap<String, String>();
		mJsonObject = new JSONObject();
		// Daten einfuellen
		mContactInformations.put("mID", "42");
		mJsonArray.put("42");
		mJsonObject.put("id", "42");
		// Namen:
		mContactInformations.put("mNameDisplayName", "Doe, John");
		mJsonArray.put("Doe, John");
		mJsonObject.put("display_name", "Doe, John");
		mContactInformations.put("mNameGivenName", "John");
		mJsonArray.put("John");
		mJsonObject.put("first_name", "John");
		mContactInformations.put("mNameFamilyName", "Doe");
		mJsonArray.put("Doe");
		mJsonObject.put("last_name", "Doe");
		mContactInformations.put("mNameMiddleName", "Williams");
		mJsonArray.put("Williams");
		mJsonObject.put("second_name", "Williams");
		mContactInformations.put("mNameSuffix", "Jr.");
		mJsonArray.put("Jr.");
		mJsonObject.put("suffix", "Jr.");
		mContactInformations.put("mNamePrefix", "Dr.");
		mJsonArray.put("Dr.");
		mJsonObject.put("title", "Dr.");
		mContactInformations.put("mNicknameTypeDefault", "JohnyBoy");
		mJsonArray.put("JohnyBoy");
		mJsonObject.put("nickname", "JohnyBoy");
		// Privat-Adresse:
		mContactInformations.put("mPostalTypeHomeStreet", "JohnStreet");
		mJsonArray.put("JohnStreet");
		mJsonObject.put("street_home", "JohnStreet");
		mContactInformations.put("mPostalTypeHomePostalCode", "JohnPostalCode");
		mJsonArray.put("JohnPostalCode");
		mJsonObject.put("postal_code_home", "JohnPostalCode");
		mContactInformations.put("mPostalTypeHomeCity", "JohnCity");
		mJsonArray.put("JohnCity");
		mJsonObject.put("city_home", "JohnCity");
		mContactInformations.put("mPostalTypeHomeState", "JohnState");
		mJsonArray.put("JohnState");
		mJsonObject.put("state_home", "JohnState");
		mContactInformations.put("mPostalTypeHomeCountry", "JohnCountry");
		mJsonArray.put("JohnCountry");
		mJsonObject.put("country_home", "JohnCountry");
		// Events:
		if(pWithDates) {
			mContactInformations.put("mEventTypeBirthday", "1990-08-02");
			mJsonArray.put("649555200000"); // Konvertierung!
			mJsonObject.put("birthday", "649555200000");
			// mContactInformations.put("mEventTypeBirthday", "2011-12-21");
			// mJsonArray.put("1324451719572"); // Konvertierung!
			// mJsonObject.put("birthday", "1324422000000");
			// mJsonObject.put("birthday", "1324425600000");
			mContactInformations.put("mEventTypeAnniversary", "1990-08-02");
			mJsonArray.put("649555200000"); // Konvertierung!
			mJsonObject.put("anniversary", "649555200000");
			// mContactInformations.put("mEventTypeAnniversary", "2011-12-21");
			// mJsonArray.put("1324451719572"); // Konvertierung!
			// mJsonObject.put("anniversary", "1324425600000");
			// mJsonObject.put("anniversary", "1324422000000");
		} else {
			mJsonArray.put("");
			mJsonArray.put("");
		}
		// Relations:
		mContactInformations.put("mRelationTypeSpouse", "JohnSpouse");
		mJsonArray.put("JohnSpouse");
		mJsonObject.put("spouse_name", "JohnSpouse");
		mContactInformations.put("mRelationTypeAssistant", "JohnAssistant");
		mJsonArray.put("JohnAssistant");
		mJsonObject.put("assistant_name", "JohnAssistant");
		mContactInformations.put("mRelationTypeManager", "JohnManager");
		mJsonArray.put("JohnManager");
		mJsonObject.put("manager_name", "JohnManager");
		// Notiz:
		mContactInformations.put("mNote", "Johns Notiz");
		mJsonArray.put("Johns Notiz");
		mJsonObject.put("note", "Johns Notiz");
		// Arbeitsstelle:
		mContactInformations.put("mOrganizationTypeWorkDepartment",
				"JohnDepartment");
		mJsonArray.put("JohnDepartment");
		mJsonObject.put("department", "JohnDepartment");
		mContactInformations.put("mOrganizationTypeWorkTitle", "JohnWorkTitle");
		mJsonArray.put("JohnWorkTitle");
		mJsonObject.put("position", "JohnWorkTitle");
		mContactInformations.put("mOrganizationTypeWorkJobDescription",
				"JohnJobDescription");
		mJsonArray.put("JohnJobDescription");
		mJsonObject.put("employee_type", "JohnJobDescription");
		mContactInformations.put("mOrganizationTypeWorkOfficeLocation",
				"JohnOfficeLocation");
		mJsonArray.put("JohnOfficeLocation");
		mJsonObject.put("room_number", "JohnOfficeLocation");
		mContactInformations.put("mOrganizationTypeWorkCompany", "JohnCompany");
		mJsonArray.put("JohnCompany");
		mJsonObject.put("company", "JohnCompany");
		// Arbeitsadresse
		mContactInformations.put("mPostalTypeWorkStreet", "DoeStreet");
		mJsonArray.put("DoeStreet");
		mJsonObject.put("street_business", "DoeStreet");
		mContactInformations.put("mPostalTypeWorkPostalCode", "DoePostalCode");
		mJsonArray.put("DoePostalCode");
		mJsonObject.put("postal_code_business", "DoePostalCode");
		mContactInformations.put("mPostalTypeWorkCity", "DoeCity");
		mJsonArray.put("DoeCity");
		mJsonObject.put("city_business", "DoeCity");
		mContactInformations.put("mPostalTypeWorkState", "DoeState");
		mJsonArray.put("DoeState");
		mJsonObject.put("state_business", "DoeState");
		mContactInformations.put("mPostalTypeWorkCountry", "DoeCountry");
		mJsonArray.put("DoeCountry");
		mJsonObject.put("country_business", "DoeCountry");
		// Andere Adresse:
		mContactInformations.put("mPostalTypeOtherStreet", "WayneStreet");
		mJsonArray.put("WayneStreet");
		mJsonObject.put("street_other", "WayneStreet");
		mContactInformations.put("mPostalTypeOtherCity", "WayneCity");
		mJsonArray.put("WayneCity");
		mJsonObject.put("city_other", "WayneCity");
		mContactInformations.put("mPostalTypeOtherPostalCode",
				"WaynePostalCode");
		mJsonArray.put("WaynePostalCode");
		mJsonObject.put("postal_code_other", "WaynePostalCode");
		mContactInformations.put("mPostalTypeOtherState", "WayneState");
		mJsonArray.put("WayneState");
		mJsonObject.put("state_other", "WayneState");
		mContactInformations.put("mPostalTypeOtherCountry", "WayneCountry");
		mJsonArray.put("WayneCountry");
		mJsonObject.put("country_other", "WayneCountry");
		// Telephonadressen:
		mContactInformations.put("mPhoneTypeWork1", "101010");
		mJsonArray.put("101010");
		mJsonObject.put("telephone_business1", "101010");
		mContactInformations.put("mPhoneTypeWork2", "42");
		mJsonArray.put("42");
		mJsonObject.put("telephone_business2", "42");
		mContactInformations.put("mPhoneTypeFaxWork", "1337");
		mJsonArray.put("1337");
		mJsonObject.put("fax_business", "1337");
		mContactInformations.put("mPhoneTypeCallback", "54");
		mJsonArray.put("54");
		mJsonObject.put("telephone_callback", "54");
		mContactInformations.put("mPhoneTypeCar", "12");
		mJsonArray.put("12");
		mJsonObject.put("telephone_car", "12");
		mContactInformations.put("mPhoneTypeCompanyMain", "11");
		mJsonArray.put("11");
		mJsonObject.put("telephone_company", "11");
		mContactInformations.put("mPhoneTypeAssistant", "13");
		mJsonArray.put("13");
		mJsonObject.put("telephone_assistant", "13");
		mContactInformations.put("mPhoneTypeTelex", "14");
		mJsonArray.put("14");
		mJsonObject.put("telephone_telex", "14");
		mContactInformations.put("mPhoneTypeHome1", "15");
		mJsonArray.put("15");
		mJsonObject.put("telephone_home1", "15");
		mContactInformations.put("mPhoneTypeHome2", "15");
		mJsonArray.put("15");
		mJsonObject.put("telephone_home2", "15");
		mContactInformations.put("mPhoneTypeFaxHome", "17");
		mJsonArray.put("17");
		mJsonObject.put("fax_home", "17");
		mContactInformations.put("mPhoneTypeRadio", "18");
		mJsonArray.put("18");
		mJsonObject.put("telephone_radio", "18");
		mContactInformations.put("mPhoneTypeWorkMobile", "19");
		mJsonArray.put("19");
		mJsonObject.put("cellular_telephone1", "19");
		mContactInformations.put("mPhoneTypeMobile", "20");
		mJsonArray.put("20");
		mJsonObject.put("cellular_telephone2", "20");
		mContactInformations.put("mPhoneTypeOther", "21");
		mJsonArray.put("21");
		mJsonObject.put("telephone_other", "21");
		mContactInformations.put("mPhoneTypeOtherFax", "22");
		mJsonArray.put("22");
		mJsonObject.put("fax_other", "22");
		mContactInformations.put("mPhoneTypeISDN", "23");
		mJsonArray.put("23");
		mJsonObject.put("telephone_isdn", "23");
		mContactInformations.put("mPhoneTypePager", "24");
		mJsonArray.put("24");
		mJsonObject.put("telephone_pager", "24");
		mContactInformations.put("mPhoneTypeMain", "25");
		mJsonArray.put("25");
		mJsonObject.put("telephone_primary", "25");
		mContactInformations.put("mPhoneTypeTTYTDD", "26");
		mJsonArray.put("26");
		mJsonObject.put("telephone_ttytdd", "26");
		// Emails:
		mContactInformations.put("mEmailTypeWork", "john.doe@sap.com");
		mJsonArray.put("john.doe@sap.com");
		mJsonObject.put("email1", "john.doe@sap.com");
		mContactInformations.put("mEmailTypeHome", "john.doe@web.de");
		mJsonArray.put("john.doe@web.de");
		mJsonObject.put("email2", "john.doe@web.de");
		mContactInformations.put("mEmailTypeOther", "john.doe@gmx.de");
		mJsonArray.put("john.doe@gmx.de");
		mJsonObject.put("email3", "john.doe@gmx.de");
		// Website/URL;
		mContactInformations.put("mWebsiteURL", "asdf");
		mJsonArray.put("asdf");
		mJsonObject.put("url", "asdf");
		// InstantMessenger:
		mContactInformations.put("mImTypeWorkIm1", "JohnDoe");
		mJsonArray.put("JohnDoe");
		mJsonObject.put("instant_messenger1", "JohnDoe");
		mContactInformations.put("mImTypeHomeIm1", "Johne.Doe");
		mJsonArray.put("Johne.Doe");
		mJsonObject.put("instant_messenger2", "Johne.Doe");
		// DistributionList:
		mJsonArray
				.put("[{\"id\":1,\"mail\":\"john.doe@gmx.de\",\"display_name\":\"John Doe\",\"mail_field\":1},{\"id\":2,\"mail\":\"jane.doe@gmx.de\",\"display_name\":\"Jane Doe\",\"mail_field\":2}]");
		// mContactInformations.put("mDistributionList",
		// "[{\"id\":1,\"mail\":\"john.doe@gmx.de\",\"display_name\":\"John Doe\",\"mail_field\":1},{\"id\":2,\"mail\":\"jane.doe@gmx.de\",\"display_name\":\"Jane Doe\",\"mail_field\":2}]");
		// mJsonObject.put("instant_messenger2",
		// "[{\"id\":1,\"mail\":\"john.doe@gmx.de\",\"display_name\":\"John Doe\",\"mail_field\":1},{\"id\":2,\"mail\":\"jane.doe@gmx.de\",\"display_name\":\"Jane Doe\",\"mail_field\":2}]");
	}

	protected void whipeOXContacts() {
		// Clear all created Contacts
		OXAccountManager accountManager = new OXAccountManagerMockImpl();
		Uri rawContactUri = RawContacts.CONTENT_URI
				.buildUpon()
				.appendQueryParameter(RawContacts.ACCOUNT_NAME,
						accountManager.getOXAccount().name)
				.appendQueryParameter(RawContacts.ACCOUNT_TYPE,
						accountManager.getOXAccount().type).build();
		Cursor cursor = getContext().getContentResolver().query(rawContactUri,
				null, null, null, RawContacts._ID);
		try {
			while (cursor.moveToNext()) {
				getContext().getContentResolver().delete(
						RawContacts.CONTENT_URI
								.buildUpon()
								.appendQueryParameter(
										ContactsContract.CALLER_IS_SYNCADAPTER,
										"true").build(),
						RawContacts._ID + " = ?",
						new String[] { cursor.getString(cursor
								.getColumnIndex(RawContacts._ID)) });
			}
		} finally {
			cursor.close();
		}
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		whipeOXContacts();
	}
}
