/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.provider.ContactsContract;

import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactBuilder;
import com.alien.oxclient.model.OXContactJSONArrayBuilder;
import com.alien.oxclient.model.OXContactRawContactIdBuilder;
import com.alien.oxclient.test.OXContactTestCase;

public class TestOXContact extends OXContactTestCase {

	/**
	 * Test method "insertContact()"
	 */
	public void testInsertContact() throws JSONException, RemoteException,
			OperationApplicationException {
		insertContact();
	}

	public void testInsertContactWithSomeData() throws JSONException,
			RemoteException, OperationApplicationException {
		mContactInformations = new LinkedHashMap<String, String>();
		mContactInformations.put("mID", "42");
		mContactInformations.put("mNameDisplayName", "Doe, Jon");
		mContactInformations.put("mNameGivenName", "Jon");
		mContactInformations.put("mNameFamilyName", "Doe");
		mContactInformations.put("mPhoneTypeWork1", "23");
		mContactInformations.put("mPostalTypeHomeStreet", "HomeStreet");
		mContactInformations.put("mPostalTypeHomePostalCode", "13377");
		mContact = new OXContactBuilder().addContext(getContext())
				.addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		assertNotNull(mContact);
		ArrayList<ContentProviderOperation> operations = mContact
				.insertContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] result = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		mContact.setRawContactId(String.valueOf(ContentUris
				.parseId(result[0].uri)));
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		contactRead.setVersion(mContact.getVersion());
		assertEquals(mContact, contactRead);
	}

	public void testInsertContactWithSomeOtherData() throws JSONException,
			RemoteException, OperationApplicationException {
		setupContactInformations(true);
		mContactInformations.remove("mRelationTypeAssistant");
		mContactInformations.remove("mPhoneTypeCar");
		mContactInformations.remove("mWebsiteURL");
		mContactInformations.remove("mPostalTypeWorkCity");
		mContactInformations.remove("mPhoneTypeWork1");
		mContact = new OXContactBuilder().addContext(getContext())
				.addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		assertNotNull(mContact);
		ArrayList<ContentProviderOperation> operations = mContact
				.insertContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] result = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		mContact.setRawContactId(String.valueOf(ContentUris
				.parseId(result[0].uri)));
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		contactRead.setVersion(mContact.getVersion());
		assertEquals(mContact, contactRead);
	}

	/**
	 * Test method "addOXId()"
	 */
	public void testAddOXId() throws JSONException, RemoteException,
			OperationApplicationException, NoSuchFieldException,
			IllegalArgumentException, IllegalAccessException {
		setupContactInformations(true);
		mContactInformations.remove("mID");
		OXContact contactWrite = new OXContactBuilder()
				.addContext(getContext()).addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		assertNotNull(contactWrite);
		// Add OXContact without OXId
		ArrayList<ContentProviderOperation> operations = contactWrite
				.insertContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] result = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		contactWrite.setRawContactId(String.valueOf(ContentUris
				.parseId(result[0].uri)));
		// Add OXId afterwards
		operations = contactWrite.addOXId("23");
		result = resolver.applyBatch(ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(
						Long.parseLong(contactWrite.getRawContactId()))
				.getOXObject();
		// Force the Version- and RawContactfield to a specific value so equals
		// turns out true, as they are not interesting for this test
		contactWrite.setVersion(contactRead.getVersion());
		contactWrite.setRawContactId(contactRead.getRawContactId());
		// Check for equality
		assertEquals(contactWrite, contactRead);
	}

	/**
	 * Test method "deleteContact()"
	 */
	public void testDeleteContact() throws JSONException, RemoteException,
			OperationApplicationException {
		insertContact(); // Setup testdata
		ArrayList<ContentProviderOperation> operations = mContact
				.deleteContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] result = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		checkForContactAmount(0);
	}

	/**
	 * Test method "updateContact()"
	 */
	/** update */
	public void testUpdateContactOnlyUpdatesWithOneChangeBeeingEmail()
			throws RemoteException, JSONException,
			OperationApplicationException {
		insertContact(); // Setup testdata
		// Make some changes
		mContact.putContactInformation("mEmailTypeWork", "john.doe@ibm.com");
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		mContact.setDistributionList(contactRead.getDistributionList());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	public void testUpdateContactOnlyUpdatesWithOneChangeBeeingPhoneWork2()
			throws RemoteException, JSONException,
			OperationApplicationException {
		insertContact(); // Setup testdata
		// Make some changes
		mContact.putContactInformation("mPhoneTypeWork2", "23");
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		mContact.setDistributionList(contactRead.getDistributionList());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	public void testUpdateContactOnlyUpdatesWithOneChangeBeeingNamePrefix()
			throws RemoteException, JSONException,
			OperationApplicationException {
		insertContact(); // Setup testdata
		// Make some changes
		mContact.putContactInformation("mNamePrefix", "Prof.");
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		mContact.setDistributionList(contactRead.getDistributionList());
		// Force the Displayname to equality, as it is set by the system based
		// on usersettings!
		mContact.putContactInformation("mNameDisplayName",
				contactRead.getContactInformation("mNameDisplayName"));
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	public void testUpdateContactOnlyUpdatesWithTwoChangesBeeingOrganization()
			throws RemoteException, JSONException,
			OperationApplicationException {
		insertContact(); // Setup testdata
		// Make some changes
		mContact.putContactInformation("mOrganizationTypeWorkJobDescription",
				"Senior Architekt");
		mContact.putContactInformation("mOrganizationTypeWorkCompany", "SAP AG");
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		mContact.setDistributionList(contactRead.getDistributionList());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	public void testUpdateContactOnlyUpdatesWithTwoChangesBeeingAdress()
			throws RemoteException, JSONException,
			OperationApplicationException {
		insertContact(); // Setup testdata
		// Make some changes
		mContact.putContactInformation("mPostalTypeHomeCity", "Las Vegas");
		mContact.putContactInformation("mPostalTypeHomeState", "Nevada");
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		mContact.setDistributionList(contactRead.getDistributionList());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	public void testUpdateContactOnlyUpdatesWithServeralChanges()
			throws RemoteException, JSONException,
			OperationApplicationException {
		insertContact(); // Setup testdata
		// Make some changes
		mContact.putContactInformation("mRelationTypeAssistant", "Jenkins");
		mContact.putContactInformation("mPhoneTypeCar", "84");
		mContact.putContactInformation("mWebsiteURL",
				"http://www.IamAwesome.net/");
		mContact.putContactInformation("mPostalTypeWorkCity", "Philadelphia");
		mContact.putContactInformation("mImTypeHomeIm1", "Awesomo3000");
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		mContact.setDistributionList(contactRead.getDistributionList());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	/** delete */
	public void testUpdateContactOnlyDeleteWithOneChangeBeeingEmail()
			throws RemoteException, JSONException,
			OperationApplicationException {
		insertContact(); // Setup testdata
		// Make some changes
		mContact.removeContactInformation("mEmailTypeWork");
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		mContact.setDistributionList(contactRead.getDistributionList());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	public void testUpdateContactOnlyDeleteWithOneChangeBeeingPhoneWork2()
			throws RemoteException, JSONException,
			OperationApplicationException {
		insertContact(); // Setup testdata
		// Make some changes
		mContact.removeContactInformation("mPhoneTypeWork2");
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		mContact.setDistributionList(contactRead.getDistributionList());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	public void testUpdateContactOnlyDeletesWithOneChangeBeeingNamePrefix()
			throws RemoteException, JSONException,
			OperationApplicationException {
		insertContact(); // Setup testdata
		// Make some changes
		mContact.removeContactInformation("mNamePrefix");
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		mContact.setDistributionList(contactRead.getDistributionList());
		// Force the Displayname to equality, as it is set by the system based
		// on usersettings!
		mContact.putContactInformation("mNameDisplayName",
				contactRead.getContactInformation("mNameDisplayName"));
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	public void testUpdateContactOnlyDeletesWithTwoChangesBeeingOrganization()
			throws RemoteException, JSONException,
			OperationApplicationException {
		insertContact(); // Setup testdata
		// Make some changes
		mContact.removeContactInformation("mOrganizationTypeWorkJobDescription");
		mContact.removeContactInformation("mOrganizationTypeWorkCompany");
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		mContact.setDistributionList(contactRead.getDistributionList());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	public void testUpdateContactOnlyDeletesWithTwoChangesBeeingAdress()
			throws RemoteException, JSONException,
			OperationApplicationException {
		insertContact(); // Setup testdata
		// Make some changes
		mContact.removeContactInformation("mPostalTypeHomeCity");
		mContact.removeContactInformation("mPostalTypeHomeState");
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		mContact.setDistributionList(contactRead.getDistributionList());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	public void testUpdateContactOnlyDeletesWithServeralChanges()
			throws RemoteException, JSONException,
			OperationApplicationException {
		insertContact(); // Setup testdata
		// Make some changes
		mContact.removeContactInformation("mRelationTypeAssistant");
		mContact.removeContactInformation("mPhoneTypeCar");
		mContact.removeContactInformation("mWebsiteURL");
		mContact.removeContactInformation("mPostalTypeWorkCity");
		mContact.removeContactInformation("mImTypeHomeIm1");
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		mContact.setDistributionList(contactRead.getDistributionList());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	/** insert */
	public void testUpdateContactOnlyInsertsWithOneChangeBeeingEmail()
			throws RemoteException, JSONException,
			OperationApplicationException {
		setupContactInformations(true);
		mContactInformations.remove("mEmailTypeWork");
		insertCustomContact(mContactInformations); // Setup testdata
		// Reset ContactInformations
		mContactInformations.clear();
		mContactInformations = null;
		setupContactInformations(true);
		mContact.setContactInformations(mContactInformations);
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	public void testUpdateContactOnlyInsertsWithOneChangeBeeingPhoneWork2()
			throws RemoteException, JSONException,
			OperationApplicationException {
		setupContactInformations(true);
		mContactInformations.remove("mPhoneTypeWork2");
		insertCustomContact(mContactInformations); // Setup testdata
		// Make some changes
		setupContactInformations(true);
		mContact.setContactInformations(mContactInformations);
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	public void testUpdateContactOnlyInsertsWithOneChangeBeeingNamePrefix()
			throws RemoteException, JSONException,
			OperationApplicationException {
		setupContactInformations(true);
		mContactInformations.remove("mNamePrefix");
		insertCustomContact(mContactInformations); // Setup testdata
		// Make some changes
		setupContactInformations(true);
		mContact.setContactInformations(mContactInformations);
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		// Force the Displayname to equality, as it is set by the system based
		// on usersettings!
		mContact.putContactInformation("mNameDisplayName",
				contactRead.getContactInformation("mNameDisplayName"));
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	public void testUpdateContactOnlyInsertsWithTwoChangeBeeingNamePrefix()
			throws RemoteException, JSONException,
			OperationApplicationException {
		setupContactInformations(true);
		mContactInformations.remove("mNamePrefix");
		mContactInformations.remove("mNameSuffix");
		insertCustomContact(mContactInformations); // Setup testdata
		// Make some changes
		setupContactInformations(true);
		mContact.setContactInformations(mContactInformations);
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		// Force the Displayname to equality, as it is set by the system based
		// on usersettings!
		mContact.putContactInformation("mNameDisplayName",
				contactRead.getContactInformation("mNameDisplayName"));
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	public void testUpdateContactOnlyInsertsWithTwoChangesBeeingOrganization()
			throws RemoteException, JSONException,
			OperationApplicationException {
		setupContactInformations(true);
		mContactInformations.remove("mOrganizationTypeWorkJobDescription");
		mContactInformations.remove("mOrganizationTypeWorkCompany");
		insertCustomContact(mContactInformations); // Setup testdata
		// Make some changes
		setupContactInformations(true);
		mContact.setContactInformations(mContactInformations);
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	public void testUpdateContactOnlyInsertsWithTwoChangesBeeingAdress()
			throws RemoteException, JSONException,
			OperationApplicationException {
		setupContactInformations(true);
		mContactInformations.remove("mPostalTypeHomeCity");
		mContactInformations.remove("mPostalTypeHomeState");
		insertCustomContact(mContactInformations); // Setup testdata
		// Make some changes
		setupContactInformations(true);
		mContact.setContactInformations(mContactInformations);
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	public void testUpdateContactOnlyInsertsWithServeralChanges()
			throws RemoteException, JSONException,
			OperationApplicationException {
		setupContactInformations(true);
		mContactInformations.remove("mRelationTypeAssistant");
		mContactInformations.remove("mPhoneTypeCar");
		mContactInformations.remove("mWebsiteURL");
		mContactInformations.remove("mPostalTypeWorkCity");
		mContactInformations.remove("mImTypeHomeIm1");
		insertCustomContact(mContactInformations); // Setup testdata
		// Make some changes
		setupContactInformations(true);
		mContact.setContactInformations(mContactInformations);
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	/** insert & update */
	public void testUpdateContactInsertAndUpdateWithServeralChanges()
			throws RemoteException, JSONException,
			OperationApplicationException {
		setupContactInformations(true);
		mContactInformations.remove("mRelationTypeAssistant");
		mContactInformations.remove("mPhoneTypeCar");
		mContactInformations.remove("mWebsiteURL");
		mContactInformations.remove("mPostalTypeWorkCity");
		mContactInformations.remove("mImTypeHomeIm1");
		insertCustomContact(mContactInformations); // Setup testdata
		// Make some changes
		setupContactInformations(true);
		mContact.setContactInformations(mContactInformations);
		mContact.putContactInformation("mNote",
				"This is another very important Note!");
		mContact.putContactInformation("mPhoneTypeAssistant", "54");
		mContact.putContactInformation("mEmailTypeOther", "john.doe@strato.com");
		mContact.putContactInformation("mPostalTypeOtherState", "California");
		mContact.putContactInformation("mImTypeWorkIm1", "Awesomo2000");
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	/** delete & update */
	public void testUpdateContactDeleteAndUpdateWithServeralChanges()
			throws RemoteException, JSONException,
			OperationApplicationException {
		insertContact(); // Setup testdata
		// Make some changes
		mContact.removeContactInformation("mRelationTypeAssistant");
		mContact.removeContactInformation("mPhoneTypeCar");
		mContact.removeContactInformation("mWebsiteURL");
		mContact.removeContactInformation("mPostalTypeWorkCity");
		mContact.removeContactInformation("mImTypeHomeIm1");
		mContact.putContactInformation("mNote",
				"This is another very important Note!");
		mContact.putContactInformation("mPhoneTypeAssistant", "54");
		mContact.putContactInformation("mEmailTypeOther", "john.doe@strato.com");
		mContact.putContactInformation("mPostalTypeOtherState", "California");
		mContact.putContactInformation("mRelationTypeManager", "Hudson");
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		mContact.setDistributionList(contactRead.getDistributionList());
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	/** insert & delete */
	public void testUpdateContactInsertAndDeleteWithServeralChanges()
			throws RemoteException, JSONException,
			OperationApplicationException {
		setupContactInformations(true);
		mContactInformations.remove("mRelationTypeAssistant");
		mContactInformations.remove("mPhoneTypeCar");
		mContactInformations.remove("mWebsiteURL");
		mContactInformations.remove("mPostalTypeWorkCity");
		mContactInformations.remove("mImTypeHomeIm1");
		insertCustomContact(mContactInformations); // Setup testdata
		// Make some changes
		setupContactInformations(true);
		mContact.setContactInformations(mContactInformations);
		mContact.removeContactInformation("mNameSuffix");
		mContact.removeContactInformation("mOrganizationTypeWorkTitle");
		mContact.removeContactInformation("mRelationTypeManager");
		mContact.removeContactInformation("mEventTypeBirthday");
		mContact.removeContactInformation("mWebsiteURL");
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		// Force the Displayname to equality, as it is set by the system based
		// on usersettings!
		mContact.putContactInformation("mNameDisplayName",
				contactRead.getContactInformation("mNameDisplayName"));
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	/** insert & delete & update */
	public void testUpdateContactInsertAndDeleteAndUpdateWithServeralChanges()
			throws RemoteException, JSONException,
			OperationApplicationException {
		whipeOXContacts();
		setupContactInformations(true);
		mContactInformations.remove("mRelationTypeAssistant");
		mContactInformations.remove("mPhoneTypeCar");
		mContactInformations.remove("mWebsiteURL");
		mContactInformations.remove("mPostalTypeWorkCity");
		mContactInformations.remove("mPhoneTypeWork1");
		insertCustomContact(mContactInformations); // Setup testdata
		// Make some changes
		setupContactInformations(true);
		mContact.setContactInformations(mContactInformations);
		mContact.removeContactInformation("mNameSuffix");
		mContact.removeContactInformation("mOrganizationTypeWorkTitle");
		mContact.removeContactInformation("mRelationTypeManager");
		mContact.removeContactInformation("mEventTypeBirthday");
		mContact.removeContactInformation("mPhoneTypeISDN");
		mContact.putContactInformation("mEventTypeAnniversary", "1325753911000");
		mContact.putContactInformation("mEmailTypeHome", "john.doe@gmail.com");
		mContact.putContactInformation("mPhoneTypeTTYTDD", "32");
		mContact.putContactInformation("mPhoneTypeHome1", "24");
		mContact.putContactInformation("mPhoneTypeHome2", "16");
		ArrayList<ContentProviderOperation> operations = mContact
				.updateContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Read OXContact
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		mContact.setVersion(contactRead.getVersion());
		// Force the Displayname to equality, as it is set by the system based
		// on usersettings!
		mContact.putContactInformation("mNameDisplayName",
				contactRead.getContactInformation("mNameDisplayName"));
		// Check for equality
		assertEquals(mContact, contactRead);
	}

	private void insertCustomContact(
			LinkedHashMap<String, String> pContactInformations)
			throws JSONException, RemoteException,
			OperationApplicationException {
		mContact = new OXContactBuilder().addContext(getContext())
				.addTimestamp(mDate)
				.addContactInformations(pContactInformations).getOXObject();
		assertNotNull(mContact);
		ArrayList<ContentProviderOperation> operations = mContact
				.insertContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		mContact.setRawContactId(String.valueOf(ContentUris
				.parseId(results[0].uri)));
		checkForContactAmount(1);
	}

	/**
	 * Test method "toJSONObject()"
	 * 
	 * @throws JSONException
	 */
	public void testToJsonObjectWithAllInformations() throws JSONException {
		setupContactInformations(true);
		OXContact contact = new OXContactBuilder()
				.addContext(new RenamingMockContext(getContext()))
				.addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		assertNotNull(contact);
		// Remove distribution_list
		mJsonObject.remove("distribution_list");
		assertEquals(mJsonObject.toString(), contact.toJSONObject().toString());
	}

	/**
	 * Test method "hasDistributionList()"
	 * 
	 * @throws JSONException
	 */
	public void testHasDistributionList() throws JSONException {
		setupContactInformations(true);
		OXContact contact = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(new Date())
				.addFromJSONArray(mJsonArray).getOXObject();
		assertNotNull(contact);
		assertTrue(contact.hasDistributionList());
	}

	/**
	 * Test method "getDistributionList()"
	 * 
	 * @throws JSONException
	 */
	public void testGetDistributionList() throws JSONException {
		setupContactInformations(true);
		OXContact contact = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(new Date())
				.addFromJSONArray(mJsonArray).getOXObject();
		assertNotNull(contact);
		assertNotNull(contact.getDistributionList());
	}

	/**
	 * Test method "setDistributionList()"
	 * 
	 * @throws JSONException
	 */
	public void testSetDistributionList() throws JSONException {
		setupContactInformations(true);
		String sampleData = "[{id: 1, email: 2}, {id: 4, email: 3}]";
		OXContact contact = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(new Date())
				.addFromJSONArray(mJsonArray).getOXObject();
		assertNotNull(contact);
		contact.setDistributionList(sampleData);
		assertEquals(sampleData, contact.getDistributionList());
	}

	/**
	 * Test method "hasId()"
	 * 
	 * @throws JSONException
	 */
	public void testHasId() throws JSONException {
		JSONArray array = new JSONArray();
		String value = "42";
		array.put(value); // index 0 = OXID
		OXContact contact = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(new Date())
				.addFromJSONArray(array).getOXObject();
		assertNotNull(contact);
		assertTrue(contact.hasId());
	}

	/**
	 * Test method "getId()"
	 * 
	 * @throws JSONException
	 */
	public void testGetId() throws JSONException {
		JSONArray array = new JSONArray();
		String value = "42";
		array.put(value); // index 0 = OXID
		OXContact contact = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(new Date())
				.addFromJSONArray(array).getOXObject();
		assertNotNull(contact);
		assertEquals(value, contact.getId());
	}

	/**
	 * Test method "toPrintableString()"
	 * 
	 * @throws JSONException
	 */
	public void testToPrintableString() {
		String key = "mNameDisplayName";
		LinkedHashMap<String, String> contactInformations = new LinkedHashMap<String, String>();
		contactInformations.put(key, "John Doe");
		OXContact contact = new OXContactBuilder()
				.addContext(new RenamingMockContext(getContext()))
				.addContactInformations(contactInformations).getOXObject();
		assertNotNull(contact);
		assertEquals(contact.getContactInformation(key),
				contact.toPrintableString());
	}

	/**
	 * Test method "toString()"
	 * 
	 * @throws JSONException
	 */
	public void testToString() throws JSONException {
		setupContactInformations(true);
		OXContact contact = new OXContactJSONArrayBuilder()
				.addContext(new RenamingMockContext(getContext()))
				.addTimestamp(new Date()).addFromJSONArray(mJsonArray)
				.getOXObject();
		assertNotNull(contact);
		assertNotNull(contact.toString());
	}

	/**
	 * Test method "hashCode"
	 */
	public void testHashCode() {
		OXContact contact = new OXContactBuilder()
				.addContext(new RenamingMockContext(getContext()))
				.addContactInformations(new LinkedHashMap<String, String>())
				.getOXObject();
		assertNotNull(contact);
		try {
			contact.hashCode();
			fail("Should have thrown UnsupportedOperationException");
		} catch (UnsupportedOperationException expected) {
		}
	}

	/**
	 * Test method "equals"
	 */
	public void testEquals() {
		OXContact contact = new OXContactBuilder()
				.addContext(new RenamingMockContext(getContext()))
				.addContactInformations(new LinkedHashMap<String, String>())
				.getOXObject();
		Date date = new Date();
		assertFalse(contact.equals(date));
	}

	/**
	 * Test method "getTimestamp()"
	 */
	public void testGetTimestamp() throws JSONException {
		Date date = new Date();
		OXContact contact = new OXContactJSONArrayBuilder()
				.addContext(new RenamingMockContext(getContext()))
				.addTimestamp(date).addFromJSONArray(new JSONArray())
				.getOXObject();
		assertNotNull(contact);
		assertEquals(date, contact.getTimestamp());
	}

	/**
	 * Test method "getRawContactId()"
	 */
	public void testGetRawContactId() throws JSONException {
		String key = "mNameDisplayName";
		LinkedHashMap<String, String> contactInformations = new LinkedHashMap<String, String>();
		contactInformations.put(key, "John Doe");
		OXContact contact = new OXContactBuilder().addContext(getContext())
				.addContactInformations(contactInformations).getOXObject();
		assertNotNull(contact);
		assertNull(contact.getRawContactId());
	}
}
