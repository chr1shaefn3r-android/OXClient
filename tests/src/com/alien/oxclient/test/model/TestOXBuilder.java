package com.alien.oxclient.test.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Data;

import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactBuilder;
import com.alien.oxclient.model.OXContactJSONArrayBuilder;
import com.alien.oxclient.model.OXContactJSONObjectBuilder;
import com.alien.oxclient.model.OXContactOXIdBuilder;
import com.alien.oxclient.model.OXContactRawContactIdBuilder;
import com.alien.oxclient.test.OXContactTestCase;

public class TestOXBuilder extends OXContactTestCase {

	/**
	 * Simple Constructor
	 */
	public void testSimpleConstructor() {
		OXContact contact = new OXContactBuilder().addContext(null)
				.addTimestamp(new Date()).getOXObject();
		assertNotNull(contact);
	}

	/**
	 * Test JSONArrayBuilder
	 */
	public void testJsonArrayConstructor() throws JSONException {
		OXContact contact = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(new Date())
				.addFromJSONArray(new JSONArray()).getOXObject();
		assertNotNull(contact);
	}

	public void testJSONArrayBuilderWithOneInformation() throws JSONException {
		JSONArray array = new JSONArray();
		array.put("42"); // index 0 = OXID
		// Kontrolliste anlegen
		LinkedHashMap<String, String> contactInformations = new LinkedHashMap<String, String>();
		contactInformations.put("mID", "42");
		// Objekt erzeugen
		OXContact contact = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(new Date())
				.addFromJSONArray(array).getOXObject();
		// Asserts
		assertNotNull(contact);
		assertEquals(contactInformations, contact.getContactInformations());
	}

	public void testJsonArrayConstructorWithAllInformationsExceptDates()
			throws JSONException {
		setupContactInformations(false);
		OXContact contact = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(new Date())
				.addFromJSONArray(mJsonArray).getOXObject();
		assertNotNull(contact);
		assertEquals(mContactInformations, contact.getContactInformations());
	}

	/**
	 * JSONObject Constructor
	 * 
	 * @throws JSONException
	 */
	public void testJsonObjectConstructor() throws JSONException {
		OXContact contact = new OXContactJSONObjectBuilder()
				.addContext(getContext()).addTimestamp(new Date())
				.addFromJSONObject(new JSONObject()).getOXObject();
		assertNotNull(contact);
	}

	public void testJsonObjectConstructorWithOneInformation()
			throws JSONException {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("id", "42");
		// Kontrolliste anlegen
		LinkedHashMap<String, String> contactInformations = new LinkedHashMap<String, String>();
		contactInformations.put("mID", "42");
		// Objekt erzeugen
		OXContact contact = new OXContactJSONObjectBuilder()
				.addContext(getContext()).addTimestamp(new Date())
				.addFromJSONObject(jsonObject).getOXObject();
		// Asserts
		assertNotNull(contact);
		assertEquals(contactInformations, contact.getContactInformations());
	}

	public void testJsonObjectConstructorWithAllInformationsExceptDates()
			throws JSONException {
		setupContactInformations(false);
		OXContact contact = new OXContactJSONObjectBuilder()
				.addContext(getContext()).addTimestamp(new Date())
				.addFromJSONObject(mJsonObject).getOXObject();
		assertNotNull(contact);
		assertEquals(mContactInformations, contact.getContactInformations());
	}

	public void testJsonConstructorWithAllInformations() throws JSONException {
		setupContactInformations(true);
		OXContact contact = new OXContactJSONObjectBuilder()
				.addContext(getContext()).addTimestamp(new Date())
				.addFromJSONObject(mJsonObject).getOXObject();
		assertNotNull(contact);
		assertEquals(mContactInformations, contact.getContactInformations());
	}

	/**
	 * RawContactId Constructor
	 */
	public void testRawContactIdConstructor() throws JSONException,
			RemoteException, OperationApplicationException,
			NoSuchFieldException, IllegalArgumentException,
			IllegalAccessException {
		whipeOXContacts();
		insertContact(); // Setup testdata
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext()).addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(mContact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to a specific value so equals turns out true,
		// as it is not interesting for this test
		mContact.setVersion(contactRead.getVersion());
		mContact.setDistributionList(contactRead.getDistributionList());
		assertEquals(mContact, contactRead);
	}

	public void testRawContactIdConstructorWithNullEmailtype()
			throws JSONException, RemoteException,
			OperationApplicationException, NoSuchFieldException,
			IllegalArgumentException, IllegalAccessException {
		Uri contentUri = Data.CONTENT_URI
				.buildUpon()
				.appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER,
						"true").build();
		LinkedHashMap<String, String> contactInformations = new LinkedHashMap<String, String>();
		contactInformations.put("mID", "42");
		contactInformations.put("mEmailTypeWork", "john.doe@sap.com");
		contactInformations.put("mEmailTypeHome", "john.doe@web.de");
		contactInformations.put("mEmailTypeOther", "john.doe@gmx.de");
		OXContact contact = new OXContactBuilder().addContext(getContext())
				.addTimestamp(mDate)
				.addContactInformations(contactInformations).getOXObject();
		ArrayList<ContentProviderOperation> operations = contact.insertContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] results = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		// Set emailtype to zero
		operations.clear();
		for(ContentProviderResult result : results) {
			if(result.uri.toString().contains("raw_contact")) continue;
			operations.add(ContentProviderOperation
					.newUpdate(contentUri)
					.withSelection(
							Data._ID + "=?",
							new String[] { String.valueOf(ContentUris
									.parseId(result.uri)) })
					.withValue(Data.DATA2, "0").build());
		}
		results = resolver.applyBatch(ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext()).addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(contact.getRawContactId()))
				.getOXObject();
		// Force the Version- and RawContactIdfield to a specific value so
		// equals turns out true, as they are not interesting for this test
		contact.setVersion(contactRead.getVersion());
		contact.setRawContactId(contactRead.getRawContactId());
		assertEquals(contact, contactRead);
	}

	/**
	 * OXId Constructor
	 */
	public void testOXIdConstructor() throws JSONException, RemoteException,
			OperationApplicationException, NoSuchFieldException,
			IllegalArgumentException, IllegalAccessException {
		whipeOXContacts();
		insertContact(); // Setup testdata
		OXContact contactRead = new OXContactOXIdBuilder()
				.addContext(getContext()).addTimestamp(mDate)
				.addFromOXId(mContact.getContactInformation("mID")).getOXObject();
		// Force the Versionfield to a specific value so equals turns out true,
		// as it is not interesting for this test
		mContact.setVersion(contactRead.getVersion());
		mContact.setDistributionList(contactRead.getDistributionList());
		assertEquals(mContact, contactRead);
	}
}
