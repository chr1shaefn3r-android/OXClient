package com.alien.oxclient.test;

import com.alien.oxclient.OXDecisionImpl;

public class TestOXDecision extends OXTestCase {

	private OXDecisionImpl mDecisionImpl;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mDecisionImpl = new OXDecisionImpl(getContext());
	}

	public void testShouldSyncPicturesPrefOffWifiOff() {
		mOXPreferencesMockImpl.setSyncPicturesOnlyOnWifi(false);
		mOXDeviceStateMockImpl.setIsOnWifi(false);
		assertTrue(mDecisionImpl.shouldSyncPictures());
	}

	public void testShouldSyncPicturesPrefOffWifiOn() {
		mOXPreferencesMockImpl.setSyncPicturesOnlyOnWifi(false);
		mOXDeviceStateMockImpl.setIsOnWifi(true);
		assertTrue(mDecisionImpl.shouldSyncPictures());
	}

	public void testShouldSyncPicturesPrefOnWifiOff() {
		mOXPreferencesMockImpl.setSyncPicturesOnlyOnWifi(true);
		mOXDeviceStateMockImpl.setIsOnWifi(false);
		assertFalse(mDecisionImpl.shouldSyncPictures());
	}

	public void testShouldSyncPicturesPrefOnWifiOn() {
		mOXPreferencesMockImpl.setSyncPicturesOnlyOnWifi(true);
		mOXDeviceStateMockImpl.setIsOnWifi(true);
		assertTrue(mDecisionImpl.shouldSyncPictures());
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		mDecisionImpl = null;
	}

}
