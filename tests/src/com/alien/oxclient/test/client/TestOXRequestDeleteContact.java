package com.alien.oxclient.test.client;

import java.io.IOException;

import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;

import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.client.OXRequestDeleteContact;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactBuilder;

public class TestOXRequestDeleteContact extends OXRequestTestCase {

	private OXContact mContact;
	private String mFolderId = "23";

	@Override
	protected OXRequest doCreateOXRequest() {
		mContact = new OXContactBuilder().addContext(getContext())
				.addContactInformations(mContactInformations).getOXObject();
		return new OXRequestDeleteContact(mContact, mFolderId, mDate, mSession);
	}

	@Override
	protected String doCreateExpectedUri(boolean pEncrypted) {
		return getScheme(pEncrypted)
				+ mOXAccountManagerMockImpl.getAccountServer()
				+ "/ajax/contacts?action=delete" + "&timestamp="
				+ String.valueOf(mDate.getTime()) + "&session="
				+ mSession.getSessionID();
	}

	public void testEntity() throws ParseException, IOException, JSONException {
		mContact = new OXContactBuilder().addContext(getContext())
				.addContactInformations(mContactInformations).getOXObject();
		OXRequestDeleteContact createContact = new OXRequestDeleteContact(
				mContact, mFolderId, mSession);
		String entity = EntityUtils.toString(createContact.doCreateEntity());
		assertTrue(entity.contains("\"folder\":\"" + mFolderId + "\""));
		assertFalse(entity.contains("\"id\""));
	}

}
