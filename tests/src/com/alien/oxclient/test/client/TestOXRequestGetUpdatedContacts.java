package com.alien.oxclient.test.client;

import java.util.Date;

import android.net.Uri;

import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.client.OXRequestGetUpdatedContacts;

public class TestOXRequestGetUpdatedContacts extends OXRequestTestCase {

	private String mColumns = "1,42,1337";
	private String mColumnsUrlEncoded = Uri.encode(mColumns);
	private String mFolderId = "23";
	private Date mNow = new Date();

	@Override
	protected OXRequest doCreateOXRequest() {
		return new OXRequestGetUpdatedContacts(mNow, mColumns, mFolderId,
				mSession);
	}

	@Override
	protected String doCreateExpectedUri(boolean pEncrypted) {
		return getScheme(pEncrypted)
				+ mOXAccountManagerMockImpl.getAccountServer()
				+ "/ajax/contacts?action=updates&timestamp=" + mNow.getTime()
				+ "&session=" + mSession.getSessionID() + "&columns="
				+ mColumnsUrlEncoded + "&folder=" + mFolderId;

	}

}
