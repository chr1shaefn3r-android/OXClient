package com.alien.oxclient.test.client;

import android.net.Uri;

import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.client.OXRequestGetAllContacts;

public class TestOXRequestGetAllContacts extends OXRequestTestCase {

	private String mColumns = "1,42,1337";
	private String mColumnsUrlEncoded = Uri.encode(mColumns);
	private String mFolderId = "23";

	@Override
	protected OXRequest doCreateOXRequest() {
		return new OXRequestGetAllContacts(mColumns, mFolderId, mSession);
	}

	@Override
	protected String doCreateExpectedUri(boolean pEncrypted) {
		return getScheme(pEncrypted)
				+ mOXAccountManagerMockImpl.getAccountServer()
				+ "/ajax/contacts?action=all&session="
				+ mSession.getSessionID() + "&columns=" + mColumnsUrlEncoded
				+ "&folder=" + mFolderId;
	}
}
