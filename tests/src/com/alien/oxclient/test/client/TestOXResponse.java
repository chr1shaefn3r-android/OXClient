package com.alien.oxclient.test.client;

import junit.framework.TestCase;

import com.alien.oxclient.client.OXResponse;

public class TestOXResponse extends TestCase {

	public void testOneTestToNearlyRuleThemAll() {
		Long timestamp = 42L;
		String data = "This is data, I swear!";
		String error = "You Shall not Pass!";
		String errorCode = "1338";
		String errorParams = "[]";
		OXResponse response = new OXResponse(timestamp, data, error,
				errorParams, errorCode);
		assertTrue(response.hasTimestamp());
		assertTrue(response.hasData());
		assertTrue(response.hasError());
		assertTrue(response.hasErrorParams());
		assertTrue(response.hasErrorCode());
		assertEquals(timestamp, response.getTimestamp());
		assertEquals(data, response.getData());
		assertEquals(error, response.getError());
		assertEquals(errorParams, response.getErrorParams());
		assertEquals(errorCode, response.getErrorCode());
		assertEquals("OXResponse [mTimestamp=" + timestamp + ", mData=" + data
				+ ", mError=" + error + ", mErrorCode=" + errorCode
				+ ", mErrorParams=" + errorParams + ", mBitmap=null]",
				response.toString());
	}

	public void testOneTestToRuleThemAll() {
		Long timestamp = -1L;
		String data = "This is data, I swear!";
		String error = "You Shall not Pass!";
		String errorCode = "1338";
		String errorParams = "[]";
		OXResponse response = new OXResponse(timestamp, data, error,
				errorParams, errorCode);
		assertFalse(response.hasTimestamp());
		assertTrue(response.hasData());
		assertTrue(response.hasError());
		assertTrue(response.hasErrorParams());
		assertTrue(response.hasErrorCode());
		assertEquals(timestamp, response.getTimestamp());
		assertEquals(data, response.getData());
		assertEquals(error, response.getError());
		assertEquals(errorParams, response.getErrorParams());
		assertEquals(errorCode, response.getErrorCode());
	}
}
