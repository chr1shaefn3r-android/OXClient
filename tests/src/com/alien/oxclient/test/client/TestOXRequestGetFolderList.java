package com.alien.oxclient.test.client;

import android.net.Uri;

import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.client.OXRequestGetFolderList;

public class TestOXRequestGetFolderList extends OXRequestTestCase {

	private String mColumns = "1,300";
	private String mColumnsUrlEncoded = Uri.encode(mColumns);
	private String mParent = "5";

	@Override
	protected OXRequest doCreateOXRequest() {
		return new OXRequestGetFolderList(mSession, mParent, mColumns);
	}

	@Override
	protected String doCreateExpectedUri(boolean pEncrypted) {
		return getScheme(pEncrypted)
				+ mOXAccountManagerMockImpl.getAccountServer()
				+ "/ajax/folders?session=" + mSession.getSessionID()
				+ "&action=list&columns=" + mColumnsUrlEncoded + "&parent="
				+ mParent;
	}
}
