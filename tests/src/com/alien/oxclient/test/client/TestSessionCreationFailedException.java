package com.alien.oxclient.test.client;

import junit.framework.TestCase;

import com.alien.oxclient.client.SessionCreationFailedException;

public class TestSessionCreationFailedException extends TestCase {

	public void testThrowableConstructor() {
		SessionCreationFailedException exception = new SessionCreationFailedException(
				null);
		assertNotNull(exception);
	}

	public void testMessageThrowableConstructor() {
		SessionCreationFailedException exception = new SessionCreationFailedException(
				"", null);
		assertNotNull(exception);
	}
}
