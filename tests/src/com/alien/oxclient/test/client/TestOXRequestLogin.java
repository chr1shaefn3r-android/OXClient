package com.alien.oxclient.test.client;

import com.alien.oxclient.client.OXPostRequest;
import com.alien.oxclient.client.OXRequestLogin;

public class TestOXRequestLogin extends OXPostRequestTestCase {

	@Override
	protected OXPostRequest doCreateOXRequest() {
		return new OXRequestLogin(mOXAccountManagerMockImpl.getAccountServer(),
				"john", "j0hn");
	}

	@Override
	protected String doCreateExpectedUri(boolean pEncrypted) {
		return getScheme(pEncrypted)
				+ mOXAccountManagerMockImpl.getAccountServer()
				+ "/ajax/login?action=login";
	}

	@Override
	protected String doCreateExpectedEntityString() {
		return "name=john&password=j0hn";
	}
}
