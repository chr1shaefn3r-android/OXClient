package com.alien.oxclient.test.client;

import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.client.OXRequestDownloadPicture;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactBuilder;

public class TestOXRequestDownloadPicture extends OXRequestTestCase {

	private OXContact mContact;

	@Override
	protected OXRequest doCreateOXRequest() {
		mContact = new OXContactBuilder().addContext(getContext())
				.addContactInformations(mContactInformations).getOXObject();
		mContact.getContactInformations()
				.put("mPicture",
						"/ajax/image/contact/picture?folder=59&id=7531&timestamp=1369227588209");
		return new OXRequestDownloadPicture(mContact, mSession);
	}

	@Override
	protected String doCreateExpectedUri(boolean pEncrypted) {
		return getScheme(pEncrypted)
				+ mOXAccountManagerMockImpl.getAccountServer()
				+ "/ajax/image/contact/picture?session="
				+ mSession.getSessionID()
				+ "&folder=59&id=7531&timestamp=1369227588209";
	}

}
