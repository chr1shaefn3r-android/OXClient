package com.alien.oxclient.test.client;

import android.graphics.Bitmap;

import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.client.OXRequestUpdateContactWithImage;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactBuilder;

public class TestOXRequestUpdateContactWithImage extends
		OXRequestTestCase {

	private OXContact mContact;
	private String mFolderId = "23";

	@Override
	protected OXRequest doCreateOXRequest() {
		mContact = new OXContactBuilder().addContext(getContext())
				.addContactInformations(mContactInformations).getOXObject();
		mContact.setPicture(Bitmap.createBitmap(2, 2, Bitmap.Config.ALPHA_8));
		return new OXRequestUpdateContactWithImage(mContact, mFolderId, mDate,
				mSession);
	}

	@Override
	protected String doCreateExpectedUri(boolean pEncrypted) {
		return getScheme(pEncrypted)
				+ mOXAccountManagerMockImpl.getAccountServer()
				+ "/ajax/contacts?action=update&session="
				+ mSession.getSessionID();
	}
}
