package com.alien.oxclient.test.client;

import android.net.Uri;

import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.test.OXNetworkTestCase;

public abstract class OXRequestTestCase extends OXNetworkTestCase {

	protected static final String HTTP_SCHEME = "http://";
	protected static final String HTTPS_SCHEME = "https://";

	/**
	 * Tests if the abstract {@link OXRequest} builds correct URIs.
	 */
	public void testCorrectUri() {
		OXRequest request = doCreateOXRequest();
		Uri createUrl = request.createUrl(false);
		Uri createUrlEncrypted = request.createUrl(true);
		assertEquals("[FailMessage] actual url: '" + createUrl.toString()
				+ "' expected: '" + doCreateExpectedUri(false) + "'",
				doCreateExpectedUri(false), createUrl.toString());
		assertEquals(
				"[FailMessage] actual url: '" + createUrlEncrypted.toString()
						+ "'", doCreateExpectedUri(true),
				createUrlEncrypted.toString());
	}

	protected abstract OXRequest doCreateOXRequest();

	protected abstract String doCreateExpectedUri(boolean pEncrypted);

	protected String getScheme(boolean pEncrypted) {
		return pEncrypted ? HTTPS_SCHEME : HTTP_SCHEME;
	}
}
