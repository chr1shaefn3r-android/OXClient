package com.alien.oxclient.test.client;

import junit.framework.TestCase;
import android.net.Uri;

import com.alien.oxclient.client.OXRequestAttribute;

public class TestOXRequestAttribute extends TestCase {

	private OXRequestAttribute mOXRequestAttribute;
	private Uri.Builder mUriBuilder;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mUriBuilder = new Uri.Builder();
		mOXRequestAttribute = new OXRequestAttribute(mUriBuilder);
	}

	public void testAddAction() {
		String action = "update";
		mOXRequestAttribute.addAction(action);
		assertTrue(mUriBuilder.build().toString().contains("action=" + action));
	}

	public void testAddColumns() {
		String columns = "1";
		mOXRequestAttribute.addColumns(columns);
		assertTrue(mUriBuilder.build().toString()
				.contains("columns=" + columns));
	}

	public void testAddFolder() {
		String folder = "2";
		mOXRequestAttribute.addFolder(folder);
		assertTrue(mUriBuilder.build().toString().contains("folder=" + folder));
	}

	public void testAddFolderId() {
		String folderId = "3";
		mOXRequestAttribute.addFolderId(folderId);
		assertTrue(mUriBuilder.build().toString()
				.contains("folder_id=" + folderId));
	}

	public void testAddId() {
		String id = "update";
		mOXRequestAttribute.addId(id);
		assertTrue(mUriBuilder.build().toString().contains("id=" + id));
	}

	public void testAddParent() {
		String parent = "3";
		mOXRequestAttribute.addParent(parent);
		assertTrue(mUriBuilder.build().toString().contains("parent=" + parent));
	}

	public void testAddTimestamp() {
		String timestamp = "1234577843";
		mOXRequestAttribute.addTimestamp(timestamp);
		assertTrue(mUriBuilder.build().toString()
				.contains("timestamp=" + timestamp));
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		mOXRequestAttribute = null;
		mUriBuilder = null;
	}

}
