package com.alien.oxclient.test.client;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;

import com.alien.oxclient.client.OXPostRequest;

public abstract class OXPostRequestTestCase extends OXRequestTestCase {

	public void testCorrectEntity() throws ParseException, IOException,
			JSONException {
		OXPostRequest request = doCreateOXRequest();
		HttpEntity entity = request.doCreateEntity();
		String entityContent = EntityUtils.toString(entity);
		assertEquals(doCreateExpectedEntityString(), entityContent);
	}

	protected abstract OXPostRequest doCreateOXRequest();

	protected abstract String doCreateExpectedEntityString();
}
