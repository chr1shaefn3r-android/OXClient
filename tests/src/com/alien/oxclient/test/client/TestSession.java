/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.client;

import junit.framework.TestCase;

import org.apache.http.client.CookieStore;
import org.apache.http.client.protocol.ClientContext;

import com.alien.oxclient.client.Session;

public class TestSession extends TestCase {

	/**
	 * Whole Class can be tested in one test
	 */
	public void testClass() {
		// Data the test is based on
		String authToken = "sessionID=a235fae104f34833a8df8a72cd9d899b;JSESSIONID=0e9ebe31e50c6db5ec3fba9134e61d19.APP1;"
				+ "open-xchange-secret-YtGl3FOv6xh4AmQSDhGYFw=b6eba18fb1dc5098bf9cbcb6cf281c25;";
		String server = "aliens.homelinux.org"; // Needs to pass
												// OXUtils.checkAndCorrectServer()!

		Session session = new Session(authToken, server);
		assertNotNull(session);
		assertEquals("a235fae104f34833a8df8a72cd9d899b", session.getSessionID());
		assertEquals(authToken, session.getAuthToken());
		// Only basic test because comparing httpContexts reliable is hard
		assertNotNull(session.getLocalContext());
		CookieStore one = (CookieStore) session.getLocalContext().getAttribute(
				ClientContext.COOKIE_STORE);
		assertEquals(2, one.getCookies().size());
	}
}
