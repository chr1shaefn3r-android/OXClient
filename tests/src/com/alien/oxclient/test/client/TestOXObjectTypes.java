package com.alien.oxclient.test.client;

import junit.framework.TestCase;

import com.alien.oxclient.client.OXObjectTypes;

public class TestOXObjectTypes extends TestCase {

	public void testValueOf() {
		assertEquals(OXObjectTypes.CONTACTS, OXObjectTypes.valueOf("CONTACTS"));
	}

	public void testValues() {
		assertTrue(OXObjectTypes.values().length > 0);
	}
}
