package com.alien.oxclient.test.client;

import android.net.Uri;

import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.client.OXRequestGetRootFolderList;

public class TestOXRequestGetRootFolderList extends OXRequestTestCase {

	private String mColumns = "1,300";
	private String mColumnsUrlEncoded = Uri.encode(mColumns);

	@Override
	protected OXRequest doCreateOXRequest() {
		return new OXRequestGetRootFolderList(mColumns, mSession);
	}

	@Override
	protected String doCreateExpectedUri(boolean pEncrypted) {
		return getScheme(pEncrypted)
				+ mOXAccountManagerMockImpl.getAccountServer()
				+ "/ajax/folders?action=root&columns=" + mColumnsUrlEncoded
				+ "&session=" + mSession.getSessionID();
	}
}
