package com.alien.oxclient.test.client;

import com.alien.oxclient.client.Session;
import com.alien.oxclient.client.SessionBuilder;
import com.alien.oxclient.client.SessionCreationFailedException;
import com.alien.oxclient.test.OXNetworkTestCase;

public class TestSessionBuilder extends OXNetworkTestCase {

	public void testConstructor() {
		SessionBuilder sessionBuilder = new SessionBuilder();
		assertNotNull(sessionBuilder);
	}

	public void testCreateWithBoolean() throws SessionCreationFailedException {
		Session session = SessionBuilder.createSession();
		assertEquals(mSession, session);
	}

	public void testCreateWithBooleanFailed() {
		mOXAccountManagerMockImpl.setAuthToken(null);
		try {
			SessionBuilder.createSession();
		} catch (SessionCreationFailedException expected) {
			assertNotNull(expected.getCause());
			assertTrue(expected.getCause().getMessage()
					.startsWith("Failed to retrieve authentication token"));
		}
	}

	public void testCreateWithBooleanForced()
			throws SessionCreationFailedException {
		Session session = SessionBuilder.createNewSession(mSession
				.getAuthToken());
		assertEquals(mSession, session);
	}
}
