package com.alien.oxclient.test.client;

import java.io.IOException;

import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;

import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.client.OXRequestUpdateContact;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactBuilder;

public class TestOXRequestUpdateContact extends OXRequestTestCase {

	private OXContact mContact;
	private String mFolderId = "23";

	@Override
	protected OXRequest doCreateOXRequest() {
		mContact = new OXContactBuilder().addContext(getContext())
				.addContactInformations(mContactInformations).getOXObject();
		return new OXRequestUpdateContact(mContact, mFolderId, mDate, mSession);
	}

	@Override
	protected String doCreateExpectedUri(boolean pEncrypted) {
		return getScheme(pEncrypted)
				+ mOXAccountManagerMockImpl.getAccountServer()
				+ "/ajax/contacts?action=update&id=" + mContact.getId()
				+ "&timestamp=" + String.valueOf(mDate.getTime()) + "&folder="
				+ mFolderId + "&session=" + mSession.getSessionID();
	}

	public void testEntity() throws ParseException, IOException, JSONException {
		mContact = new OXContactBuilder().addContext(getContext())
				.addContactInformations(mContactInformations).getOXObject();
		OXRequestUpdateContact updateContact = new OXRequestUpdateContact(
				mContact, mFolderId, mDate, mSession);
		String entity = EntityUtils.toString(updateContact.doCreateEntity());
		assertTrue(entity.contains("{"));
		assertTrue(entity.contains("}"));
	}
}
