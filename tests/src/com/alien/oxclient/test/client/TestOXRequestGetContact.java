package com.alien.oxclient.test.client;

import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.client.OXRequestGetContact;

public class TestOXRequestGetContact extends OXRequestTestCase {

	private String mOXId = "1";
	private String mFolderId = "42";

	@Override
	protected OXRequest doCreateOXRequest() {
		return new OXRequestGetContact(mOXId, mFolderId, mSession);
	}

	@Override
	protected String doCreateExpectedUri(boolean pEncrypted) {
		return getScheme(pEncrypted)
				+ mOXAccountManagerMockImpl.getAccountServer()
				+ "/ajax/contacts?action=get&id=" + mOXId + "&session="
				+ mSession.getSessionID() + "&folder=" + mFolderId;
	}
}
