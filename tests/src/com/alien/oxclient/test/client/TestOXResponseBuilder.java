package com.alien.oxclient.test.client;

import junit.framework.TestCase;

import com.alien.oxclient.client.OXResponse;
import com.alien.oxclient.client.OXResponseBuilder;

public class TestOXResponseBuilder extends TestCase {

	private OXResponseBuilder mResponseBuilder;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mResponseBuilder = new OXResponseBuilder();
	}

	public void testSuccessMessage() {
		OXResponse actualResponse = mResponseBuilder
				.parseMultiPart("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\"><html><head><META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><script type=\"text/javascript\">(parent.callback_new || window.opener && window.opener.callback_new)"
						+ "({\"timestamp\":1367871429168,\"data\":{\"id\":7518}})</script></head></html>");
		OXResponse expectedResponse = new OXResponse(
				Long.parseLong("1367871429168"), "{\"id\":7518}", null, null,
				null);
		assertEquals(expectedResponse, actualResponse);
	}

	public void testErrorMessage() {
		OXResponse actualResponse = mResponseBuilder
				.parseMultiPart("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\"><html><head><META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><script type=\"text/javascript\">(parent.callback_new || window.opener && window.opener.callback_new)"
						+ "({\"category\":1,\"error_params\":[\"image1\",\"application/octet-stream\"],\"error\":\"The file \\\"image1\\\" (\\\"application/octet-stream\\\") can't be imported as image. Only image types (JPG, GIF, BMP or PNG) are supported.\",\"error_id\":\"-22453143-17516\",\"categories\":\"USER_INPUT\",\"code\":\"SVL-0022\"})</script></head></html>");
		OXResponse expectedResponse = new OXResponse(
				null,
				null,
				"The file \"image1\" (\"application/octet-stream\") can't be imported as image. Only image types (JPG, GIF, BMP or PNG) are supported.",
				"[\"image1\",\"application\\/octet-stream\"]", "SVL-0022");
		assertEquals(expectedResponse, actualResponse);
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		mResponseBuilder = null;
	}
}
