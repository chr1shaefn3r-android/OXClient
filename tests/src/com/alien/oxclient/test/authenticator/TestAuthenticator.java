package com.alien.oxclient.test.authenticator;

import android.accounts.AccountManager;
import android.os.Bundle;

import com.alien.oxclient.authenticator.Authenticator;
import com.alien.oxclient.test.OXTestCase;

public class TestAuthenticator extends OXTestCase {

	private Authenticator mAuthenticator;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mAuthenticator = new Authenticator(
				new RenamingMockContext(getContext()));
	}

	public void testPreConditions() {
		assertNotNull(mAuthenticator);
	}

	public void testHasFeatures() {
		Bundle result = mAuthenticator.hasFeatures(null, null, null);
		assertFalse(result.getBoolean(AccountManager.KEY_BOOLEAN_RESULT));
	}

	public void testGetAuthTokenLabel() {
		String label = mAuthenticator.getAuthTokenLabel("main");
		assertEquals("OXClient", label);
	}

	public void testGetAuthTokenLabelWithUnknownType() {
		String label = mAuthenticator.getAuthTokenLabel("NotKnownType");
		assertNull(label);
	}

	public void testEditProperties() {
		try {
			mAuthenticator.editProperties(null, null);
			fail("Should have thrown UnsupportedOperationException!");
		} catch (UnsupportedOperationException expected) {
		}
	}
}
