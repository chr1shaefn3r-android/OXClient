/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.authenticator;

import android.content.Intent;
import android.os.IBinder;
import android.test.ServiceTestCase;

import com.alien.oxclient.authenticator.AuthenticationService;

public class TestAuthenticationService extends
		ServiceTestCase<AuthenticationService> {

	public TestAuthenticationService(String name) {
		super(AuthenticationService.class);
		setName(name);
	}

	public void testStartable() {
		Intent startIntent = new Intent();
		startIntent.setClass(getContext(), AuthenticationService.class);
		startService(startIntent);
	}

	public void testBindable() {
		Intent startIntent = new Intent();
		startIntent.setClass(getContext(), AuthenticationService.class);
		IBinder service = bindService(startIntent);
		assertNotNull(service);
	}
}
