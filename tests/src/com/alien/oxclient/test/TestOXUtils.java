/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test;

import org.json.JSONException;
import org.json.JSONObject;

import com.alien.oxclient.OXUtilsImpl;
import com.alien.oxclient.mockinterfaces.OXUtils;

public class TestOXUtils extends OXTestCase {

	private OXUtils mUtils;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mUtils = new OXUtilsImpl(new RenamingMockContext(getContext()));
	}

	/**
	 * Constructor
	 */
	public void testConstructor() {
		OXUtilsImpl utils = new OXUtilsImpl();
		assertNotNull(utils);
	}

	public void testConstructorWithContext() {
		OXUtilsImpl utils = new OXUtilsImpl(new RenamingMockContext(
				getContext()));
		assertNotNull(utils);
	}

	/**
	 * Test method: checkAndcorrectServerInput
	 */
	/** Clean Ip */
	public void testCheckAndcorrectServerInputIpClean() {
		assertEquals("10.13.37.10",
				mUtils.checkAndcorrectServerInput("10.13.37.10"));
	}

	/** Clean URL */
	public void testCheckAndcorrectServerInputUrlClean() {
		assertEquals("example.com",
				mUtils.checkAndcorrectServerInput("example.com"));
	}

	/** TrailingSlash Ip */
	public void testCheckAndcorrectServerInputIpTrailingSlash() {
		assertEquals("10.13.37.10",
				mUtils.checkAndcorrectServerInput("10.13.37.10/"));
	}

	/** TrailingSlash URL */
	public void testCheckAndcorrectServerInputUrlTrailingSlash() {
		assertEquals("example.com",
				mUtils.checkAndcorrectServerInput("example.com/"));
	}

	/** http://-Prefix Ip */
	public void testCheckAndcorrectServerInputIpHttpPrefix() {
		assertEquals("10.13.37.10",
				mUtils.checkAndcorrectServerInput("http://10.13.37.10"));
	}

	/** http://-Prefix URL */
	public void testCheckAndcorrectServerInputUrlHttpPrefix() {
		assertEquals("example.com",
				mUtils.checkAndcorrectServerInput("http://example.com"));
	}

	/** Bad URL */
	public void testCheckAndcorrectServerInputUrlBad() {
		try {
			mUtils.checkAndcorrectServerInput("example");
			fail("Should have raised an IllegalArgumentException");
		} catch (IllegalArgumentException expected) {
		}
	}

	/** Bad Ip */
	public void testCheckAndcorrectServerInputIpBad() {
		try {
			mUtils.checkAndcorrectServerInput("10.10");
			fail("Should have raised an IllegalArgumentException");
		} catch (IllegalArgumentException expected) {
		}
	}

	/** Bad Prefix URL */
	public void testCheckAndcorrectServerInputUrlBadPrefix() {
		try {
			mUtils.checkAndcorrectServerInput("htt:/example.com");
			fail("Should have raised an IllegalArgumentException");
		} catch (IllegalArgumentException expected) {
		}
	}

	/** Bad Prefix Ip */
	public void testCheckAndcorrectServerInputIpBadPrefix() {
		try {
			mUtils.checkAndcorrectServerInput("htt:/10.13.37.10");
			fail("Should have raised an IllegalArgumentException");
		} catch (IllegalArgumentException expected) {
		}
	}

	/** Bad Prefix URL 2 */
	public void testCheckAndcorrectServerInputUrlBadPrefix2() {
		assertEquals("example.com",
				mUtils.checkAndcorrectServerInput("ht://example.com"));
	}

	/** Bad Prefix Ip 2 */
	public void testCheckAndcorrectServerInputIpBadPrefix2() {
		assertEquals("10.13.37.10",
				mUtils.checkAndcorrectServerInput("ht://10.13.37.10"));
	}

	/**
	 * Test method: constructColumns
	 */
	public void testConstructColumns() {
		String columns = "1,500,501,502,503,504,505,515,506,507,508,509,510,511,517,516,536,537,518,"
				+ "519,520,521,522,569,523,525,526,527,528,"
				+ "538,539,540,598,541,542,543,544,545,546,547,568,563,548,549,550,562,"
				+ "551,552,553,554,559,560,561,564,555,556,557,558,565,566,592,570";
		assertEquals(columns, mUtils.constructColumns());
	}

	/**
	 * Test method: errorToString
	 */
	public void testErrorToString() {
		// 12-21 22:23:02.473: E/OXClient(11589): jsonString:
		// {"timestamp":1299233762454,"category":13,"error_params":["localhost","",10,1,"generic failure"],"error":"Wrong or missing login data to access mail server %1$s with login %2$s (user=%3$s, context=%4$s). Error message from mail server: %5$s","error_id":"1257736297-8372","data":[["59","Kontakte","contacts"]],"code":"MSG-1001"}
		String expected = "Wrong or missing login data to access mail server localhost with login  (user=10, context=1). Error message from mail server: generic failure";
		JSONObject json;
		try {
			json = new JSONObject(
					"{\"timestamp\":1299233762454,\"category\":13,\"error_params\":[\"localhost\",\"\",10,1,\"generic failure\"],\"error\":\"Wrong or missing login data to access mail server %1$s with login %2$s (user=%3$s, context=%4$s). Error message from mail server: %5$s\",\"error_id\":\"1257736297-8372\",\"data\":[[\"59\",\"Kontakte\",\"contacts\"]],\"code\":\"MSG-1001\"}");
			assertEquals(expected, mUtils.errorToString(json));
		} catch (JSONException e) {
			fail("Failed to build Test-JSONObject");
		}
	}

	public void testErrorToStringWithNull() {
		try {
			mUtils.errorToString(null);
			fail("Should have raised an NullPointerException");
		} catch (NullPointerException expected) {
		}
	}

	public void testErrorToStringWithNoError() {
		assertNull(mUtils.errorToString(new JSONObject()));
	}

	public void testErrorToStringWithNoErrorParams() {
		String expected = "Wrong or missing login data to access mail server localhost with login  (user=10, context=1). Error message from mail server: generic failure";
		JSONObject json;
		try {
			json = new JSONObject(
					"{\"timestamp\":1299233762454,\"category\":13,\"error\":\"Wrong or missing login data to access mail server localhost with login  (user=10, context=1). Error message from mail server: generic failure\",\"error_id\":\"1257736297-8372\",\"data\":[[\"59\",\"Kontakte\",\"contacts\"]],\"code\":\"MSG-1001\"}");
			assertEquals(expected, mUtils.errorToString(json));
		} catch (JSONException e) {
			fail("Failed to build Test-JSONObject");
		}
	}
}
