package com.alien.oxclient.test.action;

import org.json.JSONException;

import android.graphics.Bitmap;

import com.alien.oxclient.action.OXAction;
import com.alien.oxclient.action.OXActionGetPictureIntoContact;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactJSONObjectBuilder;
import com.alien.oxclient.test.OXNetworkTestCase;

public class TestOXActionGetPictureIntoContact extends OXNetworkTestCase {

	public void testWithCleanResult() throws JSONException {
		mOXDecisionMockImpl.setShouldSyncPictures(true);
		mOXNetworkMockImpl.setTimedout(false);
		Bitmap result = Bitmap.createBitmap(2, 2, Bitmap.Config.ALPHA_8);
		mOXNetworkMockImpl.setBitmap(result);
		OXContact contact = new OXContactJSONObjectBuilder()
				.addContext(getContext()).addFromJSONObject(mJsonObject)
				.getOXObject();
		contact.getContactInformations()
				.put("mPicture",
						"/ajax/image/contact/picture?folder=59&id=7531&timestamp=1369227588209");
		OXAction<Bitmap> action = new OXActionGetPictureIntoContact(
				getContext(), contact);
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), result, action.getResult());
		assertEquals(action.getErrorMessage(), result, contact.getPicture());
	}
}
