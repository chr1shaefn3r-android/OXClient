package com.alien.oxclient.test.action;

import java.util.ArrayList;

import org.json.JSONException;

import com.alien.oxclient.action.OXActionGetRootFolderList;
import com.alien.oxclient.test.OXNetworkTestCase;

public class TestOXActionGetRootFolderList extends OXNetworkTestCase {

	// "data":[["1","Persoenliche Ordner","system"],["2","Oeffentliche Ordner","system"],["3","Freigegebene Ordner","system"],["9","InfoStore","infostore"]]
	public void testWithCleanResult() throws JSONException {
		mOXNetworkMockImpl.setTimedout(false);
		String getRootFolderResponse = "{\"timestamp\":1299233500103,\"data\":[[\"Persoenliche Ordner\"],[\"Oeffentliche Ordner\"],[\"Freigegebene Ordner\"],[\"InfoStore\"]]}";
		mOXNetworkMockImpl.setResponse(getRootFolderResponse);
		ArrayList<String> expected = new ArrayList<String>();
		expected.add("Persoenliche Ordner");
		expected.add("Oeffentliche Ordner");
		expected.add("Freigegebene Ordner");
		expected.add("InfoStore");
		OXActionGetRootFolderList action = new OXActionGetRootFolderList(
				getContext());
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), expected, action.getResult());
	}

	public void testCleanResultWithWarningMessage() throws JSONException {
		mOXNetworkMockImpl.setTimedout(false);
		String warningMessage = "This is a warning!";
		String getRootFolderResponse = "{\"timestamp\":1299233500103,\"data\":[],\"error\":\""
				+ warningMessage + "\"}";
		mOXNetworkMockImpl.setResponse(getRootFolderResponse);
		OXActionGetRootFolderList action = new OXActionGetRootFolderList(
				getContext());
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasWarningMessage());
		assertEquals(action.getErrorMessage(), warningMessage,
				action.getWarningMessage());
	}

	public void testForTimestamp() throws JSONException {
		mOXNetworkMockImpl.setTimedout(false);
		String getRootFolderResponse = "{\"timestamp\":1299233500103,\"data\":[]}";
		Long time = Long.valueOf(1299233500103L);
		mOXNetworkMockImpl.setResponse(getRootFolderResponse);
		OXActionGetRootFolderList action = new OXActionGetRootFolderList(
				getContext());
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasTimestamp());
		assertEquals(action.getErrorMessage(), time, action.getTimestamp());
	}

	public void testWithInvalidJsonResult() {
		mOXNetworkMockImpl.setTimedout(false);
		String getRootFolderResponse = "{\"timestamp\":1299233500103\"data\"[[\"Persoenliche Ordner\"],[\"Oeffentliche Ordner\"],[\"Freigegebene Ordner\"],[\"InfoStore\"]]}";
		mOXNetworkMockImpl.setResponse(getRootFolderResponse);
		OXActionGetRootFolderList action = new OXActionGetRootFolderList(
				getContext());
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), true, action.hasErrorMessage());
		assertEquals(action.getErrorMessage(), true, action.getErrorMessage()
				.startsWith("JSONException"));
		assertEquals(true, action.getResult().get(0)
				.startsWith("JSONException"));
	}

	public void testIfHttpScNotOk() {
		mOXNetworkMockImpl.setTimedout(false);
		mOXNetworkMockImpl.setFailingHttpStatusCode();
		OXActionGetRootFolderList action = new OXActionGetRootFolderList(
				getContext());
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), true, action.hasErrorMessage());
		assertEquals(action.getErrorMessage(), true, action.getErrorMessage()
				.startsWith("HttpResponseException"));
		assertEquals(true,
				action.getResult().get(0).startsWith("HttpResponseException"));
	}

	public void testIfTimedOut() {
		mOXNetworkMockImpl.setTimedout(true);
		OXActionGetRootFolderList action = new OXActionGetRootFolderList(
				getContext());
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), true, action.hasErrorMessage());
		assertEquals(action.getErrorMessage(), true, action.getErrorMessage()
				.startsWith("ConnectTimeout"));
		assertEquals(true,
				action.getResult().get(0).startsWith("ConnectTimeout"));
	}

	public void testIfTimedOutWhileOffline() {
		mOXNetworkMockImpl.setTimedout(true);
		mOXUtilsMockImpl.setOnlineStatus(false);
		OXActionGetRootFolderList action = new OXActionGetRootFolderList(
				getContext());
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), true, action.hasErrorMessage());
		assertEquals(action.getErrorMessage(), true, action.getErrorMessage()
				.startsWith("ConnectTimeout"));
		// Nur ein softer Test auf die groesse des Arrays, da der tatsaechliche
		// Text erst zur Laufzeit ausgewaehlt wird
		assertEquals(1, action.getResult().size());
	}

	public void testIfResolveableSessionError() throws JSONException {
		mOXNetworkMockImpl.setTimedout(false);
		String errorMessage = "Something went horribly wrong!";
		String getRootFolderErrorResponse = "{\"timestamp\":1299233500103,\"data\":\"\",\"error\":\""
				+ errorMessage
				+ "\", \"error_params\":\"\", \"code\":\"SES-0205\"}";
		mOXNetworkMockImpl.setResponse(getRootFolderErrorResponse);
		String getRootFolderResponse = "{\"timestamp\":1299233500103,\"data\":[[\"Persoenliche Ordner\"],[\"Oeffentliche Ordner\"],[\"Freigegebene Ordner\"],[\"InfoStore\"]]}";
		mOXNetworkMockImpl.setSecondResponse(getRootFolderResponse);
		ArrayList<String> expected = new ArrayList<String>();
		expected.add("Persoenliche Ordner");
		expected.add("Oeffentliche Ordner");
		expected.add("Freigegebene Ordner");
		expected.add("InfoStore");
		OXActionGetRootFolderList action = new OXActionGetRootFolderList(
				getContext());
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), expected, action.getResult());
	}
}
