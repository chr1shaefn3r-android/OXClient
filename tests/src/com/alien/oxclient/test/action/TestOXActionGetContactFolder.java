package com.alien.oxclient.test.action;

import org.json.JSONException;

import com.alien.oxclient.action.OXActionGetContactFolder;
import com.alien.oxclient.action.OXFolderModules;
import com.alien.oxclient.model.OXFolder;
import com.alien.oxclient.test.OXNetworkTestCase;

public class TestOXActionGetContactFolder extends OXNetworkTestCase {

	public void testWithCleanResult() throws JSONException {
		mOXAccountManagerMockImpl.setContactsFolder("wrong!");
		mOXAccountManagerMockImpl.setContactsFolderId("wrong!!");
		mOXNetworkMockImpl.setTimedout(false);
		String getRootFolderResponse = "{\"timestamp\":1299233500103,\"data\":[[\"60\",\"Tasks\",\"tasks\"],[\"59\",\"Contacts\",\"contacts\"],[\"58\",\"Calendar\",\"calendar\"]]}";
		mOXNetworkMockImpl.setResponse(getRootFolderResponse);
		OXFolder expected = new OXFolder("59", "Contacts",
				OXFolderModules.CONTACTS);
		OXActionGetContactFolder action = new OXActionGetContactFolder(
				getContext());
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), expected, action.getResult());
		assertEquals("59", mOXAccountManagerMockImpl.getContactsFolderId());
		assertEquals("Contacts", mOXAccountManagerMockImpl.getContactsFolder());
	}
}
