package com.alien.oxclient.test.action;

import java.util.ArrayList;
import java.util.Date;

import org.json.JSONException;

import com.alien.oxclient.action.OXActionGetAllContactIds;
import com.alien.oxclient.test.OXNetworkTestCase;

public class TestOXActionGetAllContactIds extends OXNetworkTestCase {

	public void testWithCleanResult() throws JSONException {
		mOXNetworkMockImpl.setTimedout(false);
		String getAllContactsResponse = "{\"timestamp\":1358919838004,\"data\":[[1],[2],[23],[42],[1337]]}";
		Date date = new Date();
		date.setTime(1358919838004L);
		mOXNetworkMockImpl.setResponse(getAllContactsResponse);
		ArrayList<String> result = new ArrayList<String>();
		result.add("1");
		result.add("2");
		result.add("23");
		result.add("42");
		result.add("1337");
		OXActionGetAllContactIds action = new OXActionGetAllContactIds(
				getContext());
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), result, action.getResult());
	}
}
