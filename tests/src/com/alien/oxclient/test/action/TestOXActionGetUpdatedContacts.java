package com.alien.oxclient.test.action;

import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.OXUtilsImpl;
import com.alien.oxclient.action.OXActionGetUpdatedContacts;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactJSONArrayBuilder;
import com.alien.oxclient.test.OXNetworkTestCase;

public class TestOXActionGetUpdatedContacts extends OXNetworkTestCase {

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		OXApplication.setUtils(new OXUtilsImpl(getContext()));
	}

	public void testWithCleanResult() throws JSONException {
		mOXNetworkMockImpl.setTimedout(false);
		// ImTypeWorkIm1&ImTypeHomeIm1 hinzugefuegt
		// DisplayName&FamilyName geupdated
		String jsonArrayStringJohnDoe = "[1,\"John Da\",\"John\",\"Da\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,\"ImWorkJohnDoe\",\"ImHomeJohnDoe\"]";
		// Callback&Car hinzugefuegt
		// 22->23 geupdated
		// 424242 geloescht
		String jsonArrayStringJaneDoe = "[2,\"Jane Doe\",\"Jane\",\"Doe\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,\"23\",null,null,\"85\",\"900\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,\"jane.doe@sap.com\",\"jane.doe@web.de\",\"jane.doe@gmx.de\",\"asdf\",null,null]";
		String getAllContactsResponse = "{\"timestamp\":1358919838004,\"data\":["
				+ jsonArrayStringJohnDoe + "," + jsonArrayStringJaneDoe + "]}";
		Date date = new Date();
		date.setTime(1358919838004L);
		mOXNetworkMockImpl.setResponse(getAllContactsResponse);
		OXContact expectedJohnDoe = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(date)
				.addFromJSONArray(new JSONArray(jsonArrayStringJohnDoe))
				.getOXObject();
		OXContact expectedJaneDoe = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(date)
				.addFromJSONArray(new JSONArray(jsonArrayStringJaneDoe))
				.getOXObject();
		OXActionGetUpdatedContacts action = new OXActionGetUpdatedContacts(
				getContext(), new Date());
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), 2, action.getResult().size());
		assertEquals(action.getErrorMessage(), expectedJohnDoe, action
				.getResult().get(0));
		assertEquals(action.getErrorMessage(), expectedJaneDoe, action
				.getResult().get(1));
	}
}
