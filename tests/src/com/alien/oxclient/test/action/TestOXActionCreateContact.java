package com.alien.oxclient.test.action;

import org.json.JSONException;

import android.graphics.Bitmap;

import com.alien.oxclient.action.OXAction;
import com.alien.oxclient.action.OXActionCreateContact;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactJSONObjectBuilder;
import com.alien.oxclient.test.OXNetworkTestCase;

public class TestOXActionCreateContact extends OXNetworkTestCase {

	public void testWithCleanResultNoPicture() throws JSONException {
		mOXDecisionMockImpl.setShouldSyncPictures(false);
		mOXNetworkMockImpl.setTimedout(false);
		String result = "42";
		String createContactResponse = "{\"timestamp\":1358919838004,\"data\":{\"id\":"
				+ result + "}}";
		mOXNetworkMockImpl.setResponse(createContactResponse);
		OXContact contact = new OXContactJSONObjectBuilder()
				.addContext(getContext()).addFromJSONObject(mJsonObject)
				.getOXObject();
		OXAction<String> action = new OXActionCreateContact(getContext(),
				contact);
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), result, action.getResult());
	}

	public void testWithCleanResultWithPictureContactHasPicture()
			throws JSONException {
		mOXDecisionMockImpl.setShouldSyncPictures(true);
		mOXNetworkMockImpl.setTimedout(false);
		String result = "42";
		String createContactResponse = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\"><html><head><META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><script type=\"text/javascript\">(parent.callback_new || window.opener && window.opener.callback_new)"
				+ "({\"timestamp\":1367871429168,\"data\":{\"id\":"
				+ result
				+ "}})</script></head></html>";
		mOXNetworkMockImpl.setResponse(createContactResponse);
		OXContact contact = new OXContactJSONObjectBuilder()
				.addContext(getContext()).addFromJSONObject(mJsonObject)
				.getOXObject();
		contact.setPicture(Bitmap.createBitmap(2, 2, Bitmap.Config.ALPHA_8));
		OXAction<String> action = new OXActionCreateContact(getContext(),
				contact);
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), result, action.getResult());
	}

	public void testWithCleanResultWithPictureContactHasNoPicture()
			throws JSONException {
		mOXDecisionMockImpl.setShouldSyncPictures(true);
		mOXNetworkMockImpl.setTimedout(false);
		String result = "42";
		String createContactResponse = "{\"timestamp\":1358919838004,\"data\":{\"id\":"
				+ result + "}}";
		mOXNetworkMockImpl.setResponse(createContactResponse);
		OXContact contact = new OXContactJSONObjectBuilder()
				.addContext(getContext()).addFromJSONObject(mJsonObject)
				.getOXObject();
		contact.setPicture(null);
		OXAction<String> action = new OXActionCreateContact(getContext(),
				contact);
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), result, action.getResult());
	}
}
