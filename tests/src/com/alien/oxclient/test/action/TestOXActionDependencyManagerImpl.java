package com.alien.oxclient.test.action;

import java.util.Date;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.OXUtilsImpl;
import com.alien.oxclient.action.OXActionDependencyManagerImpl;
import com.alien.oxclient.mockinterfaces.OXUtils;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactBuilder;
import com.alien.oxclient.test.OXTestCase;

public class TestOXActionDependencyManagerImpl extends OXTestCase {

	private OXActionDependencyManagerImpl mOXActionDependencyManagerImpl;
	private OXUtils mOXUtils;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mOXActionDependencyManagerImpl = new OXActionDependencyManagerImpl(
				getContext());
		mOXUtils = new OXUtilsImpl(getContext());
		OXApplication.setUtils(mOXUtils);
	}

	public void testConstructor() {
		assertNotNull(mOXActionDependencyManagerImpl);
	}

	public void testgetOXActionDeleteContact() {
		assertNotNull(mOXActionDependencyManagerImpl
				.getOXActionDeleteContact(null));
	}

	public void testgetOXActionUpdateContact() {
		OXContact contact = new OXContactBuilder().addContactInformations(
				mContactInformations).getOXObject();
		assertNotNull(mOXActionDependencyManagerImpl
				.getOXActionUpdateContact(contact));
	}

	public void testgetOXActionCreateContact() {
		assertNotNull(mOXActionDependencyManagerImpl
				.getOXActionCreateContact(null));
	}

	public void testgetOXActionGetContact() {
		assertNotNull(mOXActionDependencyManagerImpl
				.getOXActionGetContact(null));
	}

	public void testgetOXActionGetPictureIntoContact() {
		assertNotNull(mOXActionDependencyManagerImpl
				.getOXActionGetPictureIntoContact(null));
	}

	public void testgetOXActionGetContactFolder() {
		assertNotNull(mOXActionDependencyManagerImpl
				.getOXActionGetContactFolder());
	}

	public void testgetOXActionGetRootFolderList() {
		assertNotNull(mOXActionDependencyManagerImpl
				.getOXActionGetRootFolderList());
	}

	public void testgetOXActionLogin() {
		assertNotNull(mOXActionDependencyManagerImpl.getOXActionLogin("", "",
				""));
	}

	public void testgetOXActionGetAllContacts() {
		assertNotNull(mOXActionDependencyManagerImpl
				.getOXActionGetAllContacts());
	}

	public void testgetOXActionGetAllContactIds() {
		assertNotNull(mOXActionDependencyManagerImpl
				.getOXActionGetAllContactIds());
	}

	public void testgetOXActionGetUpdatedContacts() {
		assertNotNull(mOXActionDependencyManagerImpl
				.getOXActionGetUpdatedContacts(new Date()));
	}
}
