package com.alien.oxclient.test.action.mocks;

import java.util.ArrayList;

import com.alien.oxclient.model.OXFolder;

public class OXActionGetFolderListMock extends
		OXActionMockImpl<ArrayList<OXFolder>> {

	private ArrayList<OXFolder> mResult = null;

	@Override
	public boolean hasResult() {
		return mResult != null;
	}

	@Override
	public ArrayList<OXFolder> getResult() {
		return mResult;
	}

	public void setResult(ArrayList<OXFolder> pResult) {
		this.mResult = pResult;
	}
}
