package com.alien.oxclient.test.action.mocks;

import java.util.ArrayList;

public class OXActionGetAllContactIdsMock extends
		OXActionMockImpl<ArrayList<String>> {

	private ArrayList<String> mResult;

	public OXActionGetAllContactIdsMock() {
		mResult = new ArrayList<String>();
		mResult.add("1");
		mResult.add("2");
		mResult.add("3");
		mResult.add("4");
	}

	@Override
	public ArrayList<String> getResult() {
		return mResult;
	}

	@Override
	public boolean hasResult() {
		return(mResult != null);
	}

	public void setResult(ArrayList<String> pResult) {
		this.mResult = pResult;
	}
}
