package com.alien.oxclient.test.action.mocks;

import com.alien.oxclient.model.OXFolder;

public class OXActionGetContactFolderMock extends
		OXActionMockImpl<OXFolder> {

	private OXFolder mResult = null;

	@Override
	public boolean hasResult() {
		return mResult != null;
	}

	@Override
	public OXFolder getResult() {
		return mResult;
	}

	public void setResult(OXFolder pResult) {
		this.mResult = pResult;
	}
}
