package com.alien.oxclient.test.action.mocks;

import android.graphics.Bitmap;

public class OXActionGetPictureIntoContactMock extends OXActionMockImpl<Bitmap> {

	private Bitmap mResult;

	@Override
	public boolean hasResult() {
		return(mResult != null);
	}

	@Override
	public Bitmap getResult() {
		return this.mResult;
	}

	public void setBitmap(Bitmap pResult) {
		this.mResult = pResult;
	}
}
