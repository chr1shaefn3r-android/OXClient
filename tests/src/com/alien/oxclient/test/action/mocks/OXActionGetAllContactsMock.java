package com.alien.oxclient.test.action.mocks;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;

import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactJSONArrayBuilder;
import com.alien.oxclient.test.syncadapter.TestContactManager;

public class OXActionGetAllContactsMock extends
		OXActionMockImpl<ArrayList<OXContact>> {

	private ArrayList<OXContact> mResult;

	public OXActionGetAllContactsMock(Context pContext)
			throws JSONException {
		mResult = new ArrayList<OXContact>();
		OXContact johnDoe = new OXContactJSONArrayBuilder()
				.addContext(pContext)
				.addFromJSONArray(
						new JSONArray(TestContactManager.mContacts.get(0)))
				.getOXObject();
		mResult.add(johnDoe);
		OXContact janeDoe = new OXContactJSONArrayBuilder()
				.addContext(pContext)
				.addFromJSONArray(
						new JSONArray(TestContactManager.mContacts.get(1)))
				.getOXObject();
		mResult.add(janeDoe);
		OXContact maxMustermann = new OXContactJSONArrayBuilder()
				.addContext(pContext)
				.addFromJSONArray(
						new JSONArray(TestContactManager.mContacts.get(2)))
				.getOXObject();
		mResult.add(maxMustermann);
		OXContact erikaMustermann = new OXContactJSONArrayBuilder()
				.addContext(pContext)
				.addFromJSONArray(
						new JSONArray(TestContactManager.mContacts.get(3)))
				.getOXObject();
		mResult.add(erikaMustermann);
	}

	@Override
	public boolean hasResult() {
		return(mResult != null && mResult.size() > 0);
	}

	@Override
	public ArrayList<OXContact> getResult() {
		return mResult;
	}

	public void setResult(ArrayList<OXContact> pResult) {
		this.mResult = pResult;
	}
}
