package com.alien.oxclient.test.action.mocks;

import android.text.TextUtils;

public class OXActionLoginMock extends OXActionMockImpl<String> {

	private String mAuthtoken;

	public boolean hasAuthtoken() {
		return !TextUtils.isEmpty(mAuthtoken);
	}

	public String getAuthtoken() {
		return mAuthtoken;
	}

	public void setAuthtoken(String pAuthtoken) {
		this.mAuthtoken = pAuthtoken;
	}

	@Override
	public boolean hasResult() {
		return hasAuthtoken();
	}

	@Override
	public String getResult() {
		return getAuthtoken();
	}
}
