package com.alien.oxclient.test.action.mocks;

import java.util.ArrayList;

public class OXActionGetRootFolderListMock extends
		OXActionMockImpl<ArrayList<String>> {

	private ArrayList<String> mResult = null;

	@Override
	public boolean hasResult() {
		return mResult != null;
	}

	@Override
	public ArrayList<String> getResult() {
		return mResult;
	}

	public void setResult(ArrayList<String> pResult) {
		this.mResult = pResult;
	}
}
