package com.alien.oxclient.test.action.mocks;

import android.text.TextUtils;

public class OXActionDeleteContactMock extends OXActionMockImpl<String> {

	private String mResult = "[]";

	@Override
	public String getResult() {
		return mResult;
	}

	@Override
	public boolean hasResult() {
		return !TextUtils.isEmpty(mResult);
	}

	public void setResult(String pResult) {
		this.mResult = pResult;
	}
}
