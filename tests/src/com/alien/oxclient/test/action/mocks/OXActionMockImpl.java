package com.alien.oxclient.test.action.mocks;

import java.util.Date;

import android.text.TextUtils;

import com.alien.oxclient.action.OXAction;

public abstract class OXActionMockImpl<T> implements OXAction<T> {

	protected String mErrorMessage = null;
	protected String mWarningMessage = null;

	@Override
	public void perform() {
		// Do nothing, only mock
	}

	@Override
	public void perform(boolean pEncrypted) {
		// Do nothing, only mock
	}

	@Override
	public boolean hasErrorMessage() {
		return !TextUtils.isEmpty(mErrorMessage);
	}

	@Override
	public String getErrorMessage() {
		return mErrorMessage;
	}

	public void setErrorMessage(String pErrorMessage) {
		this.mErrorMessage = pErrorMessage;
	}

	@Override
	public boolean hasWarningMessage() {
		return !TextUtils.isEmpty(mWarningMessage);
	}

	@Override
	public String getWarningMessage() {
		return mWarningMessage;
	}

	public void setWarningMessage(String pWarningMessage) {
		this.mWarningMessage = pWarningMessage;
	}

	@Override
	public boolean hasTimestamp() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Long getTimestamp() {
		return new Date().getTime();
	}

	public abstract boolean hasResult();

	public abstract T getResult();
}
