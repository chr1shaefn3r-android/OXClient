package com.alien.oxclient.test.action.mocks;

import com.alien.oxclient.model.OXContact;

public class OXActionGetContactMock extends OXActionMockImpl<OXContact> {

	private OXContact mResult;

	@Override
	public OXContact getResult() {
		return mResult;
	}

	@Override
	public boolean hasResult() {
		return(mResult != null);
	}

	public void setResult(OXContact pResult) {
		this.mResult = pResult;
	}
}
