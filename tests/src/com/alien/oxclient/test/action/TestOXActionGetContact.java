package com.alien.oxclient.test.action;

import java.util.Date;
import java.util.LinkedHashMap;

import org.json.JSONException;

import com.alien.oxclient.action.OXAction;
import com.alien.oxclient.action.OXActionGetContact;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactBuilder;
import com.alien.oxclient.test.OXNetworkTestCase;

public class TestOXActionGetContact extends OXNetworkTestCase {

	public void testWithCleanResult() throws JSONException {
		mOXNetworkMockImpl.setTimedout(false);
		String getContactResponse = "{\"timestamp\":1358919838004,\"data\":{\"id\":6738,\"private_flag\":false,\"company\":\"Ides\",\"number_of_images\":0,\"uid\":\"90404860-5d28-4ca5-b1c5-99e735e2dc3c\",\"display_name\":\"Doe, John\",\"last_name\":\"Doe\",\"email1\":\"john.doe@ides.de\",\"folder_id\":59,\"useCount\":0,\"fax_business\":\"22\",\"created_by\":10,\"first_name\":\"John\",\"last_modified_utc\":1358919838004,\"number_of_attachments\":0,\"modified_by\":10,\"file_as\":\"John, Doe\",\"user_id\":0,\"last_modified\":1358923438004,\"creation_date\":1337790550392}}";
		Date date = new Date();
		date.setTime(1358919838004L);
		LinkedHashMap<String, String> contactInformations = new LinkedHashMap<String, String>();
		contactInformations.put("mID", "6738");
		contactInformations.put("mOrganizationTypeWorkCompany", "Ides");
		contactInformations.put("mNameDisplayName", "Doe, John");
		contactInformations.put("mNameFamilyName", "Doe");
		contactInformations.put("mNameGivenName", "John");
		contactInformations.put("mEmailTypeWork", "john.doe@ides.de");
		contactInformations.put("mPhoneTypeFaxWork", "22");
		mOXNetworkMockImpl.setResponse(getContactResponse);
		OXContact expected = new OXContactBuilder().addContext(getContext())
				.addTimestamp(date).addContactInformations(contactInformations)
				.getOXObject();
		OXAction<OXContact> action = new OXActionGetContact(getContext(),
				"6738");
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), expected, action.getResult());
	}
}
