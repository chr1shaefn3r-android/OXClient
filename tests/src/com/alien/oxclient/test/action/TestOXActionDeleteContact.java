package com.alien.oxclient.test.action;

import java.util.Date;

import org.json.JSONException;

import com.alien.oxclient.action.OXActionDeleteContact;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactBuilder;
import com.alien.oxclient.test.OXNetworkTestCase;

public class TestOXActionDeleteContact extends OXNetworkTestCase {

	public void testWithCleanResult() throws JSONException {
		mOXNetworkMockImpl.setTimedout(false);
		Date date = new Date();
		String result = "{}";
		String updateContactResponse = "{\"timestamp\":1358919838004,\"data\":"
				+ result + "}";
		date.setTime(1358919838004L);
		mOXNetworkMockImpl.setResponse(updateContactResponse);
		OXContact oxContact = new OXContactBuilder().addContext(getContext())
				.addTimestamp(date)
				.addContactInformations(mContactInformations).getOXObject();
		OXActionDeleteContact action = new OXActionDeleteContact(getContext(),
				oxContact);
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), result, action.getResult());
	}
}
