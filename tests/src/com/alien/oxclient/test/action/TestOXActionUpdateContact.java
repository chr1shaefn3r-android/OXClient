package com.alien.oxclient.test.action;

import java.util.Date;

import org.json.JSONException;

import android.graphics.Bitmap;

import com.alien.oxclient.action.OXAction;
import com.alien.oxclient.action.OXActionUpdateContact;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactBuilder;
import com.alien.oxclient.model.OXContactJSONObjectBuilder;
import com.alien.oxclient.test.OXNetworkTestCase;

public class TestOXActionUpdateContact extends OXNetworkTestCase {

	public void testWithCleanResultNoPicture() throws JSONException {
		mOXNetworkMockImpl.setTimedout(false);
		Date date = new Date();
		String result = "{}";
		String updateContactResponse = "{\"timestamp\":1358919838004,\"data\":"
				+ result + "}";
		date.setTime(1358919838004L);
		mOXNetworkMockImpl.setResponse(updateContactResponse);
		OXContact oxContact = new OXContactBuilder().addContext(getContext())
				.addTimestamp(date)
				.addContactInformations(mContactInformations).getOXObject();
		OXActionUpdateContact action = new OXActionUpdateContact(getContext(),
				oxContact);
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), result, action.getResult());
	}

	public void testWithCleanResultWithPictureContactHasPicture()
			throws JSONException {
		mOXDecisionMockImpl.setShouldSyncPictures(true);
		mOXNetworkMockImpl.setTimedout(false);
		String result = "12";
		String updateContactResponse = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\"><html><head><META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><script type=\"text/javascript\">(parent.callback_new || window.opener && window.opener.callback_new)"
				+ "({\"timestamp\":1367871429168,\"data\":"
				+ result
				+ "})</script></head></html>";
		mOXNetworkMockImpl.setResponse(updateContactResponse);
		OXContact contact = new OXContactJSONObjectBuilder()
				.addContext(getContext()).addFromJSONObject(mJsonObject)
				.getOXObject();
		contact.setPicture(Bitmap.createBitmap(2, 2, Bitmap.Config.ALPHA_8));
		OXAction<String> action = new OXActionUpdateContact(getContext(),
				contact);
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), result, action.getResult());
	}

	public void testWithCleanResultWithPictureContactHasNoPicture()
			throws JSONException {
		mOXDecisionMockImpl.setShouldSyncPictures(true);
		mOXNetworkMockImpl.setTimedout(false);
		String result = "23";
		String updateContactResponse = "{\"timestamp\":1358919838004,\"data\":"
				+ result + "}";
		mOXNetworkMockImpl.setResponse(updateContactResponse);
		OXContact contact = new OXContactJSONObjectBuilder()
				.addContext(getContext()).addFromJSONObject(mJsonObject)
				.getOXObject();
		contact.setPicture(null);
		OXAction<String> action = new OXActionUpdateContact(getContext(),
				contact);
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), result, action.getResult());
	}
}
