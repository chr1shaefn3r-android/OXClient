package com.alien.oxclient.test.action;

import java.util.ArrayList;

import org.json.JSONException;

import com.alien.oxclient.action.OXActionGetFolderList;
import com.alien.oxclient.action.OXFolderModules;
import com.alien.oxclient.model.OXFolder;
import com.alien.oxclient.test.OXNetworkTestCase;

public class TestOXActionGetFolderList extends OXNetworkTestCase {

	public void testWithCleanResult() throws JSONException {
		mOXNetworkMockImpl.setTimedout(false);
		String getRootFolderResponse = "{\"timestamp\":1299233500103,\"data\":[[\"60\",\"Tasks\",\"tasks\"],[\"59\",\"Contacts\",\"contacts\"],[\"58\",\"Calendar\",\"calendar\"]]}";
		mOXNetworkMockImpl.setResponse(getRootFolderResponse);
		ArrayList<OXFolder> expected = new ArrayList<OXFolder>();
		expected.add(new OXFolder("60", "Tasks", OXFolderModules.TASKS));
		expected.add(new OXFolder("59", "Contacts", OXFolderModules.CONTACTS));
		expected.add(new OXFolder("58", "Calendar", OXFolderModules.CALENDAR));
		OXActionGetFolderList action = new OXActionGetFolderList(
				getContext());
		action.perform();
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), expected, action.getResult());
	}
}
