package com.alien.oxclient.test.action;

import org.json.JSONException;

import com.alien.oxclient.action.OXAction;
import com.alien.oxclient.action.OXActionLogin;
import com.alien.oxclient.test.OXNetworkTestCase;

public class TestOXActionLogin extends OXNetworkTestCase {

	// '{"random":"75475c9e0df34d3b9484d13c9a364052","session":"9bd745cfe05a45fead7637b535688d65"}'
	// 'JSESSIONID' : '153ed8e47eda4cfa96340b3358202437.OX1'
	// 'open-xchange-secret-pjycW2J620Uu410UGJijw' :
	// 'ead49305ae7045888ecd921d1e9657f2'
	// 'open-xchange-public-session' : 'f3ddc3d68dd940d687785aaa398ca95a'
	// pAuthToken:
	// 'sessionID=9bd745cfe05a45fead7637b535688d65;JSESSIONID=153ed8e47eda4cfa96340b3358202437.OX1;open-xchange-secret-pjycW2J620Uu410UGJijw=ead49305ae7045888ecd921d1e9657f2;open-xchange-public-session=f3ddc3d68dd940d687785aaa398ca95a;'

	public void testWithCleanResult() throws JSONException {
		mOXNetworkMockImpl.setTimedout(false);
		String sessionId = "9bd745cfe05a45fead7637b535688d65";
		String loginResponse = "{\"random\":\"75475c9e0df34d3b9484d13c9a364052\",\"session\":\""
				+ sessionId + "\"}";
		mOXNetworkMockImpl.setResponse(loginResponse);
		String nameCookie1 = "JSESSIONID";
		String valueCookie1 = "153ed8e47eda4cfa96340b3358202437.OX1";
		String nameCookie2 = "open-xchange-secret-pjycW2J620Uu410UGJijw";
		String valueCookie2 = "ead49305ae7045888ecd921d1e9657f2";
		String nameCookie3 = "open-xchange-public-session";
		String valueCookie3 = "f3ddc3d68dd940d687785aaa398ca95a";
		mOXNetworkMockImpl.addCookie(nameCookie1, valueCookie1,
				mOXAccountManagerMockImpl.getAccountServer());
		mOXNetworkMockImpl.addCookie(nameCookie2, valueCookie2,
				mOXAccountManagerMockImpl.getAccountServer());
		mOXNetworkMockImpl.addCookie(nameCookie3, valueCookie3,
				mOXAccountManagerMockImpl.getAccountServer());
		OXAction<String> action = new OXActionLogin<String>(getContext(),
				mOXAccountManagerMockImpl.getAccountServer(), "john", "j0hn");
		action.perform();
		String expectedAuthtoken = "sessionID=" + sessionId + ";" + nameCookie1
				+ "=" + valueCookie1 + ";" + nameCookie2 + "=" + valueCookie2
				+ ";" + nameCookie3 + "=" + valueCookie3 + ";";
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), expectedAuthtoken,
				action.getResult());
	}

	public void testIfHttpScNotOk() {
		mOXNetworkMockImpl.setTimedout(false);
		mOXNetworkMockImpl.setFailingHttpStatusCode();
		OXAction<String> action = new OXActionLogin<String>(getContext(),
				mOXAccountManagerMockImpl.getAccountServer(), "john", "j0hn");
		action.perform();
		assertEquals(action.getErrorMessage(), false, action.hasResult());
		assertEquals(action.getErrorMessage(), true, action.hasErrorMessage());
		assertEquals(action.getErrorMessage(), true, action.getErrorMessage()
				.startsWith("HttpResponseException"));
	}

	public void testIfResolveableError() {
		mOXNetworkMockImpl.setTimedout(false);
		String errorMessage = "Guess what, something went wrong.";
		String loginErrorResponse = "{\"timestamp\":1299233500103,\"data\":\"\",\"error\":\""
				+ errorMessage
				+ "\", \"error_params\":\"\", \"code\":\"SES-0205\"}";
		mOXNetworkMockImpl.setResponse(loginErrorResponse);
		String sessionId = "9bd745cfe05a45fead7637b535688d65";
		String loginResponse = "{\"random\":\"75475c9e0df34d3b9484d13c9a364052\",\"session\":\""
				+ sessionId + "\"}";
		mOXNetworkMockImpl.setSecondResponse(loginResponse);
		String nameCookie1 = "JSESSIONID";
		String valueCookie1 = "153ed8e47eda4cfa96340b3358202437.OX1";
		String nameCookie2 = "open-xchange-secret-pjycW2J620Uu410UGJijw";
		String valueCookie2 = "ead49305ae7045888ecd921d1e9657f2";
		String nameCookie3 = "open-xchange-public-session";
		String valueCookie3 = "f3ddc3d68dd940d687785aaa398ca95a";
		mOXNetworkMockImpl.addCookie(nameCookie1, valueCookie1,
				mOXAccountManagerMockImpl.getAccountServer());
		mOXNetworkMockImpl.addCookie(nameCookie2, valueCookie2,
				mOXAccountManagerMockImpl.getAccountServer());
		mOXNetworkMockImpl.addCookie(nameCookie3, valueCookie3,
				mOXAccountManagerMockImpl.getAccountServer());
		OXAction<String> action = new OXActionLogin<String>(getContext(),
				mOXAccountManagerMockImpl.getAccountServer(), "john", "j0hn");
		action.perform();
		String expectedAuthtoken = "sessionID=" + sessionId + ";" + nameCookie1
				+ "=" + valueCookie1 + ";" + nameCookie2 + "=" + valueCookie2
				+ ";" + nameCookie3 + "=" + valueCookie3 + ";";
		assertEquals(action.getErrorMessage(), true, action.hasResult());
		assertEquals(action.getErrorMessage(), expectedAuthtoken,
				action.getResult());
	}
}
