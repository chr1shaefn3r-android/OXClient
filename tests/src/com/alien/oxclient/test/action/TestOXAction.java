package com.alien.oxclient.test.action;

import junit.framework.TestCase;

import com.alien.oxclient.action.OXActionImpl;
import com.alien.oxclient.client.OXResponse;

public class TestOXAction extends TestCase {

	public void testErrorToStringWithoutErrorParams() {
		OXResponse response = new OXResponse(1299233762454L, null,
				"This is an error without params", "", "MSG-1000");
		assertEquals("This is an error without params",
				OXActionImpl.errorToString(response));
	}

	public void testErrorToStringWithErrorParams() {
		OXResponse response = new OXResponse(1299233762454L, null,
				"This is an error with params. (server=%1$s | user=%2$s)",
				"[\"localhost\",\"\"]", "MSG-1000");
		assertEquals(
				"This is an error with params. (server=localhost | user=)",
				OXActionImpl.errorToString(response));
	}
}
