/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test;

import java.util.ArrayList;

import org.json.JSONException;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.provider.ContactsContract;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.OXUtilsImpl;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactJSONArrayBuilder;

public class OXContactTestCase extends OXTestCase {

	protected OXContact mContact;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
	}

	public void insertContact() throws JSONException, RemoteException,
			OperationApplicationException {
		setupContactInformations(true);
		mContact = new OXContactJSONArrayBuilder().addContext(getContext())
				.addTimestamp(mDate).addFromJSONArray(mJsonArray).getOXObject();
		assertNotNull(mContact);
		ArrayList<ContentProviderOperation> operations = mContact.insertContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] result = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		mContact.setRawContactId(String.valueOf(ContentUris.parseId(result[0].uri)));
		checkForContactAmount(1);
	}
}
