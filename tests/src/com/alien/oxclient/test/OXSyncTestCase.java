package com.alien.oxclient.test;

import android.content.SyncResult;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.test.action.mocks.OXActionCreateContactMockImpl;
import com.alien.oxclient.test.action.mocks.OXActionDeleteContactMock;
import com.alien.oxclient.test.action.mocks.OXActionGetAllContactIdsMock;
import com.alien.oxclient.test.action.mocks.OXActionGetUpdatedContactsMock;
import com.alien.oxclient.test.action.mocks.OXActionUpdateContactMock;
import com.alien.oxclient.test.mocks.OXActionDependencyManagerMockImpl;
import com.alien.oxclient.test.mocks.OXContentResolverMockImpl;

public class OXSyncTestCase extends OXNetworkTestCase {

	protected SyncResult mSyncResult = new SyncResult();
	protected OXActionDependencyManagerMockImpl mOXActionDependencyManagerMockImpl;
	protected OXActionDeleteContactMock mOXActionDeleteContactMockImpl;
	protected OXActionUpdateContactMock mOXActionUpdateContactMockImpl;
	protected OXActionCreateContactMockImpl mOXActionCreateContactMockImpl;
	protected OXActionGetAllContactIdsMock mOXActionGetAllContactIdsMockImpl;
	protected OXActionGetUpdatedContactsMock mOXActionGetAllUpdatedContactsMockImpl;
	protected OXContentResolverMockImpl mOXContentResolverMockImpl;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mOXContentResolverMockImpl = new OXContentResolverMockImpl(getContext());
		OXApplication.setOXContentResolver(mOXContentResolverMockImpl);
		mOXActionDependencyManagerMockImpl = new OXActionDependencyManagerMockImpl();
		OXApplication
				.setActionDependencyManager(mOXActionDependencyManagerMockImpl);
		mOXActionDeleteContactMockImpl = new OXActionDeleteContactMock();
		mOXActionDependencyManagerMockImpl
				.setOXActionDeleteContact(mOXActionDeleteContactMockImpl);
		mOXActionUpdateContactMockImpl = new OXActionUpdateContactMock();
		mOXActionDependencyManagerMockImpl
				.setOXActionUpdateContact(mOXActionUpdateContactMockImpl);
		mOXActionCreateContactMockImpl = new OXActionCreateContactMockImpl();
		mOXActionDependencyManagerMockImpl
				.setOXActionCreateContact(mOXActionCreateContactMockImpl);
		mOXActionGetAllContactIdsMockImpl = new OXActionGetAllContactIdsMock();
		mOXActionDependencyManagerMockImpl
				.setOXActionGetAllContactIdsMockImpl(mOXActionGetAllContactIdsMockImpl);
		mOXActionGetAllUpdatedContactsMockImpl = new OXActionGetUpdatedContactsMock(
				getContext());
		mOXActionDependencyManagerMockImpl
				.setOXActionGetUpdatedContactsMockImpl(mOXActionGetAllUpdatedContactsMockImpl);
	}
}
