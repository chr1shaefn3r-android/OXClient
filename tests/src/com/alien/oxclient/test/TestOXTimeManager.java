package com.alien.oxclient.test;

import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Set;

import junit.framework.TestCase;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.OXTimeManager;
import com.alien.oxclient.mockinterfaces.OXAccountManager;
import com.alien.oxclient.mockinterfaces.OXCalendar;
import com.alien.oxclient.test.mocks.OXAccountManagerMockImpl;
import com.alien.oxclient.test.mocks.OXCalendarMockImpl;
import com.alien.oxclient.test.mocks.OXPreferencesMockImpl;

public class TestOXTimeManager extends TestCase {

	private OXPreferencesMockImpl mPrefs;
	private OXAccountManager mAccManager;
	private OXCalendar mCalendar;
	private static HashMap<String, String> mOXToAndroidDateVaulesEuropeBrussels = new HashMap<String, String>();
	static {
		mOXToAndroidDateVaulesEuropeBrussels.put("653097600000", "1990-09-12");
		mOXToAndroidDateVaulesEuropeBrussels.put("749260800000", "1993-09-29");
		mOXToAndroidDateVaulesEuropeBrussels.put("-446256000000", "1955-11-11");
	}
	private static HashMap<String, String> mOXToAndroidDateVaulesEuropeBerlin = new HashMap<String, String>();;
	static {
		mOXToAndroidDateVaulesEuropeBerlin.put("649555200000", "1990-08-02");
		mOXToAndroidDateVaulesEuropeBerlin.put("708566400000", "1992-06-15");
		mOXToAndroidDateVaulesEuropeBerlin.put("-883958400000", "1941-12-28");
	}

	@Override
	protected void setUp() throws Exception {
		mPrefs = new OXPreferencesMockImpl();
		OXApplication.setPrefs(mPrefs);
		mAccManager = new OXAccountManagerMockImpl();
		OXApplication.setAccountManager(mAccManager);
	}

	public void testConstructor() throws CloneNotSupportedException {
		// Today is April 30th (Monday)
		mCalendar = new OXCalendarMockImpl(2012, Calendar.APRIL, 30, 12, 0, 0);
		OXApplication.setCalendar(mCalendar);
		OXTimeManager timeManager = new OXTimeManager();
		assertNotNull(timeManager);
	}

	public void testInMaintimeByDay() throws ParseException,
			CloneNotSupportedException {
		// Today is April 30th (Monday)
		mCalendar = new OXCalendarMockImpl(2012, Calendar.APRIL, 30, 12, 0, 0);
		OXApplication.setCalendar(mCalendar);
		// Maintime is Monday-Friday (standard 8 to 8)
		mPrefs.setMaintimeDays("2;3;4;5;6");
		// We should be in Maintime
		assertTrue(new OXTimeManager().isInMaintime());
	}

	public void testOutOfMaintimeByDay() throws ParseException,
			CloneNotSupportedException {
		// Today is April 30th (Monday)
		mCalendar = new OXCalendarMockImpl(2012, Calendar.APRIL, 30, 12, 0, 0);
		OXApplication.setCalendar(mCalendar);
		// Maintime is Wednesday-Friday (standard 8 to 8)
		mPrefs.setMaintimeDays("4;5;6");
		// We should not be in Maintime
		assertFalse(new OXTimeManager().isInMaintime());
	}

	public void testOutOfMaintimeByHour() throws ParseException,
			CloneNotSupportedException {
		// Today is April 30th (Monday)
		mCalendar = new OXCalendarMockImpl(2012, Calendar.APRIL, 30, 22, 0, 0);
		OXApplication.setCalendar(mCalendar);
		// Maintime is Wednesday-Friday (standard 8 to 8)
		mPrefs.setMaintimeDays("4;5;6");
		// We should not be in Maintime
		assertFalse(new OXTimeManager().isInMaintime());
	}

	public void testDifferenceInSecondsToMaintimeFull() {
		// Today is April 30th (Monday)
		mCalendar = new OXCalendarMockImpl(2012, Calendar.APRIL, 30, 2, 0, 0);
		OXApplication.setCalendar(mCalendar);
		// Maintime is Monday-Friday (standard 8 to 8)
		mPrefs.setMaintimeDays("2;3;4;5;6");
		// A complete sync-cycle fits
		assertEquals(Long.parseLong(mPrefs.getSecondarytimeSyncFrequency()),
				new OXTimeManager().differenceInSeconds());
	}

	public void testDifferenceInSecondsToMaintimeSqueezed() {
		// Today is April 30th (Monday)
		mCalendar = new OXCalendarMockImpl(2012, Calendar.APRIL, 30, 7, 0, 0);
		OXApplication.setCalendar(mCalendar);
		// Maintime is Monday-Friday (standard 8 to 8)
		mPrefs.setMaintimeDays("2;3;4;5;6");
		// Till maintime it is one hour = 3600s
		assertTrue(3600L <= new OXTimeManager().differenceInSeconds());
		assertTrue(3700L > new OXTimeManager().differenceInSeconds());
	}

	public void testDifferenceInSecondsToSecondMaintimeFull() {
		// Today is April 30th (Monday)
		mCalendar = new OXCalendarMockImpl(2012, Calendar.APRIL, 30, 12, 00, 0);
		OXApplication.setCalendar(mCalendar);
		// Maintime is Monday-Friday (standard 8 to 8)
		mPrefs.setMaintimeDays("2;3;4;5;6");
		// A complete sync-cycle fits
		assertEquals(Long.parseLong(mPrefs.getMaintimeSyncFrequency()),
				new OXTimeManager().differenceInSeconds());
	}

	public void testDifferenceInSecondsToSecondMaintimeSqueezed() {
		// Today is April 30th (Monday)
		mCalendar = new OXCalendarMockImpl(2012, Calendar.APRIL, 30, 19, 53, 0);
		OXApplication.setCalendar(mCalendar);
		// Maintime is Monday-Friday (standard 8 to 8)
		mPrefs.setMaintimeDays("2;3;4;5;6");
		// Till maintime it is seven minutes = 420s
		long differenceInSeconds = new OXTimeManager().differenceInSeconds();
		assertTrue(420L <= differenceInSeconds);
		assertTrue(430L > differenceInSeconds);
	}

	public void testDifferenceInSecondsOneAfterMaintime() {
		// Today is April 30th (Monday)
		mCalendar = new OXCalendarMockImpl(2012, Calendar.APRIL, 30, 20, 01, 0);
		OXApplication.setCalendar(mCalendar);
		// Maintime is Monday-Friday (standard 8 to 8)
		mPrefs.setMaintimeDays("2;3;4;5;6");
		assertEquals(Long.parseLong(mPrefs.getSecondarytimeSyncFrequency()),
				new OXTimeManager().differenceInSeconds());
	}

	public void testConvertOXTime2AndroidWithEuropeBrussels()
			throws CloneNotSupportedException {
		Set<String> keys = mOXToAndroidDateVaulesEuropeBrussels.keySet();
		for(String key : keys) {
			assertEquals(mOXToAndroidDateVaulesEuropeBrussels.get(key),
					new OXTimeManager().convertOXTime2Android(key));
		}
	}

	public void testConvertAndroidTime2OXWithEuropeBrussels()
			throws CloneNotSupportedException {
		Set<String> keys = mOXToAndroidDateVaulesEuropeBrussels.keySet();
		for(String key : keys) {
			assertEquals(
					key,
					new OXTimeManager()
							.convertAndroidTime2OX(mOXToAndroidDateVaulesEuropeBrussels
									.get(key)));
		}
	}

	public void testConvertOXTime2AndroidWithEuropeBerlin()
			throws CloneNotSupportedException {
		Set<String> keys = mOXToAndroidDateVaulesEuropeBerlin.keySet();
		for(String key : keys) {
			assertEquals(mOXToAndroidDateVaulesEuropeBerlin.get(key),
					new OXTimeManager().convertOXTime2Android(key));
		}
	}

	public void testConvertAndroidTime2OXWithEuropeBerlin()
			throws CloneNotSupportedException {
		Set<String> keys = mOXToAndroidDateVaulesEuropeBerlin.keySet();
		for(String key : keys) {
			assertEquals(
					key,
					new OXTimeManager()
							.convertAndroidTime2OX(mOXToAndroidDateVaulesEuropeBerlin
									.get(key)));
		}
	}
}
