/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test;

import android.test.ApplicationTestCase;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.test.mocks.OXAccountManagerMockImpl;
import com.alien.oxclient.test.mocks.OXContentResolverMockImpl;
import com.alien.oxclient.test.mocks.OXPreferencesMockImpl;
import com.alien.oxclient.test.mocks.OXUtilsMockImpl;

public class TestOXApplication extends ApplicationTestCase<OXApplication> {

	private OXApplication mApplication;

	public TestOXApplication(String name) {
		super(OXApplication.class);
		setName(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
		createApplication();
		mApplication = getApplication();
	}

	public void testPreconditions() {
		assertNotNull(mApplication);
	}

	public void testGetAccountManager() {
		assertNotNull(OXApplication.getAccountManager());
	}

	public void testSetAccountManager() {
		OXAccountManagerMockImpl mockAccountManager = new OXAccountManagerMockImpl();
		OXApplication.setAccountManager(mockAccountManager);
		assertEquals(mockAccountManager, OXApplication.getAccountManager());
	}

	public void testGetUtils() {
		assertNotNull(OXApplication.getUtils());
	}

	public void testSetUtils() {
		OXUtilsMockImpl mockUtils = new OXUtilsMockImpl();
		OXApplication.setUtils(mockUtils);
		assertEquals(mockUtils, OXApplication.getUtils());
	}

	public void testGetPrefs() {
		assertNotNull(OXApplication.getPrefs());
	}

	public void testSetPrefs() {
		OXPreferencesMockImpl mockPrefs = new OXPreferencesMockImpl();
		OXApplication.setPrefs(mockPrefs);
		assertEquals(mockPrefs, OXApplication.getPrefs());
	}

	public void testGetOXContentResolver() {
		assertNotNull(OXApplication.getOXContentResolver());
	}

	public void testSetOXContentResolver() {
		OXContentResolverMockImpl mockOXCtr = new OXContentResolverMockImpl(
				getContext());
		OXApplication.setOXContentResolver(mockOXCtr);
		assertEquals(mockOXCtr, OXApplication.getOXContentResolver());
	}

	public void testGetOXCalendar() {
		assertNotNull(OXApplication.getCalendar());
	}
}
