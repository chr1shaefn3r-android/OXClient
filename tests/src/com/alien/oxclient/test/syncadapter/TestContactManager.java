/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.test.syncadapter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.OperationApplicationException;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.RawContacts;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.OXUtilsImpl;
import com.alien.oxclient.mockinterfaces.OXAccountManager;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactBuilder;
import com.alien.oxclient.model.OXContactJSONArrayBuilder;
import com.alien.oxclient.model.OXContactRawContactIdBuilder;
import com.alien.oxclient.syncadapter.ContactManager;
import com.alien.oxclient.test.OXSyncTestCase;
import com.alien.oxclient.test.action.mocks.OXActionGetAllContactsMock;
import com.alien.oxclient.test.mocks.OXAccountManagerMockImpl;
import com.alien.oxclient.test.mocks.OXNotificationManagerMockImpl;

public class TestContactManager extends OXSyncTestCase {

	private ArrayList<OXContact> mConflictContact;
	private ArrayList<OXContact> mTestContact;
	private ArrayList<ContentProviderOperation> mOperations;
	private ArrayList<String> mRawContactIds;
	private ArrayList<OXContact> mControlConflictFree;
	public static ArrayList<String> mContacts = new ArrayList<String>();
	static {
		mContacts
				.add("[1,\"John Doe\",\"John\",\"Doe\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]");
		mContacts
				.add("[2,\"Jane Doe\",\"Jane\",\"Doe\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,\"424242\",null,null,null,null,null,null,null,null,\"22\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,\"jane.doe@sap.com\",\"jane.doe@web.de\",\"jane.doe@gmx.de\",\"asdf\",null,null]");
		mContacts
				.add("[3,\"Mustermann, Max\",\"Max\",\"Mustermann\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,\"Musterstrasse 1\",\"13377\",\"Musterstadt\",\"Musterstaat\",\"Musterland\",null,null,null,null,null,\"22\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]");
		mContacts
				.add("[4,\"Mustermann, Erika\",\"Erika\",\"Mustermann\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,\"Musterfirma\",null,null,\"Musterstadt\",null,null,null,null,null,null,null,\"55\",null,\"66\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,\"erika.mustermann@musterfirma.de\",\"erika.mustermann@web.de\",null,null,null,null]");
	}
	public static ArrayList<String> mUpdatedContacts = new ArrayList<String>();
	static {
		// ImTypeWorkIm1&ImTypeHomeIm1 hinzugefuegt
		// DisplayName&FamilyName geupdated
		mUpdatedContacts
				.add("[1,\"John Da\",\"John\",\"Da\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,\"ImWorkJohnDoe\",\"ImHomeJohnDoe\"]");
		// Callback&Car hinzugefuegt
		// 22->23 geupdated
		// 424242 geloescht
		mUpdatedContacts
				.add("[2,\"Jane Doe\",\"Jane\",\"Doe\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,\"23\",null,null,\"85\",\"900\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,\"jane.doe@sap.com\",\"jane.doe@web.de\",\"jane.doe@gmx.de\",\"asdf\",null,null]");
		// RelationTypeManager&Nickname hinzugefuegt
		// 13377->42424 und Musterstadt->MusterCity geupdated
		// 22 geloescht
		mUpdatedContacts
				.add("[3,\"Mustermann, Max\",\"Max\",\"Mustermann\",null,null,null,\"NicknameMax\",null,null,null,null,null,null,null,null,null,\"Diese Notiz ist toll :)\",null,null,null,null,null,null,\"Musterstrasse 1\",\"42424\",\"MusterCity\",\"Musterstaat\",\"Musterland\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]");
		// Prefix&Nickname hinzugefuegt
		// Musterstadt -> MusterCity geupdated
		// Firma geloescht
		mUpdatedContacts
				.add("[4,\"Mustermann, Erika\",\"Erika\",\"Mustermann\",null,null,\"Mrs\",\"NicknameErika\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,\"MusterCity\",null,null,null,null,null,null,null,\"55\",null,\"66\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,\"erika.mustermann@musterfirma.de\",\"erika.mustermann@web.de\",null,null,null,null]");
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		// Dependencies Injection
		OXApplication.setNotificationManager(new OXNotificationManagerMockImpl(
				null));
		mSyncResult = new SyncResult();
		mRawContactIds = new ArrayList<String>();

	}

	/**
	 * Constructor
	 */
	public void testConstructor() {
		ContactManager cManager = new ContactManager();
		assertNotNull(cManager);
	}

	/**
	 * onPerformSync
	 */
	public void testOnPerformSyncWithNullDate() throws JSONException,
			RemoteException, OperationApplicationException, ParseException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		OXActionGetAllContactsMock action = new OXActionGetAllContactsMock(
				getContext());
		mOXActionDependencyManagerMockImpl
				.setOXActionGetAllContactsMockImpl(action);
		// Prefs.getLastSync is by default zero!
		ArrayList<String> rawContactIds = new ArrayList<String>();
		// TODO:
		ContactManager.onPerformSync(getContext(), null, new Bundle(), null,
				null, mSyncResult, rawContactIds);
		checkForContactAmount(4);
		assertEquals(4, mSyncResult.stats.numInserts);
		assertTrue(mOXPreferencesMockImpl.getLastSync() > 0);
		assertEquals(ContactsContract.AUTHORITY,
				mOXContentResolverMockImpl.getAuthority());
		assertEquals(Long.parseLong(mOXPreferencesMockImpl.getSyncFrequency()),
				mOXContentResolverMockImpl.getFrequency());
		assertEquals(mOXAccountManagerMockImpl.getOXAccount(),
				mOXContentResolverMockImpl.getAccount());
		// Check contacts
		for(int i = 0; i < rawContactIds.size(); i++) {
			OXContact contactRead = new OXContactRawContactIdBuilder()
					.addContext(getContext()).addTimestamp(mDate)
					.addFromRowContactId(Long.valueOf(rawContactIds.get(i)))
					.getOXObject();
			OXContact contact = new OXContactJSONArrayBuilder()
					.addContext(new RenamingMockContext(getContext()))
					.addTimestamp(mDate)
					.addFromJSONArray(
							new JSONArray(TestContactManager.mContacts.get(i)))
					.getOXObject();
			contact.setVersion(contactRead.getVersion());
			contact.setRawContactId(contactRead.getRawContactId());
			assertEquals(contact, contactRead);
		}
	}

	public void testOnPerformSyncWithNoInternet() throws JSONException,
			RemoteException, OperationApplicationException, ParseException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		mOXPreferencesMockImpl.setLastSync(new Date().getTime());
		mOXActionGetAllContactIdsMockImpl.setResult(null);
		ArrayList<String> rawContactIds = new ArrayList<String>();
		ContactManager.onPerformSync(getContext(), null, new Bundle(), null,
				null, mSyncResult, rawContactIds);
		checkForContactAmount(0);
		assertEquals(0, rawContactIds.size());
	}

	public void testOnPerformSyncWithDateFourUpdates() throws JSONException,
			RemoteException, OperationApplicationException, ParseException {
		whipeOXContacts();
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		mOXPreferencesMockImpl.setLastSync(new Date().getTime());
		// Insert four contacts
		ArrayList<OXContact> insertedContacts = new ArrayList<OXContact>();
		for(int i = 0; i < 4; i++) {
			OXContact contact = new OXContactJSONArrayBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromJSONArray(
							new JSONArray(TestContactManager.mContacts.get(i)))
					.getOXObject();
			ArrayList<ContentProviderOperation> ops = contact.insertContact();
			ContentProviderResult[] results = getContext().getContentResolver()
					.applyBatch(ContactsContract.AUTHORITY, ops);
			assertEquals(ops.size(), results.length);
			checkForWorkDone(results);
			contact.setRawContactId(String.valueOf(ContentUris
					.parseId(results[0].uri)));
			insertedContacts.add(contact);
		}
		ContactManager.onPerformSync(getContext(), null, new Bundle(), null,
				null, mSyncResult, new ArrayList<String>());
		checkForContactAmount(4);
		assertEquals(4, mSyncResult.stats.numUpdates);
		assertTrue(mOXPreferencesMockImpl.getLastSync() > 0);
		assertEquals(ContactsContract.AUTHORITY,
				mOXContentResolverMockImpl.getAuthority());
		assertEquals(Long.parseLong(mOXPreferencesMockImpl.getSyncFrequency()),
				mOXContentResolverMockImpl.getFrequency());
		assertEquals(mOXAccountManagerMockImpl.getOXAccount(),
				mOXContentResolverMockImpl.getAccount());
		// Check contacts
		for(int i = 0; i < insertedContacts.size(); i++) {
			OXContact contactRead = new OXContactRawContactIdBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromRowContactId(
							Long.valueOf(insertedContacts.get(i)
									.getRawContactId())).getOXObject();
			OXContact contact = new OXContactJSONArrayBuilder()
					.addContext(new RenamingMockContext(getContext()))
					.addTimestamp(mDate)
					.addFromJSONArray(
							new JSONArray(TestContactManager.mUpdatedContacts
									.get(i))).getOXObject();
			contact.setVersion(contactRead.getVersion());
			contact.setRawContactId(contactRead.getRawContactId());
			// Force the Displayname to equality, as it is set by the system
			// based on usersettings!
			contact.putContactInformation("mNameDisplayName",
					contactRead.getContactInformation("mNameDisplayName"));
			assertEquals(contact, contactRead);
		}
	}

	public void testOnPerformSyncWithDateTwoUpdatesAndTwoInserts()
			throws JSONException, RemoteException,
			OperationApplicationException, ParseException {
		whipeOXContacts();
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		mOXPreferencesMockImpl.setLastSync(new Date().getTime());
		// Insert two contacts
		ArrayList<OXContact> insertedContacts = new ArrayList<OXContact>();
		for(int i = 0; i < 2; i++) {
			OXContact contact = new OXContactJSONArrayBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromJSONArray(
							new JSONArray(TestContactManager.mContacts.get(i)))
					.getOXObject();
			ArrayList<ContentProviderOperation> ops = contact.insertContact();
			ContentProviderResult[] results = getContext().getContentResolver()
					.applyBatch(ContactsContract.AUTHORITY, ops);
			assertEquals(ops.size(), results.length);
			checkForWorkDone(results);
			contact.setRawContactId(String.valueOf(ContentUris
					.parseId(results[0].uri)));
			insertedContacts.add(contact);
		}
		ArrayList<String> rawContactIds = new ArrayList<String>();
		ContactManager.onPerformSync(getContext(), null, new Bundle(), null,
				null, mSyncResult, rawContactIds);
		checkForContactAmount(4);
		assertEquals(2, mSyncResult.stats.numUpdates);
		assertEquals(2, mSyncResult.stats.numInserts);
		assertTrue(mOXPreferencesMockImpl.getLastSync() > 0);
		assertEquals(ContactsContract.AUTHORITY,
				mOXContentResolverMockImpl.getAuthority());
		assertEquals(Long.parseLong(mOXPreferencesMockImpl.getSyncFrequency()),
				mOXContentResolverMockImpl.getFrequency());
		assertEquals(mOXAccountManagerMockImpl.getOXAccount(),
				mOXContentResolverMockImpl.getAccount());
		// Check for updated-ontacts
		for(int i = 0; i < insertedContacts.size(); i++) {
			OXContact contactRead = new OXContactRawContactIdBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromRowContactId(
							Long.valueOf(insertedContacts.get(i)
									.getRawContactId())).getOXObject();
			OXContact contact = new OXContactJSONArrayBuilder()
					.addContext(new RenamingMockContext(getContext()))
					.addTimestamp(mDate)
					.addFromJSONArray(
							new JSONArray(TestContactManager.mUpdatedContacts
									.get(i))).getOXObject();
			contact.setVersion(contactRead.getVersion());
			contact.setRawContactId(contactRead.getRawContactId());
			assertEquals(contact, contactRead);
		}
		// Check for inserted-contacts
		for(int i = 0; i < insertedContacts.size(); i++) {
			OXContact contactRead = new OXContactRawContactIdBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromRowContactId(
							Long.valueOf(insertedContacts.get(i)
									.getRawContactId())).getOXObject();
			OXContact contact = new OXContactJSONArrayBuilder()
					.addContext(new RenamingMockContext(getContext()))
					.addTimestamp(mDate)
					.addFromJSONArray(
							new JSONArray(TestContactManager.mUpdatedContacts
									.get(i))).getOXObject();
			contact.setVersion(contactRead.getVersion());
			contact.setRawContactId(contactRead.getRawContactId());
			assertEquals(contact, contactRead);
		}
	}

	public void testOnPerformSyncWithDeletedServer() throws JSONException,
			RemoteException, OperationApplicationException, ParseException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		mOXPreferencesMockImpl.setLastSync(new Date().getTime());
		int contactAmount = 2;
		for(int i = 0; i < contactAmount; i++) {
			OXContact contact = new OXContactJSONArrayBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromJSONArray(
							new JSONArray(TestContactManager.mContacts.get(i)))
					.getOXObject();
			ArrayList<ContentProviderOperation> ops = contact.insertContact();
			ContentProviderResult[] results = getContext().getContentResolver()
					.applyBatch(ContactsContract.AUTHORITY, ops);
			assertEquals(ops.size(), results.length);
			checkForWorkDone(results);
		}
		// Make the "server" return no contacts
		mOXActionGetAllContactIdsMockImpl.setResult(new ArrayList<String>());
		ContactManager.onPerformSync(getContext(), null, new Bundle(), null,
				null, mSyncResult, new ArrayList<String>());
		assertTrue(mSyncResult.tooManyDeletions);
		// Zeigt in diesem Fall nur an wie viele Kontakte geloescht worden
		// waeren
		assertEquals(contactAmount, mSyncResult.stats.numDeletes);
		checkForContactAmount(contactAmount);
	}

	public void testOnPerformSyncWithDeletedServerButOverrideRequested()
			throws JSONException, RemoteException,
			OperationApplicationException, ParseException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		mOXPreferencesMockImpl.setLastSync(new Date().getTime());
		int contactAmount = 4;
		for(int i = 0; i < contactAmount; i++) {
			OXContact contact = new OXContactJSONArrayBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromJSONArray(
							new JSONArray(TestContactManager.mContacts.get(i)))
					.getOXObject();
			ArrayList<ContentProviderOperation> ops = contact.insertContact();
			ContentProviderResult[] results = getContext().getContentResolver()
					.applyBatch(ContactsContract.AUTHORITY, ops);
			assertEquals(ops.size(), results.length);
			checkForWorkDone(results);
		}
		// Make the "server" return no contacts
		mOXActionGetAllContactIdsMockImpl.setResult(new ArrayList<String>());
		Bundle bundle = new Bundle();
		bundle.putBoolean(
				ContentResolver.SYNC_EXTRAS_OVERRIDE_TOO_MANY_DELETIONS, true);
		ContactManager.onPerformSync(getContext(), null, bundle, null, null,
				mSyncResult, new ArrayList<String>());
		assertEquals(contactAmount, mSyncResult.stats.numDeletes);
		checkForContactAmount(0);
	}

	public void testOnPerformSyncWithDeletedServerButDiscardRequested()
			throws JSONException, RemoteException,
			OperationApplicationException, ParseException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		mOXPreferencesMockImpl.setLastSync(new Date().getTime());
		int contactAmount = 4;
		for(int i = 0; i < contactAmount; i++) {
			OXContact contact = new OXContactJSONArrayBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromJSONArray(
							new JSONArray(TestContactManager.mContacts.get(i)))
					.getOXObject();
			ArrayList<ContentProviderOperation> ops = contact.insertContact();
			ContentProviderResult[] results = getContext().getContentResolver()
					.applyBatch(ContactsContract.AUTHORITY, ops);
			assertEquals(ops.size(), results.length);
			checkForWorkDone(results);
		}
		// Make the "server" return no contacts
		mOXActionGetAllContactIdsMockImpl.setResult(new ArrayList<String>());
		Bundle bundle = new Bundle();
		bundle.putBoolean(ContentResolver.SYNC_EXTRAS_DISCARD_LOCAL_DELETIONS,
				true);
		ContactManager.onPerformSync(getContext(), null, bundle, null, null,
				mSyncResult, new ArrayList<String>());
		assertEquals(contactAmount, mSyncResult.stats.numInserts);
		checkForContactAmount(contactAmount);
	}

	public void testOnPerformSyncWithDeletedHandy() throws RemoteException,
			JSONException, OperationApplicationException, ParseException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		mOXPreferencesMockImpl.setLastSync(new Date().getTime());
		int contactAmount = 4;
		for(int i = 0; i < contactAmount; i++) {
			OXContact contact = new OXContactJSONArrayBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromJSONArray(
							new JSONArray(TestContactManager.mContacts.get(i)))
					.getOXObject();
			ArrayList<ContentProviderOperation> ops = contact.insertContact();
			ContentProviderResult[] results = getContext().getContentResolver()
					.applyBatch(ContactsContract.AUTHORITY, ops);
			assertEquals(ops.size(), results.length);
			checkForWorkDone(results);
			Builder setDeletedFlag = ContentProviderOperation
					.newUpdate(
							RawContacts.CONTENT_URI
									.buildUpon()
									.appendQueryParameter(
											ContactsContract.CALLER_IS_SYNCADAPTER,
											"true").build())
					.withSelection(RawContacts._ID + "=?",
							new String[] { contact.getRawContactId() })
					.withValue(RawContacts.DELETED, "1");
			ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
			operations.add(setDeletedFlag.build());
			results = getContext().getContentResolver().applyBatch(
					ContactsContract.AUTHORITY, operations);
			assertEquals(operations.size(), results.length);
		}
		ContactManager.onPerformSync(getContext(), null, new Bundle(), null,
				null, mSyncResult, new ArrayList<String>());
		assertTrue(mSyncResult.tooManyDeletions);
		// Server gibt standardmaessig vier Kontakte zurueck ...
		assertEquals(contactAmount, mSyncResult.stats.numDeletes);
		assertEquals(contactAmount, mSyncResult.stats.numEntries);
		checkForContactAmount(contactAmount);
	}

	public void testOnPerformSyncWithDeletedHandyButOverrideRequested()
			throws RemoteException, JSONException,
			OperationApplicationException, ParseException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		mOXPreferencesMockImpl.setLastSync(new Date().getTime());
		int contactAmount = 4;
		for(int i = 0; i < contactAmount; i++) {
			OXContact contact = new OXContactJSONArrayBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromJSONArray(
							new JSONArray(TestContactManager.mContacts.get(i)))
					.getOXObject();
			ArrayList<ContentProviderOperation> ops = contact.insertContact();
			ContentProviderResult[] results = getContext().getContentResolver()
					.applyBatch(ContactsContract.AUTHORITY, ops);
			assertEquals(ops.size(), results.length);
			checkForWorkDone(results);
			Builder setDeletedFlag = ContentProviderOperation
					.newUpdate(
							RawContacts.CONTENT_URI
									.buildUpon()
									.appendQueryParameter(
											ContactsContract.CALLER_IS_SYNCADAPTER,
											"true").build())
					.withSelection(RawContacts._ID + "=?",
							new String[] { contact.getRawContactId() })
					.withValue(RawContacts.DELETED, "1");
			ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
			operations.add(setDeletedFlag.build());
			results = getContext().getContentResolver().applyBatch(
					ContactsContract.AUTHORITY, operations);
			assertEquals(operations.size(), results.length);
		}
		Bundle bundle = new Bundle();
		bundle.putBoolean(
				ContentResolver.SYNC_EXTRAS_OVERRIDE_TOO_MANY_DELETIONS, true);
		ContactManager.onPerformSync(getContext(), null, bundle, null, null,
				mSyncResult, new ArrayList<String>());
		// Server gibt standardmaessig vier Kontakte zurueck ...
		assertEquals(contactAmount, mSyncResult.stats.numDeletes);
		checkForContactAmount(0);
	}

	public void testOnPerformSyncWithDeletedHandyButDiscardRequested()
			throws RemoteException, JSONException,
			OperationApplicationException, ParseException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		mOXPreferencesMockImpl.setLastSync(new Date().getTime());
		int contactAmount = 4;
		for(int i = 0; i < contactAmount; i++) {
			OXContact contact = new OXContactJSONArrayBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromJSONArray(
							new JSONArray(TestContactManager.mContacts.get(i)))
					.getOXObject();
			ArrayList<ContentProviderOperation> ops = contact.insertContact();
			ContentProviderResult[] results = getContext().getContentResolver()
					.applyBatch(ContactsContract.AUTHORITY, ops);
			assertEquals(ops.size(), results.length);
			checkForWorkDone(results);
			Builder setDeletedFlag = ContentProviderOperation
					.newUpdate(
							RawContacts.CONTENT_URI
									.buildUpon()
									.appendQueryParameter(
											ContactsContract.CALLER_IS_SYNCADAPTER,
											"true").build())
					.withSelection(RawContacts._ID + "=?",
							new String[] { contact.getRawContactId() })
					.withValue(RawContacts.DELETED, "1");
			ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
			operations.add(setDeletedFlag.build());
			results = getContext().getContentResolver().applyBatch(
					ContactsContract.AUTHORITY, operations);
			assertEquals(operations.size(), results.length);
		}
		Bundle bundle = new Bundle();
		bundle.putBoolean(ContentResolver.SYNC_EXTRAS_DISCARD_LOCAL_DELETIONS,
				true);
		ContactManager.onPerformSync(getContext(), null, bundle, null, null,
				mSyncResult, new ArrayList<String>());
		// Server gibt standardmaessig vier Kontakte zurueck ...
		assertEquals(contactAmount, mSyncResult.stats.numInserts);
		checkForContactAmount(contactAmount);
	}

	public void testOnPerformSyncWithOneConflict() throws JSONException,
			RemoteException, OperationApplicationException, ParseException {
		whipeOXContacts();
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		// Setup changed contact
		OXContact contact1 = new OXContactJSONArrayBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromJSONArray(
						new JSONArray(TestContactManager.mUpdatedContacts
								.get(0))).getOXObject();
		ArrayList<ContentProviderOperation> operations = contact1
				.insertContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] result = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		contact1.setRawContactId(String.valueOf(ContentUris
				.parseId(result[0].uri)));
		Builder resetDirtyFlag = ContentProviderOperation
				.newUpdate(
						RawContacts.CONTENT_URI
								.buildUpon()
								.appendQueryParameter(
										ContactsContract.CALLER_IS_SYNCADAPTER,
										"true").build())
				.withSelection(RawContacts._ID + "=?",
						new String[] { contact1.getRawContactId() })
				.withValue(RawContacts.DIRTY, "1");
		operations.clear();
		operations.add(resetDirtyFlag.build());
		result = resolver.applyBatch(ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		// Setup time:
		mOXPreferencesMockImpl.setLastSync(new Date().getTime());
		ArrayList<String> rawContactIds = new ArrayList<String>();
		ContactManager.onPerformSync(getContext(), null, new Bundle(), null,
				null, mSyncResult, rawContactIds);
		// Vorraussetzung fuer die Asserts: Die MockImplementierung der HttpApi
		// gibt vier Kontakte zurueck {1,2,3,4}
		// Da wir jetzt einen Konflikt erzeugt haben teilt es sich 1 zu 3 auf
		// die beiden Statistiken auf
		assertEquals(1, mSyncResult.stats.numConflictDetectedExceptions);
		assertEquals(3, mSyncResult.stats.numInserts);
		assertEquals(3, rawContactIds.size());
		// Insgesamt bleiben am Ende aber trotzdem 4 Kontakte übrig, da der
		// Konflikt ein Kontakt ist
		// der bereits existiert + drei Kontakte vom Server
		checkForContactAmount(4);
	}

	public void testOnPerformSyncWithTwoConflicts() throws JSONException,
			RemoteException, OperationApplicationException, ParseException {
		whipeOXContacts();
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		// Setup changed contact
		OXContact contact1 = new OXContactJSONArrayBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromJSONArray(
						new JSONArray(TestContactManager.mUpdatedContacts
								.get(0))).getOXObject();
		OXContact contact2 = new OXContactJSONArrayBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromJSONArray(
						new JSONArray(TestContactManager.mUpdatedContacts
								.get(1))).getOXObject();
		ArrayList<ContentProviderOperation> operations = contact1
				.insertContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] result = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		contact1.setRawContactId(String.valueOf(ContentUris
				.parseId(result[0].uri)));
		operations.clear();
		operations.addAll(contact2.insertContact());
		result = resolver.applyBatch(ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		contact2.setRawContactId(String.valueOf(ContentUris
				.parseId(result[0].uri)));
		Builder resetDirtyFlag = ContentProviderOperation
				.newUpdate(
						RawContacts.CONTENT_URI
								.buildUpon()
								.appendQueryParameter(
										ContactsContract.CALLER_IS_SYNCADAPTER,
										"true").build())
				.withSelection(RawContacts._ID + "=?",
						new String[] { contact1.getRawContactId() })
				.withValue(RawContacts.DIRTY, "1");
		operations.clear();
		operations.add(resetDirtyFlag.build());
		resetDirtyFlag = ContentProviderOperation
				.newUpdate(
						RawContacts.CONTENT_URI
								.buildUpon()
								.appendQueryParameter(
										ContactsContract.CALLER_IS_SYNCADAPTER,
										"true").build())
				.withSelection(RawContacts._ID + "=?",
						new String[] { contact2.getRawContactId() })
				.withValue(RawContacts.DIRTY, "1");
		operations.add(resetDirtyFlag.build());
		result = resolver.applyBatch(ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		// Setup time:
		mOXPreferencesMockImpl.setLastSync(new Date().getTime());
		ArrayList<String> rawContactIds = new ArrayList<String>();
		ContactManager.onPerformSync(getContext(), null, new Bundle(), null,
				null, mSyncResult, rawContactIds);
		// Vorraussetzung fuer die Asserts: Die MockImplementierung der HttpApi
		// gibt vier Kontakte zurueck {1,2,3,4}
		// Da wir jetzt zwei Konflikte erzeugt haben teilt es sich 2 zu 2 auf
		// die beiden Statistiken auf
		assertEquals(2, mSyncResult.stats.numConflictDetectedExceptions);
		assertEquals(2, mSyncResult.stats.numInserts);
		assertEquals(2, rawContactIds.size());
		checkForContactAmount(4);
	}

	public void testOnPerformSyncWithConflictedButAgainEditedContact()
			throws JSONException, RemoteException,
			OperationApplicationException, ParseException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		mOXPreferencesMockImpl.setLastSync(new Date().getTime());
		setupContactInformations(true);
		OXContact contact = new OXContactBuilder().addContext(getContext())
				.addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		ArrayList<ContentProviderOperation> operations = contact
				.insertContact();
		ContentProviderResult[] results = getContext().getContentResolver()
				.applyBatch(ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		checkForWorkDone(results);
		contact.setRawContactId(String.valueOf(ContentUris
				.parseId(results[0].uri)));
		// Set the preconditions: marked as conflict, but again edited
		Builder resetDirtyFlag = ContentProviderOperation
				.newUpdate(
						RawContacts.CONTENT_URI
								.buildUpon()
								.appendQueryParameter(
										ContactsContract.CALLER_IS_SYNCADAPTER,
										"true").build())
				.withSelection(RawContacts._ID + "=?",
						new String[] { contact.getRawContactId() })
				.withValue(RawContacts.DIRTY, "1")
				.withValue(RawContacts.SYNC1, "1");
		operations.clear();
		operations.add(resetDirtyFlag.build());
		results = getContext().getContentResolver().applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), results.length);
		Bundle bundle = new Bundle();
		bundle.putBoolean(ContentResolver.SYNC_EXTRAS_DISCARD_LOCAL_DELETIONS,
				true);
		ArrayList<String> rawContactIds = new ArrayList<String>();
		mOXActionGetAllContactIdsMockImpl.setResult(new ArrayList<String>());
		ContactManager.onPerformSync(getContext(), null, bundle, null, null,
				mSyncResult, rawContactIds);
		assertEquals(0, rawContactIds.size());
		assertEquals(0, mSyncResult.stats.numInserts);
		assertEquals(0, mSyncResult.stats.numDeletes);
		assertEquals(0, mSyncResult.stats.numUpdates);
		assertEquals(0, mSyncResult.stats.numConflictDetectedExceptions);
	}

	/**
	 * syncToPhone
	 */
	public void testSyncToPhoneWithTwoInserts() throws JSONException,
			NumberFormatException, RemoteException,
			OperationApplicationException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		setupContactInformations(true);
		ArrayList<OXContact> conflictFreeContacts = new ArrayList<OXContact>();
		OXContact contact1 = new OXContactBuilder().addContext(getContext())
				.addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		conflictFreeContacts.add(contact1);
		mJsonArray = new JSONArray();
		mJsonArray.put(0, "23");
		mJsonArray.put(1, "Doe, Jane");
		mJsonArray.put(2, "Jane");
		mJsonArray.put(3, "Doe");
		mJsonArray.put(4, "Penny");
		mJsonArray.put(5, "JaneGirl");
		OXContact contact2 = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(mDate)
				.addFromJSONArray(mJsonArray).getOXObject();
		conflictFreeContacts.add(contact2);
		ContactManager.syncToPhone(getContext(), conflictFreeContacts,
				mSyncResult, null);
		assertEquals(2, mSyncResult.stats.numInserts);
		checkForContactAmount(2);
		OXAccountManager accountManager = new OXAccountManagerMockImpl();
		Uri rawContactUri = RawContacts.CONTENT_URI
				.buildUpon()
				.appendQueryParameter(RawContacts.ACCOUNT_NAME,
						accountManager.getOXAccount().name)
				.appendQueryParameter(RawContacts.ACCOUNT_TYPE,
						accountManager.getOXAccount().type).build();
		Cursor cursor = getContext().getContentResolver().query(rawContactUri,
				new String[] { RawContacts._ID }, null, null, RawContacts._ID);
		if(cursor.moveToFirst()) {
			OXContact contactRead = new OXContactRawContactIdBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromRowContactId(
							Long.parseLong(cursor.getString(cursor
									.getColumnIndex(RawContacts._ID))))
					.getOXObject();
			// Force the Version-, Distributionlist- and RawContactIdField to
			// equality, as they are not interesting for this test
			contact1.setVersion(contactRead.getVersion());
			contact1.setRawContactId(contactRead.getRawContactId());
			assertEquals(contact1, contactRead);
		} else {
			fail("There is no first contact!");
		}
		if(cursor.moveToNext()) {
			OXContact contactRead = new OXContactRawContactIdBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromRowContactId(
							Long.parseLong(cursor.getString(cursor
									.getColumnIndex(RawContacts._ID))))
					.getOXObject();
			// Force the Version- and RawContactIdField to equality, as they are
			// not interesting for this test
			contact2.setVersion(contactRead.getVersion());
			contact2.setRawContactId(contactRead.getRawContactId());
			contact2.setDistributionList(contactRead.getDistributionList());
			assertEquals(contact2, contactRead);
		} else {
			fail("There is no second contact!");
		}
	}

	public void testSyncToPhoneWithOneInsertButOneDistributionListContact()
			throws JSONException, NumberFormatException, RemoteException,
			OperationApplicationException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		setupContactInformations(true);
		ArrayList<OXContact> conflictFreeContacts = new ArrayList<OXContact>();
		OXContact contact1 = new OXContactBuilder().addContext(getContext())
				.addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		conflictFreeContacts.add(contact1);
		// DistributionList is already included in mJsonArry!
		mJsonArray.put(0, "23");
		mJsonArray.put(1, "Doe, Jane");
		mJsonArray.put(2, "Jane");
		mJsonArray.put(4, "Penny");
		mJsonArray.put(5, "JaneGirl");
		OXContact contact2 = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(mDate)
				.addFromJSONArray(mJsonArray).getOXObject();
		conflictFreeContacts.add(contact2);
		ContactManager.syncToPhone(getContext(), conflictFreeContacts,
				mSyncResult, null);
		assertEquals(1, mSyncResult.stats.numInserts);
		checkForContactAmount(1);
		OXAccountManager accountManager = new OXAccountManagerMockImpl();
		Uri rawContactUri = RawContacts.CONTENT_URI
				.buildUpon()
				.appendQueryParameter(RawContacts.ACCOUNT_NAME,
						accountManager.getOXAccount().name)
				.appendQueryParameter(RawContacts.ACCOUNT_TYPE,
						accountManager.getOXAccount().type).build();
		Cursor cursor = getContext().getContentResolver().query(rawContactUri,
				new String[] { RawContacts._ID }, null, null, RawContacts._ID);
		if(cursor.moveToFirst()) {
			OXContact contactRead = new OXContactRawContactIdBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromRowContactId(
							Long.parseLong(cursor.getString(cursor
									.getColumnIndex(RawContacts._ID))))
					.getOXObject();
			// Force the Version-, Distributionlist- and RawContactIdField to
			// equality, as they are not interesting for this test
			contact1.setVersion(contactRead.getVersion());
			contact1.setRawContactId(contactRead.getRawContactId());
			assertEquals(contact1, contactRead);
		} else {
			fail("There is no first contact!");
		}
	}

	public void testSyncToPhoneWithOneUpdate() throws JSONException,
			RemoteException, OperationApplicationException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		createAmountOfFullContacts(1);
		setupContactInformations(true);
		ArrayList<OXContact> conflictFreeContacts = new ArrayList<OXContact>();
		mContactInformations.put("mID", "1");
		OXContact contact = new OXContactBuilder().addContext(getContext())
				.addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		conflictFreeContacts.add(contact);
		ContactManager.syncToPhone(getContext(), conflictFreeContacts,
				mSyncResult, null);
		assertEquals(1, mSyncResult.stats.numUpdates);
		for(OXContact contactWrote : mTestContact) {
			OXContact contactRead = new OXContactRawContactIdBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromRowContactId(
							Long.parseLong(contactWrote.getRawContactId()))
					.getOXObject();
			// Force the Versionfield to equality, as it is not interesting for
			// this test
			contactWrote.setVersion(contactRead.getVersion());
			contactWrote.setDistributionList(contactRead.getDistributionList());
			assertEquals(contactWrote, contactRead);
		}
	}

	public void testSyncToPhoneWithOneUpdateCustomData() throws JSONException,
			RemoteException, OperationApplicationException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		ArrayList<OXContact> conflictFreeContacts = new ArrayList<OXContact>();
		LinkedHashMap<String, String> contactInformations = new LinkedHashMap<String, String>();
		contactInformations.put("mID", "3497");
		contactInformations.put("mNameDisplayName", "Doe, John");
		contactInformations.put("mNameGivenName", "John");
		contactInformations.put("mNameFamilyName", "Doe");
		contactInformations.put("mPhoneTypeFaxWork", "33");
		contactInformations.put("mPhoneTypeWorkMobile", "22");
		OXContact contactWrite = new OXContactBuilder()
				.addContext(getContext()).addTimestamp(mDate)
				.addContactInformations(contactInformations).getOXObject();
		ArrayList<ContentProviderOperation> operations = contactWrite
				.insertContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] result = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		contactWrite.setRawContactId(String.valueOf(ContentUris
				.parseId(result[0].uri)));
		// Aenderungen am Kontakt:
		contactInformations.put("mNameDisplayName", "Argel, John");
		contactInformations.put("mNameFamilyName", "Argel");
		contactInformations.put("mEmailTypeWork", "john.doe@sap.com");
		contactInformations.put("mEmailTypeHome", "john.doe@web.de");
		contactInformations.put("mPhoneTypeWork1", "11");
		contactInformations.put("mPhoneTypeWork2", "22");
		contactInformations.put("mPhoneTypeHome2", "44");
		contactInformations.put("mPhoneTypeFaxWork", "44");
		contactInformations.remove("mPhoneTypeWorkMobile");
		JSONArray jsonArray = new JSONArray(
				"[3497,\"John Argel\",\"John\",\"Argel\",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,\"11\",\"22\",\"44\",null,null,null,null,null,null,\"44\",null,null,null,null,null,null,null,null,null,null,\"john.doe@sap.com\",\"john.doe@web.de\",null,null,null,null]");
		OXContact serverContact = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(mDate)
				.addFromJSONArray(jsonArray).getOXObject();
		conflictFreeContacts.add(serverContact);
		ContactManager.syncToPhone(getContext(), conflictFreeContacts,
				mSyncResult, null);
		assertEquals(1, mSyncResult.stats.numUpdates);
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext())
				.addTimestamp(mDate)
				.addFromRowContactId(
						Long.parseLong(contactWrite.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		contactWrite.setVersion(contactRead.getVersion());
		// Force the Displayname to equality, as it is set by the system based
		// on usersettings!
		contactWrite.putContactInformation("mNameDisplayName",
				contactRead.getContactInformation("mNameDisplayName"));
		assertEquals(contactWrite, contactRead);
	}

	public void testSyncToPhoneWithTwoUpdates() throws JSONException,
			RemoteException, OperationApplicationException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		createAmountOfFullContacts(2);
		setupContactInformations(true);
		ArrayList<OXContact> conflictFreeContacts = new ArrayList<OXContact>();
		mContactInformations.put("mID", "1");
		OXContact contact1 = new OXContactBuilder().addContext(getContext())
				.addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		conflictFreeContacts.add(contact1);
		mJsonArray = new JSONArray();
		mJsonArray.put(0, "2");
		OXContact contact2 = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(mDate)
				.addFromJSONArray(mJsonArray).getOXObject();
		conflictFreeContacts.add(contact2);
		ContactManager.syncToPhone(getContext(), conflictFreeContacts,
				mSyncResult, null);
		assertEquals(2, mSyncResult.stats.numUpdates);
		for(OXContact contactWrote : conflictFreeContacts) {
			OXContact contactRead = new OXContactRawContactIdBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromRowContactId(
							Long.parseLong(contactWrote.getRawContactId()))
					.getOXObject();
			// Force the Versionfield to equality, as it is not interesting for
			// this test
			contactWrote.setVersion(contactRead.getVersion());
			contactWrote.setRawContactId(contactRead.getRawContactId());
			contactWrote.setDistributionList(contactRead.getDistributionList());
			assertEquals(contactWrote, contactRead);
		}
	}

	public void testSyncToPhoneWithOneUpdateButOneDistributionListContact()
			throws JSONException, RemoteException,
			OperationApplicationException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		createAmountOfFullContacts(2);
		setupContactInformations(true);
		ArrayList<OXContact> conflictFreeContacts = new ArrayList<OXContact>();
		mContactInformations.put("mID", "1");
		OXContact contact1 = new OXContactBuilder().addContext(getContext())
				.addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		conflictFreeContacts.add(contact1);
		// DistributionList is already included in mJsonArry!
		mJsonArray.put(0, "2");
		OXContact contact2 = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(mDate)
				.addFromJSONArray(mJsonArray).getOXObject();
		conflictFreeContacts.add(contact2);
		ContactManager.syncToPhone(getContext(), conflictFreeContacts,
				mSyncResult, null);
		assertEquals(1, mSyncResult.stats.numUpdates);
		for(OXContact contactWrote : mTestContact) {
			OXContact contactRead = new OXContactRawContactIdBuilder()
					.addContext(getContext())
					.addTimestamp(mDate)
					.addFromRowContactId(
							Long.parseLong(contactWrote.getRawContactId()))
					.getOXObject();
			// Force the Versionfield to equality, as it is not interesting for
			// this test
			contactWrote.setVersion(contactRead.getVersion());
			contactWrote.setDistributionList(contactRead.getDistributionList());
			assertEquals(contactWrote, contactRead);
		}
	}

	/**
	 * syncToServer
	 */
	public void testSyncToServerWithOnePush() throws NumberFormatException,
			JSONException, RemoteException, OperationApplicationException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		// Create a OXContact-instance without OXId
		setupContactInformations(true);
		mContactInformations.remove("mID");
		OXContact contact = new OXContactBuilder()
				.addContext(new RenamingMockContext(getContext()))
				.addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		assertNotNull(contact);
		// Add it to the ContactsContract-ContentProvider
		ArrayList<ContentProviderOperation> operations = contact
				.insertContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] result = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		// Get the rawContactId of the newly created contact
		String rawContactId = String
				.valueOf(ContentUris.parseId(result[0].uri));
		contact.setRawContactId(rawContactId);
		mRawContactIds.add(rawContactId);
		// Define a OXId the new contact will get
		String oxId = "3456";
		mOXActionCreateContactMockImpl.setResult(oxId);
		contact.putContactInformation("mID", oxId);
		// Call ContactManager
		ContactManager.syncToServer(getContext(), mRawContactIds, mDate,
				mSyncResult);
		assertEquals(1, mSyncResult.stats.numInserts);
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext()).addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(contact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		contact.setVersion(contactRead.getVersion());
		assertEquals(contact, contactRead);
	}

	public void testSyncToServerWithOneUpdate() throws NumberFormatException,
			JSONException, RemoteException, OperationApplicationException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		setupContactInformations(true);
		OXContact contact = new OXContactBuilder().addContext(getContext())
				.addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		assertNotNull(contact);
		// Add it to the ContactsContract-ContentProvider
		ArrayList<ContentProviderOperation> operations = contact
				.insertContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] result = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		// Get the rawContactId of the newly created contact
		String rawContactId = String
				.valueOf(ContentUris.parseId(result[0].uri));
		contact.setRawContactId(rawContactId);
		mRawContactIds.add(rawContactId);
		// Change Contact
		contact.putContactInformation("mPostalTypeHomeStreet", "JohnyStreet");
		contact.putContactInformation("mPostalTypeHomePostalCode",
				"JohnyPostalCode");
		contact.putContactInformation("mPhoneTypeWork2", "54");
		contact.putContactInformation("mPhoneTypeHome1", "65");
		// Update Contact
		operations = contact.updateContact();
		result = resolver.applyBatch(ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		// Call ContactManager
		ContactManager.syncToServer(getContext(), mRawContactIds, mDate,
				mSyncResult);
		assertEquals(1, mSyncResult.stats.numUpdates);
		OXContact contactRead = new OXContactRawContactIdBuilder()
				.addContext(getContext()).addTimestamp(mDate)
				.addFromRowContactId(Long.parseLong(contact.getRawContactId()))
				.getOXObject();
		// Force the Versionfield to equality, as it is not interesting for this
		// test
		contact.setVersion(contactRead.getVersion());
		assertEquals(contact, contactRead);
	}

	public void testSyncToServerWithOneDelete() throws NumberFormatException,
			JSONException, RemoteException, OperationApplicationException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		setupContactInformations(true);
		OXContact contact = new OXContactBuilder()
				.addContext(new RenamingMockContext(getContext()))
				.addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		assertNotNull(contact);
		ArrayList<ContentProviderOperation> operations = contact
				.insertContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] result = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		// Get the rawContactId of the newly created contact
		String rawContactId = String
				.valueOf(ContentUris.parseId(result[0].uri));
		contact.setRawContactId(rawContactId);
		Builder resetDirtyFlag = ContentProviderOperation
				.newUpdate(
						RawContacts.CONTENT_URI
								.buildUpon()
								.appendQueryParameter(
										ContactsContract.CALLER_IS_SYNCADAPTER,
										"true").build())
				.withSelection(RawContacts._ID + "=?",
						new String[] { contact.getRawContactId() })
				.withValue(RawContacts.DELETED, "1");
		operations.clear();
		operations.add(resetDirtyFlag.build());
		result = resolver.applyBatch(ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		// Call ContactManager
		ContactManager.syncToServer(getContext(), new ArrayList<String>(),
				mDate, mSyncResult);
		assertEquals(1, mSyncResult.stats.numDeletes);
		checkForContactAmount(0);
	}

	public void testSyncToServerWithNewButMultipleEditedContact()
			throws NumberFormatException, JSONException, RemoteException,
			OperationApplicationException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		ArrayList<String> rawContacts = new ArrayList<String>();
		setupContactInformations(true);
		// New Contacts don't have an ID!
		mContactInformations.remove("mID");
		OXContact contact = new OXContactBuilder()
				.addContext(new RenamingMockContext(getContext()))
				.addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		assertNotNull(contact);
		ArrayList<ContentProviderOperation> operations = contact
				.insertContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] result = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		// Get the rawContactId of the newly created contact
		String rawContactId = String
				.valueOf(ContentUris.parseId(result[0].uri));
		contact.setRawContactId(rawContactId);
		rawContacts.add(rawContactId);
		// Simulate the edit by setting dirty and increment version
		Builder resetDirtyFlag = ContentProviderOperation
				.newUpdate(
						RawContacts.CONTENT_URI
								.buildUpon()
								.appendQueryParameter(
										ContactsContract.CALLER_IS_SYNCADAPTER,
										"true").build())
				.withSelection(RawContacts._ID + "=?",
						new String[] { contact.getRawContactId() })
				.withValue(RawContacts.DIRTY, "1")
				.withValue(RawContacts.VERSION, "4");
		operations.clear();
		operations.add(resetDirtyFlag.build());
		result = resolver.applyBatch(ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		mOXActionCreateContactMockImpl.setResult("54");
		// Call ContactManager
		ContactManager.syncToServer(getContext(), rawContacts, mDate,
				mSyncResult);
		assertEquals(1, mSyncResult.stats.numInserts);
		checkForContactAmount(1);
		checkForDirtyFlagSet(false, contact.getRawContactId());
		checkForSourceIdSet(true, contact.getRawContactId());
	}

	/**
	 * changedContacts
	 */
	public void testChangedContacts() throws JSONException, RemoteException,
			OperationApplicationException {
		// Setup minimal contacts
		mContactInformations = new LinkedHashMap<String, String>();
		mContactInformations.put("mID", "1");
		mContactInformations.put("mNameGivenName", "John");
		OXContact contact1 = new OXContactBuilder().addContext(getContext())
				.addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		ArrayList<ContentProviderOperation> operations = contact1
				.insertContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] result = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		contact1.setRawContactId(String.valueOf(ContentUris
				.parseId(result[0].uri)));
		mJsonArray = new JSONArray();
		mJsonArray.put("42");
		mJsonArray.put("Musterfrau, Erika");
		mJsonArray.put("Erika");
		mJsonArray.put("Musterfrau");
		OXContact contact2 = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(mDate)
				.addFromJSONArray(mJsonArray).getOXObject();
		operations = contact2.insertContact();
		result = resolver.applyBatch(ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		contact2.setRawContactId(String.valueOf(ContentUris
				.parseId(result[0].uri)));
		operations.clear();
		Builder resetDirtyFlag = ContentProviderOperation
				.newUpdate(
						RawContacts.CONTENT_URI
								.buildUpon()
								.appendQueryParameter(
										ContactsContract.CALLER_IS_SYNCADAPTER,
										"true").build())
				.withSelection(RawContacts._ID + "=?",
						new String[] { contact2.getRawContactId() })
				.withValue(RawContacts.DIRTY, "1");
		operations.add(resetDirtyFlag.build());
		result = resolver.applyBatch(ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		ArrayList<String> contacts = ContactManager
				.changedContacts(getContext());
		assertEquals(1, contacts.size());
		assertEquals(contact2.getRawContactId(), contacts.get(0));
	}

	public void testChangedContactsWithNewContact() throws JSONException,
			RemoteException, OperationApplicationException {
		OXApplication.setUtils(new OXUtilsImpl(new RenamingMockContext(
				getContext())));
		setupContactInformations(true);
		// New Contacts don't have any ID
		mContactInformations.remove("mID");
		OXContact contact1 = new OXContactBuilder().addContext(getContext())
				.addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		ArrayList<ContentProviderOperation> operations = contact1
				.insertContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] result = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		contact1.setRawContactId(String.valueOf(ContentUris
				.parseId(result[0].uri)));
		// Simulate the edit by setting dirty and increment version
		Builder simulateEditContact = ContentProviderOperation
				.newUpdate(
						RawContacts.CONTENT_URI
								.buildUpon()
								.appendQueryParameter(
										ContactsContract.CALLER_IS_SYNCADAPTER,
										"true").build())
				.withSelection(RawContacts._ID + "=?",
						new String[] { contact1.getRawContactId() })
				.withValue(RawContacts.DIRTY, "1")
				.withValue(RawContacts.VERSION, "4");
		operations.clear();
		operations.add(simulateEditContact.build());
		result = resolver.applyBatch(ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		OXContact contact2 = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(mDate)
				.addFromJSONArray(mJsonArray).getOXObject();
		operations = contact2.insertContact();
		result = resolver.applyBatch(ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		contact2.setRawContactId(String.valueOf(ContentUris
				.parseId(result[0].uri)));
		Builder resetDirtyFlag = ContentProviderOperation
				.newUpdate(
						RawContacts.CONTENT_URI
								.buildUpon()
								.appendQueryParameter(
										ContactsContract.CALLER_IS_SYNCADAPTER,
										"true").build())
				.withSelection(RawContacts._ID + "=?",
						new String[] { contact2.getRawContactId() })
				.withValue(RawContacts.DIRTY, "1");
		operations.clear();
		operations.add(resetDirtyFlag.build());
		result = resolver.applyBatch(ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		ArrayList<String> contacts = ContactManager
				.changedContacts(getContext());
		assertEquals(2, contacts.size());
	}

	/**
	 * deletedContacts
	 */
	public void testDeletedContacts() throws JSONException, RemoteException,
			OperationApplicationException {
		// Setup minimal contacts
		mContactInformations = new LinkedHashMap<String, String>();
		mContactInformations.put("mID", "1");
		mContactInformations.put("mNameGivenName", "John");
		OXContact contact1 = new OXContactBuilder().addContext(getContext())
				.addTimestamp(mDate)
				.addContactInformations(mContactInformations).getOXObject();
		ArrayList<ContentProviderOperation> operations = contact1
				.insertContact();
		ContentResolver resolver = getContext().getContentResolver();
		ContentProviderResult[] result = resolver.applyBatch(
				ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		contact1.setRawContactId(String.valueOf(ContentUris
				.parseId(result[0].uri)));
		mJsonArray = new JSONArray();
		mJsonArray.put("42");
		mJsonArray.put("Musterfrau, Erika");
		mJsonArray.put("Erika");
		mJsonArray.put("Musterfrau");
		OXContact contact2 = new OXContactJSONArrayBuilder()
				.addContext(getContext()).addTimestamp(mDate)
				.addFromJSONArray(mJsonArray).getOXObject();
		operations = contact2.insertContact();
		result = resolver.applyBatch(ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		assertEquals(operations.size(), result.length);
		contact2.setRawContactId(String.valueOf(ContentUris
				.parseId(result[0].uri)));
		operations.clear();
		Builder resetDirtyFlag = ContentProviderOperation
				.newUpdate(
						RawContacts.CONTENT_URI
								.buildUpon()
								.appendQueryParameter(
										ContactsContract.CALLER_IS_SYNCADAPTER,
										"true").build())
				.withSelection(RawContacts._ID + "=?",
						new String[] { contact2.getRawContactId() })
				.withValue(RawContacts.DELETED, "1");
		operations.add(resetDirtyFlag.build());
		result = resolver.applyBatch(ContactsContract.AUTHORITY, operations);
		assertEquals(operations.size(), result.length);
		ArrayList<OXContact> contacts = ContactManager.deletedContacts(
				getContext(), mDate);
		assertEquals(1, contacts.size());
		OXContact contactRead = contacts.get(0);
		contact2.setVersion(contactRead.getVersion());
		assertEquals(contact2, contactRead);
	}

	/**
	 * checkForConflicts
	 */
	public void testCheckForConflicts() {
		setupCheckForConflicts();
		ContactManager.checkForConflicts(new RenamingMockContext(getContext()),
				mConflictContact, mOperations, mRawContactIds, mSyncResult);
		assertEquals(0, mSyncResult.stats.numConflictDetectedExceptions);
		assertEquals(0, mOperations.size());
	}

	public void testCheckForConflictsWithNoConflicts() {
		setupCheckForConflicts();
		mRawContactIds.add("5");
		mRawContactIds.add("6");
		ContactManager.checkForConflicts(new RenamingMockContext(getContext()),
				mConflictContact, mOperations, mRawContactIds, mSyncResult);
		assertEquals(0, mSyncResult.stats.numConflictDetectedExceptions);
		assertEquals(0, mOperations.size());
	}

	public void testCheckForConflictsWithOneConflicts() {
		setupCheckForConflicts();
		mRawContactIds.add("1");
		mRawContactIds.add("5");
		mControlConflictFree.add(mConflictContact.get(1));
		mControlConflictFree.add(mConflictContact.get(2));
		mControlConflictFree.add(mConflictContact.get(3));
		ContactManager.checkForConflicts(getContext(), mConflictContact,
				mOperations, mRawContactIds, mSyncResult);
		assertEquals(mControlConflictFree.size(), mConflictContact.size());
		assertEquals(mControlConflictFree, mConflictContact);
		assertEquals(1, mSyncResult.stats.numConflictDetectedExceptions);
		assertEquals(2, mOperations.size());
	}

	public void testCheckForConflictsWithTwoflicts() {
		setupCheckForConflicts();
		mRawContactIds.add("1");
		mRawContactIds.add("2");
		mRawContactIds.add("5");
		mControlConflictFree.add(mConflictContact.get(2));
		mControlConflictFree.add(mConflictContact.get(3));
		ContactManager.checkForConflicts(getContext(), mConflictContact,
				mOperations, mRawContactIds, mSyncResult);
		assertEquals(mControlConflictFree.size(), mConflictContact.size());
		assertEquals(mControlConflictFree, mConflictContact);
		assertEquals(2, mSyncResult.stats.numConflictDetectedExceptions);
		assertEquals(4, mOperations.size());
	}

	public void testCheckForConflictsWithOnlyConflicts() {
		setupCheckForConflicts();
		mRawContactIds.add("1");
		mRawContactIds.add("2");
		mRawContactIds.add("3");
		mRawContactIds.add("4");
		mRawContactIds.add("8");
		ContactManager.checkForConflicts(getContext(), mConflictContact,
				mOperations, mRawContactIds, mSyncResult);
		assertEquals(mControlConflictFree.size(), mConflictContact.size());
		assertEquals(mControlConflictFree, mConflictContact);
		assertEquals(4, mSyncResult.stats.numConflictDetectedExceptions);
		assertEquals(8, mOperations.size());
	}

	private void createAmountOfFullContacts(int pAmount) throws JSONException,
			RemoteException, OperationApplicationException {
		mTestContact = new ArrayList<OXContact>();
		int origAmount = pAmount;
		while (pAmount > 0) {
			setupContactInformations(true);
			mJsonArray.put(0, String.valueOf(pAmount));
			OXContact contact = new OXContactJSONArrayBuilder()
					.addContext(new RenamingMockContext(getContext()))
					.addTimestamp(mDate).addFromJSONArray(mJsonArray)
					.getOXObject();
			assertNotNull(contact);
			ArrayList<ContentProviderOperation> operations = contact
					.insertContact();
			ContentResolver resolver = getContext().getContentResolver();
			ContentProviderResult[] result = resolver.applyBatch(
					ContactsContract.AUTHORITY, operations);
			assertEquals(operations.size(), result.length);
			contact.setRawContactId(String.valueOf(ContentUris
					.parseId(result[0].uri)));
			mTestContact.add(contact);
			pAmount--;
		}
		checkForContactAmount(origAmount);
	}

	private void setupCheckForConflicts() {
		mConflictContact = new ArrayList<OXContact>();
		mOperations = new ArrayList<ContentProviderOperation>();
		LinkedHashMap<String, String> contactInformations1 = new LinkedHashMap<String, String>();
		mControlConflictFree = new ArrayList<OXContact>();
		contactInformations1.put("mID", "42");
		contactInformations1.put("mNameDisplayName", "Doe, John");
		OXContact serverContact1 = new OXContactBuilder()
				.addContext(new RenamingMockContext(getContext()))
				.addContactInformations(contactInformations1)
				.addTimestamp(new Date()).getOXObject();
		serverContact1.setRawContactId("1");
		mConflictContact.add(serverContact1);
		LinkedHashMap<String, String> contactInformations2 = new LinkedHashMap<String, String>();
		contactInformations2.put("mID", "1337");
		contactInformations2.put("mNameDisplayName", "Doe, Jane");
		OXContact serverContact2 = new OXContactBuilder()
				.addContext(new RenamingMockContext(getContext()))
				.addContactInformations(contactInformations2)
				.addTimestamp(new Date()).getOXObject();
		serverContact2.setRawContactId("2");
		mConflictContact.add(serverContact2);
		LinkedHashMap<String, String> contactInformations3 = new LinkedHashMap<String, String>();
		contactInformations3.put("mID", "54");
		contactInformations3.put("mNameDisplayName", "Mustermann, Max");
		OXContact serverContact3 = new OXContactBuilder()
				.addContext(new RenamingMockContext(getContext()))
				.addContactInformations(contactInformations3)
				.addTimestamp(new Date()).getOXObject();
		serverContact3.setRawContactId("3");
		mConflictContact.add(serverContact3);
		LinkedHashMap<String, String> contactInformations4 = new LinkedHashMap<String, String>();
		contactInformations4.put("mID", "23");
		contactInformations4.put("mNameDisplayName", "Mustermann, Erika");
		OXContact serverContact4 = new OXContactBuilder()
				.addContext(new RenamingMockContext(getContext()))
				.addContactInformations(contactInformations4)
				.addTimestamp(new Date()).getOXObject();
		serverContact4.setRawContactId("4");
		mConflictContact.add(serverContact4);
	}
}
