/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.authenticator;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.OXConstants;
import com.alien.oxclient.R;
import com.alien.oxclient.action.OXAction;
import com.alien.oxclient.activities.AccountFail;
import com.alien.oxclient.activities.AuthenticatorActivity;
import com.alien.oxclient.mockinterfaces.OXAccountManager;

/**
 * This class is an implementation of AbstractAccountAuthenticator for
 * authenticating accounts in the com.alien.oxclient domain.
 */
public class Authenticator extends AbstractAccountAuthenticator {
	private static final String TAG = "OXClient";

	private final Context mContext;
	private OXAccountManager mAccountManager;

	public Authenticator(Context pContext) {
		super(pContext);
		this.mContext = pContext;
	}

	@Override
	public Bundle addAccount(AccountAuthenticatorResponse pResponse,
			String pAccountType, String pAuthTokenType,
			String[] pRequiredFeatures, Bundle pOptions) {
		Log.d(TAG, "[=>][Authenticator->addAccount] called");
		if(OXApplication.getUtils().hasOXAccount()) {
			Log.d(TAG,
					"    [*][Authenticator->addAccount] System already has OXAccount");
			Bundle bundle = new Bundle();
			Intent i = new Intent(mContext, AccountFail.class);
			i.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE,
					pResponse);
			bundle.putParcelable(AccountManager.KEY_INTENT, i);
			Log.d(TAG, "[<=][Authenticator->addAccount] finished");
			return bundle;
		} else {
			Log.d(TAG,
					"    [*][Authenticator->addAccount] OXAccount needs to be added");
			final Intent intent = new Intent(mContext,
					AuthenticatorActivity.class);
			intent.putExtra(OXConstants.PARAM_AUTHTOKEN_TYPE, pAuthTokenType);
			intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE,
					pResponse);
			final Bundle bundle = new Bundle();
			bundle.putParcelable(AccountManager.KEY_INTENT, intent);
			Log.d(TAG, "[<=][Authenticator->addAccount] finished");
			return bundle;
		}
	}

	@Override
	public Bundle confirmCredentials(AccountAuthenticatorResponse pResponse,
			Account pAccount, Bundle pOptions) {
		Log.d(TAG, "[=>][Authenticator->confirmCredentials] called");
		mAccountManager = OXApplication.getAccountManager();
		if(pOptions != null
				&& pOptions.containsKey(AccountManager.KEY_PASSWORD)) {
			final String password = pOptions
					.getString(AccountManager.KEY_PASSWORD);
			final OXAction<String> verified = onlineConfirmPassword(
					pAccount.name, password, mAccountManager.getAccountServer());
			final Bundle result = new Bundle();
			result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT,
					verified.hasResult());
			Log.d(TAG, "[<=][Authenticator->confirmCredentials] finished");
			return result;
		}
		// Launch AuthenticatorActivity to confirm credentials
		final Intent intent = new Intent(mContext, AuthenticatorActivity.class);
		intent.putExtra(OXConstants.PARAM_USERNAME, pAccount.name);
		intent.putExtra(OXConstants.PARAM_SERVER,
				mAccountManager.getAccountServer());
		intent.putExtra(OXConstants.PARAM_CONFIRMCREDENTIALS, true);
		intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE,
				pResponse);
		final Bundle bundle = new Bundle();
		bundle.putParcelable(AccountManager.KEY_INTENT, intent);
		Log.d(TAG, "[<=][Authenticator->confirmCredentials] finished");
		return bundle;
	}

	@Override
	public Bundle editProperties(AccountAuthenticatorResponse pResponse,
			String pAccountType) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Bundle getAuthToken(AccountAuthenticatorResponse pResponse,
			Account pAccount, String pAuthTokenType, Bundle pLoginOptions) {
		Log.d(TAG, "[=>][Authenticator->getAuthToken] called");
		// If the caller requested an authToken type we don't support, then
		// return an error
		if(!pAuthTokenType.equals(mContext.getString(R.string.AUTHTOKEN_TYPE))) {
			final Bundle result = new Bundle();
			result.putString(AccountManager.KEY_ERROR_MESSAGE,
					"invalid authTokenType");
			Log.d(TAG,
					"[<=][Authenticator->getAuthToken] finished with error: 'invalid authTokenType'");
			return result;
		}

		// Extract the username and password from the Account Manager, and ask
		// the server for an appropriate AuthToken.
		mAccountManager = OXApplication.getAccountManager();
		final String password = mAccountManager.getPassword(pAccount);
		String errormessage = null;
		if(password != null) {
			final OXAction<String> verified = onlineConfirmPassword(
					pAccount.name, password, mAccountManager.getUserData(
							pAccount, OXConstants.PARAM_SERVER));
			if(verified.hasResult()) {
				Log.d(TAG,
						"    [*][Authenticator->getAuthToken] Password verified");
				final Bundle result = new Bundle();
				result.putString(AccountManager.KEY_ACCOUNT_NAME, pAccount.name);
				result.putString(AccountManager.KEY_ACCOUNT_TYPE,
						mContext.getString(R.string.ACCOUNT_TYPE));
				result.putString(AccountManager.KEY_AUTHTOKEN,
						verified.getResult());
				Log.d(TAG, "[<=][Authenticator->getAuthToken] finished");
				return result;
			} else {
				errormessage = verified.getErrorMessage();
			}
		}
		Log.d(TAG, "    [*][Authenticator->getAuthToken] Password not verified");

		// If we get here, then we couldn't access the user's password - so we
		// need to re-prompt them for their credentials. We do that by creating
		// an intent to display our AuthenticatorActivity panel.
		final Intent intent = new Intent(mContext, AuthenticatorActivity.class);
		intent.putExtra(OXConstants.PARAM_USERNAME, pAccount.name);
		intent.putExtra(OXConstants.PARAM_AUTHTOKEN_TYPE, pAuthTokenType);
		intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE,
				pResponse);
		final Bundle bundle = new Bundle();
		bundle.putParcelable(AccountManager.KEY_INTENT, intent);
		if(errormessage != null) bundle.putString(OXConstants.OXERROR,
				errormessage);
		else bundle.putString(OXConstants.OXERROR,
				mContext.getString(R.string.error_without_errormessage));
		Log.d(TAG, "[<=][Authenticator->getAuthToken] finished");
		return bundle;
	}

	@Override
	public String getAuthTokenLabel(String pAuthTokenType) {
		Log.d(TAG, "[=>][Authenticator->getAuthTokenLabel] called");
		if(pAuthTokenType.equals(mContext.getString(R.string.AUTHTOKEN_TYPE))) {
			Log.d("OXClient",
					"[<=][Authenticator->getAuthTokenLabel] finished / return: "
							+ mContext.getString(R.string.label));
			return mContext.getString(R.string.label);
		}
		Log.d(TAG,
				"[<=][Authenticator->getAuthTokenLabel] finished / return: null");
		return null;
	}

	@Override
	public Bundle hasFeatures(AccountAuthenticatorResponse pResponse,
			Account pAccount, String[] pFeatures) {
		Log.d(TAG, "[=>][Authenticator->hasFeatures] called");
		final Bundle result = new Bundle();
		result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false);
		return result;
	}

	/**
	 * Validates user's password on the server
	 */
	private OXAction<String> onlineConfirmPassword(String pUsername,
			String pPassword, String pServer) {
		Log.d(TAG, "[=>][Authenticator->onlineConfirmPassword] called");
		final OXAction<String> loginAction = OXApplication
				.getActionDependencyManager().getOXActionLogin(pServer,
						pUsername, pPassword);
		loginAction.perform();
		return loginAction;
	}

	@Override
	public Bundle updateCredentials(AccountAuthenticatorResponse pResponse,
			Account pAccount, String pAuthTokenType, Bundle pLoginOptions) {
		Log.d(TAG, "[=>][Authenticator->updateCredentials] called");
		final Intent intent = new Intent(mContext, AuthenticatorActivity.class);
		intent.putExtra(OXConstants.PARAM_USERNAME, pAccount.name);
		intent.putExtra(OXConstants.PARAM_SERVER,
				mAccountManager.getAccountServer());
		intent.putExtra(OXConstants.PARAM_AUTHTOKEN_TYPE, pAuthTokenType);
		intent.putExtra(OXConstants.PARAM_CONFIRMCREDENTIALS, false);
		final Bundle bundle = new Bundle();
		bundle.putParcelable(AccountManager.KEY_INTENT, intent);
		Log.d(TAG, "[<=][Authenticator->updateCredentials] finished");
		return bundle;
	}
}
