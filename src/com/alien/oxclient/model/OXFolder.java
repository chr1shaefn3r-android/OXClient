/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.model;

import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

import com.alien.oxclient.action.OXFolderModules;

public class OXFolder extends OXObject {

	private String mId;
	private String mTitle;
	private OXFolderModules mModule;

	public OXFolder(String pId, String pTitle, OXFolderModules pFolderModule) {
		this.mId = pId;
		this.mTitle = pTitle;
		this.mModule = pFolderModule;
	}

	@Override
	public boolean hasId() {
		return !TextUtils.isEmpty(mId);
	}

	@Override
	public String getId() {
		return mId;
	}

	public String getTitle() {
		return this.mTitle;
	}

	public OXFolderModules getModule() {
		return mModule;
	}

	@Override
	public JSONObject toJSONObject() throws JSONException {
		JSONObject jsonFolder = new JSONObject();
		jsonFolder.put("id", mId);
		jsonFolder.put("title", mTitle);
		jsonFolder.put("module", mModule.getName());
		return jsonFolder;
	}

	@Override
	public String toString() {
		return "OXFolder [mId=" + mId + ", mTitle=" + mTitle + ", mModule="
				+ mModule + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) { return true; }
		if(obj == null) { return false; }
		if(getClass() != obj.getClass()) { return false; }
		OXFolder other = (OXFolder) obj;
		if(mId == null) {
			if(other.mId != null) { return false; }
		} else if(!mId.equals(other.mId)) { return false; }
		if(mModule != other.mModule) { return false; }
		if(mTitle == null) {
			if(other.mTitle != null) { return false; }
		} else if(!mTitle.equals(other.mTitle)) { return false; }
		return true;
	}
}
