/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.model;

import java.util.Date;
import java.util.LinkedHashMap;

import android.content.Context;
import android.graphics.Bitmap;

public class OXContactBuilder extends OXObjectBuilder {

	protected Context mContext;
	protected Date mTimestamp;
	protected LinkedHashMap<String, String> mContactInformations = new LinkedHashMap<String, String>();
	protected String mDistributionList;
	protected String mRawContactId;
	protected Bitmap mPicture;
	protected String mVersion;

	@Override
	public OXContactBuilder addContext(Context pContext) {
		mContext = pContext;
		return this;
	}

	@Override
	public OXContactBuilder addTimestamp(Date pTimestamp) {
		mTimestamp = pTimestamp;
		return this;
	}

	public OXContactBuilder addContactInformations(
			LinkedHashMap<String, String> pContactInformations) {
		mContactInformations = pContactInformations;
		return this;
	}

	public OXContactBuilder addRawContactId(long pRawContactId) {
		mRawContactId = String.valueOf(pRawContactId);
		return this;
	}

	public OXContactBuilder addPicture(Bitmap pPicture) {
		mPicture = pPicture;
		return this;
	}

	public OXContactBuilder addVersion(String pVersion) {
		mVersion = pVersion;
		return this;
	}

	@Override
	public OXContact getOXObject() {
		OXContact mContact = new OXContact();
		mContact.setContext(mContext);
		mContact.setRawContactId(mRawContactId);
		mContact.setContactInformations(mContactInformations);
		mContact.setTimestamp(mTimestamp);
		mContact.setPicture(mPicture);
		mContact.setDistributionList(mDistributionList);
		mContact.setVersion(mVersion);
		return mContact;
	}
}
