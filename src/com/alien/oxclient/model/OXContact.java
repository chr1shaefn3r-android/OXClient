/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.model;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.RawContactsEntity;
import android.text.TextUtils;
import android.util.Log;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.OXConstants;
import com.alien.oxclient.OXTimeManager;
import com.alien.oxclient.R;
import com.alien.oxclient.provider.OXMapping;
import com.alien.oxclient.provider.OXMappingDatabaseHelper;

public class OXContact extends OXObject {
	private static final String TAG = "OXClient-SyncAlg";

	// Events (Geburtstag, Jahrestag) werden im Androidformat ("yyyy-MM-dd")
	// gespeichert.
	private LinkedHashMap<String, String> mContactInformations = new LinkedHashMap<String, String>();
	private Bitmap mPicture = null;
	private Context mContext;
	private Date mTimestamp;
	private String mVersion;
	private String mRawContactId;
	private String mDistributionList;
	private Uri mContentUri = Data.CONTENT_URI
			.buildUpon()
			.appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER,
					"true").build();
	private Uri mRawContactUri = RawContacts.CONTENT_URI
			.buildUpon()
			.appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER,
					"true").build();

	public ArrayList<ContentProviderOperation> insertContact()
			throws JSONException {
		return insertContact(true);
	}

	public ArrayList<ContentProviderOperation> insertContact(
			boolean pSyncAdapter) throws JSONException {
		OXMappingDatabaseHelper oxMappingDBHelper = new OXMappingDatabaseHelper(
				mContext);
		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
		int rawContactId = ops.size();
		List<String> alreadyAddedKeys = new ArrayList<String>();
		String oldColumn = "";
		String newColumn = "";
		boolean flag = true;
		JSONObject sync1 = new JSONObject();
		Builder rawContact = ContentProviderOperation
				.newInsert(
						RawContacts.CONTENT_URI
								.buildUpon()
								.appendQueryParameter(
										ContactsContract.CALLER_IS_SYNCADAPTER,
										String.valueOf(pSyncAdapter)).build())
				.withValue(RawContacts.ACCOUNT_TYPE,
						OXApplication.getAccountManager().getOXAccount().type)
				.withValue(RawContacts.ACCOUNT_NAME,
						OXApplication.getAccountManager().getOXAccount().name)
				.withYieldAllowed(true);
		if(mContactInformations.containsKey("mID")) rawContact.withValue(
				RawContacts.SOURCE_ID, mContactInformations.get("mID"));
		if(!pSyncAdapter) rawContact.withValue(RawContacts.DIRTY, "1");
		ops.add(rawContact.build());
		Iterator<String> iterator = mContactInformations.keySet().iterator();
		while (iterator.hasNext()) {
			String contactInformationsKey = iterator.next();
			// Nur weitermachen, wenn der key noch nicht eingefuegt wurde! Und
			// es keine Ausnahme ist!
			if(contactInformationsKey.compareTo("mID") == 0
					&& !TextUtils.isEmpty(contactInformationsKey)) {
				JSONObject object = new JSONObject();
				object.put("mID", mContactInformations.get("mID"));
				Builder mID = ContentProviderOperation
						.newInsert(mContentUri)
						.withValueBackReference(Data.RAW_CONTACT_ID,
								rawContactId)
						.withValue(Data.MIMETYPE, OXConstants.MIMETYPE_OXID)
						.withValue(Data.DATA1, mContactInformations.get("mID"))
						.withValue(Data.DATA2,
								mContext.getString(R.string.ox_profile_text))
						.withValue(
								Data.DATA3,
								mContext.getString(R.string.ox_profile_edit_contact))
						.withValue(Data.SYNC1, object.toString());
				ops.add(mID.build());
			} else if(!alreadyAddedKeys.contains(contactInformationsKey)) {
				String[] projection = new String[] {
						OXMapping.ANDROID_MIMETYPE, OXMapping.ANDROID_TYPE };
				Cursor c = oxMappingDBHelper.query(projection,
						OXMapping.OXMAPPING_KEY + " = ?",
						new String[] { contactInformationsKey }, null, true);
				String androidMimeType = "";
				String androidType = "";
				try {
					c.moveToFirst();
					androidMimeType = c.getString(c
							.getColumnIndex(OXMapping.ANDROID_MIMETYPE));
					androidType = String.valueOf(c.getInt(c
							.getColumnIndex(OXMapping.ANDROID_TYPE)));
				} finally {
					c.close();
				}
				projection = new String[] { OXMapping.ANDROID_COLUMN,
						OXMapping.ANDROID_MIMETYPE, OXMapping.ANDROID_TYPE,
						OXMapping.OXMAPPING_KEY };
				Cursor cursor = oxMappingDBHelper.query(null,
						OXMapping.ANDROID_MIMETYPE + " = ? AND "
								+ OXMapping.ANDROID_TYPE + " = ?",
						new String[] { androidMimeType, androidType }, null,
						true);
				try {
					Builder attribute = ContentProviderOperation
							.newInsert(mContentUri);
					attribute.withValueBackReference(Data.RAW_CONTACT_ID,
							rawContactId);
					oldColumn = "-1";
					sync1 = new JSONObject();
					while (cursor.moveToNext()) {
						newColumn = cursor.getString(cursor
								.getColumnIndex(OXMapping.ANDROID_COLUMN));
						String key = cursor.getString(cursor
								.getColumnIndex(OXMapping.OXMAPPING_KEY));
						String value = mContactInformations.get(key);
						if(newColumn.compareTo(oldColumn) == 0) {
							attribute.withValue(Data.SYNC1, sync1.toString());
							ContentProviderOperation operation = attribute
									.build();
							ops.add(operation);
							if(TextUtils.isEmpty(value)) {
								flag = false;
								continue;
							}
							sync1 = new JSONObject();
							attribute = ContentProviderOperation
									.newInsert(mContentUri);
							attribute.withValueBackReference(
									Data.RAW_CONTACT_ID, rawContactId);
						}
						alreadyAddedKeys.add(key);
						String mappingValue = value != null ? value : "";
						sync1.put(key, mappingValue);
						String mimeType = cursor.getString(cursor
								.getColumnIndex(OXMapping.ANDROID_MIMETYPE));
						attribute.withValue(Data.MIMETYPE, mimeType);
						if(!TextUtils.isEmpty(value)) {
							attribute.withValue(newColumn, value);
							if(!mimeType
									.equalsIgnoreCase(StructuredName.CONTENT_ITEM_TYPE)) {
								// Die Spalte in die der Typ eingetragen wird
								// heisst IMMER "data2"
								int type = cursor
										.getInt(cursor
												.getColumnIndex(OXMapping.ANDROID_TYPE));
								attribute.withValue(Data.DATA2, type);
							}
							flag = true;
						}
						oldColumn = newColumn;
					}
					if(flag) {
						attribute.withValue(Data.SYNC1, sync1.toString());
						ContentProviderOperation operation = attribute.build();
						ops.add(operation);
					}
				} finally {
					cursor.close();
					oxMappingDBHelper.close();
				}
			}
		}
		// Add the picture if there is one
		if(this.mPicture != null) {
			ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
			this.mPicture.compress(Bitmap.CompressFormat.PNG, 100,
					arrayOutputStream);
			byte[] bitmapAsByteArray = arrayOutputStream.toByteArray();
			Builder insertPicture = ContentProviderOperation
					.newInsert(mContentUri);
			insertPicture
					.withValueBackReference(Data.RAW_CONTACT_ID, rawContactId)
					.withValue(
							ContactsContract.Data.MIMETYPE,
							ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
					.withValue(ContactsContract.CommonDataKinds.Photo.PHOTO,
							bitmapAsByteArray);
			ops.add(insertPicture.build());
		}
		return ops;
	}

	public JSONObject toJSONObject() throws JSONException {
		OXTimeManager oxTimeManager = new OXTimeManager();
		OXMappingDatabaseHelper oxMappingDBHelper = new OXMappingDatabaseHelper(
				mContext);
		JSONObject oxContact = new JSONObject();
		String[] projection = new String[] { OXMapping.OXMAPPING_KEY,
				OXMapping.OX_NAME };
		Cursor cursor = oxMappingDBHelper.query(projection, null, null,
				OXMapping.OX_POSITION);
		try {
			while (cursor.moveToNext()) {
				String key = cursor.getString(cursor
						.getColumnIndex(OXMapping.OXMAPPING_KEY));
				String value;
				// Convertierung der Zeit von Android- nach openXChangeformat
				if(key.compareTo("mEventTypeBirthday") == 0) {
					value = oxTimeManager
							.convertAndroidTime2OX(this.mContactInformations
									.get("mEventTypeBirthday"));
				} else if(key.compareTo("mEventTypeAnniversary") == 0) {
					value = oxTimeManager
							.convertAndroidTime2OX(this.mContactInformations
									.get("mEventTypeAnniversary"));
				} else {
					// Keine Konvertierung benoetigt
					value = this.mContactInformations.get(key);
				}
				String oxName = cursor.getString(cursor
						.getColumnIndex(OXMapping.OX_NAME));
				if(!TextUtils.isEmpty(value)) {
					oxContact.put(oxName, value);
				} else {
					oxContact.put(oxName, "");
				}
			}
		} finally {
			cursor.close();
			oxMappingDBHelper.close();
		}
		// Remove distribution_list
		oxContact.remove("distribution_list");
		return oxContact;
	}

	public ArrayList<ContentProviderOperation> resetDirtyFlag() {
		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
		String rawContactId = getRawContactId();
		Builder resetDirtyFlag = ContentProviderOperation
				.newUpdate(mRawContactUri)
				.withSelection(RawContacts._ID + "=?",
						new String[] { rawContactId })
				.withValue(RawContacts.DIRTY, "0");
		ops.add(resetDirtyFlag.build());
		return ops;
	}

	public ArrayList<ContentProviderOperation> updateContact()
			throws NumberFormatException, JSONException, RemoteException,
			OperationApplicationException {
		return updateContact(true);
	}

	public ArrayList<ContentProviderOperation> updateContact(
			boolean pSyncAdapter) throws NumberFormatException, JSONException,
			RemoteException, OperationApplicationException {
		Uri contentUri = Data.CONTENT_URI
				.buildUpon()
				.appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER,
						String.valueOf(pSyncAdapter)).build();
		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
		OXMappingDatabaseHelper oxMappingDBHelper = new OXMappingDatabaseHelper(
				mContext);
		// Alten Contact erstellen um geloeschte Informationen zu finden
		OXContact oldContact = new OXContactRawContactIdBuilder()
				.addContext(mContext).addTimestamp(mTimestamp)
				.addFromRowContactId(Long.parseLong(getRawContactId()))
				.getOXObject();
		// TODO: Fix workaround!
		// Put mPicture in there to get updateData called with it oO
		oldContact.putContactInformation("mPicture", "pictureOld");
		mContactInformations.put("mPicture", "pictureNew");
		Iterator<String> iterator = mContactInformations.keySet().iterator();
		while (iterator.hasNext()) {
			String key = iterator.next();
			updateData(key, oldContact.getContactInformation(key), contentUri,
					oxMappingDBHelper);
		}
		// TODO: Fix Workaround (siehe Oben)!
		oldContact.removeContactInformation("mPicture");
		mContactInformations.remove("mPicture");
		// Notify about the changes
		mContext.getContentResolver().notifyChange(mContentUri, null);
		// Was im neuen Benutzer fehlt (bzw. im bestehenden zu viel ist)
		Set<String> keySetNewContact = mContactInformations.keySet();
		Set<String> intersectionOldMinusNew = oldContact
				.keySetContactInformations();
		intersectionOldMinusNew.removeAll(keySetNewContact);
		// => alles was in intersectionOldMinusNew jetzt drinsteht muss im Handy
		// geloescht werden!
		for(String key : intersectionOldMinusNew) {
			Log.d(TAG, "[*][OXContact] deleteData(" + key
					+ ", contentUri, oxMappingDBHelper)");
			ops.addAll(deleteData(key, contentUri, oxMappingDBHelper));
		}
		oxMappingDBHelper.close();
		return ops;
	}

	public ArrayList<ContentProviderOperation> deleteContact() {
		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
		ops.add(ContentProviderOperation
				.newDelete(mRawContactUri)
				.withSelection(RawContacts._ID + " = ?",
						new String[] { getRawContactId() }).build());
		return ops;
	}

	private void updateData(String pKey, String pOldValue, Uri pContentUri,
			OXMappingDatabaseHelper pOXMappingDBHelper) throws JSONException,
			RemoteException, OperationApplicationException {
		// OXId needs to be untouched!
		if(pKey.equals("mID")) return;
		String value = mContactInformations.get(pKey);
		if(!TextUtils.isEmpty(pOldValue) && pOldValue.equals(value)) return;
		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
		String[] projection = new String[] { OXMapping.ANDROID_COLUMN,
				OXMapping.ANDROID_MIMETYPE, OXMapping.ANDROID_TYPE };
		Cursor cursor = pOXMappingDBHelper.query(projection,
				OXMapping.OXMAPPING_KEY + " = ?", new String[] { pKey }, null,
				true);
		try {
			String column, mimeType, type;
			if(cursor.moveToFirst()) {
				column = cursor.getString(cursor
						.getColumnIndex(OXMapping.ANDROID_COLUMN));
				mimeType = cursor.getString(cursor
						.getColumnIndex(OXMapping.ANDROID_MIMETYPE));
				type = cursor.getString(cursor
						.getColumnIndex(OXMapping.ANDROID_TYPE));
			} else {
				throw new RuntimeException("Didn't find '" + pKey
						+ "' in OXMapping-Contentprovider!\nTried to update '"
						+ pKey + "' from contact '" + this.toPrintableString()
						+ "'.");
			}
			long dataId = getDataID(pKey);
			if(dataId >= 0) {
				Log.d(TAG, "[*][OXContact] updateData '" + pKey + "' with id '"
						+ dataId + "'");
				JSONObject sync1 = getSync1(dataId);
				sync1.put(pKey, value);
				Builder attribute = ContentProviderOperation.newUpdate(
						pContentUri).withSelection(Data._ID + "=?",
						new String[] { String.valueOf(dataId) });
				if(!mimeType.equalsIgnoreCase(StructuredName.CONTENT_ITEM_TYPE)) {
					attribute.withValue(Data.DATA2, type);
				}
				if(mimeType.equalsIgnoreCase(Photo.CONTENT_ITEM_TYPE)) {
					if(mPicture != null) {
						ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
						this.mPicture.compress(Bitmap.CompressFormat.PNG, 100,
								arrayOutputStream);
						byte[] bitmapAsByteArray = arrayOutputStream
								.toByteArray();
						attribute.withValue(column, bitmapAsByteArray);
					}
				} else {
					attribute.withValue(column, value);
				}
				attribute.withValue(Data.SYNC1, sync1.toString());
				ops.add(attribute.build());
				OXApplication.getOXContentResolver().applyBatch(
						ContactsContract.AUTHORITY, ops);
			} else {
				Log.d(TAG, "[*][OXContact] insertData '" + pKey + "'");
				// Eine ganz neue Zeile einfuegen (mit kompletter
				// Data.SYNC1-Spalte!)
				JSONObject sync1 = new JSONObject();
				if(!mimeType.equalsIgnoreCase(Phone.CONTENT_ITEM_TYPE)) {
					projection = new String[] { OXMapping.ANDROID_COLUMN,
							OXMapping.OXMAPPING_KEY };
					Cursor c = pOXMappingDBHelper.query(null,
							OXMapping.ANDROID_MIMETYPE + " = ? AND "
									+ OXMapping.ANDROID_TYPE + " = ?",
							new String[] { mimeType, type }, null);
					try {
						// Fill sync1 with all the possible keys.
						while (c.moveToNext()) {
							sync1.put(c.getString(c
									.getColumnIndex(OXMapping.OXMAPPING_KEY)),
									"");
						}
					} finally {
						c.close();
					}
				}
				sync1.put(pKey, value);
				Builder attribute = ContentProviderOperation
						.newInsert(pContentUri);
				attribute.withValue(Data.RAW_CONTACT_ID, getRawContactId())
						.withValue(Data.MIMETYPE, mimeType)
						.withValue(Data.SYNC1, sync1.toString());
				if(!mimeType.startsWith(StructuredName.MIMETYPE)) {
					attribute.withValue(Data.DATA2, cursor.getString(cursor
							.getColumnIndex(OXMapping.ANDROID_TYPE)));
				}
				if(mimeType.equalsIgnoreCase(Photo.CONTENT_ITEM_TYPE)) {
					if(mPicture != null) {
						ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
						this.mPicture.compress(Bitmap.CompressFormat.PNG, 100,
								arrayOutputStream);
						byte[] bitmapAsByteArray = arrayOutputStream
								.toByteArray();
						attribute.withValue(column, bitmapAsByteArray);
					}
				} else {
					attribute.withValue(column, value);
				}
				ContentProviderOperation operation = attribute.build();
				ops.add(operation);
				OXApplication.getOXContentResolver().applyBatch(
						ContactsContract.AUTHORITY, ops);
			}
		} finally {
			cursor.close();
		}
	}

	private ArrayList<ContentProviderOperation> deleteData(String pKey,
			Uri pContentUri, OXMappingDatabaseHelper pOXMappingDBHelper)
			throws JSONException {
		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
		String[] projection = new String[] { OXMapping.ANDROID_COLUMN };
		Cursor cursor = pOXMappingDBHelper.query(projection,
				OXMapping.OXMAPPING_KEY + " = ?", new String[] { pKey }, null);
		try {
			String column;
			if(cursor.moveToFirst()) {
				column = cursor.getString(cursor
						.getColumnIndex(OXMapping.ANDROID_COLUMN));
			} else {
				throw new RuntimeException("Didn't find '" + pKey
						+ "' in OXMapping-Contentprovider!\nTried to delete '"
						+ pKey + "' from contact '" + this.toPrintableString()
						+ "'.");
			}
			long dataId = getDataID(pKey);
			if(dataId < 0) { throw new RuntimeException(
					"The information '"
							+ pKey
							+ "' could not be translated into a dataId!\nTried to delete '"
							+ pKey + "' from contact '"
							+ this.toPrintableString() + "'."); }
			JSONObject sync1 = getSync1(dataId);
			sync1.remove(pKey);
			ops.add(ContentProviderOperation
					.newUpdate(pContentUri)
					.withSelection(Data._ID + "=?",
							new String[] { String.valueOf(dataId) })
					.withValue(column, "")
					.withValue(Data.SYNC1, sync1.toString()).build());
		} finally {
			cursor.close();
		}
		return ops;
	}

	private JSONObject getSync1(long pDataId) throws JSONException {
		Cursor c = mContext.getContentResolver().query(mContentUri,
				new String[] { Data.SYNC1 }, Data._ID + "=?",
				new String[] { String.valueOf(pDataId) }, null);
		try {
			if(c.moveToFirst()) {
				return new JSONObject(c.getString(c.getColumnIndex(Data.SYNC1)));
			} else {
				throw new RuntimeException("Didn't find '" + pDataId
						+ "' in Datatable of ContactsContract-Contentprovider!");
			}
		} finally {
			c.close();
		}
	}

	private long getDataID(String pKey) {
		String selection = Data.SYNC1 + " LIKE ? AND " + Data.RAW_CONTACT_ID
				+ "=?";
		String[] selectionArgs = new String[] { "%" + pKey + "%",
				getRawContactId() };
		String[] projection = new String[] { Data._ID };
		Cursor cursor = mContext.getContentResolver().query(mContentUri,
				projection, selection, selectionArgs, null);
		try {
			if(cursor.moveToFirst()) {
				long result = cursor.getLong(cursor.getColumnIndex(Data._ID));
				return result;
			} else {
				return -1;
			}
		} finally {
			cursor.close();
		}
	}

	public ArrayList<ContentProviderOperation> addOXId(String pOXId) {
		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
		this.mContactInformations.put("mID", pOXId);
		ops.addAll(addCustomContactInformationOXId());
		Builder attribute = ContentProviderOperation
				.newUpdate(mRawContactUri)
				.withSelection(RawContacts._ID + "=?",
						new String[] { getRawContactId() })
				.withValue(RawContacts.SOURCE_ID,
						mContactInformations.get("mID"));
		ops.add(attribute.build());
		return ops;
	}

	public ArrayList<ContentProviderOperation> markAsConflict() {
		return changeConflict(true);
	}

	public ArrayList<ContentProviderOperation> unmarkAsConflict() {
		return changeConflict(false);
	}

	private ArrayList<ContentProviderOperation> changeConflict(
			boolean pIsConflict) {
		ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
		Builder rawContact = ContentProviderOperation
				.newUpdate(
						RawContacts.CONTENT_URI
								.buildUpon()
								.appendQueryParameter(
										ContactsContract.CALLER_IS_SYNCADAPTER,
										"true").build())
				.withSelection(RawContacts._ID + "=?",
						new String[] { this.getRawContactId() })
				.withValue(RawContacts.SYNC1, pIsConflict == true ? "1" : "0");
		operations.add(rawContact.build());
		return operations;
	}

	private ArrayList<ContentProviderOperation> addCustomContactInformationOXId() {
		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
		Builder mID = ContentProviderOperation
				.newInsert(mContentUri)
				.withValue(Data.RAW_CONTACT_ID, getRawContactId())
				.withValue(Data.MIMETYPE, OXConstants.MIMETYPE_OXID)
				.withValue(Data.DATA1, mContactInformations.get("mID"))
				.withValue(Data.DATA2,
						mContext.getString(R.string.ox_profile_text))
				.withValue(Data.DATA3,
						mContext.getString(R.string.ox_profile_edit_contact));
		ops.add(mID.build());
		return ops;
	}

	public static String getRawContactId(String pOXId, ContentResolver pCtr) {
		String[] projection = new String[] { RawContactsEntity._ID };
		Cursor cursor = pCtr.query(RawContactsEntity.CONTENT_URI, projection,
				RawContactsEntity.MIMETYPE + " = ? AND "
						+ RawContactsEntity.DATA1 + " = ?", new String[] {
						OXConstants.MIMETYPE_OXID, String.valueOf(pOXId) },
				null);
		try {
			if(cursor.moveToFirst()) {
				String result = cursor.getString(cursor
						.getColumnIndex(RawContactsEntity._ID));
				return result;
			} else {
				return null;
			}
		} finally {
			cursor.close();
		}
	}

	/**
	 * releaseContext() - Set Context to null in order to release ressources and
	 * avoid memory leaks
	 */
	public void releaseContext() {
		this.setContext(null);
	}

	public void setContext(Context pContext) {
		this.mContext = pContext;
	}

	public String removeContactInformation(Object pKey) {
		this.somethingChanged();
		return this.mContactInformations.remove(pKey);
	}

	public String putContactInformation(String pKey, String pValue) {
		this.somethingChanged();
		return this.mContactInformations.put(pKey, pValue);
	}

	public String getContactInformation(Object pKey) {
		return this.mContactInformations.get(pKey);
	}

	public Set<String> keySetContactInformations() {
		return this.mContactInformations.keySet();
	}

	public boolean hasDistributionList() {
		return !TextUtils.isEmpty(mDistributionList);
	}

	public boolean hasId() {
		return mContactInformations.containsKey("mID");
	}

	public String getId() {
		return mContactInformations.get("mID");
	}

	public String getRawContactId() {
		if(mRawContactId != null) return mRawContactId;
		else return getRawContactId(getId(), mContext.getContentResolver());
	}

	public void setRawContactId(String pRawContactId) {
		this.mRawContactId = pRawContactId;
	}

	public void setContactInformations(
			LinkedHashMap<String, String> pContactInformations) {
		this.mContactInformations = pContactInformations;
	}

	public String getVersion() {
		return mVersion;
	}

	public void setVersion(String pVersion) {
		this.mVersion = pVersion;
	}

	public String getDistributionList() {
		return mDistributionList;
	}

	public void setDistributionList(String pDistributionList) {
		this.mDistributionList = pDistributionList;
	}

	public void setTimestamp(Date pTimestamp) {
		this.mTimestamp = pTimestamp;
	}

	public Date getTimestamp() {
		return mTimestamp;
	}

	public String getPictureServerPath() {
		return this.mContactInformations.get("mPicture");
	}

	public String toPrintableString() {
		return mContactInformations.get("mNameDisplayName");
	}

	public void setPicture(Bitmap pPicture) {
		somethingChanged();
		this.mPicture = pPicture;
	}

	public Bitmap getPicture() {
		return this.mPicture;
	}

	/**
	 * ONLY for UNITTESTS! Do not use outsite of the Testproject.
	 * 
	 * @return The underlying LinkedHashMap, where all informations are stored.
	 */
	public LinkedHashMap<String, String> getContactInformations() {
		return this.mContactInformations;
	}

	@Override
	public String toString() {
		return getClass().getName()
				+ "["
				+ "Timestamp="
				+ (mTimestamp == null ? "null" : mTimestamp.toGMTString())
				+ ", "
				+ "Version="
				+ (mVersion == null ? "null" : mVersion)
				+ ", "
				+ "RawContactId="
				+ (mRawContactId == null ? "null" : mRawContactId.toString())
				+ ", "
				+ "DistributionList="
				+ (mDistributionList == null ? "null" : mDistributionList)
				+ ", "
				+ "ContactInformations="
				+ (mContactInformations == null ? "null" : mContactInformations
						.toString()) + "]";
	}

	@Override
	public boolean equals(Object o) {
		// Return true if the objects are identical.
		// (This is just an optimization, not required for correctness.)
		if(this == o) { return true; }

		// Return false if the other object has the wrong type.
		// This type may be an interface depending on the interface's
		// specification.
		if(!(o instanceof OXContact)) { return false; }

		// Cast to the appropriate type.
		// This will succeed because of the instanceof, and lets us access
		// private fields.
		OXContact lhs = (OXContact) o;

		// Check each field. Primitive fields, reference fields, and nullable
		// reference
		// fields are all treated differently.
		// return primitiveField == lhs.primitiveField &&
		// referenceField.equals(lhs.referenceField) &&
		// (nullableField == null ? lhs.nullableField == null :
		// nullableField.equals(lhs.nullableField));
		return (mTimestamp == null ? lhs.mTimestamp == null : mTimestamp
				.equals(lhs.mTimestamp))
				&& (mVersion == null ? lhs.mVersion == null : mVersion
						.equals(lhs.mVersion))
				&& (mDistributionList == null ? lhs.mDistributionList == null
						: mDistributionList.equals(lhs.mDistributionList))
				&& (mRawContactId == null ? lhs.mRawContactId == null
						: mRawContactId.equals(lhs.mRawContactId))
				&& (mContactInformations == null ? lhs.mContactInformations == null
						: mContactInformations.equals(lhs.mContactInformations));

	}
}
