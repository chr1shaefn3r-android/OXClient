/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.model;

import org.json.JSONArray;
import org.json.JSONException;

import android.database.Cursor;

import com.alien.oxclient.provider.OXMapping;

public class OXContactJSONArrayBuilder extends OXContactJSONBuilder {

	@Override
	protected String[] getProjection() {
		return new String[] { OXMapping.OXMAPPING_KEY, OXMapping.OX_POSITION };
	}

	@Override
	protected String getSortOrder() {
		return OXMapping.OX_POSITION;
	}

	@Override
	protected String getValue(Cursor pCursor, Object pDaten)
			throws JSONException {
		JSONArray jsonArray = (JSONArray) pDaten;
		int jsonArraykey = pCursor.getInt(pCursor
				.getColumnIndex(OXMapping.OX_POSITION));
		if(jsonArraykey >= jsonArray.length()) {
			// Don't proceed with this value if the key we want to access is
			// greater than the length of the array
			return null;
		} else {
			return jsonArray.getString(jsonArraykey);
		}
	}
}
