/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class represents the generalized Contacts/Calendar/Tasks. It should
 * function as model which also provides an Interface to be used by the internal
 * HTTP-API.
 */
public abstract class OXObject {
	// Set to true when information is altered via put/remove-Methods
	protected boolean mDirty = false;

	protected void somethingChanged() {
		this.mDirty = true;
	}

	public boolean isChanged() {
		return this.mDirty;
	}

	public void cleanDirtyFlag() {
		this.mDirty = false;
	}

	public abstract boolean hasId();

	public abstract String getId();

	public abstract JSONObject toJSONObject() throws JSONException;

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException();
	}
}
