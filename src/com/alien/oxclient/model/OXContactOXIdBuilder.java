/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.model;

import java.util.Date;

import org.json.JSONException;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract.RawContactsEntity;

import com.alien.oxclient.OXConstants;

public class OXContactOXIdBuilder extends OXContactRawContactIdBuilder {

	@Override
	public OXContactOXIdBuilder addContext(Context pContext) {
		super.addContext(pContext);
		return this;
	}

	@Override
	public OXContactOXIdBuilder addTimestamp(Date pTimestamp) {
		super.addTimestamp(pTimestamp);
		return this;
	}

	public OXContactOXIdBuilder addFromOXId(String pOXId)
			throws NumberFormatException, JSONException {
		addFromRowContactId(Long.parseLong(getRawContactId(pOXId)));
		return this;
	}

	public String getRawContactId(String pOXId) {
		String[] projection = new String[] { RawContactsEntity._ID };
		Cursor cursor = mContext.getContentResolver().query(
				RawContactsEntity.CONTENT_URI,
				projection,
				RawContactsEntity.MIMETYPE + " = ? AND "
						+ RawContactsEntity.DATA1 + " = ?",
				new String[] { OXConstants.MIMETYPE_OXID, pOXId }, null);
		try {
			if(cursor.moveToFirst()) {
				String result = cursor.getString(cursor
						.getColumnIndex(RawContactsEntity._ID));
				return result;
			} else {
				return null;
			}
		} finally {
			cursor.close();
		}
	}
}
