/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.model;

import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.RawContacts.Entity;
import android.text.TextUtils;

import com.alien.oxclient.preferences.OXPreferencesImpl;
import com.alien.oxclient.provider.OXMapping;
import com.alien.oxclient.provider.OXMappingDatabaseHelper;

public class OXContactRawContactIdBuilder extends OXContactBuilder {

	private static final String[] mProjection = new String[] { Data._ID, /* 0 */
	Data.DATA1,/* 1 */Data.DATA2,/* 2 */Data.DATA3,/* 3 */Data.DATA4,/* 4 */
	Data.DATA5,/* 5 */Data.DATA6,/* 6 */Data.DATA7,/* 7 */Data.DATA8,/* 8 */
	Data.DATA9,/* 9 */Data.DATA10,/* 10 */Data.DATA11,/* 11 */Data.DATA12,/* 12 */
	Data.DATA13,/* 13 */Data.DATA14,/* 14 */Data.DATA15,/* 15 */
	Data.MIMETYPE/* 16 */, Data.SYNC1 /* 17 */
	};
	private Uri mRawContactUri = RawContacts.CONTENT_URI
			.buildUpon()
			.appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER,
					"true").build();

	@Override
	public OXContactRawContactIdBuilder addContext(Context pContext) {
		super.addContext(pContext);
		return this;
	}

	@Override
	public OXContactRawContactIdBuilder addTimestamp(Date pTimestamp) {
		super.addTimestamp(pTimestamp);
		return this;
	}

	public OXContactRawContactIdBuilder addFromRowContactId(long pRawContactId)
			throws JSONException {
		addRawContactId(pRawContactId);
		addVersion(getVersion(pRawContactId));
		ContentResolver ctr = mContext.getContentResolver();
		OXPreferencesImpl prefs = new OXPreferencesImpl(mContext);
		OXMappingDatabaseHelper oxMappingDBHelper = new OXMappingDatabaseHelper(
				mContext);
		Uri rawContactUri = ContentUris.withAppendedId(mRawContactUri,
				pRawContactId);
		Uri entityUri = Uri.withAppendedPath(rawContactUri,
				Entity.CONTENT_DIRECTORY);
		Cursor c = ctr.query(entityUri, mProjection, null, null, null);
		try {
			ArrayList<String> addedColumns = new ArrayList<String>();
			while (c.moveToNext()) {
				String mimeType = c.getString(16); // MIMETYPE
				// If mimeType not set there is nothing to do with this
				// information
				if(mimeType == null) {
					continue;
				}
				String type = String.valueOf(c.getInt(2)); // TYPE
				String syncKey = "";
				String jsonString = c.getString(17);
				if(!TextUtils.isEmpty(jsonString)) {
					JSONObject sync1 = new JSONObject(jsonString); // SYNC1
					JSONArray array = sync1.names();
					if(array != null && array.length() == 1) syncKey = array
							.getString(0);
				}
				if(mimeType.equals(Email.CONTENT_ITEM_TYPE) && type.equals("0")) {
					// Custom Emailmapping is only needed when emailtype is
					// equals zero. This happens for example on HTC Desire
					String value = c.getString(c.getColumnIndex(Data.DATA1));
					if(!mContactInformations.containsKey(prefs
							.getFirstEmailMapping())) {
						mContactInformations.put(prefs.getFirstEmailMapping(),
								value);
						continue;
					} else if(!mContactInformations.containsKey(prefs
							.getSecondEmailMapping())) {
						mContactInformations.put(prefs.getSecondEmailMapping(),
								value);
						continue;
					} else if(!mContactInformations.containsKey(prefs
							.getThirdEmailMapping())) {
						mContactInformations.put(prefs.getThirdEmailMapping(),
								value);
						continue;
					}
				} else if(mimeType.equals(Photo.CONTENT_ITEM_TYPE)) {
					byte[] bytes = c.getBlob(c.getColumnIndex(Photo.PHOTO));
					if(bytes != null) {
						this.mPicture = BitmapFactory.decodeByteArray(bytes, 0,
								bytes.length, null);
					}
					continue;
				}
				// Den OXMappingContentProvider fragen, welche Werte sich hinter
				// dem MIMETYPE (c.getInt(16)) und dem TYPE (c.getInt(2))
				// verstecken um sie dann an die richtigen Stellen in der
				// Contact-LinkedHashMap schreiben zu koennen
				String[] projection = new String[] { OXMapping.ANDROID_COLUMN,
						OXMapping.OXMAPPING_KEY };
				Cursor cursor = oxMappingDBHelper.query(projection,
						OXMapping.ANDROID_MIMETYPE + " = ? AND "
								+ OXMapping.ANDROID_TYPE + " = ?",
						new String[] { mimeType, type }, null, true);
				try {
					while (cursor.moveToNext()) {
						String key = "";
						if(TextUtils.isEmpty(syncKey)) {
							key = cursor.getString(cursor
									.getColumnIndex(OXMapping.OXMAPPING_KEY));
						} else {
							key = syncKey;
							// Set syncKey empty so next times it will get into
							// the ifclause
							syncKey = "";
						}
						String column = cursor.getString(cursor
								.getColumnIndex(OXMapping.ANDROID_COLUMN));
						if(addedColumns.contains(column)) {
							continue;
						}
						String value = c.getString(c.getColumnIndex(column));
						if(!TextUtils.isEmpty(value)) {
							if(mimeType.startsWith(Phone.CONTENT_ITEM_TYPE)) value = value
									.replaceAll("([^+\\d])", "");
							if(!mContactInformations.containsKey(key)) {
								mContactInformations.put(key, value);
								addedColumns.add(column);
							}
						}
					}
				} finally {
					cursor.close();
				}
				addedColumns.clear();
			}
		} finally {
			c.close();
			oxMappingDBHelper.close();
		}
		return this;
	}

	private String getVersion(long pRawContactId) {
		String[] projection = new String[] { RawContacts.VERSION };
		Cursor cursor = mContext.getContentResolver().query(
				RawContacts.CONTENT_URI, projection, RawContacts._ID + " = ?",
				new String[] { String.valueOf(pRawContactId) }, null);
		try {
			if(cursor.moveToFirst()) {
				String result = cursor.getString(cursor
						.getColumnIndex(RawContacts.VERSION));
				return result;
			} else {
				throw new NullPointerException("NO VERSION-FIELD?!?");
			}
		} finally {
			cursor.close();
		}
	}
}
