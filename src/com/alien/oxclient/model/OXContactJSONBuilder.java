/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.model;

import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.alien.oxclient.OXTimeManager;
import com.alien.oxclient.provider.OXMapping;
import com.alien.oxclient.provider.OXMappingDatabaseHelper;

public abstract class OXContactJSONBuilder extends OXContactBuilder {

	@Override
	public OXContactJSONBuilder addContext(Context pContext) {
		super.addContext(pContext);
		return this;
	}

	@Override
	public OXContactJSONBuilder addTimestamp(Date pTimestamp) {
		super.addTimestamp(pTimestamp);
		return this;
	}

	public OXContactJSONBuilder addFromJSONObject(JSONObject pJSONObject)
			throws JSONException {
		buildFromJSON(pJSONObject);
		return this;
	}

	public OXContactJSONBuilder addFromJSONArray(JSONArray pJSONArray)
			throws JSONException {
		buildFromJSON(pJSONArray);
		return this;
	}

	private void buildFromJSON(Object pData) throws JSONException {
		OXTimeManager oxTimeManager = new OXTimeManager();
		OXMappingDatabaseHelper oxMappingDBHelper = new OXMappingDatabaseHelper(
				mContext);
		String[] projection = getProjection();
		Cursor cursor = oxMappingDBHelper.query(projection, null, null,
				getSortOrder(), true);
		try {
			while (cursor.moveToNext()) {
				String key = cursor.getString(cursor
						.getColumnIndex(OXMapping.OXMAPPING_KEY));
				String value = getValue(cursor, pData);
				if(value != null && value.compareTo("null") != 0) {
					// Convertierung der Zeit von openXChange- nach
					// Androidformat
					if(key.compareTo("mEventTypeBirthday") == 0) {
						value = oxTimeManager.convertOXTime2Android(value);
					} else if(key.compareTo("mEventTypeAnniversary") == 0) {
						value = oxTimeManager.convertOXTime2Android(value);
					} else if(key.compareTo("mDistributionList") == 0) {
						mDistributionList = value;
						break; // We don't want to put this value into the
								// ContactInformations
					}
					if(!TextUtils.isEmpty(value)) {
						mContactInformations.put(key, value);
					}
				}
			}
		} finally {
			cursor.close();
			oxMappingDBHelper.close();
		}
	}

	protected abstract String[] getProjection();

	protected abstract String getSortOrder();

	protected abstract String getValue(Cursor pCursor, Object pDaten)
			throws JSONException;
}
