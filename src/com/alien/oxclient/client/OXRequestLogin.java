/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.client;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import android.net.Uri.Builder;

/**
 * This concrete request represents an openXchange "login request".
 */
public class OXRequestLogin extends OXPostRequest {

	private HttpContext mHttpContext;
	private String mName;
	private String mPassword;

	public OXRequestLogin(String pHost, String pName, String pPassword) {
		super(pHost, new StringBuilder("login"), null);
		this.mName = pName;
		this.mPassword = pPassword;
		CookieStore cookieStore = new BasicCookieStore();
		mHttpContext = new BasicHttpContext();
		mHttpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
	}

	@Override
	public Builder doCreateGetParameterBuilder(Builder builder) {
		return new OXRequestAttribute(builder).addAction("login").done();
	}

	@Override
	public HttpEntity doCreateEntity() throws UnsupportedEncodingException {
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("name", mName));
		params.add(new BasicNameValuePair("password", mPassword));

		HttpEntity entity = new UrlEncodedFormEntity(params);
		return entity;
	}

	@Override
	protected String sendOverNetwork(HttpRequestBase pRequest)
			throws IOException {
		return mOXNetwork.execute(pRequest, mHttpContext);
	}

	@Override
	protected OXResponse parse(String pResponseContent) {
		return mResponseBuilder.parseLogin(pResponseContent, mHttpContext);
	}
}
