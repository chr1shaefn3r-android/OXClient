/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.client;

import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.json.JSONException;

public abstract class OXRequestMultiPart extends OXPostRequest {

	public OXRequestMultiPart(StringBuilder pPath, Session pSession) {
		super(pPath, pSession);
	}

	@Override
	protected OXResponse parse(String pResponseContent) {
		return mResponseBuilder.parseMultiPart(pResponseContent);
	}

	@Override
	public HttpEntity doCreateEntity() throws UnsupportedEncodingException,
			JSONException {
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("json", doCreateJsonContentBody());
		entity.addPart("file", doCreateFileContentBody());
		return entity;
	}

	protected abstract ContentBody doCreateJsonContentBody()
			throws JSONException, UnsupportedEncodingException;

	protected abstract ContentBody doCreateFileContentBody();
}
