/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.client;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.Uri.Builder;

import com.alien.oxclient.model.OXObject;

public abstract class OXRequestUpdateObject extends OXPutRequest {

	private OXObject mOXObject;
	private String mFolderId;
	private Date mDate;

	public OXRequestUpdateObject(OXObject pOXObject, String pFolderId,
			Date pDate, StringBuilder pPath, Session pSession) {
		super(pPath, pSession);
		this.mOXObject = pOXObject;
		this.mFolderId = pFolderId;
		this.mDate = pDate;
	}

	@Override
	protected Builder doCreateGetParameterBuilder(Builder builder) {
		return super
				.doCreateGetParameterBuilder(new OXRequestAttribute(builder)
						.addAction("update").addId(mOXObject.getId())
						.addTimestamp(String.valueOf(mDate.getTime()))
						.addFolder(mFolderId).done());
	}

	@Override
	public HttpEntity doCreateEntity() throws UnsupportedEncodingException,
			JSONException {
		JSONObject oxObject = mOXObject.toJSONObject();
		return new StringEntity(oxObject.toString(), "UTF-8");
	}
}
