/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.client;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.net.ssl.SSLException;

import org.apache.http.auth.AuthenticationException;
import org.apache.http.client.methods.HttpRequestBase;
import org.json.JSONException;

import android.net.Uri;
import android.net.Uri.Builder;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.mockinterfaces.OXNetwork;

/**
 * Clients of an openXchange server can use requests to fire queries on an
 * openXchange server. The {@link OXRequest} represents a basic request. It is
 * extended by the concrete requests. This abstract request contains basic
 * information like url and port. Furthermore it contains the current
 * {@link Session} of the request.
 */
public abstract class OXRequest {
	protected OXResponseBuilder mResponseBuilder;
	protected OXNetwork mOXNetwork;
	/**
	 * host of the url of this request
	 */
	protected String mHost;
	/**
	 * path of the url of this request
	 */
	protected StringBuilder mPath;
	/**
	 * The corresponding {@link Session} to this request
	 */
	protected Session mSession;

	public OXRequest(StringBuilder pPath, Session pSession) {
		this(OXApplication.getAccountManager().getAccountServer(), pPath,
				pSession);
	}

	public OXRequest(String pHost, StringBuilder pPath, Session pSession) {
		this.mHost = pHost;
		this.mPath = pPath.insert(0, "ajax/");
		this.mSession = pSession;
		mResponseBuilder = new OXResponseBuilder();
		mOXNetwork = OXApplication.getOXNetwork();
	}

	/**
	 * Sends this request. The Method sends a http request. Furthermore it uses
	 * a {@link OXResponseBuilder} to parse the JSON response of the openXchange
	 * server to a OXResponse
	 * 
	 * @return a instance of {@link OXResponse}
	 * @throws IOException
	 *             if something with the connection went wrong
	 * @throws AuthenticationException
	 *             if the credentials got invalid
	 */
	public OXResponse send(boolean pEncrypted) throws IOException,
			AuthenticationException, SSLException, JSONException {
		Uri url = createUrl(pEncrypted);
		final HttpRequestBase request = doCreateRequest(url.toString());
		return this.parse(sendOverNetwork(request));
	}

	protected String sendOverNetwork(HttpRequestBase pRequest)
			throws IOException {
		return mOXNetwork.execute(pRequest, mSession.getLocalContext());
	}

	protected OXResponse parse(String pResponseContent) {
		return mResponseBuilder.parse(pResponseContent);
	}

	/**
	 * Creates the corresponding {@link Uri} to this {@link OXRequest}
	 * 
	 * @return a {@link Uri}
	 */
	public Uri createUrl(boolean pEncrypted) {
		Builder uriBuilder = new Uri.Builder().scheme(
				pEncrypted ? "https" : "http").authority(mHost);
		doCreateGetParameterBuilder(uriBuilder);
		uriBuilder.path(mPath.toString());
		return uriBuilder.build();
	}

	protected abstract HttpRequestBase doCreateRequest(String pUrlAsString)
			throws UnsupportedEncodingException, JSONException;

	protected Builder doCreateGetParameterBuilder(Builder builder) {
		return builder.appendQueryParameter("session", mSession.getSessionID());
	}
}
