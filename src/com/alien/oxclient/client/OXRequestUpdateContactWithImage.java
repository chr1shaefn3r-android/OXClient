/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.client;

import java.util.Date;

import android.net.Uri.Builder;

import com.alien.oxclient.model.OXContact;

public class OXRequestUpdateContactWithImage extends OXRequestContactMultiPart {

	public OXRequestUpdateContactWithImage(OXContact pOXContact,
			String pFolderId, Date pDate, Session pSession) {
		super(pOXContact, pFolderId, pSession);
	}

	@Override
	protected Builder doCreateGetParameterBuilder(Builder builder) {
		// Timestamp is set to "now", might need to be added as parameter to be
		// able to "force" or "not force" an update
		return super
				.doCreateGetParameterBuilder(new OXRequestAttribute(builder)
						.addAction("update").addFolder(mFolderId)
						.addId(mOXContact.getId())
						.addTimestamp(String.valueOf(new Date().getTime()))
						.done());
	}
}
