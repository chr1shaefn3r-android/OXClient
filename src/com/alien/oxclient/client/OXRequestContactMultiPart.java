/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.client;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;

import com.alien.oxclient.model.OXContact;

public abstract class OXRequestContactMultiPart extends OXRequestMultiPart {

	protected OXContact mOXContact;
	protected String mFolderId;

	public OXRequestContactMultiPart(OXContact pOXContact, String pFolderId,
			Session pSession) {
		super(new StringBuilder(OXObjectTypes.CONTACTS.getPathSuffix()),
				pSession);
		this.mOXContact = pOXContact;
		this.mFolderId = pFolderId;
	}

	@Override
	protected ContentBody doCreateJsonContentBody() throws JSONException,
			UnsupportedEncodingException {
		JSONObject oxObject = mOXContact.toJSONObject();

		doAlterJsonOXContact(oxObject);

		return new StringBody(oxObject.toString(), Charset.forName("UTF-8"));
	}

	@Override
	protected ContentBody doCreateFileContentBody() {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		mOXContact.getPicture()
				.compress(Bitmap.CompressFormat.PNG, 100, stream);
		return new ByteArrayBody(stream.toByteArray(), "image/png", "image1");
	}

	protected void doAlterJsonOXContact(JSONObject pOXJsonContact)
			throws JSONException {
		// Default: Do nothing
	}
}
