/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.client;

import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.client.CookieStore;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;

/**
 * An {@link OXResponseBuilder} is used to convert a JSON object to a
 * {@link OXResponse}.
 */
public class OXResponseBuilder {
	private static final String JSON_TIMESTAMP = "timestamp";
	private static final String JSON_DATA = "data";
	private static final String JSON_ERROR = "error";
	private static final String JSON_ERRORPARAMS = "error_params";
	private static final String JSON_ERRORCODE = "code";

	/**
	 * Builds a concrete child class of a {@link OXResponse} using the algorithm
	 * for a normal response, aka. non-login response
	 * 
	 * @return a concrete {@link OXResponse}
	 */
	public OXResponse parse(String pResponseContent) {
		try {
			JSONObject jsonAnswer = new JSONObject(pResponseContent);
			return new OXResponse(getLongField(jsonAnswer, JSON_TIMESTAMP),
					getStringField(jsonAnswer, JSON_DATA), getStringField(
							jsonAnswer, JSON_ERROR), getStringField(jsonAnswer,
							JSON_ERRORPARAMS), getStringField(jsonAnswer,
							JSON_ERRORCODE));
		} catch (Exception pException) {
			return responseForException(pException);
		}
	}

	/**
	 * Builds a concrete child class of a {@link OXResponse} using the algorithm
	 * for a multipart-response
	 * 
	 * @return a concrete {@link OXResponse}
	 */
	public OXResponse parseMultiPart(String pResponseContent) {
		try {
			Pattern jsonPattern = Pattern.compile("(\\{.*\\})",
					Pattern.CASE_INSENSITIVE);
			Matcher matcher = jsonPattern.matcher(pResponseContent);
			String jsonResponseContent = pResponseContent;
			if(matcher.find()) {
				jsonResponseContent = matcher.group(0);
			}
			JSONObject jsonAnswer = new JSONObject(jsonResponseContent);
			return new OXResponse(getLongField(jsonAnswer, JSON_TIMESTAMP),
					getStringField(jsonAnswer, JSON_DATA), getStringField(
							jsonAnswer, JSON_ERROR), getStringField(jsonAnswer,
							JSON_ERRORPARAMS), getStringField(jsonAnswer,
							JSON_ERRORCODE));
		} catch (Exception pException) {
			return responseForException(pException);
		}
	}

	/**
	 * Builds a concrete child class of a {@link OXResponse} using the algorithm
	 * for a picture download response
	 * 
	 * @return a concrete {@link OXResponse}
	 */
	public OXResponse parse(Bitmap pBitmap) {
		try {
			return new OXResponse(pBitmap);
		} catch (Exception pException) {
			return responseForException(pException);
		}
	}

	/**
	 * Builds a concrete child class of a {@link OXResponse} using the algorithm
	 * for a login response
	 * 
	 * @return a concrete {@link OXResponse}
	 */
	public OXResponse parseLogin(String pResponseContent,
			HttpContext pHttpContext) {
		OXResponse resultResponse;
		StringBuilder authTokenBuilder = new StringBuilder();
		try {
			JSONObject jsonAnswer = new JSONObject(pResponseContent);
			if(jsonAnswer.has("session")) {
				authTokenBuilder.append("sessionID="
						+ jsonAnswer.getString("session") + ";");
				List<Cookie> cookies = ((CookieStore) pHttpContext
						.getAttribute(ClientContext.COOKIE_STORE)).getCookies();
				if(!cookies.isEmpty()) {
					for(int i = 0; i < cookies.size(); i++) {
						if(cookies.get(i).getName().compareTo("JSESSIONID") == 0) {
							authTokenBuilder.append(cookies.get(i).getName()
									+ "=" + cookies.get(i).getValue() + ";");
						} else {
							// Davon ausgehend, dass ausser dem
							// JSESSIONID-Cookie nurnoch
							// das openxchange-secret in Cookieform
							// uebertragen wird:
							authTokenBuilder.append(cookies.get(i).getName()
									+ "=" + cookies.get(i).getValue() + ";");
						}
					}
				}
			}
			resultResponse = new OXResponse(getLongField(jsonAnswer,
					JSON_TIMESTAMP), authTokenBuilder.toString(),
					getStringField(jsonAnswer, JSON_ERROR), getStringField(
							jsonAnswer, JSON_ERRORPARAMS), getStringField(
							jsonAnswer, JSON_ERRORCODE));
		} catch (Exception pException) {
			resultResponse = responseForException(pException);
		}
		return resultResponse;
	}

	private OXResponse responseForException(Exception pException) {
		return new OXResponse(new Date().getTime(), null, pException.getClass()
				.getSimpleName() + ": " + pException.getLocalizedMessage(),
				null, null);
	}

	/**
	 * Returns the timestamp value of a given {@link JSONObject}. Returns null,
	 * if no timestamp is set or the timestamp was not parseable.
	 * 
	 * @param pJSON
	 *            the {@link JSONObject} to parse
	 * @param pField
	 *            the name of the field
	 * @return the timestamp of the given {@link JSONObject} or null
	 */
	private Long getLongField(JSONObject pJSON, String pField) {
		try {
			return pJSON.getLong(pField);
		} catch (JSONException e) {
			return null;
		}
	}

	/**
	 * Returns the data value of a given {@link JSONObject} and a given
	 * fieldname. Returns null, if no data is set or the data was not parseable.
	 * 
	 * @param pJSON
	 *            the {@link JSONObject} to parse
	 * @param pField
	 *            the name of the field
	 * @return the data of the field in the given {@link JSONObject} or null
	 */
	private String getStringField(JSONObject pJSON, String pField) {
		try {
			return pJSON.getString(pField);
		} catch (JSONException e) {
			return null;
		}
	}
}
