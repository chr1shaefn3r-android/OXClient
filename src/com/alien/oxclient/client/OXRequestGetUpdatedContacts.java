/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.client;

import java.util.Date;

import android.net.Uri.Builder;

public class OXRequestGetUpdatedContacts extends OXRequestGetUpdatedObjects {

	private String mColumns;
	private String mFolderId;

	public OXRequestGetUpdatedContacts(Date pLastUpdated, String pColumns,
			String pFolderId, Session pSession) {
		super(pLastUpdated, new StringBuilder(
				OXObjectTypes.CONTACTS.getPathSuffix()), pSession);
		this.mColumns = pColumns;
		this.mFolderId = pFolderId;
	}

	@Override
	public Builder doCreateGetParameterBuilder(Builder builder) {
		return new OXRequestAttribute(
				super.doCreateGetParameterBuilder(builder))
				.addColumns(mColumns).addFolder(mFolderId).done();
	}
}
