/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.client;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.mockinterfaces.OXNetwork;

public class OXNetworkImpl implements OXNetwork {

	private static String USER_AGENT = "OXClient/"
			+ OXApplication.getPrefs().getVersionCode();

	public String execute(HttpUriRequest pRequest, HttpContext pContext)
			throws IOException, HttpResponseException {
		final AndroidHttpClient client = AndroidHttpClient
				.newInstance(USER_AGENT);
		try {
			return client.execute(pRequest, new BasicResponseHandler(),
					pContext);
		} finally {
			if(client != null) client.close();
		}
	}

	public Bitmap executeToGetBitmap(HttpUriRequest pRequest,
			HttpContext pContext) throws IOException, HttpResponseException {
		final AndroidHttpClient client = AndroidHttpClient
				.newInstance(USER_AGENT);
		try {
			HttpResponse response = client.execute(pRequest, pContext);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if(statusCode == HttpStatus.SC_OK) {
				HttpEntity entity = response.getEntity();
				byte[] bytes = EntityUtils.toByteArray(entity);
				Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0,
						bytes.length);
				return bitmap;
			} else {
				throw new IOException("Download failed, HTTP response code "
						+ statusCode + " - " + statusLine.getReasonPhrase());
			}
		} finally {
			if(client != null) client.close();
		}
	}
}
