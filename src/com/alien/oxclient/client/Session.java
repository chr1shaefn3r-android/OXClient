/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.client;

import java.util.HashMap;
import java.util.Set;

import org.apache.http.client.CookieStore;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

public class Session {
	private CookieStore mCookieStore;
	private HttpContext mLocalContext;
	private String mSessionID;
	private String mAuthToken;
	private String mServer;
	private HashMap<String, String> mCookieList = new HashMap<String, String>();

	public Session(String pAuthToken, String pServer) {
		this.mServer = pServer;

		// Create a local instance of cookie store
		mCookieStore = new BasicCookieStore();

		// Create local HTTP context
		mLocalContext = new BasicHttpContext();
		// Bind custom cookie store to the local context
		mLocalContext.setAttribute(ClientContext.COOKIE_STORE, mCookieStore);

		parseAuthToken(pAuthToken);
		fillCookieStore();
	}

	private void parseAuthToken(String pAuthToken) {
		mAuthToken = pAuthToken;
		String[] splitted = pAuthToken.split(";");
		for(int i = 1; i < splitted.length; i++) {
			String[] result = splitted[i].split("=");
			mCookieList.put(result[0], result[1]);
		}
		String[] sessionID = splitted[0].split("=");
		mSessionID = sessionID[1];
	}

	private void fillCookieStore() {
		Set<String> keys = mCookieList.keySet();
		for(String cookieName : keys) {
			Cookie cookie = new BasicClientCookie(cookieName,
					mCookieList.get(cookieName));
			((BasicClientCookie) cookie).setDomain(mServer);
			mCookieStore.addCookie(cookie);
		}
	}

	public HttpContext getLocalContext() {
		return mLocalContext;
	}

	public String getSessionID() {
		return mSessionID;
	}

	public String getAuthToken() {
		return mAuthToken;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		Session other = (Session) obj;
		if(mAuthToken == null) {
			if(other.mAuthToken != null) return false;
		} else if(!mAuthToken.equals(other.mAuthToken)) return false;
		if(mSessionID == null) {
			if(other.mSessionID != null) return false;
		} else if(!mSessionID.equals(other.mSessionID)) return false;
		for(int i = 0; i < mCookieStore.getCookies().size(); i++) {
			Cookie ourCookie = mCookieStore.getCookies().get(i);
			Cookie theircookie = other.mCookieStore.getCookies().get(i);
			if(!ourCookie.getName().equals(theircookie.getName())) return false;
			if(!ourCookie.getValue().equals(theircookie.getValue())) return false;
		}
		return true;
	}
}
