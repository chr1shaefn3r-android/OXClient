/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.client;

import android.net.Uri;

public class OXRequestAttribute {

	private Uri.Builder mUriBuilder;

	public OXRequestAttribute(Uri.Builder pUriBuilder) {
		this.mUriBuilder = pUriBuilder;
	}

	public OXRequestAttribute addAction(String pAction) {
		mUriBuilder.appendQueryParameter("action", pAction);
		return this;
	}

	public OXRequestAttribute addId(String pId) {
		mUriBuilder.appendQueryParameter("id", pId);
		return this;
	}

	public OXRequestAttribute addColumns(String pColumns) {
		mUriBuilder.appendQueryParameter("columns", pColumns);
		return this;
	}

	public OXRequestAttribute addFolder(String pFolderId) {
		mUriBuilder.appendQueryParameter("folder", pFolderId);
		return this;
	}

	public OXRequestAttribute addFolderId(String pFolderId) {
		mUriBuilder.appendQueryParameter("folder_id", pFolderId);
		return this;
	}

	public OXRequestAttribute addTimestamp(String pTimestamp) {
		mUriBuilder.appendQueryParameter("timestamp", pTimestamp);
		return this;
	}

	public OXRequestAttribute addParent(String pParent) {
		mUriBuilder.appendQueryParameter("parent", pParent);
		return this;
	}

	public Uri.Builder done() {
		return mUriBuilder;
	}
}
