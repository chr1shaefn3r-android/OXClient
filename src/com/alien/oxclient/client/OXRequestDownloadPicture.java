/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.client;

import java.io.IOException;
import java.util.HashMap;

import javax.net.ssl.SSLException;

import org.apache.http.auth.AuthenticationException;
import org.apache.http.client.methods.HttpRequestBase;
import org.json.JSONException;

import android.graphics.Bitmap;
import android.net.Uri;
import android.net.Uri.Builder;

import com.alien.oxclient.model.OXContact;

public class OXRequestDownloadPicture extends OXGetRequest {

	private String mQuery;

	public OXRequestDownloadPicture(OXContact pContact, Session pSession) {
		super(new StringBuilder(), pSession);
		String[] pathPlusQuery = pContact.getPictureServerPath().split("\\?");
		this.mPath = new StringBuilder(pathPlusQuery[0]);
		this.mQuery = pathPlusQuery[1];
	}

	@Override
	public OXResponse send(boolean pEncrypted) throws IOException,
			AuthenticationException, SSLException, JSONException {
		Uri url = createUrl(pEncrypted);
		final HttpRequestBase request = doCreateRequest(url.toString());
		return this.parse(sendOverNetworkToGetBitmap(request));
	}

	private Bitmap sendOverNetworkToGetBitmap(HttpRequestBase pRequest)
			throws IOException {
		Bitmap bitmap = mOXNetwork.executeToGetBitmap(pRequest,
				mSession.getLocalContext());
		return bitmap;
	}

	@Override
	public Builder doCreateGetParameterBuilder(Builder builder) {
		String[] queryParts = mQuery.split("&");
		HashMap<String, String> queryValues = new HashMap<String, String>();
		for(String querypart : queryParts) {
			String[] querySplit = querypart.split("=");
			queryValues.put(querySplit[0], querySplit[1]);
		}
		return new OXRequestAttribute(
				super.doCreateGetParameterBuilder(builder))
				.addFolder(queryValues.get("folder"))
				.addId(queryValues.get("id"))
				.addTimestamp(queryValues.get("timestamp")).done();
	}

	private OXResponse parse(Bitmap pBitmap) {
		return mResponseBuilder.parse(pBitmap);
	}
}
