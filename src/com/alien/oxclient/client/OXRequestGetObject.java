/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.client;

import android.net.Uri.Builder;

/**
 * This abstract request represents an openXchange "read-an-object request".
 * Therefore it gathers all informations needed to request an object in the
 * openXchange context.
 */
public abstract class OXRequestGetObject extends OXGetRequest {

	private String mOXId;

	public OXRequestGetObject(String pOXId, StringBuilder pPath,
			Session pSession) {
		super(pPath, pSession);
		this.mOXId = pOXId;
	}

	protected Builder doCreateGetParameterBuilder(Builder builder) {
		return super
				.doCreateGetParameterBuilder(new OXRequestAttribute(builder)
						.addAction("get").addId(mOXId).done());
	}
}
