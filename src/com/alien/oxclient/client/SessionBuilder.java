/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.client;

import org.apache.http.HttpException;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.mockinterfaces.OXAccountManager;

public class SessionBuilder {

	public static Session createNewSession(String pAuthTokenToInvalidate)
			throws SessionCreationFailedException {
		OXApplication.getAccountManager().invalidateAuthToken(
				OXAccountManager.ACCOUNT_TYPE, pAuthTokenToInvalidate);

		return SessionBuilder.createSession();
	}

	public static Session createSession() throws SessionCreationFailedException {
		try {
			String authToken = OXApplication
					.getAccountManager()
					.blockingGetAuthToken(OXAccountManager.AUTHTOKEN_TYPE, true);
			if(authToken != null) {
				return new Session(authToken, OXApplication.getAccountManager()
						.getAccountServer());
			} else {
				throw new HttpException(
						"Failed to retrieve authentication token. Authentication Token was null!");
			}
		} catch (Exception e) {
			throw new SessionCreationFailedException(e);
		}
	}
}
