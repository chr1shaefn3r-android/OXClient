/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.client;

import android.graphics.Bitmap;
import android.text.TextUtils;

/**
 * POJO-representation of a response of the openXchange-server.
 */
public class OXResponse {
	private Long mTimestamp;
	private String mData;
	private String mError;
	private String mErrorCode;
	private String mErrorParams;
	private Bitmap mBitmap;

	public OXResponse(Long pTimestamp, String pData, String pError,
			String pErrorParams, String pErrorCode) {
		this.mTimestamp = pTimestamp;
		this.mData = pData;
		this.mError = pError;
		this.mErrorCode = pErrorCode;
		this.mErrorParams = pErrorParams;
		this.mBitmap = null;
	}

	public OXResponse(Bitmap pBitmap) {
		this.mBitmap = pBitmap;
		this.mTimestamp = 0L;
		this.mData = "";
		this.mError = "";
		this.mErrorCode = "";
		this.mErrorParams = "";
	}

	public boolean hasTimestamp() {
		return this.hasPositivValue(mTimestamp);
	}

	public boolean hasData() {
		return !TextUtils.isEmpty(mData) || (mBitmap != null);
	}

	public boolean hasError() {
		return !TextUtils.isEmpty(mError);
	}

	public boolean hasErrorParams() {
		return !TextUtils.isEmpty(mErrorParams);
	}

	public boolean hasErrorCode() {
		return !TextUtils.isEmpty(mErrorCode);
	}

	public Long getTimestamp() {
		return mTimestamp;
	}

	public String getData() {
		return mData;
	}

	public Bitmap getBitmap() {
		return mBitmap;
	}

	public String getError() {
		return mError;
	}

	public String getErrorParams() {
		return mErrorParams;
	}

	public String getErrorCode() {
		return mErrorCode;
	}

	private boolean hasPositivValue(Long pTimestamp) {
		if(pTimestamp != null && pTimestamp >= 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "OXResponse [mTimestamp=" + mTimestamp + ", mData=" + mData
				+ ", mError=" + mError + ", mErrorCode=" + mErrorCode
				+ ", mErrorParams=" + mErrorParams + ", mBitmap=" + mBitmap
				+ "]";
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		OXResponse other = (OXResponse) obj;
		if(mBitmap == null) {
			if(other.mBitmap != null) return false;
		} else if(!mBitmap.equals(other.mBitmap)) return false;
		if(mData == null) {
			if(other.mData != null) return false;
		} else if(!mData.equals(other.mData)) return false;
		if(mError == null) {
			if(other.mError != null) return false;
		} else if(!mError.equals(other.mError)) return false;
		if(mErrorCode == null) {
			if(other.mErrorCode != null) return false;
		} else if(!mErrorCode.equals(other.mErrorCode)) return false;
		if(mErrorParams == null) {
			if(other.mErrorParams != null) return false;
		} else if(!mErrorParams.equals(other.mErrorParams)) return false;
		if(mTimestamp == null) {
			if(other.mTimestamp != null) return false;
		} else if(!mTimestamp.equals(other.mTimestamp)) return false;
		return true;
	}

}
