/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.action;

import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.client.OXGetRequest;
import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.client.OXRequestGetContact;
import com.alien.oxclient.client.OXResponse;
import com.alien.oxclient.client.Session;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactJSONObjectBuilder;
import com.alien.oxclient.model.OXObject;

/**
 * This class represents the action to get a single contact from the openXchange
 * Server. Therefore it uses the {@link OXGetRequest} to send a request. The
 * corresponding {@link OXResponse} and {@link OXObject} will be processed here.
 * 
 */
public class OXActionGetContact extends OXActionGetObject<OXContact> {

	private OXContact mResult;

	public OXActionGetContact(Context pContext, String pOXId) {
		super(pContext, pOXId);
	}

	@Override
	protected OXRequest doCreateRequest(Session pSession) {
		return new OXRequestGetContact(mOXId, mFolderId, pSession);
	}

	@Override
	protected void doCreateResultForSuccess(OXResponse pResponse)
			throws JSONException {
		super.doCreateResultForSuccess(pResponse);
		mResult = new OXContactJSONObjectBuilder().addContext(mContext)
				.addTimestamp(new Date(pResponse.getTimestamp()))
				.addFromJSONObject(new JSONObject(pResponse.getData()))
				.getOXObject();
		if(OXApplication.getDecision().shouldSyncPictures()) {
			OXApplication.getActionDependencyManager()
					.getOXActionGetPictureIntoContact(mResult).perform();
		}
	}

	@Override
	public boolean hasResult() {
		return(mResult != null);
	}

	public OXContact getResult() {
		return mResult;
	}
}
