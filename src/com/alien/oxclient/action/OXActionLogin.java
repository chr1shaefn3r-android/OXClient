/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.action;

import android.content.Context;

import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.client.OXRequestLogin;
import com.alien.oxclient.client.Session;
import com.alien.oxclient.client.SessionCreationFailedException;

/**
 * login - Module 'Login' (2.1)
 */
public class OXActionLogin<T> extends OXActionImpl<String> {

	private String mHost;
	private String mUsername;
	private String mPassword;

	public OXActionLogin(Context pContext, String pHost, String pUsername,
			String pPassword) {
		super(pContext);
		this.mHost = pHost;
		this.mUsername = pUsername;
		this.mPassword = pPassword;
	}

	@Override
	protected Session doCreateSession() throws SessionCreationFailedException {
		return null;
	}

	@Override
	protected OXRequest doCreateRequest(Session pSession) {
		return new OXRequestLogin(mHost, mUsername, mPassword);
	}

	public boolean hasAuthtoken() {
		return(mOXResponse != null && mOXResponse.hasData());
	}

	public String getAuthtoken() {
		return mOXResponse.getData();
	}

	@Override
	public boolean hasResult() {
		return hasAuthtoken();
	}

	@Override
	public String getResult() {
		return getAuthtoken();
	}
}
