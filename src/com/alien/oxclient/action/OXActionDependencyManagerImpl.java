/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.action;

import java.util.ArrayList;
import java.util.Date;

import android.content.Context;
import android.graphics.Bitmap;

import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXFolder;

public class OXActionDependencyManagerImpl implements OXActionDependencyManager {

	private Context mContext;

	public OXActionDependencyManagerImpl(Context pContext) {
		this.mContext = pContext;
	}

	public OXAction<String> getOXActionDeleteContact(OXContact pContact) {
		return new OXActionDeleteContact(mContext, pContact);
	}

	public OXAction<String> getOXActionUpdateContact(OXContact pContact) {
		return new OXActionUpdateContact(mContext, pContact);
	}

	public OXAction<String> getOXActionCreateContact(OXContact pContact) {
		return new OXActionCreateContact(mContext, pContact);
	}

	public OXAction<OXContact> getOXActionGetContact(String pOXId) {
		return new OXActionGetContact(mContext, pOXId);
	}

	public OXAction<Bitmap> getOXActionGetPictureIntoContact(OXContact pContact) {
		return new OXActionGetPictureIntoContact(mContext, pContact);
	}

	public OXAction<OXFolder> getOXActionGetContactFolder() {
		return new OXActionGetContactFolder(mContext);
	}

	public OXAction<ArrayList<String>> getOXActionGetRootFolderList() {
		return new OXActionGetRootFolderList(mContext);
	}

	public OXAction<String> getOXActionLogin(String pHost, String pUsername,
			String pPassword) {
		return new OXActionLogin<String>(mContext, pHost, pUsername, pPassword);
	}

	public OXAction<ArrayList<OXContact>> getOXActionGetAllContacts() {
		return new OXActionGetAllContacts(mContext);
	}

	public OXAction<ArrayList<String>> getOXActionGetAllContactIds() {
		return new OXActionGetAllContactIds(mContext);
	}

	public OXAction<ArrayList<OXContact>> getOXActionGetUpdatedContacts(
			Date pLastUpdated) {
		return new OXActionGetUpdatedContacts(mContext, pLastUpdated);
	}
}
