/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.action;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;

import com.alien.oxclient.client.OXResponse;

public abstract class OXActionGetAllIds extends
		OXActionGetObjects<ArrayList<String>> {

	protected String mIdColumn = "1";
	protected ArrayList<String> mResult;

	public OXActionGetAllIds(Context pContext) {
		super(pContext);
	}

	@Override
	protected void doCreateResultForSuccess(OXResponse pResponse)
			throws JSONException {
		super.doCreateResultForSuccess(pResponse);
		JSONArray idArray = new JSONArray(pResponse.getData());
		mResult = new ArrayList<String>();
		for(int i = 0; i < idArray.length(); i++) {
			mResult.add(idArray.getJSONArray(i).getString(0));
		}
	}

	@Override
	public boolean hasResult() {
		return(mResult != null && mResult.size() > 0);
	}

	@Override
	public ArrayList<String> getResult() {
		return mResult;
	}
}
