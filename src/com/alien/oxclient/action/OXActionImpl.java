/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.action;

import java.util.Date;

import org.apache.http.auth.AuthenticationException;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.text.TextUtils;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.client.OXResponse;
import com.alien.oxclient.client.Session;
import com.alien.oxclient.client.SessionBuilder;
import com.alien.oxclient.client.SessionCreationFailedException;
import com.alien.oxclient.model.OXObject;

/**
 * This class will utilize {@link OXRequest}, {@link OXResponse} and
 * {@link OXObject} to perform a certain action, like "Create a Contact",
 * "Delete a task". It is also supposed perform the check, whether a failed
 * request could be fixed and resend.
 */
public abstract class OXActionImpl<T> implements OXAction<T> {

	protected Context mContext;
	protected String mErrorMessage;
	private String mWarningMessage;
	private boolean mForceNewSession = false;
	protected OXResponse mOXResponse;

	public OXActionImpl(Context pContext) {
		this.mContext = pContext;
	}

	public void perform() {
		this.perform(OXApplication.getAccountManager().getEncrypted());
	}

	public void perform(boolean pEncrypted) {
		int retryCount = -1;
		boolean isResolveableError = false;
		do {
			retryCount++;
			try {
				Session session = doCreateSession();
				if(mForceNewSession && session != null) {
					session = doCreateNewSession(session.getAuthToken());
					isResolveableError = mForceNewSession = false;
				}
				OXRequest request = doCreateRequest(session);
				mOXResponse = request.send(pEncrypted);
				if(!mOXResponse.hasData()) {
					if(this.checkIfResolveableError(mOXResponse)) {
						this.changeSetupToFixError(mOXResponse);
						isResolveableError = true;
					} else {
						isResolveableError = false;
					}
					doCreateResultForError(mOXResponse);
				} else {
					doCreateResultForSuccess(mOXResponse);
					isResolveableError = false;
				}
			} catch (AuthenticationException e) {
				isResolveableError = mForceNewSession = true;
			} catch (Exception e) {
				mErrorMessage = e.getClass().getSimpleName() + ": "
						+ e.getLocalizedMessage();
				mOXResponse = createOXResponseForException(mErrorMessage);
				doCreateResultForError(mOXResponse);
			}

		} while (retryCount < 2 && isResolveableError);
	}

	private boolean checkIfResolveableError(OXResponse pResponse) {
		// SES-0206: Your session was invalidated. Please try again.
		// SES-0205: Client IP has changed
		// SES-0203: Session expired!
		if(pResponse.hasErrorCode()
				&& (pResponse.getErrorCode().compareTo("SES-0205") == 0
						|| pResponse.getErrorCode().compareTo("SES-0203") == 0 || pResponse
						.getErrorCode().compareTo("SES-0206") == 0)) {
			return true;
		} else {
			return false;
		}
	}

	private void changeSetupToFixError(OXResponse pResponse) {
		if(pResponse.getErrorCode().compareTo("SES-0205") == 0
				|| pResponse.getErrorCode().compareTo("SES-0203") == 0
				|| pResponse.getErrorCode().compareTo("SES-0206") == 0) {
			mForceNewSession = true;
		}
	}

	protected abstract OXRequest doCreateRequest(Session pSession);

	protected Session doCreateSession() throws SessionCreationFailedException {
		return SessionBuilder.createSession();
	}

	protected Session doCreateNewSession(String pAuthTokenToInvalidate)
			throws SessionCreationFailedException {
		return SessionBuilder.createNewSession(pAuthTokenToInvalidate);
	}

	protected void doCreateResultForError(OXResponse pResponse) {
		mErrorMessage = OXActionImpl.errorToString(pResponse);
	}

	private OXResponse createOXResponseForException(String pExceptionMessage) {
		return new OXResponse(new Date().getTime(), null, pExceptionMessage,
				null, null);
	}

	protected void doCreateResultForSuccess(OXResponse pResponse)
			throws JSONException {
		if(mOXResponse.hasError()) {
			mWarningMessage = OXActionImpl.errorToString(pResponse);
		}
	}

	public abstract boolean hasResult();

	public abstract T getResult();

	public boolean hasErrorMessage() {
		return !TextUtils.isEmpty(mErrorMessage);
	}

	public String getErrorMessage() {
		return mErrorMessage;
	}

	public boolean hasWarningMessage() {
		return !TextUtils.isEmpty(mWarningMessage);
	}

	public String getWarningMessage() {
		return mWarningMessage;
	}

	public boolean hasTimestamp() {
		return(mOXResponse != null && mOXResponse.hasTimestamp());
	}

	public Long getTimestamp() {
		return mOXResponse.getTimestamp();
	}

	public static String errorToString(OXResponse pResponse) {
		String error = null;
		try {
			if(pResponse.hasErrorParams()) {
				String[] array = pResponse.getError()
						.replaceAll("%\\d\\$([sd])", "%$1").split("%");
				error = array[0];
				for(int i = 1, j = 0; i < array.length; i++, j++) {
					error = error
							+ String.format("%" + array[i], new JSONArray(
									pResponse.getErrorParams()).getString(j));
				}
			} else {
				error = pResponse.getError();
			}
		} catch (JSONException e) {
			error = "[OXAction->errorToString] JSONException: "
					+ e.getMessage();
		}
		return error;
	}
}
