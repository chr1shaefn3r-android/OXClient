/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.action;

import android.content.Context;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.client.OXRequestCreateContact;
import com.alien.oxclient.client.OXRequestCreateContactWithImage;
import com.alien.oxclient.client.Session;
import com.alien.oxclient.model.OXContact;

public class OXActionCreateContact extends OXActionCreateObject<String> {

	private OXContact mContact;

	public OXActionCreateContact(Context pContext, OXContact pContact) {
		super(pContext);
		this.mContact = pContact;
	}

	@Override
	protected OXRequest doCreateRequest(Session pSession) {
		if(OXApplication.getDecision().shouldSyncPictures()
				&& mContact.getPicture() != null) {
			return new OXRequestCreateContactWithImage(mContact, mFolderId,
					pSession);
		} else {
			return new OXRequestCreateContact(mContact, mFolderId, pSession);
		}
	}
}
