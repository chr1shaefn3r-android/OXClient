/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.action;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.client.OXResponse;

public abstract class OXActionCreateObject<T> extends
		OXActionImpl<java.lang.String> {

	protected String mFolderId;
	protected String mOxIdOfNewContact;

	public OXActionCreateObject(Context pContext) {
		super(pContext);
		mFolderId = OXApplication.getAccountManager().getContactsFolderId();
	}

	@Override
	protected void doCreateResultForSuccess(OXResponse pResponse)
			throws JSONException {
		super.doCreateResultForSuccess(pResponse);
		JSONObject jsonObject = new JSONObject(pResponse.getData());
		mOxIdOfNewContact = jsonObject.getString("id");
	}

	@Override
	public boolean hasResult() {
		return !TextUtils.isEmpty(mOxIdOfNewContact);
	}

	@Override
	public String getResult() {
		return mOxIdOfNewContact;
	}
}
