/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.action;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.R;
import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.client.OXRequestGetRootFolderList;
import com.alien.oxclient.client.OXResponse;
import com.alien.oxclient.client.Session;

/**
 * getRootDirectoryFolderList - Module 'Folders' (4.2)
 */
public class OXActionGetRootFolderList extends OXActionImpl<ArrayList<String>> {

	private ArrayList<String> mResult;

	public OXActionGetRootFolderList(Context pContext) {
		super(pContext);
	}

	@Override
	protected OXRequest doCreateRequest(Session pSession) {
		return new OXRequestGetRootFolderList("300", pSession);
	}

	@Override
	protected void doCreateResultForSuccess(OXResponse pResponse)
			throws JSONException {
		super.doCreateResultForSuccess(pResponse);
		mResult = new ArrayList<String>();
		JSONArray data = new JSONArray(pResponse.getData());
		for(int i = 0; i < data.length(); i++)
			mResult.add(data.getJSONArray(i).getString(0));
	}

	@Override
	protected void doCreateResultForError(OXResponse pResponse) {
		super.doCreateResultForError(pResponse);
		// TODO: Kann hier rausgeschmissen werden sobald MessageView
		// extrahiert und wiederverwendbar ist. Dann uebernimmt sowas
		// naemlich der OXActionAsyncTask
		mResult = new ArrayList<String>();
		if(OXApplication.getUtils().isOnline()) {
			mResult.add(this.getErrorMessage());
		} else {
			mResult.add(mContext.getString(R.string.error_no_connection));
		}
	}

	@Override
	public boolean hasResult() {
		return(mResult != null && mResult.size() > 0);
	}

	public ArrayList<String> getResult() {
		return mResult;
	}
}
