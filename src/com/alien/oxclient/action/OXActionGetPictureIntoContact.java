/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.action;

import org.json.JSONException;

import android.content.Context;
import android.graphics.Bitmap;

import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.client.OXRequestDownloadPicture;
import com.alien.oxclient.client.OXResponse;
import com.alien.oxclient.client.Session;
import com.alien.oxclient.model.OXContact;

public class OXActionGetPictureIntoContact extends OXActionImpl<Bitmap> {

	private OXContact mContact;
	private Bitmap mResult;

	public OXActionGetPictureIntoContact(Context pContext, OXContact pContact) {
		super(pContext);
		this.mContact = pContact;
	}

	@Override
	protected OXRequest doCreateRequest(Session pSession) {
		return new OXRequestDownloadPicture(mContact, pSession);
	}

	@Override
	protected void doCreateResultForSuccess(OXResponse pResponse)
			throws JSONException {
		super.doCreateResultForSuccess(pResponse);
		this.mResult = pResponse.getBitmap();
		this.mContact.setPicture(mResult);
	}

	@Override
	public boolean hasResult() {
		return(mResult != null);
	}

	@Override
	public Bitmap getResult() {
		return mResult;
	}

}
