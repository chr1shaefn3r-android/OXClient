/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.action;

import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.client.OXResponse;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactJSONArrayBuilder;

public abstract class OXActionGetContacts extends
		OXActionGetObjects<ArrayList<OXContact>> {

	protected ArrayList<OXContact> mResult;
	protected String mColumns;

	public OXActionGetContacts(Context pContext) {
		super(pContext);
		this.mColumns = OXApplication.getUtils().constructColumns();
	}

	@Override
	protected void doCreateResultForSuccess(OXResponse pResponse)
			throws JSONException {
		super.doCreateResultForSuccess(pResponse);
		Date date = new Date(pResponse.getTimestamp());
		mResult = new ArrayList<OXContact>();
		JSONArray array = new JSONArray(pResponse.getData());
		for(int i = 0; i < array.length(); i++) {
			OXContact contact = new OXContactJSONArrayBuilder()
					.addContext(mContext).addTimestamp(date)
					.addFromJSONArray(array.getJSONArray(i)).getOXObject();
			mResult.add(contact);
			if(OXApplication.getDecision().shouldSyncPictures()) {
				OXApplication.getActionDependencyManager()
						.getOXActionGetPictureIntoContact(contact).perform();
			}
		}
	}

	public boolean hasResult() {
		return mResult != null;
	}

	public ArrayList<OXContact> getResult() {
		return mResult;
	}
}
