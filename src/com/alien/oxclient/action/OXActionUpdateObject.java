/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.action;

import java.util.Date;

import org.json.JSONException;

import android.content.Context;
import android.text.TextUtils;

import com.alien.oxclient.client.OXResponse;
import com.alien.oxclient.model.OXObject;

public abstract class OXActionUpdateObject extends OXActionImpl<String> {

	protected String mOXId;
	protected Date mDate;
	protected String mFolderId;
	protected String mResult;

	public OXActionUpdateObject(Context pContext, OXObject pOXObject,
			String pFolderId, Date pDate) {
		super(pContext);
		this.mOXId = pOXObject.getId();
		this.mDate = pDate;
		this.mFolderId = pFolderId;
	}

	public OXActionUpdateObject(Context pContext, OXObject pOXObject,
			String pFolderId) {
		this(pContext, pOXObject, pFolderId, new Date());
	}

	@Override
	protected void doCreateResultForSuccess(OXResponse pResponse)
			throws JSONException {
		super.doCreateResultForSuccess(pResponse);
		this.mResult = pResponse.getData();
	}

	@Override
	public boolean hasResult() {
		return !TextUtils.isEmpty(mResult);
	}

	@Override
	public String getResult() {
		return mResult;
	}
}
