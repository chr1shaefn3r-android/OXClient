/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.action;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.client.OXRequest;
import com.alien.oxclient.client.OXRequestGetFolderList;
import com.alien.oxclient.client.OXResponse;
import com.alien.oxclient.client.Session;
import com.alien.oxclient.mockinterfaces.OXAccountManager;
import com.alien.oxclient.model.OXFolder;

public class OXActionGetContactFolder extends OXActionImpl<OXFolder> {

	protected String mParent;
	protected String mColumns;
	private OXFolder mResult;

	public OXActionGetContactFolder(Context pContext) {
		super(pContext);
		this.mParent = "1"; // TODO: Investigate why this is actually working.
		this.mColumns = "1,300,301";
	}

	@Override
	protected OXRequest doCreateRequest(Session pSession) {
		return new OXRequestGetFolderList(pSession, mParent, mColumns);
	}

	@Override
	protected void doCreateResultForSuccess(OXResponse pResponse)
			throws JSONException {
		super.doCreateResultForSuccess(pResponse);
		JSONArray array = new JSONArray(pResponse.getData());
		for(int i = 0; i < array.length(); i++) {
			JSONArray folder = array.getJSONArray(i);
			if(folder.getString(2).equals(OXFolderModules.CONTACTS.getName())) mResult = new OXFolder(
					folder.getString(0), folder.getString(1),
					OXFolderModules.valueOf(folder.getString(2).toUpperCase()));
		}
		OXAccountManager accountManager = OXApplication.getAccountManager();
		accountManager.setContactsFolder(mResult.getTitle());
		accountManager.setContactsFolderId(mResult.getId());
	}

	public OXFolder getResult() {
		return mResult;
	}

	@Override
	public boolean hasResult() {
		return(mResult != null);
	}

}
