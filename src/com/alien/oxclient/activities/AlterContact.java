/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.activities;

import java.util.ArrayList;

import org.json.JSONException;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.OperationApplicationException;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TabHost;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.OXConstants;
import com.alien.oxclient.R;
import com.alien.oxclient.fragments.AlterContactFragment;
import com.alien.oxclient.fragments.AlterContactFragment.OnAlterContactInterface;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.ui.Tab;
import com.alien.oxclient.ui.TabsAdapter;

public abstract class AlterContact extends FragmentActivity implements
		OnAlterContactInterface {

	private TabHost mTabHost;
	protected OXContact mContact;
	private boolean mPhoneUI;
	private boolean mWriteContact = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.altercontact);

		// Set the attribute "windowSoftInputMode" to "adjustPan" to fix the
		// EditText-in-ListView-Focus-Bug
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

		// Retain saved user-instance
		Object contact = getLastCustomNonConfigurationInstance();
		if(contact != null && contact instanceof OXContact) {
			mContact = (OXContact) contact;
			mContact.setContext(this);
		} else {
			mContact = initializeContact();
		}

		ViewPager viewPager = (ViewPager) findViewById(R.id.altercontact_pager);
		mPhoneUI = (viewPager != null);

		// Liste aller Tabs anlegen
		ArrayList<Tab> tabList = new ArrayList<Tab>();
		Bundle args = new Bundle();
		args.putInt(OXConstants.ALTERCONTACT_ARGS_KEY_GROUP,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		args.putString(OXConstants.ALTERCONTACT_ARGS_LABEL,
				getString(R.string.altercontact_tab_work));
		tabList.add(new Tab(this, getString(R.string.altercontact_tab_work),
				AlterContactFragment.class, R.drawable.ic_altercontact, args));
		args = new Bundle();
		args.putInt(OXConstants.ALTERCONTACT_ARGS_KEY_GROUP,
				OXConstants.ALTERCONTACT_GROUP_HOME);
		// TODO: Evtl. mit einem auf @dimen/-Wert besser bedient
		args.putBoolean(OXConstants.ALTERCONTACT_ARGS_SHOW_CALENDARVIEW,
				mPhoneUI);
		args.putString(OXConstants.ALTERCONTACT_ARGS_LABEL,
				getString(R.string.altercontact_tab_home));
		tabList.add(new Tab(this, getString(R.string.altercontact_tab_home),
				AlterContactFragment.class, R.drawable.ic_altercontact, args));
		args = new Bundle();
		args.putInt(OXConstants.ALTERCONTACT_ARGS_KEY_GROUP,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		args.putString(OXConstants.ALTERCONTACT_ARGS_LABEL,
				getString(R.string.altercontact_tab_other));
		tabList.add(new Tab(this, getString(R.string.altercontact_tab_other),
				AlterContactFragment.class, R.drawable.ic_altercontact, args));
		args = new Bundle();
		args.putInt(OXConstants.ALTERCONTACT_ARGS_KEY_GROUP,
				OXConstants.ALTERCONTACT_GROUP_PIC);
		args.putString(OXConstants.ALTERCONTACT_ARGS_LABEL,
				getString(R.string.altercontact_tab_pic));
		tabList.add(new Tab(this, getString(R.string.altercontact_tab_pic),
				AlterContactFragment.class, R.drawable.ic_altercontact, args));

		if(mPhoneUI) {
			// Phone setup
			mTabHost = (TabHost) findViewById(android.R.id.tabhost);
			mTabHost.setup();

			TabsAdapter tabsAdapter = new TabsAdapter(this, mTabHost, viewPager);
			// Liste der Tabs dem TabHost bekanntmachen
			for(Tab tab : tabList) {
				tab.addTab(tabsAdapter, mTabHost);
			}

			if(savedInstanceState != null) {
				mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
			}
		} else {
			LinearLayout rootLayout = (LinearLayout) findViewById(R.id.altercontact_linearlayout);
			LayoutParams lp = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f);
			// Auf ein 7"-Zoll-Tablet passen maximal drei Spalten
			// evtl. in Zukunft noch mit 10"-Zoll-Tablet-Layout und mehr Spalten
			// TODO: Evtl. mit einem auf @dimen/-Wert besser bedient
			for(int i = 0; i < 3; i++) {
				LinearLayout ll = new LinearLayout(this);
				int id = i + 1;
				ll.setId(id);
				ll.setBackgroundResource(R.drawable.ox_frame);
				rootLayout.addView(ll, lp);
				tabList.get(i).addFragment(getSupportFragmentManager(), id);
			}
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if(mPhoneUI) outState.putString("tab", mTabHost.getCurrentTabTag());
	}

	@Override
	public Object onRetainCustomNonConfigurationInstance() {
		mContact.releaseContext();
		return mContact;
	}

	@Override
	protected void onPause() {
		super.onPause();
		if(mWriteContact && mContact.isChanged()) {
			// TODO: Should happen asynchronously, takes a moment with a
			// picture!
			writeContact();
			requestSync();
		}
	}

	protected abstract OXContact initializeContact();

	public void forbidWriteAndSync() {
		mWriteContact = false;
	}

	public void allowWriteAndSync() {
		mWriteContact = true;
	}

	protected void writeContact() {
		try {
			OXApplication.getOXContentResolver().applyBatch(
					ContactsContract.AUTHORITY, handleChanges());
			mContact.cleanDirtyFlag();
		} catch (Exception e) {
			throw new RuntimeException(e.getClass().getSimpleName() + ": "
					+ e.getLocalizedMessage());
		}
	}

	private void requestSync() {
		ContentResolver.requestSync(OXApplication.getAccountManager()
				.getOXAccount(), ContactsContract.AUTHORITY, new Bundle());
	}

	protected abstract ArrayList<ContentProviderOperation> handleChanges()
			throws NumberFormatException, JSONException, RemoteException,
			OperationApplicationException;

	public OXContact getContact() {
		return mContact;
	}

	/*****************************************************************************************************************************/
	/* --> .:Menu-Methoden:. <-- */
	/*****************************************************************************************************************************/
	@Override
	public boolean onCreateOptionsMenu(Menu pMenu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.altercontact, pMenu);
		return super.onCreateOptionsMenu(pMenu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem pMenuItem) {
		switch (pMenuItem.getItemId()) {
		case R.id.altercontact_menu_revert:
			mWriteContact = false;
			finish();
			break;
		case R.id.altercontact_menu_save:
			writeContact();
			finish();
			break;
		}
		return super.onOptionsItemSelected(pMenuItem);
	}
}
