/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.activities;

import java.util.ArrayList;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.alien.oxclient.OXActionAsyncTask;
import com.alien.oxclient.OXActionAsyncTask.OnTaskCompletedListener;
import com.alien.oxclient.OXApplication;
import com.alien.oxclient.R;
import com.alien.oxclient.action.OXAction;
import com.alien.oxclient.dialog.ProgressDialogFragment.OnProgressDialogInterface;

/**
 * Activity to show the user a list of contacts. So far only the top level
 * folders.
 */
public class FolderList extends FragmentActivity implements
		OnProgressDialogInterface, OnTaskCompletedListener, OnUnitTestHooks {
	private static final String TAG = "OXClient";

	private ListView mListView;
	private ArrayList<String> mFolders;
	private FoldersTask mFoldersTask;
	private boolean unitTestHookAfterFirstListRefresh = true;

	/**
	 * After a screen orientation change, we associate the current ( = newly
	 * created) activity with the restored asyncTask. On a clean activity
	 * startup, we create a new task and associate the current activity.
	 */
	@Override
	protected void onCreate(Bundle pSavedInstanceState) {
		super.onCreate(pSavedInstanceState);
		Log.d(TAG, "[=>][FoldersTask->onCreate] called");
		setContentView(android.R.layout.list_content);

		if(OXApplication.getUtils().hasActionBar(this)) getActionBar()
				.setDisplayHomeAsUpEnabled(true);

		mListView = (ListView) findViewById(android.R.id.list);

		Object retained = getLastCustomNonConfigurationInstance();
		if(retained instanceof FoldersTask) {
			Log.i(TAG, "Reclaiming previous background task.");
			mFoldersTask = (FoldersTask) retained;
			mFoldersTask.setActivity(this);
		} else {
			Log.i(TAG, "Creating new background task.");
			mFoldersTask = new FoldersTask(this);
			mFoldersTask.execute();
		}
		Log.d(TAG, "[<=][FoldersTask->onCreate] finished");
	}

	@Override
	protected void onPause() {
		if(mFoldersTask != null) {
			mFoldersTask.setActivity(null);
		}
		super.onPause();
	}

	@Override
	public Object onRetainCustomNonConfigurationInstance() {
		mFoldersTask.setActivity(null);
		return mFoldersTask;
	}

	@Override
	protected void onResume() {
		if(mFoldersTask != null) {
			mFoldersTask.setActivity(this);
		}
		super.onResume();
	}

	public void onTaskCompleted() {
		this.mFolders = mFoldersTask.getListItems();
		refreshListView();
		DialogFragment dialogFragment = (DialogFragment) getSupportFragmentManager()
				.findFragmentByTag(PROGRESS_DIALOG_FRAGMENT);
		if(dialogFragment != null) {
			dialogFragment.dismiss();
		}
	}

	private void refreshListView() {
		ListAdapter adapter = new ArrayAdapter<String>(FolderList.this,
				android.R.layout.simple_list_item_1, mFolders);
		mListView.setAdapter(adapter);
		this.unitTestHookAfterFirstListRefresh = false;
	}

	public boolean isFirstRoundtripFinished() {
		return unitTestHookAfterFirstListRefresh;
	}

	/*****************************************************************************************************************************/
	/* --> .:Menu-Methoden:. <-- */
	/*****************************************************************************************************************************/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.folderlist, menu);
		// Calling super after populating the menu is necessary here to ensure
		// that the
		// action bar helpers have a chance to handle this event.
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// app icon in Action Bar clicked; go up
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.menu_refresh:
			mFoldersTask = new FoldersTask(this);
			mFoldersTask.execute(false);
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/*****************************************************************************************************************************/
	/* --> .:DialogCallbacks:. <-- */
	/*****************************************************************************************************************************/
	// Callback from ProgressDialogFragment
	public void onProgressDialogCancel() {
		cancelFolders();
	}

	/*****************************************************************************************************************************/
	/* --> .:FoldersTask:. <-- */
	/*****************************************************************************************************************************/
	private class FoldersTask extends OXActionAsyncTask {

		private ArrayList<String> mFolders;

		public FoldersTask(Activity pActivity) {
			super(pActivity);
		}

		public ArrayList<String> getListItems() {
			return mFolders;
		}

		protected OXAction<ArrayList<String>> doInBackground(
				Boolean... pForceNew) {
			OXAction<ArrayList<String>> rootFolderListAction = OXApplication
					.getActionDependencyManager()
					.getOXActionGetRootFolderList();
			rootFolderListAction.perform();
			mFolders = rootFolderListAction.getResult();
			return rootFolderListAction;
		}

		@Override
		protected void onPreExecute() {
			// TODO: Fix for new setup
			// getActionBarHelper().setRefreshActionItemState(true);
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(OXAction<?> pAction) {
			// TODO: Fix for new setup
			// getActionBarHelper().setRefreshActionItemState(false);
			super.onPostExecute(pAction);
		}

		@Override
		protected FragmentManager getActivitysSupportFragmentManager() {
			return getSupportFragmentManager();
		}
	}

	/**
	 * Cancel FoldersTask
	 */
	private void cancelFolders() {
		if(mFoldersTask != null
				&& mFoldersTask.getStatus() == AsyncTask.Status.RUNNING) {
			mFoldersTask.cancel(false);
		}
		return;
	}
}
