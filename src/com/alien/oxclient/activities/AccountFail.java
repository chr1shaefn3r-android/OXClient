/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.activities;

import android.accounts.AccountAuthenticatorActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.alien.oxclient.R;

public class AccountFail extends AccountAuthenticatorActivity {
	private static final String TAG = "OXClient";

	@Override
	public void onCreate(Bundle pIcicle) {
		Log.d(TAG, "[OXClientAccountFail] onCreate()");
		super.onCreate(pIcicle);
		Toast.makeText(this, R.string.error_only_one_account, Toast.LENGTH_LONG)
				.show();
		finish();
	}
}
