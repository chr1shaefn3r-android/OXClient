/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.activities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.json.JSONException;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorDescription;
import android.accounts.OnAccountsUpdateListener;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.CharArrayBuffer;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.QuickContactBadge;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.alien.oxclient.ContactPhotoLoader;
import com.alien.oxclient.OXApplication;
import com.alien.oxclient.R;
import com.alien.oxclient.action.OXAction;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactRawContactIdBuilder;

/**
 * Activity to show a list of rawcontacts to the user. He can select which of
 * them he would like to upload to his server.
 */
public class UploadContacts extends FragmentActivity implements
		OnAccountsUpdateListener {

	// Constants:
	private static final String PROGRESSBAR_UPDATE = "progressbarupdate";
	private static final String LISTVIEW_UPDATE = "listviewupdate";
	private static final int SORT = Menu.FIRST;
	private static final String CUSTOM_ADDRESSBOOK = "Adress Book";
	private String[] mProjection = new String[] { Data.MIMETYPE, // 0
			StructuredName.DISPLAY_NAME, // 1
			RawContacts.Entity.RAW_CONTACT_ID, // 2
			RawContacts.ACCOUNT_TYPE, // 3
			RawContacts.ACCOUNT_NAME, // 4
			Contacts._ID, // 5
			Contacts.LOOKUP_KEY, // 6
			Contacts.PHOTO_ID // 7

	};

	// Variables:
	private Handler mHandler = new Handler();
	private SimpleCursorAdapter mCursorAdapter;
	private ListView mListView;
	private Button ButtonUploadContacts;
	private ArrayList<AccountData> mAccounts;
	private AccountAdapter mAccountAdapter;
	private Spinner mAccountSpinner;
	private AccountData mSelectedAccount;
	private ArrayList<Boolean> mCheckedPositions = new ArrayList<Boolean>();
	private LayoutInflater mInflater;
	private ContactPhotoLoader mPhotoLoader;
	private UploadContactsTask mUploadContactsTask;
	private PowerManager.WakeLock mWakeLock;
	private OXClientUploadContactsProgressDialog mDialog;

	private static final String TAG = "OXClient";

	@Override
	protected void onCreate(Bundle pSavedInstanceState) {
		super.onCreate(pSavedInstanceState);
		Log.d(TAG, "[=>][OXClientUploadContacts->onCreate] called");
		setContentView(R.layout.uploadcontacts);

		if(OXApplication.getUtils().hasActionBar(this)) getActionBar()
				.setDisplayHomeAsUpEnabled(true);

		mAccountSpinner = (Spinner) findViewById(R.id.uploadcontacts_accountSpinner);
		mListView = (ListView) findViewById(R.id.uploadcontacts_listView);
		ButtonUploadContacts = (Button) findViewById(R.id.uploadcontacts_button_upload);
		mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// Initialize PowerManager and WakeLock
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
				"My WakeLock");
		mWakeLock.acquire();

		// Create new PhotoLoader
		mPhotoLoader = new ContactPhotoLoader(this,
				R.drawable.ic_oxcontact_picture);

		// Prepare model for account spinner
		mAccounts = new ArrayList<AccountData>();
		mAccountAdapter = new AccountAdapter(this, mAccounts);
		mAccountSpinner.setAdapter(mAccountAdapter);

		Cursor cursor = getContentResolver()
				.query(ContactsContract.Data.CONTENT_URI,
						mProjection,
						Data.MIMETYPE + " = '"
								+ StructuredName.CONTENT_ITEM_TYPE + "'", null,
						StructuredName.DISPLAY_NAME);

		mCursorAdapter = new MyCursorAdapter(this,
				R.layout.list_item_2_multiple_choice, cursor, new String[] {},
				new int[] {});
		mListView.setAdapter(mCursorAdapter);
		mListView.setItemsCanFocus(false);
		mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		mListView.setFastScrollEnabled(true);
		refreshCheckedPositions();

		mListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				boolean currentlyChecked = mCheckedPositions.get(position);
				mCheckedPositions.set(position, !currentlyChecked);
			}
		});

		ButtonUploadContacts.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mDialog = new OXClientUploadContactsProgressDialog(
						UploadContacts.this);
				mDialog.show();
				startTask();
			}
		});

		// Register handlers for UI elements
		mAccountSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long j) {
				updateAccountSelection();
				if(mSelectedAccount.getType().compareTo(CUSTOM_ADDRESSBOOK) == 0) {
					String selection = "";
					for(int i = 0; i < mAccounts.size(); i++)
						selection = selection.concat(RawContacts.ACCOUNT_TYPE
								+ " != '" + mAccounts.get(i).getType()
								+ "' AND " + RawContacts.ACCOUNT_NAME + " != '"
								+ mAccounts.get(i).getName() + "' AND ");
					selection = selection.concat(Data.MIMETYPE + " = '"
							+ StructuredName.CONTENT_ITEM_TYPE + "'");
					refreshListView(selection);
				} else {
					refreshListView(Data.MIMETYPE + " = '"
							+ StructuredName.CONTENT_ITEM_TYPE + "' AND "
							+ RawContacts.ACCOUNT_TYPE + " = '"
							+ mSelectedAccount.getType() + "' AND "
							+ RawContacts.ACCOUNT_NAME + " = '"
							+ mSelectedAccount.getName() + "'");
				}
			}

			public void onNothingSelected(AdapterView<?> view) {
				// We don't need to worry about nothing being selected, since
				// Spinners don't allow this.
			}
		});

		// Prepare the system account manager. On registering the listener
		// below, we also ask for
		// an initial callback to pre-populate the account list.
		AccountManager.get(this).addOnAccountsUpdatedListener(this, null, true);

		registerForContextMenu(mListView); // zeige popup menue fuer Liste

		Log.d(TAG, "[<=][OXClientUploadContacts->onCreate] finished");
	}

	@Override
	protected void onResume() {
		if(!mWakeLock.isHeld()) {
			mWakeLock.acquire();
		}
		mPhotoLoader.resume();
		super.onResume();
	}

	@Override
	protected void onStop() {
		mWakeLock.release();
		super.onStop();
	}

	@Override
	public void onDestroy() {
		cancelUploadContacts();
		// Remove AccountManager callback
		AccountManager.get(this).removeOnAccountsUpdatedListener(this);
		mPhotoLoader.stop();
		super.onDestroy();
	}

	/**
	 * Start the uploadtask
	 */
	private void startTask() {
		Log.d(TAG, "[=>][OXClientUploadContacts->startTask] called");
		mDialog.setCurrentStatus(getString(R.string.uploadcontacts_prepare_upload));
		mDialog.hideButtons();
		final List<Long> listOfContactIDs = new ArrayList<Long>();
		new Thread(new Runnable() {
			public void run() {
				for(int i = 0; i < mCursorAdapter.getCount(); i++) {
					if(mCheckedPositions.get(i)) {
						Cursor cursor = (Cursor) mCursorAdapter.getItem(i);
						listOfContactIDs.add(cursor.getLong(2)); // 2=RawContacts.Entity.RAW_CONTACT_ID
					}
				}
				Log.d(TAG,
						"    [*][OXClientUploadContacts->startTask] mListOfContactIDs.size(): "
								+ listOfContactIDs.size());
				mHandler.post(new Runnable() {
					@SuppressWarnings("unchecked")
					public void run() {
						if(listOfContactIDs.size() > 0) {
							// Start AsyncTask:
							mUploadContactsTask = (UploadContactsTask) new UploadContactsTask()
									.execute(listOfContactIDs);
						} else {
							mDialog.addItem(getString(R.string.error_no_contact_to_upload));
							mDialog.setCurrentStatus(getString(R.string.error_no_contact_to_upload));
							mDialog.setIndeterminateFalse();
							mDialog.showButtons();
							mDialog.disableRetryButton();
						}
					}
				});
			}
		}).start();
		Log.d(TAG, "[<=][OXClientUploadContacts->startTask] finished");
	}

	/**
	 * Disable all items in the list
	 */
	private void disableAllListItems() {
		for(int i = 0; i < mCursorAdapter.getCount(); i++)
			mCheckedPositions.set(i, false);
		mListView.invalidateViews();
	}

	/**
	 * Enable all items in the list
	 */
	private void enableAllListItems() {
		for(int i = 0; i < mCursorAdapter.getCount(); i++)
			mCheckedPositions.set(i, true);
		mListView.invalidateViews();
	}

	/**
	 * Returns the position of the item specified by both parameters
	 * 
	 * @param pAccountType
	 *            The AccountType to search for
	 * @param pAccountName
	 *            The AccountName to search for
	 * @return The position of the item
	 */
	private Integer getItemPosition(String pAccountType, String pAccountName) {
		Log.v(TAG,
				"[=>][OXClientUploadContacts->getItemPosition] called, checking: "
						+ pAccountType + " / " + pAccountName);
		SpinnerAdapter spinnerAdapter = mAccountSpinner.getAdapter();
		Integer fallback = null;
		for(int i = 0; i < spinnerAdapter.getCount(); i++) {
			AccountData ad = (AccountData) spinnerAdapter.getItem(i);
			if(ad.getType().compareTo(pAccountType) == 0
					&& ad.getName().compareTo(pAccountName) == 0) {
				Log.v(TAG,
						"[<=][OXClientUploadContacts->getItemPosition] finished with i: "
								+ i);
				return i;
			} else if(ad.getType().compareTo(CUSTOM_ADDRESSBOOK) == 0
					&& ad.getName().compareTo(CUSTOM_ADDRESSBOOK) == 0) {
				// If we come past our special item => save position
				fallback = i;

			}
		}
		if(fallback != null) {
			Log.v(TAG,
					"[<=][OXClientUploadContacts->getItemPosition] finished with fallback: "
							+ fallback.toString());
			return fallback;
		} else {
			Log.v(TAG,
					"[<=][OXClientUploadContacts->getItemPosition] finished with null");
			return null;
		}
	}

	/*************************************************************************/
	/* Menu-Methoden */
	/*************************************************************************/
	@Override
	public boolean onCreateOptionsMenu(Menu pMenu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.uploadcontacts, pMenu);
		return super.onCreateOptionsMenu(pMenu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem pMenuItem) {
		switch (pMenuItem.getItemId()) {
		case android.R.id.home:
			// app icon in Action Bar clicked; go up
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.uploadcontacts_menu_enableall:
			enableAllListItems();
			break;
		case R.id.uploadcontacts_menu_disableall:
			disableAllListItems();
			break;
		case R.id.uploadcontacts_menu_showall:
			refreshListView(Data.MIMETYPE + " = '"
					+ StructuredName.CONTENT_ITEM_TYPE + "'");
			break;
		}
		return super.onOptionsItemSelected(pMenuItem);
	}

	/**
	 * Erzeuge Context(Popup)-Menue, durch langen Tap auf einen Listeintrag
	 * ausgewaehlt
	 */
	@Override
	public void onCreateContextMenu(ContextMenu pMenu, View pView,
			ContextMenuInfo pMenuInfo) {
		if(pView.equals(mListView)) {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) pMenuInfo;
			if(info.id >= 0) {
				pMenu.add(0, SORT, 0, R.string.uploadcontacts_contextmenu_sort);
				return;
			}
		}
		super.onCreateContextMenu(pMenu, pView, pMenuInfo);
	}

	/**
	 * Context-Menue fuer List-Eintrag wurde durch Tap-and-Hold ausgewaehlt
	 */
	@Override
	public boolean onContextItemSelected(MenuItem pMenuItem) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) pMenuItem
				.getMenuInfo();
		switch (pMenuItem.getItemId()) {
		case SORT:
			Cursor cursor = (Cursor) mCursorAdapter.getItem(info.position);
			refreshListView(Data.MIMETYPE
					+ " = '"
					+ StructuredName.CONTENT_ITEM_TYPE
					+ "' AND "
					+ RawContacts.ACCOUNT_TYPE
					+ " = '"
					+ cursor.getString(cursor
							.getColumnIndex(ContactsContract.RawContacts.ACCOUNT_TYPE))
					+ "' AND "
					+ RawContacts.ACCOUNT_NAME
					+ " = '"
					+ cursor.getString(cursor
							.getColumnIndex(ContactsContract.RawContacts.ACCOUNT_NAME))
					+ "'");
			Integer position = getItemPosition(
					cursor.getString(cursor
							.getColumnIndex(ContactsContract.RawContacts.ACCOUNT_TYPE)),
					cursor.getString(cursor
							.getColumnIndex(ContactsContract.RawContacts.ACCOUNT_NAME)));
			if(position != null) mAccountSpinner.setSelection(position);
			break;
		}
		return super.onContextItemSelected(pMenuItem);
	}

	/**
	 * Refresh the mListView, e.g. after resorting the list
	 */
	private void refreshListView(String pSelection) {
		Log.d(TAG, "[=>][OXClientUploadContacts->refreshListView] called");
		Cursor cursor = getContentResolver().query(
				ContactsContract.Data.CONTENT_URI, mProjection, pSelection,
				null, StructuredName.DISPLAY_NAME);
		mCursorAdapter = new MyCursorAdapter(this,
				R.layout.list_item_2_multiple_choice, cursor, new String[] {},
				new int[] {});
		mListView.setAdapter(mCursorAdapter);
		refreshCheckedPositions();
		Log.d(TAG, "[<=][OXClientUploadContacts->refreshListView] finished");
	}

	/**
	 * Refreshes the mCheckedPositions, e.g. after a new Account has been
	 * selected.
	 */
	private void refreshCheckedPositions() {
		mCheckedPositions.clear();
		for(int i = 0; i < mCursorAdapter.getCount(); i++)
			mCheckedPositions.add(false);
	}

	/**
	 * Update account selection. If NO_ACCOUNT is selected, then we prohibit
	 * inserting new contacts.
	 */
	private void updateAccountSelection() {
		// Read current account selection
		mSelectedAccount = (AccountData) mAccountSpinner.getSelectedItem();
	}

	public void onAccountsUpdated(Account[] pAccounts) {
		Log.d(TAG, "[=>][OXClientUploadContacts->onAccountsUpdated] called");
		// Clear out any old data to prevent duplicates
		mAccounts.clear();

		// Get account data from system
		AuthenticatorDescription[] accountTypes = AccountManager.get(this)
				.getAuthenticatorTypes();

		// Populate tables
		for(int i = 0; i < pAccounts.length; i++) {
			// The user may have multiple accounts with the same name, so we
			// need to construct a
			// meaningful display name for each.
			String systemAccountType = pAccounts[i].type;
			AuthenticatorDescription ad = getAuthenticatorDescription(
					systemAccountType, accountTypes);
			AccountData data = new AccountData(pAccounts[i].name, ad);
			mAccounts.add(data);
		}

		AuthenticatorDescription ad = new AuthenticatorDescription(
				CUSTOM_ADDRESSBOOK, CUSTOM_ADDRESSBOOK,
				R.drawable.ic_addressbook, R.drawable.ic_addressbook,
				R.drawable.ic_addressbook, 0);
		AccountData data = new AccountData(CUSTOM_ADDRESSBOOK, ad);
		mAccounts.add(data);

		Collections.sort(mAccounts);

		// Update the account spinner
		mAccountAdapter.notifyDataSetChanged();
		Log.d(TAG, "[<=][OXClientUploadContacts->onAccountsUpdated] finished");
	}

	/**
	 * Obtain the AuthenticatorDescription for a given account type.
	 * 
	 * @param pType
	 *            The account type to locate.
	 * @param pDictionary
	 *            An array of AuthenticatorDescriptions, as returned by
	 *            AccountManager.
	 * @return The description for the specified account type.
	 */
	private static AuthenticatorDescription getAuthenticatorDescription(
			String pType, AuthenticatorDescription[] pDictionary) {
		for(int i = 0; i < pDictionary.length; i++) {
			if(pDictionary[i].type.equals(pType)) { return pDictionary[i]; }
		}
		// No match found
		throw new RuntimeException("Unable to find matching authenticator");
	}

	/**
	 * A container class used to repreresent all known information about an
	 * account.
	 */
	private class AccountData implements Comparable<AccountData> {
		private String mName;
		private String mType;
		private CharSequence mTypeLabel;
		private Drawable mIcon;

		/**
		 * @param pName
		 *            The name of the account. This is usually the user's email
		 *            address or username.
		 * @param pDescription
		 *            The description for this account. This will be dictated by
		 *            the type of account returned, and can be obtained from the
		 *            system AccountManager.
		 */
		public AccountData(String pName, AuthenticatorDescription pDescription) {
			mName = pName;
			if(pDescription != null) {
				mType = pDescription.type;
				if(mType.compareTo(CUSTOM_ADDRESSBOOK) != 0) {
					// The type string is stored in a resource, so we need to
					// convert it into something
					// human readable.
					String packageName = pDescription.packageName;
					PackageManager pm = getPackageManager();

					if(pDescription.labelId != 0) {
						mTypeLabel = pm.getText(packageName,
								pDescription.labelId, null);
						if(mTypeLabel == null) { throw new IllegalArgumentException(
								"LabelID provided, but label not found"); }
					} else {
						mTypeLabel = "";
					}

					if(pDescription.iconId != 0) {
						mIcon = pm.getDrawable(packageName,
								pDescription.iconId, null);
						if(mIcon == null) { throw new IllegalArgumentException(
								"IconID provided, but drawable not " + "found"); }
					} else {
						mIcon = getResources().getDrawable(
								android.R.drawable.sym_def_app_icon);
					}
				} else {
					mTypeLabel = CUSTOM_ADDRESSBOOK;
					mIcon = getResources().getDrawable(
							R.drawable.ic_addressbook);
				}
			}
		}

		public String getName() {
			return mName;
		}

		public String getType() {
			return mType;
		}

		public CharSequence getTypeLabel() {
			return mTypeLabel;
		}

		public Drawable getIcon() {
			return mIcon;
		}

		public String toString() {
			return mName;
		}

		public int compareTo(AccountData pAnother) {
			final int EQUAL = 0;

			if(this == pAnother) return EQUAL;

			int comparison = this.mTypeLabel.toString().compareTo(
					pAnother.mTypeLabel.toString());
			if(comparison != EQUAL) return comparison;

			comparison = this.mName.compareTo(pAnother.mName);
			if(comparison != EQUAL) return comparison;

			comparison = this.mType.compareTo(pAnother.mType);
			if(comparison != EQUAL) return comparison;

			return EQUAL;
		}
	}

	/**
	 * Custom adapter used to display account icons and descriptions in the
	 * account spinner.
	 */
	private class AccountAdapter extends ArrayAdapter<AccountData> {
		public AccountAdapter(Context pContext,
				ArrayList<AccountData> pAccountData) {
			super(pContext, android.R.layout.simple_spinner_item, pAccountData);
			setDropDownViewResource(R.layout.account_entry);
		}

		public View getDropDownView(int pPosition, View pConvertView,
				ViewGroup pParent) {
			// Inflate a view template
			if(pConvertView == null) {
				LayoutInflater layoutInflater = getLayoutInflater();
				pConvertView = layoutInflater.inflate(R.layout.account_entry,
						pParent, false);
			}
			TextView firstAccountLine = (TextView) pConvertView
					.findViewById(R.id.uploadcontacts_accountentry_firstAccountLine);
			TextView secondAccountLine = (TextView) pConvertView
					.findViewById(R.id.uploadcontacts_accountentry_secondAccountLine);
			ImageView accountIcon = (ImageView) pConvertView
					.findViewById(R.id.uploadcontacts_accountentry_accountIcon);

			// Populate template
			AccountData data = getItem(pPosition);
			firstAccountLine.setText(data.getName());
			secondAccountLine.setText(data.getTypeLabel());
			Drawable icon = data.getIcon();
			if(icon == null) {
				icon = getResources().getDrawable(
						android.R.drawable.ic_menu_search);
			}
			accountIcon.setImageDrawable(icon);
			return pConvertView;
		}
	}

	/**
	 * Custom adapter used to display the list of contacts
	 */
	private class MyCursorAdapter extends SimpleCursorAdapter {

		private Cursor mCursor;

		public MyCursorAdapter(Context pContext, int pLayout, Cursor pCursor,
				String[] pFrom, int[] pTo) {
			super(pContext, pLayout, pCursor, pFrom, pTo);
			this.mCursor = pCursor;
		}

		// The Fastest way (reusing the view via convertView AND caching the
		// references via ViewHolder!):
		public View getView(int pPosition, View pConvertView, ViewGroup pParent) {
			ViewCache viewCache;

			mCursor.moveToPosition(pPosition);

			long photoId = 0;
			if(!mCursor.isNull(mCursor.getColumnIndex(Contacts.PHOTO_ID))) {
				photoId = mCursor.getLong(mCursor
						.getColumnIndex(Contacts.PHOTO_ID));
			}

			if(pConvertView == null) {
				pConvertView = mInflater.inflate(
						R.layout.list_item_2_multiple_choice, pParent, false);
				viewCache = new ViewCache();
				viewCache.displayName = (TextView) pConvertView
						.findViewById(R.id.uploadcontacts_listViewItem_displayName);
				viewCache.accountType = (TextView) pConvertView
						.findViewById(R.id.uploadcontacts_listViewItem_accountType);
				viewCache.checkBox = (CheckBox) pConvertView
						.findViewById(R.id.uploadcontacts_listViewItem_checkBox);
				viewCache.badge = (QuickContactBadge) pConvertView
						.findViewById(R.id.uploadcontacts_listViewItem_badge);
				pConvertView.setTag(viewCache);
			} else {
				viewCache = (ViewCache) pConvertView.getTag();
			}
			final long contactId = mCursor.getLong(mCursor
					.getColumnIndex(Contacts._ID));
			final String lookupKey = mCursor.getString(mCursor
					.getColumnIndex(Contacts.LOOKUP_KEY));
			// Set the displayName
			mCursor.copyStringToBuffer(
					mCursor.getColumnIndex(StructuredName.DISPLAY_NAME),
					viewCache.displayNameBuffer);
			int sizeDisplayName = viewCache.displayNameBuffer.sizeCopied;
			viewCache.displayName.setText(viewCache.displayNameBuffer.data, 0,
					sizeDisplayName);
			viewCache.checkBox.setChecked(mCheckedPositions.get(pPosition));

			// Set the accountType
			mCursor.copyStringToBuffer(
					mCursor.getColumnIndex(RawContacts.ACCOUNT_TYPE),
					viewCache.accountTypeBuffer);
			int sizeAccountType = viewCache.accountTypeBuffer.sizeCopied;
			viewCache.accountType.setText(viewCache.accountTypeBuffer.data, 0,
					sizeAccountType);

			viewCache.badge.assignContactUri(Contacts.getLookupUri(contactId,
					lookupKey));
			mPhotoLoader.loadPhoto(viewCache.badge, photoId);

			return pConvertView;
		}
	}

	final static class ViewCache {
		public QuickContactBadge badge;
		public TextView displayName;
		public TextView accountType;
		public CheckBox checkBox;
		public CharArrayBuffer displayNameBuffer = new CharArrayBuffer(128);
		public CharArrayBuffer accountTypeBuffer = new CharArrayBuffer(128);

	}

	/*****************************************************************************************************************************/
	/* --> .:UploadContactsTask:. <-- */
	/*****************************************************************************************************************************/
	private class UploadContactsTask extends
			AsyncTask<List<Long>, String, OXAction<String>> {
		private Date mTimeStamp;

		@Override
		protected OXAction<String> doInBackground(List<Long>... pParams) {
			Log.d(TAG, "[=>][UploadContactsTask->doInBackground] called");
			mDialog.setMaxProgress(pParams[0].size());
			for(final Long rawContactID : pParams[0]) {
				if(isCancelled()) {
					Log.d(TAG,
							"    [*][UploadContactsTask->doInBackground] isCanceled");
					return null;
				}
				publishProgress(PROGRESSBAR_UPDATE,
						getString(R.string.uploadcontacts_loading_oxuser));
				OXContact contact;
				try {
					contact = new OXContactRawContactIdBuilder()
							.addContext(UploadContacts.this)
							.addTimestamp(mTimeStamp)
							.addFromRowContactId(rawContactID).getOXObject();
				} catch (JSONException e) {
					throw new RuntimeException("JSONException: "
							+ e.getLocalizedMessage());
				}
				Log.d(TAG,
						"    [*][UploadContactsTask->doInBackground] PUTRequest ausfuehren");
				publishProgress(PROGRESSBAR_UPDATE,
						getString(R.string.uploadcontacts_execute_httprequest));
				OXAction<String> action = OXApplication
						.getActionDependencyManager().getOXActionCreateContact(
								contact);
				action.perform();
				publishProgress(PROGRESSBAR_UPDATE,
						getString(R.string.uploadcontacts_processing_data));
				if(action.hasResult()) {
					mDialog.incrementProgressByOne();
					publishProgress(LISTVIEW_UPDATE,
							contact.toPrintableString(), action.getResult());
				} else {
					return action;
				}
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			mDialog.setIndeterminateFalse();
			mTimeStamp = new Date();
			super.onPreExecute();
		}

		@Override
		protected void onProgressUpdate(String... pValues) {
			if(pValues[0].compareTo(LISTVIEW_UPDATE) == 0) {
				mDialog.addItem(getString(R.string.uploadcontacts_uploaded)
						+ ": '" + pValues[1] + "' / id: " + pValues[2]);
			} else if(pValues[0].compareTo(PROGRESSBAR_UPDATE) == 0) {
				mDialog.incrementSecondaryProgress(pValues[1]);
			}
			super.onProgressUpdate(pValues);
		}

		@Override
		protected void onPostExecute(OXAction<String> pAction) {
			if(pAction != null && pAction.hasErrorMessage()) {
				mDialog.cleanProgressBar();
				mDialog.setTextViewStatus(getString(R.string.error) + ": "
						+ pAction.getErrorMessage());
				mDialog.addItem(getString(R.string.error) + ": "
						+ pAction.getErrorMessage());
				mDialog.showButtons();
			} else {
				disableAllListItems();
				mDialog.disableRetryButton();
				mDialog.addItem(getString(R.string.uploadcontacts_upload_finished));
				mDialog.setCurrentStatus(getString(R.string.uploadcontacts_upload_finished));
				mDialog.setIndeterminateFalse();
				mDialog.showButtons();
			}
			Log.d(TAG, "[<=][UploadContactsTask->onPostExecute] finished");
		}
	}

	/**
	 * Cancel UploadContactsTask
	 */
	private void cancelUploadContacts() {
		Log.d(TAG, "[=>][OXClientUploadContacts->cancelUploadContacts] called");
		if(mUploadContactsTask != null
				&& mUploadContactsTask.getStatus() == AsyncTask.Status.RUNNING) {
			Log.d(TAG,
					"[=>][OXClientUploadContacts->cancelUploadContacts] uploadContactsTask.cancel(true)!");
			mUploadContactsTask.cancel(true);
			mUploadContactsTask = null;
		}
		Log.d(TAG,
				"[<=][OXClientUploadContacts->cancelUploadContacts] finished");
		return;
	}

	/**
	 * Custom Dialogclass to show the progress of uploading the selected
	 * contacts
	 */
	private class OXClientUploadContactsProgressDialog extends Dialog {

		private Button mButtonRetry;
		private Button mButtonOk;
		private ListView mListView;
		private ProgressBar mProgressBar;
		private TextView mTextViewStatus;
		private Context mContext;
		private ArrayList<String> mProgressItems;
		private ArrayAdapter<String> mProgressAdapter;

		public OXClientUploadContactsProgressDialog(Context pContext) {
			super(pContext);
			this.mContext = pContext;
		}

		@Override
		protected void onCreate(Bundle pSavedInstanceState) {
			super.onCreate(pSavedInstanceState);
			setContentView(R.layout.uploadcontacts_progressdialog);

			mButtonRetry = (Button) findViewById(R.id.uploadcontacts_progressdialog_button_retry);
			mButtonOk = (Button) findViewById(R.id.uploadcontacts_progressdialog_button_ok);
			mListView = (ListView) findViewById(R.id.uploadcontacts_progressdialog_listView);
			mProgressBar = (ProgressBar) findViewById(R.id.uploadcontacts_progressdialog_progressBar);
			mTextViewStatus = (TextView) findViewById(R.id.uploadcontacts_progressdialog_textView_status);

			setTitle(getString(R.string.uploadcontacts_dialog_title));

			mProgressItems = new ArrayList<String>();
			mProgressItems
					.add(getString(R.string.uploadcontacts_dialog_startup));

			mProgressAdapter = new ArrayAdapter<String>(mContext,
					R.layout.list_item_upload_contacts_progress_dialog,
					mProgressItems);

			mListView.setAdapter(mProgressAdapter);

			mProgressBar.setIndeterminate(true);
			mProgressBar.invalidate();

			setOnCancelListener(new OnCancelListener() {
				public void onCancel(DialogInterface dialog) {
					cancelUploadContacts();
				}
			});

			mButtonRetry.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					cancelUploadContacts();
					startTask();
				}
			});
			mButtonOk.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismiss();
				}
			});
		}

		public void setTextViewStatus(String pMessage) {
			mTextViewStatus.setText(pMessage);
		}

		public void setIndeterminateFalse() {
			mProgressBar.setIndeterminate(false);
			cleanProgressBar();
			mProgressBar.invalidate();
		}

		public void cleanProgressBar() {
			mProgressBar.setSecondaryProgress(0);
			mProgressBar.setProgress(0);
		}

		public void setMaxProgress(int pAmount) {
			mProgressBar.setMax(pAmount);
		}

		public void incrementProgressByOne() {
			mProgressBar.incrementProgressBy(1);
		}

		/**
		 * Increment the secondary progress by one and the status text
		 * 
		 * @param pMessage
		 *            The message to be shown in the status textview
		 */
		public void incrementSecondaryProgress(String pMessage) {
			int amount = 3; // es gibt zwei Teilschritte bis ein Kontakt
							// hochgeladen ist
			if(mProgressBar.getMax() == mProgressBar.getSecondaryProgress()) mProgressBar
					.setSecondaryProgress(mProgressBar.getMax() / amount);
			else mProgressBar.incrementSecondaryProgressBy(mProgressBar
					.getMax() / amount);
			mTextViewStatus.setText(pMessage);
		}

		/**
		 * Set indeterminate progressbar and status text
		 * 
		 * @param pMessage
		 *            The message to be shown in the status textview
		 */
		public void setCurrentStatus(String pMessage) {
			if(!mProgressBar.isIndeterminate()) {
				mProgressBar.setIndeterminate(true);
				mProgressBar.invalidate();
			}
			mTextViewStatus.setText(pMessage);
		}

		/**
		 * Make bottom located buttons visible
		 */
		public void showButtons() {
			mButtonRetry.setVisibility(Button.VISIBLE);
			mButtonOk.setVisibility(Button.VISIBLE);
		}

		/**
		 * Make bottom located buttons invisible
		 */
		public void hideButtons() {
			mButtonRetry.setVisibility(Button.GONE);
			mButtonOk.setVisibility(Button.GONE);
		}

		/**
		 * disableRetryButton() - disable the left retry-Button
		 */
		public void disableRetryButton() {
			mButtonRetry.setEnabled(false);
		}

		/**
		 * Add another item to the list and notifiy the adapter about the change
		 * 
		 * @param pMessage
		 *            The message to be added
		 */
		public void addItem(String pMessage) {
			mProgressAdapter.add(pMessage);
			mProgressAdapter.notifyDataSetChanged();
		}
	};
}