/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.activities;

import java.util.ArrayList;

import org.json.JSONException;

import android.content.ContentProviderOperation;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactBuilder;

public class AddContact extends AlterContact {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(OXApplication.getUtils().hasActionBar(this)) getActionBar()
				.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	protected OXContact initializeContact() {
		return new OXContactBuilder().addContext(this).getOXObject();
	}

	@Override
	protected ArrayList<ContentProviderOperation> handleChanges()
			throws JSONException {
		return mContact.insertContact(false);
	}

	/*************************************************************************/
	/* Menu-Methoden */
	/*************************************************************************/
	@Override
	public boolean onOptionsItemSelected(MenuItem pMenuItem) {
		switch (pMenuItem.getItemId()) {
		case android.R.id.home:
			// app icon in Action Bar clicked; go up
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(pMenuItem);
	}
}
