/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 *
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 *
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.activities;

import android.app.NotificationManager;
import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.R;
import com.alien.oxclient.fragments.ConflictFragment;
import com.alien.oxclient.fragments.ConflictsFragment.OnItemSelectedListener;
import com.alien.oxclient.fragments.ConflictsFragment.OnSearchInformationAvailable;
import com.alien.oxclient.provider.SyncConflicts;
import com.alien.oxclient.syncadapter.ContactManager;

public class SolveConflicts extends FragmentActivity implements
		OnItemSelectedListener, OnSearchInformationAvailable {

	private boolean mDualPane = false;
	private static boolean hasToBeRestarted = false; // Damn workaround
	private String mConstraint;

	@Override
	protected void onCreate(Bundle pSavedInstanceState) {
		super.onCreate(pSavedInstanceState);
		setContentView(R.layout.solveconflicts);

		if(OXApplication.getUtils().hasActionBar(this)) getActionBar()
				.setDisplayHomeAsUpEnabled(true);

		View view = findViewById(R.id.solveconflicts_resolve_frag);
		if(view != null) {
			mDualPane = true;
		} else {
			mDualPane = false;
		}

		// If we were started by notification -> delete it
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notificationManager.cancel(ContactManager.NOTIFICATION_ID);

		Intent intent = getIntent();
		if(Intent.ACTION_SEARCH.equals(intent.getAction())) {
			mConstraint = intent.getStringExtra(SearchManager.QUERY);
		} else if(Intent.ACTION_VIEW.equals(intent.getAction())) {
			Cursor c = getContentResolver().query(
					SyncConflicts.CONTENT_URI,
					new String[] { SyncConflicts.OXID, SyncConflicts.USERNAME,
							SyncConflicts.TIMESTAMP },
					SyncConflicts._ID + "= ?",
					new String[] { intent.getDataString() }, null);
			if(c.moveToFirst()) {
				this.onItemSelected(0,
						c.getString(c.getColumnIndex(SyncConflicts.OXID)),
						c.getString(c.getColumnIndex(SyncConflicts.USERNAME)),
						c.getString(c.getColumnIndex(SyncConflicts.TIMESTAMP)));
				c.close();
			} else {
				throw new RuntimeException("Could not find '"
						+ intent.getDataString()
						+ "' in SyncConflicts-Database!");
			}
		}

		if(hasToBeRestarted) {
			hasToBeRestarted = false;
			startActivity(getIntent());
			finish();
			return;
		}
	}

	/**
	 * This is a callback that the list fragment (ConflictsFragment) calls when
	 * a list item is selected
	 */
	public void onItemSelected(int pPosition, String pOxid, String pName,
			String pTimestamp) {
		if(mDualPane) {
			hasToBeRestarted = true;
			ConflictFragment frag = (ConflictFragment) getSupportFragmentManager()
					.findFragmentById(R.id.solveconflicts_resolve_frag);
			try {
				frag.updateContent(pOxid, pName, pTimestamp);
			} catch (Exception e) {
				throw new RuntimeException(e.getClass().getSimpleName()
						+ " while updating ConflictFragment");
			}
		} else {
			Intent intent = new Intent(this, SolveConflict.class);
			intent.putExtra(SyncConflicts.OXID, pOxid);
			intent.putExtra(SyncConflicts.USERNAME, pName);
			intent.putExtra(SyncConflicts.TIMESTAMP, pTimestamp);
			startActivity(intent);
		}
	}

	/**
	 * This is a callback that the list fragment (ConflictsFragment) calls when
	 * it needs the search data
	 */
	public String onSearchInformationAvailable() {
		return mConstraint;
	}

	@Override
	public boolean onSearchRequested() {
		startSearch("", false, null, false);
		return true;
	}

	/*****************************************************************************************************************************/
	/* --> .:Menu-Methoden:. <-- */
	/*****************************************************************************************************************************/
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// app icon in Action Bar clicked; go up
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
