/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.activities;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Groups;
import android.provider.ContactsContract.Settings;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.text.method.SingleLineTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.OXConstants;
import com.alien.oxclient.R;
import com.alien.oxclient.action.OXAction;
import com.alien.oxclient.mockinterfaces.OXAccountManager;

/**
 * Activity which displays login screen to the user.
 */
public class AuthenticatorActivity extends AccountAuthenticatorActivity
		implements OnUnitTestHooks {
	private static final String TAG = "OXClient";

	private boolean unitTestHookAfterFinishedLogin = true;
	private OXAccountManager mAccountManager;
	private Thread mAuthThread;
	private String mAuthtoken;
	// private String mAuthtokenType;

	/**
	 * If set we are just checking that the user knows their credentials; this
	 * doesn't cause the user's password to be changed on the device.
	 */
	private Boolean mConfirmCredentials = false;

	/** for posting authentication attempts back to UI thread */
	private final Handler mHandler = new Handler();
	private TextView mMessage;

	/** Was the original caller asking for an entirely new account? */
	protected boolean mRequestNewAccount = false;

	private String mUsername;
	private EditText mUsernameEdit;

	private String mServer;
	private EditText mServerEdit;

	private String mPassword;
	private EditText mPasswordEdit;

	private CheckedTextView mCheckedTextViewShowPassword;
	private CheckedTextView mCheckedTextViewEncrypted;

	private Button mSubmitButton;

	@Override
	public void onCreate(Bundle pIcicle) {
		super.onCreate(pIcicle);
		mAccountManager = OXApplication.getAccountManager();
		final Intent intent = getIntent();
		mUsername = intent.getStringExtra(OXConstants.PARAM_USERNAME);
		mServer = intent.getStringExtra(OXConstants.SERVER);
		// mAuthtokenType =
		// intent.getStringExtra(OXConstants.PARAM_AUTHTOKEN_TYPE);
		mRequestNewAccount = (mUsername == null);
		mConfirmCredentials = intent.getBooleanExtra(
				OXConstants.PARAM_CONFIRMCREDENTIALS, false);
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.authenticatoractivity);
		getWindow().setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,
				R.drawable.ic_sync_contacts);

		mMessage = (TextView) findViewById(R.id.authenticatoractivity_textView_message);
		mUsernameEdit = (EditText) findViewById(R.id.authenticatoractivity_editText_username);
		mPasswordEdit = (EditText) findViewById(R.id.authenticatoractivity_editText_password);
		mServerEdit = (EditText) findViewById(R.id.authenticatoractivity_editText_server);
		mCheckedTextViewShowPassword = (CheckedTextView) findViewById(R.id.authenticatoractivity_checkedTextView_show_password);
		mCheckedTextViewEncrypted = (CheckedTextView) findViewById(R.id.authenticatoractivity_checkedTextView_encrypted);
		mSubmitButton = (Button) findViewById(R.id.authenticatoractivity_button_ok);

		mUsernameEdit.setText(mUsername);
		if(mRequestNewAccount) {
			if(!TextUtils.isEmpty(mServer)) mServerEdit.setText(mServer);
			else mServerEdit.setText("");
		} else {
			if(!TextUtils.isEmpty(mServer)) mServerEdit.setText(mServer);
			else mServerEdit.setText(mAccountManager.getAccountServer());
		}
		mMessage.setText(getMessage());

		/*
		 * OnClickListener
		 */
		mSubmitButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				handleLogin(v);
			}
		});
		mCheckedTextViewEncrypted
				.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						boolean checked = mCheckedTextViewEncrypted.isChecked();
						mCheckedTextViewEncrypted.setChecked(!checked);
					}
				});
		mCheckedTextViewShowPassword
				.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						boolean checked = mCheckedTextViewShowPassword
								.isChecked();
						if(checked) {
							// Passwort soll sichtbar sein
							mPasswordEdit
									.setTransformationMethod(PasswordTransformationMethod
											.getInstance());
							mCheckedTextViewShowPassword.setChecked(!checked);
						} else {
							// Passwort soll nicht sichtbar sein
							mPasswordEdit
									.setTransformationMethod(SingleLineTransformationMethod
											.getInstance());
							mCheckedTextViewShowPassword.setChecked(!checked);
						}
					}
				});
		mPasswordEdit.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if(keyCode == KeyEvent.KEYCODE_ENTER) {
					handleLogin(v);
					return true;
				} else {
					return false;
				}
			}
		});
	}

	@Override
	protected Dialog onCreateDialog(int pId) {
		final ProgressDialog dialog = new ProgressDialog(this);
		dialog.setMessage(getText(R.string.authenticatoractivity_progressdialog_authenticating));
		dialog.setIndeterminate(true);
		dialog.setCancelable(true);
		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			public void onCancel(DialogInterface dialog) {
				if(mAuthThread != null) {
					mAuthThread.interrupt();
					finish();
				}
			}
		});
		return dialog;
	}

	/**
	 * Handles onClick event on the Submit button. Sends
	 * username/password/server to the server for authentication.
	 * 
	 * @param pView
	 *            The Submit button for which this method is invoked
	 */
	public void handleLogin(View pView) {
		if(mRequestNewAccount) {
			mUsername = mUsernameEdit.getText().toString();
		}
		mPassword = mPasswordEdit.getText().toString();
		try {
			mServer = OXApplication.getUtils().checkAndcorrectServerInput(
					mServerEdit.getText().toString());
		} catch (IllegalArgumentException e) {
			mServer = "";
		}
		if(TextUtils.isEmpty(mUsername) || TextUtils.isEmpty(mPassword)
				|| TextUtils.isEmpty(mServer)) {
			mMessage.setText(getMessage());
		} else {
			showProgress();
			// Start authenticating...
			mAuthThread = new Thread(new Runnable() {
				public void run() {
					final OXAction<String> loginAction = OXApplication
							.getActionDependencyManager().getOXActionLogin(
									mServer, mUsername, mPassword);
					loginAction.perform(mCheckedTextViewEncrypted.isChecked());
					mHandler.post(new Runnable() {
						public void run() {
							hideProgress();
							if(loginAction.hasResult()) {
								if(!mConfirmCredentials) {
									finishLogin(loginAction.getResult());
								} else {
									finishConfirmCredentials(true);
								}
							} else {
								if(mRequestNewAccount) {
									// "Please enter a valid username/password.
									mMessage.setText(getText(R.string.authenticatoractivity_loginfail_text_both)
											+ "\n"
											+ loginAction.getErrorMessage());
								} else {
									// "Please enter a valid password." (Used
									// when the
									// account is already in the database but
									// the password
									// doesn't work.)
									mMessage.setText(getText(R.string.authenticatoractivity_loginfail_text_pwonly)
											+ "\n"
											+ loginAction.getErrorMessage());
								}
							}
						}
					});
				}
			});
			mAuthThread.start();
		}
	}

	/**
	 * Called when response is received from the server for confirm credentials
	 * request. See onAuthenticationResult(). Sets the
	 * AccountAuthenticatorResult which is sent back to the caller.
	 * 
	 * @param the
	 *            confirmCredentials result.
	 */
	protected void finishConfirmCredentials(boolean pResult) {
		mAccountManager.setPassword(mPassword);
		mAccountManager.setAccountServer(mServer);
		final Intent intent = new Intent();
		intent.putExtra(AccountManager.KEY_BOOLEAN_RESULT, pResult);
		setAccountAuthenticatorResult(intent.getExtras());
		setResult(RESULT_OK, intent);
		unitTestHookAfterFinishedLogin = false;
		finish();
		Log.d(TAG,
				"[<=][AuthenticatorActivity->finishConfirmCredentials] finished");
	}

	/**
	 * Called when response is received from the server for authentication
	 * request. See onAuthenticationResult(). Sets the
	 * AccountAuthenticatorResult which is sent back to the caller. Also sets
	 * the authToken in AccountManager for this account.
	 * 
	 * @param the
	 *            confirmCredentials result.
	 */

	protected void finishLogin(String pResult) {
		final Account account = new Account(mUsername,
				getString(R.string.ACCOUNT_TYPE));
		mAuthtoken = pResult;
		Log.d(TAG,
				"    [*][AuthenticatorActivity->finishLogin] mRequestNewAccount: "
						+ mRequestNewAccount);
		if(mRequestNewAccount) {
			Bundle bundle = new Bundle();
			bundle.putString(OXConstants.SERVER, mServer);
			mAccountManager.addAccountExplicitly(account, mPassword, bundle);
			// Explizit den AuthenticationToken fuer diesen Account setzen!
			mAccountManager.setAuthToken(getString(R.string.AUTHTOKEN_TYPE),
					mAuthtoken);
			mAccountManager.setEncrypted(account,
					mCheckedTextViewEncrypted.isClickable());
			// Damit die synchronisierten Kontakte gleich von Anfang an in der
			// Uebersicht angezeigt werden:
			ContentProviderClient client = this.getContentResolver()
					.acquireContentProviderClient(
							ContactsContract.AUTHORITY_URI);
			ContentValues cv = new ContentValues();
			cv.put(Groups.ACCOUNT_NAME, account.name);
			cv.put(Groups.ACCOUNT_TYPE, account.type);
			cv.put(Settings.UNGROUPED_VISIBLE, true);
			// Sync-Standardeinstellungen setzen
			ContentResolver.setIsSyncable(account, ContactsContract.AUTHORITY,
					1);
			ContentResolver.setSyncAutomatically(account,
					ContactsContract.AUTHORITY, false);
			try {
				client.insert(
						Settings.CONTENT_URI
								.buildUpon()
								.appendQueryParameter(
										ContactsContract.CALLER_IS_SYNCADAPTER,
										"true").build(), cv);
			} catch (RemoteException e) {
				Log.e(TAG,
						"[AuthenticatorActivity->finishLogin] RemoteException: "
								+ e.getMessage());
				e.printStackTrace();
			}
		} else {
			mAccountManager.setPassword(mPassword);
		}
		final Intent intent = new Intent();
		intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, mUsername);
		intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE,
				getString(R.string.ACCOUNT_TYPE));
		// Man muesste eigentlich noch abchecken welches Authtoken_type das
		// jetzt genau war
		// Da ich hier nur eins verwende spar ich mir die Abfrage
		// if (mAuthtokenType != null &&
		// mAuthtokenType.equals(getString(R.string.AUTHTOKEN_TYPE)))
		intent.putExtra(AccountManager.KEY_AUTHTOKEN, mAuthtoken);
		setAccountAuthenticatorResult(intent.getExtras());
		setResult(RESULT_OK, intent);
		unitTestHookAfterFinishedLogin = false;
		finish();
		Log.d(TAG, "[<=][AuthenticatorActivity->finishLogin] finished");
	}

	/**
	 * Returns the message to be displayed at the top of the login dialog box.
	 */
	private CharSequence getMessage() {
		if(TextUtils.isEmpty(mUsername)) {
			// If no username, then we ask the user to log in using an
			// appropriate service.
			return getText(R.string.authenticatoractivity_newaccount_text);
		}
		if(TextUtils.isEmpty(mPassword)) {
			// We have an account but no password
			return getText(R.string.authenticatoractivity_loginfail_text_pwmissing);
		}
		if(TextUtils.isEmpty(mServer)) {
			// We have an account but no password
			return getText(R.string.authenticatoractivity_loginfail_text_server);
		}
		return null;
	}

	/**
	 * Shows the progress UI for a lengthy operation.
	 */
	protected void showProgress() {
		showDialog(0);
	}

	/**
	 * Hides the progress UI for a lengthy operation.
	 */
	protected void hideProgress() {
		dismissDialog(0);
	}

	public boolean isFirstRoundtripFinished() {
		return unitTestHookAfterFinishedLogin;
	}
}
