package com.alien.oxclient.activities;

public interface OnUnitTestHooks {

	public boolean isFirstRoundtripFinished();
}
