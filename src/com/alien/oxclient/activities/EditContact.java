/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.activities;

import java.util.ArrayList;
import java.util.Date;

import org.json.JSONException;

import android.content.ContentProviderOperation;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract.Data;

import com.alien.oxclient.R;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactRawContactIdBuilder;

public class EditContact extends AlterContact {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(getString(R.string.editcontact));
	}

	@Override
	protected OXContact initializeContact() {
		String rawContactId;
		Cursor cursor = getContentResolver().query(getIntent().getData(),
				new String[] { Data.RAW_CONTACT_ID }, null, null, null);
		try {
			if(cursor.moveToFirst()) {
				rawContactId = cursor.getString(cursor
						.getColumnIndex(Data.RAW_CONTACT_ID));
			} else {
				throw new RuntimeException("Could not find the RawContact!");
			}
		} finally {
			cursor.close();
		}
		try {
			return new OXContactRawContactIdBuilder().addContext(this)
					.addTimestamp(new Date())
					.addFromRowContactId(Long.valueOf(rawContactId))
					.getOXObject();
		} catch (NumberFormatException e) {
			throw new RuntimeException("NumberFormatException: "
					+ e.getLocalizedMessage());
		} catch (JSONException e) {
			throw new RuntimeException("JSONException: "
					+ e.getLocalizedMessage());
		}
	}

	@Override
	protected ArrayList<ContentProviderOperation> handleChanges()
			throws NumberFormatException, JSONException, RemoteException,
			OperationApplicationException {
		return mContact.updateContact(false);
	}
}
