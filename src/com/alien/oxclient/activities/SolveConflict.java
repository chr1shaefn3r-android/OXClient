/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 *
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 *
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;

import com.alien.oxclient.R;
import com.alien.oxclient.fragments.ConflictFragment;
import com.alien.oxclient.provider.SyncConflicts;

public class SolveConflict extends FragmentActivity {

	@Override
	protected void onCreate(Bundle pSavedInstanceState) {
		super.onCreate(pSavedInstanceState);
		setContentView(R.layout.solveconflict);

		setTitle(R.string.solveconflict);
		Bundle extras = getIntent().getExtras();
		if(extras != null
				&& !TextUtils.isEmpty(extras.getString(SyncConflicts.USERNAME))) {
			ConflictFragment frag = (ConflictFragment) getSupportFragmentManager()
					.findFragmentById(R.id.solveconflicts_resolve_frag);
			try {
				frag.updateContent(extras.getString(SyncConflicts.OXID),
						extras.getString(SyncConflicts.USERNAME),
						extras.getString(SyncConflicts.TIMESTAMP));
			} catch (Exception e) {
				throw new RuntimeException(e.getClass().getSimpleName()
						+ " while updating ConflictFragment");
			}
		} else {
			finish();
			return;
		}
	}
}
