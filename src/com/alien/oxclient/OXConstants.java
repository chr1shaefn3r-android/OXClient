/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient;

/**
 * Custom static constants
 */
public class OXConstants {
	// You shall not create an instance of this class!
	private OXConstants() {
	}

	/**
	 * Keys for AccountManager
	 */
	public static final String SERVER = "server";
	public static final String CONTACTSFOLDER = "contactsfolder";
	public static final String CONTACTSFOLDERID = "contactsfolderid";
	public static final String ENCRYPTED = "ssl_encrypted";

	/**
	 * Keys for the extraBundle passed to the AuthenticatorActivity
	 */
	public static final String PARAM_CONFIRMCREDENTIALS = "confirmCredentials";
	public static final String PARAM_SERVER = "server";
	public static final String PARAM_PASSWORD = "password";
	public static final String PARAM_USERNAME = "username";
	public static final String PARAM_AUTHTOKEN_TYPE = "authtokenType";
	/** Custom Mimetpy for the oxid */
	public static final String MIMETYPE_OXID = "vnd.com.google.cursor.item/vnd.alien.oxid";
	// public static final String MIMETYPE_OXID =
	// "vnd.android.cursor.item/vnd.alien.oxid";
	/** Column of the oxid */
	public static final int OXID_DATA = 1;

	/**
	 * Keys for the returnbundles from OXHttpApiHelper
	 */
	public static final String OXERROR = "oxError";
	public static final String OXERRORCODE = "oxErrorCode";
	public static final String OXDATA = "oxData";
	public static final String OXTIMESTAMP = "oxTimestamp";

	public static final String ALTERCONTACT_TAG_STRING = "value";
	/**
	 * Constants for AlterContact-Fragment args-Bundle
	 */
	public static final String ALTERCONTACT_ARGS_KEY_GROUP = "groupKey";
	public static final String ALTERCONTACT_ARGS_SHOW_CALENDARVIEW = "calendarViewShown";
	public static final String ALTERCONTACT_ARGS_LABEL = "label";

	/**
	 * Constants for AlterContact Groups
	 */
	public static final int ALTERCONTACT_GROUP_NO = 0;
	public static final int ALTERCONTACT_GROUP_WORK = 1;
	public static final int ALTERCONTACT_GROUP_HOME = 2;
	public static final int ALTERCONTACT_GROUP_OTHER = 3;
	public static final int ALTERCONTACT_GROUP_PIC = 4;
}
