package com.alien.oxclient;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import com.alien.oxclient.mockinterfaces.OXNotificationManager;

public class OXNotificationManagerImpl implements OXNotificationManager {

	private NotificationManager mNotificationManager;

	public OXNotificationManagerImpl(Context pContext) {
		mNotificationManager = (NotificationManager) pContext
				.getSystemService(Context.NOTIFICATION_SERVICE);
	}

	public void notify(int pId, Notification pNotification) {
		mNotificationManager.notify(pId, pNotification);
	}
}
