/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.preferences;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.util.AttributeSet;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.OXConstants;
import com.alien.oxclient.activities.AuthenticatorActivity;

public class ChangeCredentials extends Preference {

	public ChangeCredentials(Context pContext) {
		this(pContext, null, 0);
	}

	public ChangeCredentials(Context pContext, AttributeSet pAttributeSet) {
		this(pContext, pAttributeSet, 0);
	}

	public ChangeCredentials(final Context pContext,
			AttributeSet pAttributeSet, int pDefStyle) {
		super(pContext, pAttributeSet, pDefStyle);
		this.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
				Intent authenticatorActivity = new Intent(pContext,
						AuthenticatorActivity.class);
				Bundle b = new Bundle();
				b.putBoolean(OXConstants.PARAM_CONFIRMCREDENTIALS, true);
				b.putString(OXConstants.PARAM_USERNAME, OXApplication
						.getAccountManager().getOXAccount().name);
				b.putString(OXConstants.SERVER, OXApplication
						.getAccountManager().getAccountServer());
				authenticatorActivity.putExtras(b);
				pContext.startActivity(authenticatorActivity);
				return false;
			}
		});
	}
}
