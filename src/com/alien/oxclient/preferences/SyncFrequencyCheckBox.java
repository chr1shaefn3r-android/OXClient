package com.alien.oxclient.preferences;

import android.content.Context;
import android.preference.CheckBoxPreference;
import android.util.AttributeSet;

public class SyncFrequencyCheckBox extends CheckBoxPreference {

	public SyncFrequencyCheckBox(Context context) {
		super(context);
	}

	public SyncFrequencyCheckBox(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public SyncFrequencyCheckBox(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onAttachedToActivity() {
		super.onAttachedToActivity();
		changeState();
	}

	@Override
	protected void onClick() {
		super.onClick();
		changeState();
	}

	private void changeState() {
		this.findPreferenceInHierarchy("sync_frequency")
				.setEnabled(isChecked());
		this.findPreferenceInHierarchy("variable_sync_frequency").setEnabled(
				!isChecked());
	}
}
