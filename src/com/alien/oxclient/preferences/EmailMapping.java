/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.preferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.R;
import com.alien.oxclient.mockinterfaces.OXPreferences;
import com.alien.oxclient.ui.DragNDropListView;
import com.alien.oxclient.ui.DragNDropListView.DragListener;
import com.alien.oxclient.ui.DragNDropListView.DropListener;
import com.alien.oxclient.ui.DragNDropListView.RemoveListener;

public class EmailMapping extends DialogPreference {

	private Context mContext;
	private DragNDropListView mDragNDrop;
	private HashMap<String, String> mEmailMapping;

	public EmailMapping(Context pContext, AttributeSet pAttributeSet,
			int pDefStyle) {
		super(pContext, pAttributeSet, pDefStyle);
		initPreference(pContext);
	}

	public EmailMapping(Context pContext, AttributeSet pAttributeSet) {
		super(pContext, pAttributeSet);
		initPreference(pContext);
	}

	private void initPreference(Context pContext) {
		this.mContext = pContext;
		String[] entries = mContext.getResources().getStringArray(
				R.array.email_mapping_entries);
		String[] values = mContext.getResources().getStringArray(
				R.array.email_mapping_values);
		mEmailMapping = new HashMap<String, String>();
		mEmailMapping.put(values[0], entries[0]);
		mEmailMapping.put(values[1], entries[1]);
		mEmailMapping.put(values[2], entries[2]);
	}

	@Override
	protected void onBindDialogView(View pView) {
		mDragNDrop = (DragNDropListView) pView
				.findViewById(R.id.preferences_emailmapping_dragndrop);
		ArrayList<String> content = new ArrayList<String>();
		OXPreferences mPrefs = OXApplication.getPrefs();
		content.add(mEmailMapping.get(mPrefs.getFirstEmailMapping()));
		content.add(mEmailMapping.get(mPrefs.getSecondEmailMapping()));
		content.add(mEmailMapping.get(mPrefs.getThirdEmailMapping()));
		mDragNDrop.setAdapter(new DragNDropAdapter(mContext,
				new int[] { R.layout.dragitem },
				new int[] { R.id.preferences_emailmapping_dragItem_textView },
				content));
		mDragNDrop.setDropListener(mDropListener);
		mDragNDrop.setRemoveListener(mRemoveListener);
		mDragNDrop.setDragListener(mDragListener);
		super.onBindDialogView(pView);
	}

	@Override
	protected void onDialogClosed(boolean pPositiveResult) {
		if(pPositiveResult) {
			Set<Map.Entry<String, String>> entrySet = mEmailMapping.entrySet();
			OXPreferences mPrefs = OXApplication.getPrefs();
			mPrefs.setFirstEmailMapping(findKeyByValue((String) mDragNDrop
					.getAdapter().getItem(0), entrySet));
			mPrefs.setSecondEmailMapping(findKeyByValue((String) mDragNDrop
					.getAdapter().getItem(1), entrySet));
			mPrefs.setThirdEmailMapping(findKeyByValue((String) mDragNDrop
					.getAdapter().getItem(2), entrySet));
		}
		super.onDialogClosed(pPositiveResult);
	}

	private String findKeyByValue(String pValue,
			Set<Map.Entry<String, String>> pEntrySet) {
		for(Map.Entry<String, String> entry : pEntrySet) {
			if(entry.getValue().compareTo(pValue) == 0) { return entry.getKey(); }
		}
		return null;
	}

	private DropListener mDropListener = new DropListener() {
		public void onDrop(int from, int to) {
			ListAdapter adapter = mDragNDrop.getAdapter();
			if(adapter instanceof DragNDropAdapter) {
				((DragNDropAdapter) adapter).onDrop(from, to);
				mDragNDrop.invalidateViews();
			}
		}
	};

	private RemoveListener mRemoveListener = new RemoveListener() {
		public void onRemove(int which) {
			ListAdapter adapter = mDragNDrop.getAdapter();
			if(adapter instanceof DragNDropAdapter) {
				((DragNDropAdapter) adapter).onRemove(which);
				mDragNDrop.invalidateViews();
			}
		}
	};

	private DragListener mDragListener = new DragListener() {
		int backgroundColor = 0xe01b5e1b;
		int defaultBackgroundColor = 0xFF333333;

		public void onDrag(int x, int y, ListView listView) {
		}

		public void onStartDrag(View pItemView) {
			pItemView.setVisibility(View.INVISIBLE);
			// getDrawingCacheBackgroundColor() liefert die falsche Farbe
			// liefert Schwarz, obwohl der Dialog bzw. das ListView Grau hat,
			// daher ist defaultBackgroundColor leider hardcoded
			// defaultBackgroundColor =
			// itemView.getDrawingCacheBackgroundColor();
			pItemView.setBackgroundColor(backgroundColor);
			ImageView iv = (ImageView) pItemView
					.findViewById(R.id.preferences_emailmapping_dragItem_imageView);
			if(iv != null) iv.setVisibility(View.INVISIBLE);
		}

		public void onStopDrag(View itemView) {
			itemView.setVisibility(View.VISIBLE);
			itemView.setBackgroundColor(defaultBackgroundColor);
			ImageView iv = (ImageView) itemView
					.findViewById(R.id.preferences_emailmapping_dragItem_imageView);
			if(iv != null) iv.setVisibility(View.VISIBLE);
		}
	};

	public final class DragNDropAdapter extends BaseAdapter implements
			RemoveListener, DropListener {

		private int[] mIds;
		private int[] mLayouts;
		private LayoutInflater mInflater;
		private ArrayList<String> mContent;

		public DragNDropAdapter(Context pContext, ArrayList<String> pContent) {
			init(pContext, new int[] { android.R.layout.simple_list_item_1 },
					new int[] { android.R.id.text1 }, pContent);
		}

		public DragNDropAdapter(Context pContext, int[] pItemLayouts,
				int[] pItemIDs, ArrayList<String> pContent) {
			init(pContext, pItemLayouts, pItemIDs, pContent);
		}

		private void init(Context pContext, int[] pLayouts, int[] pIds,
				ArrayList<String> pContent) {
			this.mInflater = LayoutInflater.from(pContext);
			this.mIds = pIds;
			this.mLayouts = pLayouts;
			this.mContent = pContent;
		}

		/**
		 * The number of items in the list
		 */
		public int getCount() {
			return mContent.size();
		}

		/**
		 * Since the data comes from an array, just returning the index is
		 * sufficient to get at the data. If we were using a more complex data
		 * structure, we would return whatever object represents one row in the
		 * list.
		 */
		public String getItem(int pPosition) {
			return mContent.get(pPosition);
		}

		/**
		 * Use the array index as a unique id.
		 */
		public long getItemId(int pPosition) {
			return pPosition;
		}

		/**
		 * Make a view to hold each row.
		 */
		public View getView(int pPosition, View pConvertView, ViewGroup pParent) {
			ViewCache viewCache;

			if(pConvertView == null) {
				pConvertView = mInflater.inflate(mLayouts[0], null);
				viewCache = new ViewCache();
				viewCache.text = (TextView) pConvertView.findViewById(mIds[0]);
				pConvertView.setTag(viewCache);
			} else {
				viewCache = (ViewCache) pConvertView.getTag();
			}

			viewCache.text.setText(mContent.get(pPosition));

			return pConvertView;
		}

		public void onRemove(int pWhich) {
			if(pWhich < 0 || pWhich > mContent.size()) return;
			mContent.remove(pWhich);
		}

		public void onDrop(int pFrom, int pTo) {
			String temp = mContent.get(pFrom);
			mContent.remove(pFrom);
			mContent.add(pTo, temp);
		}
	}

	static class ViewCache {
		TextView text;
	}
}
