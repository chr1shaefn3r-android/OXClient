/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.preferences;

import android.content.Context;
import android.content.Intent;
import android.preference.Preference;
import android.util.AttributeSet;

import com.alien.oxclient.R;

public class EmailFeedback extends Preference {

	public EmailFeedback(Context pContext) {
		super(pContext);
		initPreference(pContext);
	}

	public EmailFeedback(Context pContext, AttributeSet pAttributeSet) {
		super(pContext, pAttributeSet);
		initPreference(pContext);
	}

	public EmailFeedback(Context pContext, AttributeSet pAttributeSet,
			int pDefStyle) {
		super(pContext, pAttributeSet, pDefStyle);
		initPreference(pContext);
	}

	private void initPreference(final Context pContext) {
		this.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
				final Intent emailFeedback = new Intent(
						android.content.Intent.ACTION_SEND);
				emailFeedback.setType("message/rfc822");
				emailFeedback.putExtra(android.content.Intent.EXTRA_EMAIL,
						new String[] { "oxclient@die-haefners.de" });
				emailFeedback
						.putExtra(
								android.content.Intent.EXTRA_SUBJECT,
								pContext.getString(R.string.preferences_emailfeedback_subject));
				pContext.startActivity(emailFeedback);
				// context.startActivity(Intent.createChooser(emailFeedback,
				// "Select email application."));
				return false;
			}
		});
	}
}
