/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.alien.oxclient.R;
import com.alien.oxclient.mockinterfaces.OXPreferences;

public class OXPreferencesImpl implements OXPreferences {

	private Context mContext;
	private SharedPreferences mSharedprefs;
	private SharedPreferences.Editor mEditor;

	/**
	 * Konstruktor
	 * 
	 * @params pContext
	 */
	public OXPreferencesImpl(Context pContext) {
		this.mContext = pContext;
		this.mSharedprefs = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		this.mEditor = mSharedprefs.edit();
	}

	/*
	 * VersionCode
	 */
	public int getVersionCode() {
		return mSharedprefs.getInt(VERSIONCODE, VERSIONCODE_DEFAULT);
	}

	public void setVersionCode(int pVersionCode) {
		mEditor.putInt(VERSIONCODE, pVersionCode);
		mEditor.commit();
	}

	/*
	 * Language
	 */
	public String getLanguage() {
		return mSharedprefs.getString(LANGUAGE, LANGUAGE_DEFAULT);
	}

	public String getDefaultLanguage() {
		return mSharedprefs.getString(DEFAULTLANGUAGE, DEFAULTLANGUAGE_DEFAULT);
	}

	public void setDefaultLanguage(String pDefaultLanguage) {
		mEditor.putString(DEFAULTLANGUAGE, pDefaultLanguage);
		mEditor.commit();
	}

	/*
	 * EmailMapping
	 */
	// FirstEmailMapping
	public String getFirstEmailMapping() {
		String[] emailMappingValues = this.mContext.getResources()
				.getStringArray(R.array.email_mapping_values);
		return mSharedprefs.getString(FIRST_EMAIL_MAPPING,
				emailMappingValues[0]);
	}

	public void setFirstEmailMapping(String pFirstEmailMapping) {
		mEditor.putString(FIRST_EMAIL_MAPPING, pFirstEmailMapping);
		mEditor.commit();
	}

	// SecondEmailMapping
	public String getSecondEmailMapping() {
		String[] emailMappingValues = this.mContext.getResources()
				.getStringArray(R.array.email_mapping_values);
		return mSharedprefs.getString(SECOND_EMAIL_MAPPING,
				emailMappingValues[1]);
	}

	public void setSecondEmailMapping(String pSecondEmailMapping) {
		mEditor.putString(SECOND_EMAIL_MAPPING, pSecondEmailMapping);
		mEditor.commit();
	}

	// ThirdEmailMapping
	public String getThirdEmailMapping() {
		String[] emailMappingValues = this.mContext.getResources()
				.getStringArray(R.array.email_mapping_values);
		return mSharedprefs.getString(THIRD_EMAIL_MAPPING,
				emailMappingValues[2]);
	}

	public void setThirdEmailMapping(String pThirdEmailMapping) {
		mEditor.putString(THIRD_EMAIL_MAPPING, pThirdEmailMapping);
		mEditor.commit();
	}

	/*
	 * LastSync
	 */
	public long getLastSync() {
		return mSharedprefs.getLong(LAST_SYNC, LAST_SYNC_DEFAULT);
	}

	public void setLastSync(long pLastSync) {
		mEditor.putLong(LAST_SYNC, pLastSync);
		mEditor.commit();
	}

	/*
	 * ConstantSyncFrequency
	 */
	public boolean getIsConstantSyncFrequency() {
		return mSharedprefs.getBoolean(CONSTANT_SYNC_FREQUENCY,
				CONSTANT_SYNC_FREQUENCY_DEFAULT);
	}

	/*
	 * Maintime
	 */
	public String[] getMaintimeDays() {
		return ListPreferenceMultiSelect.parseStoredValue(mSharedprefs
				.getString(MAINTIME_DAYS, MAINTIME_DAYS_DEFAULT));
	}

	public String getMaintimeBegin() {
		return mSharedprefs.getString(MAINTIME_BEGIN, MAINTIME_BEGIN_DEFAULT);
	}

	public String getMaintimeEnd() {
		return mSharedprefs.getString(MAINTIME_END, MAINTIME_END_DEFAULT);
	}

	public String getMaintimeSyncFrequency() {
		return mSharedprefs.getString(MAINTIME_SYNC_FREQUENCY,
				MAINTIME_SYNC_FREQUENCY_DEFAULT);
	}

	/*
	 * Secondarytime
	 */
	public String getSecondarytimeSyncFrequency() {
		return mSharedprefs.getString(SECONDARYTIME_SYNC_FREQUENCY,
				SECONDARYTIME_SYNC_FREQUENCY_DEFAULT);
	}

	/*
	 * SyncFrequency
	 */
	public String getSyncFrequency() {
		return mSharedprefs.getString(SYNC_FREQUENCY, SYNC_FREQUENCY_DEFAULT);
	}

	/*
	 * InformationenDialogSeen
	 */
	public boolean getInformationenDialogSeen() {
		return mSharedprefs.getBoolean(INFORMATIONDIALOG_SEEN,
				INFORMATIONDIALOG_SEEN_DEFAULT);
	}

	public void setInformationenDialogSeen(boolean pInformationenDialogSeen) {
		mEditor.putBoolean(INFORMATIONDIALOG_SEEN, pInformationenDialogSeen);
		mEditor.commit();
	}

	/*
	 * SyncPicturesOnlyOnWifi
	 */
	public boolean getSyncPicturesOnlyOnWifi() {
		return mSharedprefs.getBoolean(SYNC_PICTURES_ONLY_ON_WIFI,
				SYNC_PICTURES_ONLY_ON_WIFI_DEFAULT);
	}

	public void setSyncPicturesOnlyOnWifi(boolean pSyncPicturesOnlyOnWifi) {
		mEditor.putBoolean(SYNC_PICTURES_ONLY_ON_WIFI, pSyncPicturesOnlyOnWifi);
		mEditor.commit();
	}
}
