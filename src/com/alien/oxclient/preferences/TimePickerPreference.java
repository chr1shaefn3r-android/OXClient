package com.alien.oxclient.preferences;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TimePicker;

import com.alien.oxclient.R;

public class TimePickerPreference extends DialogPreference {

	private int mLastHour = 0;
	private int mLastMinute = 0;
	private TimePicker mTimePicker;

	public TimePickerPreference(Context context) {
		this(context, null);
	}

	public TimePickerPreference(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public TimePickerPreference(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		setPositiveButtonText(context.getString(R.string.save));
		setNegativeButtonText(context.getString(android.R.string.cancel));
	}

	@Override
	protected View onCreateDialogView() {
		mTimePicker = new TimePicker(getContext());
		mTimePicker.setIs24HourView(DateFormat.is24HourFormat(getContext()));
		return(mTimePicker);
	}

	@Override
	protected void onBindDialogView(View v) {
		super.onBindDialogView(v);
		mTimePicker.setCurrentHour(mLastHour);
		mTimePicker.setCurrentMinute(mLastMinute);
	}

	@Override
	protected void onDialogClosed(boolean positiveResult) {
		super.onDialogClosed(positiveResult);

		if(positiveResult) {
			mLastHour = mTimePicker.getCurrentHour();
			mLastMinute = mTimePicker.getCurrentMinute();

			String twoDigitFormat = "%02d";
			String time = String.format(twoDigitFormat, mLastHour) + ":"
					+ String.format(twoDigitFormat, mLastMinute);

			if(callChangeListener(time)) {
				persistString(time);
			}
		}
	}

	@Override
	protected Object onGetDefaultValue(TypedArray a, int index) {
		return(a.getString(index));
	}

	@Override
	protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
		String time = null;

		if(restoreValue) {
			if(defaultValue == null) {
				time = getPersistedString("00:00");
			} else {
				time = getPersistedString(defaultValue.toString());
			}
		} else {
			time = defaultValue.toString();
		}

		mLastHour = getHour(time);
		mLastMinute = getMinute(time);
	}

	public int getHour(String time) {
		String[] pieces = time.split(":");
		return(Integer.parseInt(pieces[0]));
	}

	public int getMinute(String time) {
		String[] pieces = time.split(":");
		return(Integer.parseInt(pieces[1]));
	}
}
