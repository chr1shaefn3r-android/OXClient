/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.preferences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.preference.ListPreference;
import android.util.AttributeSet;
import android.widget.ListView;

import com.alien.oxclient.R;

/**
 * Credits to the real author and the contributors, thanks for sharing your code
 * with the world :)
 * 
 * @author declanshanaghy http://blog.350nice.com/wp/archives/240 MultiChoice
 *         Preference Widget for Android
 */
public class ListPreferenceMultiSelect extends ListPreference {

	private static String separator = ";";
	private String checkAllKey = null;
	private boolean[] mClickedDialogEntryIndices;

	public ListPreferenceMultiSelect(Context context) {
		this(context, null);
	}

	public ListPreferenceMultiSelect(Context pContext, AttributeSet pAttrs) {
		super(pContext, pAttrs);
		TypedArray typedArray = pContext.obtainStyledAttributes(pAttrs,
				R.styleable.ListPreferenceMultiSelect);
		checkAllKey = typedArray
				.getString(R.styleable.ListPreferenceMultiSelect_checkAll);
		typedArray.recycle();

		// Initialize the array of boolean to the same size as number of entries
		mClickedDialogEntryIndices = new boolean[getEntries().length];
	}

	@Override
	public void setEntries(CharSequence[] entries) {
		super.setEntries(entries);
		// Initialize the array of boolean to the same size as number of entries
		mClickedDialogEntryIndices = new boolean[entries.length];
	}

	@Override
	protected void onPrepareDialogBuilder(Builder builder) {
		CharSequence[] entries = getEntries();
		CharSequence[] entryValues = getEntryValues();
		if(entries == null || entryValues == null
				|| entries.length != entryValues.length) { throw new IllegalStateException(
				"ListPreference requires an entries array and an entryValues array which are both the same length"); }

		restoreCheckedEntries();
		builder.setMultiChoiceItems(entries, mClickedDialogEntryIndices,
				new DialogInterface.OnMultiChoiceClickListener() {
					public void onClick(DialogInterface dialog, int which,
							boolean val) {
						if(isCheckAllValue(which) == true) {
							checkAll(dialog, val);
						}
						mClickedDialogEntryIndices[which] = val;
					}
				});
	}

	private boolean isCheckAllValue(int which) {
		final CharSequence[] entryValues = getEntryValues();
		if(checkAllKey != null) { return entryValues[which].equals(checkAllKey); }
		return false;
	}

	private void checkAll(DialogInterface dialog, boolean val) {
		ListView lv = ((AlertDialog) dialog).getListView();
		int size = lv.getCount();
		for(int i = 0; i < size; i++) {
			lv.setItemChecked(i, val);
			mClickedDialogEntryIndices[i] = val;
		}
	}

	private void restoreCheckedEntries() {
		CharSequence[] entryValues = getEntryValues();
		// Explode the string read in sharedpreferences
		String[] vals = parseStoredValue(getValue());

		if(vals != null) {
			List<String> valuesList = Arrays.asList(vals);
			for(int i = 0; i < entryValues.length; i++) {
				CharSequence entry = entryValues[i];
				if(valuesList.contains(entry)) {
					mClickedDialogEntryIndices[i] = true;
				}
			}
		}
	}

	public static String[] parseStoredValue(CharSequence val) {
		if("".equals(val)) {
			return null;
		} else {
			return ((String) val).split(separator);
		}
	}

	@Override
	protected void onDialogClosed(boolean positiveResult) {
		ArrayList<String> values = new ArrayList<String>();

		CharSequence[] entryValues = getEntryValues();
		if(positiveResult && entryValues != null) {
			for(int i = 0; i < entryValues.length; i++) {
				if(mClickedDialogEntryIndices[i] == true) {
					// Don't save the state of check all option - if any
					String val = (String) entryValues[i];
					if(checkAllKey == null
							|| (val.equals(checkAllKey) == false)) {
						values.add(val);
					}
					if(callChangeListener(val)) {
						setValue(join(values, separator));
					}
				}
			}
		}
	}

	// Credits to kurellajunior on this post
	// http://snippets.dzone.com/posts/show/91
	protected static String join(Iterable<? extends Object> pColl,
			String separator) {
		Iterator<? extends Object> oIter;
		if(pColl == null || (!(oIter = pColl.iterator()).hasNext())) return "";
		StringBuilder oBuilder = new StringBuilder(String.valueOf(oIter.next()));
		while (oIter.hasNext())
			oBuilder.append(separator).append(oIter.next());
		return oBuilder.toString();
	}
}
