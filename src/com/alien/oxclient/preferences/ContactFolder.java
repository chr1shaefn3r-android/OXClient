/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.preferences;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.preference.Preference;
import android.util.AttributeSet;
import android.widget.Toast;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.R;
import com.alien.oxclient.action.OXAction;
import com.alien.oxclient.model.OXFolder;

public class ContactFolder extends Preference {

	private boolean mVisible = false;

	public ContactFolder(Context pContext) {
		this(pContext, null, 0);
	}

	public ContactFolder(Context pContext, AttributeSet pAttributeSet) {
		this(pContext, pAttributeSet, 0);
	}

	public ContactFolder(final Context pContext, AttributeSet pAttributeSet,
			int pDefStyle) {
		super(pContext, pAttributeSet, pDefStyle);
		this.setCustomSummary();
		this.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
				new NewContactFolderIdTask().execute();
				return true;
			}
		});
	}

	@Override
	protected void onAttachedToActivity() {
		mVisible = true;
		super.onAttachedToActivity();
	}

	@Override
	protected void onPrepareForRemoval() {
		mVisible = false;
		super.onPrepareForRemoval();
	}

	@Override
	protected Parcelable onSaveInstanceState() {
		mVisible = false;
		return super.onSaveInstanceState();
	}

	private void setLoading() {
		this.setSummary(R.string.progressdialogfragment_loading_please_wait);
	}

	private void setCustomSummary() {
		this.setSummary(OXApplication.getAccountManager().getContactsFolder()
				+ " ("
				+ OXApplication.getAccountManager().getContactsFolderId() + ")");
	}

	/*****************************************************************************************************************************/
	/* --> .:NewContactFolderIdTask:. <-- */
	/*****************************************************************************************************************************/
	private class NewContactFolderIdTask extends
			AsyncTask<Boolean, Integer, OXAction<OXFolder>> {

		@Override
		protected void onPreExecute() {
			setLoading();
			super.onPreExecute();
		}

		protected OXAction<OXFolder> doInBackground(Boolean... pForces) {
			OXAction<OXFolder> action = OXApplication
					.getActionDependencyManager().getOXActionGetContactFolder();
			action.perform();
			return action;
		}

		@Override
		protected void onPostExecute(OXAction<OXFolder> pResult) {
			setCustomSummary();
			if(pResult.hasResult()) {
				if(pResult.hasErrorMessage()) {
					if(mVisible) Toast.makeText(
							getContext(),
							getContext().getString(R.string.warning) + ": "
									+ pResult.getErrorMessage(),
							Toast.LENGTH_LONG).show();
				}
			}
			super.onPostExecute(pResult);
		}
	}
}
