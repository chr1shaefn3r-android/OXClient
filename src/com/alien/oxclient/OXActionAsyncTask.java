/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.alien.oxclient.action.OXAction;
import com.alien.oxclient.dialog.ProgressDialogFragment;

public abstract class OXActionAsyncTask extends
		AsyncTask<Boolean, Integer, OXAction<?>> {

	public interface OnTaskCompletedListener {
		public static final String PROGRESS_DIALOG_FRAGMENT = "ProgressDialogFragment";

		public void onTaskCompleted();
	}

	protected OnTaskCompletedListener mListener;
	protected Context mContext;
	protected boolean mCompleted;

	public OXActionAsyncTask(Activity pActivity) {
		setActivity(pActivity);
	}

	public void setActivity(Activity pActivity) {
		mContext = pActivity;
		try {
			this.mListener = (OnTaskCompletedListener) pActivity;
		} catch (ClassCastException e) {
			throw new ClassCastException(pActivity.toString()
					+ " must implement OnTaskCompleteListener");
		}
		if(mCompleted) {
			notifiyActivityTaskCompleted();
		}
	}

	protected void notifiyActivityTaskCompleted() {
		if(mListener != null) {
			mListener.onTaskCompleted();
		}
	}

	@Override
	protected void onCancelled() {
		DialogFragment dialogFragment = (DialogFragment) getActivitysSupportFragmentManager()
				.findFragmentByTag(
						OnTaskCompletedListener.PROGRESS_DIALOG_FRAGMENT);
		if(dialogFragment != null) {
			dialogFragment.dismiss();
		}
	}

	@Override
	protected void onPostExecute(OXAction<?> pAction) {
		mCompleted = true;
		if(pAction.hasResult()) {
			if(pAction.hasErrorMessage()) {
				// TODO: MessageView einbauen sobald es extrahiert wurde und
				// wiederverwendbar ist!
				Toast.makeText(
						mContext,
						mContext.getString(R.string.warning) + ": "
								+ pAction.getErrorMessage(), Toast.LENGTH_LONG)
						.show();
			}
		} else {
			// TODO: MessageView einbauen sobald es extrahiert wurde und
			// wiederverwendbar ist!
		}
		notifiyActivityTaskCompleted();
	}

	@Override
	protected void onPreExecute() {
		DialogFragment newFragment = ProgressDialogFragment
				.newInstance(mContext);
		newFragment.show(getActivitysSupportFragmentManager(),
				OnTaskCompletedListener.PROGRESS_DIALOG_FRAGMENT);
	}

	protected abstract FragmentManager getActivitysSupportFragmentManager();

}
