package com.alien.oxclient;

import java.util.Calendar;
import java.util.Date;

import com.alien.oxclient.mockinterfaces.OXCalendar;

public class OXCalendarImpl implements OXCalendar {

	private Calendar mCalendar;

	public OXCalendarImpl() {
		this.mCalendar = Calendar.getInstance();
	}

	public long getTimeInMillis() throws IllegalArgumentException {
		return mCalendar.getTimeInMillis();
	}

	public void setHourAndMinute(Date pDate) {
		mCalendar.set(Calendar.HOUR_OF_DAY, pDate.getHours());
		mCalendar.set(Calendar.MINUTE, pDate.getMinutes());
		mCalendar.set(Calendar.SECOND, 0);
		mCalendar.set(Calendar.MILLISECOND, 0);
	}

	public int get(int pField) {
		return mCalendar.get(pField);
	}

	public OXCalendar refresh() {
		this.mCalendar = Calendar.getInstance();
		return this;
	}

	public boolean after(OXCalendar pCalendar) throws IllegalArgumentException {
		return mCalendar.after(pCalendar.getCalendar());
	}

	public boolean before(OXCalendar pCalendar) throws IllegalArgumentException {
		return mCalendar.before(pCalendar.getCalendar());
	}

	public Calendar getCalendar() {
		return mCalendar;
	}

	public Object clone() {
		return new OXCalendarImpl();
	};

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(mCalendar.get(Calendar.DATE) + ".");
		sb.append(mCalendar.get(Calendar.MONTH) + ".");
		sb.append(mCalendar.get(Calendar.YEAR) + "/");
		sb.append(mCalendar.get(Calendar.HOUR_OF_DAY) + ":");
		sb.append(mCalendar.get(Calendar.MINUTE) + ":");
		sb.append(mCalendar.get(Calendar.SECOND));
		return sb.toString();
	}
}
