/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.syncadapter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

import org.json.JSONException;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.OXClient;
import com.alien.oxclient.OXConstants;
import com.alien.oxclient.OXTimeManager;
import com.alien.oxclient.R;
import com.alien.oxclient.action.OXAction;
import com.alien.oxclient.activities.SolveConflicts;
import com.alien.oxclient.mockinterfaces.OXPreferences;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactOXIdBuilder;
import com.alien.oxclient.model.OXContactRawContactIdBuilder;
import com.alien.oxclient.provider.SyncConflicts;

@SuppressLint("NewApi")
public class ContactManager {
	private static final String TAG = "OXClient-SyncAlg";
	// Use our layout id for a unique identifier
	public static int NOTIFICATION_ID = R.drawable.ic_stat_solveconflicts;

	public static void onPerformSync(Context pContext, Account pAccount,
			Bundle pExtras, String pAuthority, ContentProviderClient pProvider,
			SyncResult pSyncResult,
			ArrayList<String> pCreatedContactsRawContactId)
			throws JSONException, RemoteException,
			OperationApplicationException, ParseException {
		if(pExtras
				.containsKey(ContentResolver.SYNC_EXTRAS_DISCARD_LOCAL_DELETIONS)) {
			Log.d(TAG,
					"[*][onPerformSync] SYNC_EXTRAS_DISCARD_LOCAL_DELETIONS was set");
			LinkedHashSet<String> localContacts = ContactManager
					.allOXContacts(pContext);
			if(localContacts.size() > 0) {
				Log.d(TAG,
						"[*][onPerformSync] uploadAllContactsFromCellPhone()");
				ContactManager.uploadAllContactsFromCellPhone(pContext,
						pSyncResult);
			} else {
				Log.d(TAG,
						"[*][onPerformSync] undeleteAllContactsOnCellPhone()");
				ContactManager.undeleteAllContactsOnCellPhone(pContext,
						new Date(), pSyncResult);
			}
			return;
		}
		Date lastUpdated = new Date();
		lastUpdated.setTime(OXApplication.getPrefs().getLastSync());
		if(lastUpdated.getTime() > 0) {
			// Den Server nach einer Liste aller Kontakte fragen, aber nur die
			// Id verlangen
			OXAction<ArrayList<String>> actionGetAllContactIds = OXApplication
					.getActionDependencyManager().getOXActionGetAllContactIds();
			actionGetAllContactIds.perform();
			if(actionGetAllContactIds.hasResult()) {
				Date date = new Date();
				date.setTime(actionGetAllContactIds.getTimestamp());
				ArrayList<String> oxIdsOnServer = actionGetAllContactIds
						.getResult();
				LinkedHashSet<String> oxIdsOnCellPhone = ContactManager
						.allOXContacts(pContext);
				// Try to detect a
				// "One side was completly deleted and would now delete the other side too"-Situation
				if((oxIdsOnCellPhone.size() > 0 && oxIdsOnServer.size() == 0)
						|| (oxIdsOnServer.size() > 0 && oxIdsOnCellPhone.size() == 0)) {
					// If the user does not force the deletion return here
					if(!pExtras
							.containsKey(ContentResolver.SYNC_EXTRAS_OVERRIDE_TOO_MANY_DELETIONS)
							&& pExtras
									.getBoolean(ContentResolver.SYNC_EXTRAS_OVERRIDE_TOO_MANY_DELETIONS) != true) {
						pSyncResult.tooManyDeletions = true;
						// Show the user how many contacts would be deleted
						pSyncResult.stats.numDeletes = oxIdsOnCellPhone.size() != 0 ? oxIdsOnCellPhone
								.size() : oxIdsOnServer.size();
						pSyncResult.stats.numEntries = oxIdsOnCellPhone.size() != 0 ? oxIdsOnCellPhone
								.size() : oxIdsOnServer.size();
						return;
					}
				}
				LinkedHashSet<String> oxIdsPhoneMinusServer = new LinkedHashSet<String>(
						oxIdsOnCellPhone);
				oxIdsPhoneMinusServer.removeAll(oxIdsOnServer);
				if(oxIdsPhoneMinusServer.size() > 0) {
					for(String oxId : oxIdsPhoneMinusServer) {
						OXContact contact = new OXContactOXIdBuilder()
								.addContext(pContext).addTimestamp(date)
								.addFromOXId(oxId).getOXObject();
						Log.d(TAG, "[*][onPerformSync] will delete '" + oxId
								+ "' named: " + contact.toPrintableString());
						OXApplication.getOXContentResolver().applyBatch(
								ContactsContract.AUTHORITY,
								contact.deleteContact());
						pSyncResult.stats.numDeletes++;
					}
					if(pExtras
							.containsKey(ContentResolver.SYNC_EXTRAS_OVERRIDE_TOO_MANY_DELETIONS)) {
						// When this was a override-onPerformSync-Call return
						// here no contacts on the server, so there surelly will
						// be no updates ...
						lastUpdated = new Date();
						return;
					}
				}
			} else {
				// If the first request to the server fails, e.g. no network
				// available
				// stop here
				return;
			}

			OXAction<ArrayList<OXContact>> actionGetAllUpdatedContacts = OXApplication
					.getActionDependencyManager()
					.getOXActionGetUpdatedContacts(lastUpdated);
			actionGetAllUpdatedContacts.perform();
			if(actionGetAllUpdatedContacts.hasResult()) {
				// Set lastUpdated after we received a result
				lastUpdated = new Date();

				Date date = new Date();
				date.setTime(actionGetAllUpdatedContacts.getTimestamp());
				ArrayList<OXContact> oxContacts = actionGetAllUpdatedContacts
						.getResult();
				// RawContactIds aller geaenderterten Kontakte auf dem Handy
				// abrufen
				ArrayList<String> changedRawContactIds = ContactManager
						.changedContacts(pContext);
				// Falls beide Listen groesser null sind, auf Konflikte
				// ueberpruefen
				if(oxContacts.size() > 0 && changedRawContactIds.size() > 0) {
					ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
					ContactManager.checkForConflicts(pContext, oxContacts, ops,
							changedRawContactIds, pSyncResult);
					pContext.getContentResolver().applyBatch(
							ContactsContract.AUTHORITY, ops);
				}
				// Kontakte synchronisieren
				syncToPhone(pContext, oxContacts, pSyncResult,
						pCreatedContactsRawContactId);
				syncToServer(pContext, changedRawContactIds, date, pSyncResult);
			}
		} else {
			Log.d(TAG, "[*][onPerformSync] downloadAllContactsFromServer()");
			ContactManager.downloadAllContactsFromServer(pContext, lastUpdated,
					pSyncResult, pCreatedContactsRawContactId);
		}
		// Wenn alles ohne Fehler durchgelaufen ist:
		OXApplication.getPrefs().setLastSync(lastUpdated.getTime());
		// Sync frequency einstellen
		OXPreferences prefs = OXApplication.getPrefs();
		long syncFrequency;
		if(prefs.getIsConstantSyncFrequency()) {
			syncFrequency = Long.parseLong(prefs.getSyncFrequency());
		} else {
			OXTimeManager timeManager = new OXTimeManager();
			syncFrequency = timeManager.differenceInSeconds();
		}
		OXApplication.getOXContentResolver().addPeriodicSync(
				OXApplication.getAccountManager().getOXAccount(),
				ContactsContract.AUTHORITY, new Bundle(), syncFrequency);
	}

	private static void downloadAllContactsFromServer(Context pContext,
			Date pLastUpdated, SyncResult pSyncResult,
			ArrayList<String> pCreatedContactsRawContactId)
			throws JSONException, RemoteException,
			OperationApplicationException {
		OXAction<ArrayList<OXContact>> actionGetAllContacts = OXApplication
				.getActionDependencyManager().getOXActionGetAllContacts();
		actionGetAllContacts.perform();
		if(actionGetAllContacts.hasResult()) {
			// Nach dem erfolgreichen abrufen Zeitpunkt des letzten Sync setzen
			pLastUpdated.setTime(new Date().getTime());
			List<OXContact> oxContacts = actionGetAllContacts.getResult();
			for(OXContact contact : oxContacts) {
				// Ignore DistributionList-Contacts
				if(!contact.hasDistributionList()) {
					ArrayList<ContentProviderOperation> ops = contact
							.insertContact();
					ContentProviderResult[] results = pContext
							.getContentResolver().applyBatch(
									ContactsContract.AUTHORITY, ops);
					pSyncResult.stats.numInserts++;
					if(pCreatedContactsRawContactId != null) pCreatedContactsRawContactId
							.add(String.valueOf(ContentUris
									.parseId(results[0].uri)));
				}
			}
		} else {
			// Etwas ist schief gelaufen, Zeitpunkt auf 0 zuruecksetzen
			pLastUpdated.setTime(0);
		}
	}

	private static void undeleteAllContactsOnCellPhone(Context pContext,
			Date pTimestamp, SyncResult pSyncResult)
			throws NumberFormatException, JSONException, RemoteException,
			OperationApplicationException {
		ArrayList<OXContact> deletedContacts = ContactManager.deletedContacts(
				pContext, pTimestamp);
		ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
		for(OXContact contact : deletedContacts) {
			Builder resetDeletedFlag = ContentProviderOperation
					.newUpdate(
							RawContacts.CONTENT_URI
									.buildUpon()
									.appendQueryParameter(
											ContactsContract.CALLER_IS_SYNCADAPTER,
											"true").build())
					.withSelection(RawContacts._ID + "=?",
							new String[] { contact.getRawContactId() })
					.withValue(RawContacts.DELETED, "0");
			operations.add(resetDeletedFlag.build());
			pSyncResult.stats.numInserts++;
		}
		pContext.getContentResolver().applyBatch(ContactsContract.AUTHORITY,
				operations);
	}

	private static void uploadAllContactsFromCellPhone(Context pContext,
			SyncResult pSyncResult) throws JSONException {
		Uri rawContactUri = RawContacts.CONTENT_URI
				.buildUpon()
				.appendQueryParameter(RawContacts.ACCOUNT_NAME,
						OXApplication.getAccountManager().getOXAccount().name)
				.appendQueryParameter(RawContacts.ACCOUNT_TYPE,
						OXApplication.getAccountManager().getOXAccount().type)
				.build();
		Cursor cursor = pContext.getContentResolver().query(rawContactUri,
				new String[] { RawContacts._ID }, null, null, null);
		while (cursor.moveToNext()) {
			OXContact contact = new OXContactRawContactIdBuilder()
					.addContext(pContext)
					.addTimestamp(new Date())
					.addRawContactId(
							cursor.getLong(cursor
									.getColumnIndex(RawContacts._ID)))
					.getOXObject();
			// TODO: Was passiert wenn diese Schleife mittendrin abbricht wegen
			// Netzwerkproblemen?
			// => Jeder Kontakt muss einen Status erhalten, sodass erkannt
			// werden kann wo es abgebrochen ist
			// (steht auch in der README)
			OXAction<String> action = OXApplication
					.getActionDependencyManager().getOXActionCreateContact(
							contact);
			action.perform();
			pSyncResult.stats.numInserts++;
		}
	}

	public static void syncToServer(Context pContext,
			List<String> pRawContacts, Date pTimestamp, SyncResult pSyncResult)
			throws NumberFormatException, JSONException, RemoteException,
			OperationApplicationException {
		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
		// Update
		for(String rawContactId : pRawContacts) {
			OXContact contact = new OXContactRawContactIdBuilder()
					.addContext(pContext).addTimestamp(pTimestamp)
					.addFromRowContactId(Long.parseLong(rawContactId))
					.getOXObject();
			if(contact.hasId()) {
				// Bestehenden Kontakt aktualisieren
				OXAction<String> actionUpdateContact = OXApplication
						.getActionDependencyManager().getOXActionUpdateContact(
								contact);
				actionUpdateContact.perform();
				if(actionUpdateContact.hasResult()) {
					ops.addAll(contact.resetDirtyFlag());
					pSyncResult.stats.numUpdates++;
				}
			} else {
				// Neuen Kontakt erstellen
				OXAction<String> action = OXApplication
						.getActionDependencyManager().getOXActionCreateContact(
								contact);
				action.perform();
				if(action.hasResult()) {
					ops.addAll(contact.addOXId(action.getResult()));
					ops.addAll(contact.resetDirtyFlag());
					pSyncResult.stats.numInserts++;
				}
			}
		}
		// Loeschen
		ArrayList<OXContact> deletedContacts = deletedContacts(pContext,
				pTimestamp);
		for(OXContact contact : deletedContacts) {
			OXAction<String> action = OXApplication
					.getActionDependencyManager().getOXActionDeleteContact(
							contact);
			if(action.hasResult()) {
				ops.addAll(contact.deleteContact());
				pSyncResult.stats.numDeletes++;
			}
		}
		OXApplication.getOXContentResolver().applyBatch(
				ContactsContract.AUTHORITY, ops);
		return;
	}

	public static void syncToPhone(Context pContext,
			List<OXContact> pOXContact, SyncResult pSyncResult,
			ArrayList<String> pCreatedContactsRawContactId)
			throws JSONException, NumberFormatException, RemoteException,
			OperationApplicationException {
		for(OXContact newContact : pOXContact) {
			// Ignore DistributionList-Contacts
			if(!newContact.hasDistributionList()) {
				Log.d(TAG,
						"[*][syncToPhone] The contact: '"
								+ newContact.toPrintableString()
								+ "' is no distributonlist");
				String rawContactId = newContact.getRawContactId();
				if(rawContactId != null) {
					Log.d(TAG,
							"[*][syncToPhone] Updating '"
									+ newContact.toPrintableString() + "'");
					// Um das aendern, einfuegen und loeschen von Informationen
					// abzuhandeln:
					pContext.getContentResolver().applyBatch(
							ContactsContract.AUTHORITY,
							newContact.updateContact());
					pSyncResult.stats.numUpdates++;
				} else {
					Log.d(TAG,
							"[*][syncToPhone] Creating '"
									+ newContact.toPrintableString() + "'");
					ContentProviderResult[] results = pContext
							.getContentResolver().applyBatch(
									ContactsContract.AUTHORITY,
									newContact.insertContact());
					pSyncResult.stats.numInserts++;
					if(pCreatedContactsRawContactId != null) pCreatedContactsRawContactId
							.add(String.valueOf(ContentUris
									.parseId(results[0].uri)));
				}
			}
		}
		return;
	}

	public static void checkForConflicts(Context pContext,
			ArrayList<OXContact> pOXContact,
			ArrayList<ContentProviderOperation> pOperations,
			List<String> pRawContactIds, SyncResult pSyncResult) {
		Log.d(TAG, "[*][checkForConflicts] called");
		for(Iterator<OXContact> contactIter = pOXContact.iterator(); contactIter
				.hasNext();) {
			OXContact contact = contactIter.next();
			String oxRawContactId = contact.getRawContactId();
			if(pRawContactIds.contains(oxRawContactId)) {
				Log.e(TAG, "[*][checkForConflicts] Konflikt erkannt: '"
						+ contact.toPrintableString() + "'");
				pOperations.addAll(reportConflict(pContext, contact));
				pOperations.addAll(contact.resetDirtyFlag());
				pRawContactIds.remove(oxRawContactId);
				contactIter.remove();
				pSyncResult.stats.numConflictDetectedExceptions++;
			}
		}
	}

	public static ArrayList<OXContact> deletedContacts(Context pContext,
			Date pTimestamp) throws NumberFormatException, JSONException {
		ArrayList<OXContact> deletedRawContacts = new ArrayList<OXContact>();
		// Android nach geloeschten RawContacts abfragen:
		Uri rawContactUri = RawContacts.CONTENT_URI
				.buildUpon()
				.appendQueryParameter(RawContacts.ACCOUNT_NAME,
						OXApplication.getAccountManager().getOXAccount().name)
				.appendQueryParameter(RawContacts.ACCOUNT_TYPE,
						OXApplication.getAccountManager().getOXAccount().type)
				.build();
		Cursor cursor = pContext.getContentResolver().query(rawContactUri,
				new String[] { RawContacts._ID }, RawContacts.DELETED + " = 1",
				null, RawContacts._ID);
		try {
			while (cursor.moveToNext()) {
				deletedRawContacts.add(new OXContactRawContactIdBuilder()
						.addContext(pContext)
						.addTimestamp(pTimestamp)
						.addFromRowContactId(
								Long.parseLong(cursor.getString(0)))
						.getOXObject());
			}
		} finally {
			cursor.close();
		}
		return deletedRawContacts;
	}

	public static LinkedHashSet<String> allOXContacts(Context pContext) {
		LinkedHashSet<String> oxIds = new LinkedHashSet<String>();
		Uri rawContactUri = RawContacts.CONTENT_URI
				.buildUpon()
				.appendQueryParameter(RawContacts.ACCOUNT_NAME,
						OXApplication.getAccountManager().getOXAccount().name)
				.appendQueryParameter(RawContacts.ACCOUNT_TYPE,
						OXApplication.getAccountManager().getOXAccount().type)
				.build();
		Cursor cursor = pContext.getContentResolver().query(
				rawContactUri,
				new String[] { RawContacts._ID },
				RawContacts.DELETED + " != 1 AND " + RawContacts.SYNC1
						+ " IS NOT \"1\"", null, RawContacts._ID);
		try {
			while (cursor.moveToNext()) {
				String selection = Data.MIMETYPE + " = ? AND "
						+ Data.RAW_CONTACT_ID + " = ?";
				String[] selectionArgs = { OXConstants.MIMETYPE_OXID,
						cursor.getString(0) };
				Cursor c = pContext.getContentResolver().query(
						Data.CONTENT_URI, new String[] { Data.DATA1 },
						selection, selectionArgs, null);
				try {
					while (c.moveToNext()) {
						oxIds.add(c.getString(0));
					}
				} finally {
					c.close();
				}
			}
		} finally {
			cursor.close();
		}
		return oxIds;
	}

	public static ArrayList<String> changedContacts(Context pContext) {
		// Android nach geaenderten RawContacts abfragen:
		Uri rawContactUri = RawContacts.CONTENT_URI
				.buildUpon()
				.appendQueryParameter(RawContacts.ACCOUNT_NAME,
						OXApplication.getAccountManager().getOXAccount().name)
				.appendQueryParameter(RawContacts.ACCOUNT_TYPE,
						OXApplication.getAccountManager().getOXAccount().type)
				.build();
		String selection = RawContacts.DIRTY + " = 1 AND "
				+ RawContacts.DELETED + " != 1 AND " + RawContacts.SYNC1
				+ " IS NOT \"1\"";
		Cursor cursor = pContext.getContentResolver().query(rawContactUri,
				new String[] { RawContacts._ID }, selection, null,
				RawContacts._ID);
		ArrayList<String> rawContactIds = new ArrayList<String>();
		try {
			while (cursor.moveToNext()) {
				rawContactIds.add(cursor.getString(0));
			}
		} finally {
			cursor.close();
		}
		return rawContactIds;
	}

	private static ArrayList<ContentProviderOperation> reportConflict(
			Context pContext, OXContact pContact) {
		ArrayList<ContentProviderOperation> operations = pContact
				.markAsConflict();
		ContentValues conflictValues = new ContentValues();
		conflictValues.put(SyncConflicts.OXID, pContact.getId());
		conflictValues
				.put(SyncConflicts.USERNAME, pContact.toPrintableString());
		conflictValues.put(SyncConflicts.TIMESTAMP, pContact.getTimestamp()
				.toGMTString());
		pContext.getContentResolver().insert(SyncConflicts.CONTENT_URI,
				conflictValues);
		// Create StatusBarNotification
		Notification notification = new NotificationCompat.Builder(pContext)
				.setSmallIcon(R.drawable.ic_stat_solveconflicts)
				.setAutoCancel(true)
				.setTicker(
						pContext.getString(R.string.notification_conflict_detected_title))
				.setContentIntent(makeDefaultIntent(pContext))
				.setContentTitle(
						pContext.getString(R.string.notification_conflict_detected_title))
				.setContentText(
						pContext.getString(R.string.notification_conflict_detected_text))
				.getNotification();
		OXApplication.getNotificationManager().notify(NOTIFICATION_ID,
				notification);
		return operations;
	}

	@SuppressLint("NewApi")
	private static PendingIntent makeDefaultIntent(Context pContext) {
		if(OXApplication.getUtils().isHoneyCombOrBetter()) {
			// A typical convention for notifications is to launch the user
			// deeply
			// into an application representing the data in the notification; to
			// accomplish this, we can build an array of intents to insert the
			// back
			// stack stack history above the item being displayed.
			Intent[] intents = new Intent[2];
			// Root-Activity
			intents[0] = Intent.makeRestartActivityTask(new ComponentName(
					pContext, OXClient.class));
			// OXClientSolveConflicts-Activity
			intents[1] = new Intent(pContext, SolveConflicts.class);
			return PendingIntent.getActivities(pContext, 0, intents,
					PendingIntent.FLAG_UPDATE_CURRENT);
		} else {
			Intent intent = new Intent(pContext, SolveConflicts.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			return PendingIntent.getActivity(pContext, 0, intent,
					PendingIntent.FLAG_UPDATE_CURRENT);
		}
	}
}
