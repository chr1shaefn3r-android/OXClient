/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.syncadapter;

import java.util.ArrayList;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;

public class SyncAdapter extends AbstractThreadedSyncAdapter {

	private final Context mContext;

	public SyncAdapter(Context pContext, boolean autoInitialize) {
		super(pContext, autoInitialize);
		this.mContext = pContext;
	}

	/**
	 * Usage of the four Sync-Own-columns: RawContacts-Table Sync1, aka
	 * data_sync1: Is set to 1, if this contact is detected to be a conflict
	 * Datas-Table Sync1, aka data_sync1: Used to refind the information
	 */
	@Override
	public void onPerformSync(Account pAccount, Bundle pExtras,
			String pAuthority, ContentProviderClient pProvider,
			SyncResult pSyncResult) {
		try {
			ContactManager
					.onPerformSync(mContext, pAccount, pExtras, pAuthority,
							pProvider, pSyncResult, new ArrayList<String>());
		} catch (Exception e) {
			throw new RuntimeException(e.getClass().getSimpleName() + ": "
					+ e.getLocalizedMessage());
		}
	}
}
