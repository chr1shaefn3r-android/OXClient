/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient;

import java.lang.reflect.Field;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;

import com.alien.oxclient.mockinterfaces.OXUtils;
import com.alien.oxclient.provider.OXMapping;
import com.alien.oxclient.provider.OXMappingDatabaseHelper;

/**
 * Utilityclass with different helpful methods
 */
public class OXUtilsImpl implements OXUtils {
	private static final String TAG = "OXClient";

	private Context mContext;

	public OXUtilsImpl(Context pContext) {
		this.mContext = pContext;
	}

	public OXUtilsImpl() {
	}

	public String constructColumns() {
		OXMappingDatabaseHelper mOXMappingDBHelper = new OXMappingDatabaseHelper(
				mContext);
		String columnsResult = "";
		Cursor c = mOXMappingDBHelper.query(
				new String[] { OXMapping.OX_NUMBER }, null, null,
				OXMapping.OX_POSITION, true);
		try {
			while (c.moveToNext()) {
				columnsResult = columnsResult.concat(c.getString(c
						.getColumnIndex(OXMapping.OX_NUMBER)));
				if(!c.isLast()) columnsResult = columnsResult.concat(",");
			}
		} finally {
			c.close();
			mOXMappingDBHelper.close();
		}
		return columnsResult;
	}

	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) mContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if(netInfo != null && netInfo.isConnected()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean hasOXAccount() {
		AccountManager am = AccountManager.get(mContext);
		Account[] accounts = am.getAccountsByType(mContext
				.getString(R.string.ACCOUNT_TYPE));
		if(accounts != null && accounts.length > 0) return true;
		else return false;
	}

	public boolean isHoneyCombOrBetter() {
		return(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB);
	}

	public boolean isIcsOrBetter() {
		return(Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH);
	}

	public boolean hasActionBar(Activity pActivity) {
		return(isIcsOrBetter() && pActivity.getActionBar() != null);
	}

	public String errorToString(JSONObject pJSON) {
		String error = null;
		try {
			if(!pJSON.isNull("error_params")) {
				String[] array = pJSON.getString("error")
						.replaceAll("%\\d\\$([sd])", "%$1").split("%");
				error = array[0];
				for(int i = 1, j = 0; i < array.length; i++, j++) {
					error = error
							+ String.format("%" + array[i],
									pJSON.getJSONArray("error_params")
											.getString(j));
				}
			} else {
				return pJSON.getString("error");
			}
		} catch (JSONException e) {
			Log.e(TAG,
					"[OXUtils->errorToString] JSONException: " + e.getMessage());
			e.printStackTrace();
		}
		return error;
	}

	public void toggleButton(Button pButton, boolean pState) {
		pButton.setEnabled(pState);
		pButton.setClickable(pState);
		pButton.setFocusable(pState);
		if(pState) { // switch-on -> make bright
			pButton.getBackground().setAlpha(255);
		} else { // switch-off -> make transparent
			pButton.getBackground().setAlpha(150);
		}
		pButton.invalidate();
		return;
	}

	public void toggleButton(ImageButton pButton, boolean pState) {
		pButton.setEnabled(pState);
		pButton.setClickable(pState);
		pButton.setFocusable(pState);
		if(pState) { // power-on -> make bright
			pButton.getBackground().setAlpha(255);
		} else { // switch-off -> make transparent
			pButton.getBackground().setAlpha(150);
		}
		pButton.invalidate();
		return;
	}

	public String checkAndcorrectServerInput(String pServer)
			throws IllegalArgumentException {
		// Get rid of starting "http://" if the user supplied it
		String result = pServer.replaceFirst("^([a-zA-Z0-9]+)://", "");
		// Get rid of trailing slash if the user supplied it
		result = result.replaceFirst("/.*$", "");
		// check if this could be a URL or an IP-Adress
		if(!result.matches("^[a-zA-Z0-9\\-\\.]+\\.[a-zA-Z]{2,3}$")
				&& !result
						.matches("\\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b")) { throw new IllegalArgumentException(
				"Given server is not URL: " + result); }
		return result;
	}

	public boolean isIntentAvailable(String pAction) {
		final Intent intent = new Intent(pAction);
		return isIntentAvailable(intent);
	}

	public boolean isIntentAvailable(Intent pIntent) {
		final PackageManager packageManager = mContext.getPackageManager();
		List<ResolveInfo> list = packageManager.queryIntentActivities(pIntent,
				PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}

	public String getStringFromName(Context pContext, String pName) {
		Field field;
		try {
			field = R.string.class.getField(pName);
			return pContext.getString(field.getInt(null));
		} catch (Exception e) {
			throw new RuntimeException(e.getClass().getSimpleName() + ": "
					+ e.getLocalizedMessage());
		}
	}
}
