/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient;

import java.io.IOException;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.os.Bundle;

import com.alien.oxclient.mockinterfaces.OXAccountManager;

public class OXAccountManagerImpl implements OXAccountManager {

	private AccountManager mAccountManager;
	private Context mContext;

	public OXAccountManagerImpl(Context pContext) {
		this.mContext = pContext;
		this.mAccountManager = AccountManager.get(mContext);
	}

	public String getAccountServer() {
		return mAccountManager.getUserData(getOXAccount(), OXConstants.SERVER);
	}

	public void setAccountServer(String pServer) {
		mAccountManager
				.setUserData(getOXAccount(), OXConstants.SERVER, pServer);
	}

	public boolean getEncrypted() {
		return Boolean.getBoolean(mAccountManager.getUserData(getOXAccount(),
				OXConstants.ENCRYPTED));
	}

	public void setEncrypted(boolean pEncrypted) {
		this.setEncrypted(getOXAccount(), pEncrypted);
	}

	public void setEncrypted(Account pAccount, boolean pEncrypted) {
		mAccountManager.setUserData(pAccount, OXConstants.ENCRYPTED,
				String.valueOf(pEncrypted));
	}

	public String getContactsFolder() {
		return mAccountManager.getUserData(getOXAccount(),
				OXConstants.CONTACTSFOLDER);
	}

	public void setContactsFolder(String pContactsFolder) {
		mAccountManager.setUserData(getOXAccount(), OXConstants.CONTACTSFOLDER,
				pContactsFolder);
	}

	public String getContactsFolderId() {
		return mAccountManager.getUserData(getOXAccount(),
				OXConstants.CONTACTSFOLDERID);
	}

	public void setContactsFolderId(String pContactsFolderId) {
		mAccountManager.setUserData(getOXAccount(),
				OXConstants.CONTACTSFOLDERID, pContactsFolderId);
	}

	public Account getOXAccount() {
		Account[] accounts = mAccountManager.getAccountsByType(mContext
				.getString(R.string.ACCOUNT_TYPE));
		if(accounts.length > 0) return accounts[0];
		else return null;
	}

	public void setPassword(String pPassword) {
		mAccountManager.setPassword(getOXAccount(), pPassword);
	}

	public void addAccountExplicitly(Account pAccount, String pPassword,
			Bundle pUserData) {
		mAccountManager.addAccountExplicitly(pAccount, pPassword, pUserData);
	}

	public void setAuthToken(String pAuthTokenType, String pAuthToken) {
		mAccountManager
				.setAuthToken(getOXAccount(), pAuthTokenType, pAuthToken);
	}

	public String getPassword(Account pAccount) {
		return mAccountManager.getPassword(pAccount);
	}

	public String getUserData(Account pAccount, String pKey) {
		return mAccountManager.getUserData(pAccount, pKey);
	}

	public void invalidateAuthToken(String pAuthTokenType, String pAuthToken) {
		mAccountManager.invalidateAuthToken(pAuthTokenType, pAuthToken);
	}

	public String blockingGetAuthToken(String pAuthTokenType,
			boolean pNotifyAuthFailure) throws OperationCanceledException,
			AuthenticatorException, IOException {
		return mAccountManager.blockingGetAuthToken(getOXAccount(),
				pAuthTokenType, pNotifyAuthFailure);
	}
}
