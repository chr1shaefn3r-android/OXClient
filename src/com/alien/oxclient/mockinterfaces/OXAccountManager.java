/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.mockinterfaces;

import java.io.IOException;

import android.accounts.Account;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.os.Bundle;

public interface OXAccountManager {

	/*
	 * These two constants HAVE TO BE IN SYNC with the corresponding
	 * string-ressources!
	 */
	public static String ACCOUNT_TYPE = "com.openxchange.android";
	public static String AUTHTOKEN_TYPE = "main";

	/**
	 * Querries the AccountManager for the Server
	 * 
	 * @return The server saved in the current OXAccount
	 */
	public String getAccountServer();

	/**
	 * Set a new server url
	 * 
	 * @param pServer
	 *            The server url to be set
	 */
	public void setAccountServer(String pServer);

	/**
	 * Querries the AccountManager for the encrypted switch
	 * 
	 * @return The boolean saved in the current OXAccount
	 */
	public boolean getEncrypted();

	/**
	 * Set a value for the encrypted switch
	 * 
	 * @param pEncrypted
	 *            The new value to be set
	 */
	public void setEncrypted(boolean pEncrypted);

	/**
	 * Set a value for the encrypted switch
	 * 
	 * @param pAccount
	 *            The account to set the value
	 * @param pEncrypted
	 *            The new value to be set
	 */
	public void setEncrypted(Account pAccount, boolean pEncrypted);

	/**
	 * Querries the AccountManager for the ContactsFolder
	 * 
	 * @return The name of the ContactsFolder saved in the current OXAccount
	 */
	public String getContactsFolder();

	/**
	 * Set a new ContactsFolder
	 * 
	 * @param pContactsFolder
	 *            The new name of the ContactsFolder
	 */
	public void setContactsFolder(String pContactsFolder);

	/**
	 * Querries the AccountManager for the Id of the ContactsFolder
	 * 
	 * @return The Id of the ContactsFolder saved in the current OXAccount
	 */
	public String getContactsFolderId();

	/**
	 * Set a new ContactsFolderId
	 * 
	 * @param pContactsFolderId
	 *            The new Id of the ContactsFolder
	 */
	public void setContactsFolderId(String pContactsFolderId);

	/**
	 * Queries the AccountManager for the one (and only) openXChange-Account
	 * 
	 * @return The one openXChange-Account
	 */
	public Account getOXAccount();

	/**
	 * Set the password of the openXChange-Account
	 * 
	 * @param pPassword
	 *            The password to be set
	 */
	public void setPassword(String pPassword);

	/**
	 * Adds a new account to the AccountManager
	 * 
	 * @param pAccount
	 *            The account to be added
	 * @param pPassword
	 *            The password to be associated with this account
	 * @param pUserData
	 *            String values to be used as userdata
	 */
	public void addAccountExplicitly(Account pAccount, String pPassword,
			Bundle pUserData);

	/**
	 * Set a authtoken to the openXChange-Account
	 * 
	 * @param pAuthTokenType
	 *            The AuthTokenType to be set
	 * @param pAuthToken
	 *            The AuthToken to be set
	 */
	public void setAuthToken(String pAuthTokenType, String pAuthToken);

	/**
	 * Gets the saved password associated with the account. This is intended for
	 * authenticators and related code; applications should get an auth token
	 * instead.
	 * 
	 * It is safe to call this method from the main thread.
	 * 
	 * This method requires the caller to hold the permission
	 * AUTHENTICATE_ACCOUNTS and to have the same UID as the account's
	 * authenticator.
	 * 
	 * @param pAccount
	 *            The account to query for a password
	 * @return The account's password, null if none or if the account doesn't
	 *         exist
	 */
	public String getPassword(Account pAccount);

	/**
	 * Gets the user data named by "key" associated with the account. This is
	 * intended for authenticators and related code to store arbitrary metadata
	 * along with accounts. The meaning of the keys and values is up to the
	 * authenticator for the account.
	 * 
	 * It is safe to call this method from the main thread.
	 * 
	 * This method requires the caller to hold the permission
	 * AUTHENTICATE_ACCOUNTS and to have the same UID as the account's
	 * authenticator.
	 * 
	 * @param pAccount
	 *            The account to query for user data
	 * @return The user data, null if the account or key doesn't exist
	 */
	public String getUserData(Account pAccount, String pKey);

	/**
	 * Tell AccountManager to throw away a AuthToken
	 * 
	 * @param pAuthTokenType
	 *            The AuthTokenType to invalidate, must not be null
	 * @param pAuthToken
	 *            The AuthToken to invalidate, may be null
	 */
	public void invalidateAuthToken(String pAuthTokenType, String pAuthToken);

	/**
	 * Get a new AuthToken from the Authenticator
	 * 
	 * @param pAuthTokenType
	 *            The AuthThokenType to get
	 * @param pNotifyAuthFailure
	 *            If true, display a notification and return null if
	 *            authentication fails; if false, prompt and wait for the user
	 *            to re-enter correct credentials before returning
	 * @return The new AuthToken
	 * @throws IOException
	 * @throws AuthenticatorException
	 * @throws OperationCanceledException
	 */
	public String blockingGetAuthToken(String pAuthTokenType,
			boolean pNotifyAuthFailure) throws OperationCanceledException,
			AuthenticatorException, IOException;
}
