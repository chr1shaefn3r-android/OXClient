package com.alien.oxclient.mockinterfaces;

import android.app.Notification;

public interface OXNotificationManager {

	/**
	 * Post a notification to be shown in the status bar. If a notification with
	 * the same id has already been posted by your application and has not yet
	 * been canceled, it will be replaced by the updated information.
	 * 
	 * @param pId
	 *            An identifier for this notification. Has to be unique.
	 * @param pNotification
	 *            A Notification object describing what to show the user. Must
	 *            not be null.
	 */
	public void notify(int pId, Notification pNotification);
}
