/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.mockinterfaces;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Button;
import android.widget.ImageButton;

public interface OXUtils {
	/**
	 * Returns a list of columns based on the OXMappingDatabase
	 * 
	 * @return A comma-seperated list of columnids
	 */
	public String constructColumns();

	/**
	 * Check for internet connectivity
	 * 
	 * @return Whether the mobile phone has a working internet connection
	 */
	public boolean isOnline();

	/**
	 * Checks wether there is already an openXChange-Account
	 * 
	 * @return Whether there is already an openXChange-Account
	 */
	public boolean hasOXAccount();

	/**
	 * Checks wether the current device is Honeycomb or better
	 * 
	 * @return Whether the current device has API-Level 11 or higher
	 */
	public boolean isHoneyCombOrBetter();

	/**
	 * Checks wether the current device is ICS or better
	 * 
	 * @return Whether the current device has API-Level 14 or higher
	 */
	public boolean isIcsOrBetter();

	/**
	 * Checks wether an ActionBar is available
	 * 
	 * @return Whether the current device has an ActionBar
	 */
	public boolean hasActionBar(Activity pActivity);

	/**
	 * Combines the error_params and the error field of the JSONObject to an
	 * errorstring.
	 * 
	 * @return The errorstring
	 */
	public String errorToString(JSONObject pJSON);

	/**
	 * Deactivate or activate a button
	 * 
	 * @params pButton The button to toggle
	 * @params pState Whether to turn it on or off
	 */
	public void toggleButton(Button pButton, boolean pState);

	/**
	 * Deactivate or activate a button
	 * 
	 * @params pButton The button to toggle
	 * @params pState Whether to turn it on or off
	 */
	public void toggleButton(ImageButton pButton, boolean pState);

	/**
	 * Check and correct server input
	 * 
	 * @params pServer The URL as a string
	 * @return The corrected string
	 * @throws IllegalArgumentException
	 */
	public String checkAndcorrectServerInput(String pServer);

	/**
	 * Indicates whether the specified action can be used as an intent. This
	 * method queries the package manager for installed packages that can
	 * respond to an intent with the specified action. If no suitable package is
	 * found, this method returns false.
	 * 
	 * @param pAction
	 *            The Intent action to check for availability.
	 * @return True if an Intent with the specified action can be sent and
	 *         responded to, false otherwise.
	 */
	public boolean isIntentAvailable(String pAction);

	public boolean isIntentAvailable(Intent pIntent);

	/**
	 * Uses reflection to grab a string from the R.string-class by name.
	 * 
	 * @param pContext
	 *            The application Context
	 * @param pName
	 *            The name of the string
	 * @return The string saved under the passed name in the R.string-Class
	 */
	public String getStringFromName(Context pContext, String pName);
}
