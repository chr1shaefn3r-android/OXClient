/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.mockinterfaces;

import java.util.Calendar;
import java.util.Date;

public interface OXCalendar {

	/**
	 * Copies the hour and minutes from a Date-Object into the calendar-object
	 * 
	 * @param pDate
	 *            The date to copy from
	 */
	public void setHourAndMinute(Date pDate);

	/**
	 * Computes the time from the fields if required and returns the time.
	 * 
	 * @return The time of this calendar
	 * @throws IllegalArgumentException
	 *             If the time is not set and the time cannot be computed from
	 *             the current field values.
	 */
	public long getTimeInMillis() throws IllegalArgumentException;

	/**
	 * Gets the value of the specified field after computing the field values by
	 * calling complete() first.
	 * 
	 * @param pField
	 *            The field to get
	 * @return The value of the specific field
	 */
	public int get(int pField);

	/**
	 * Returns whether the Date specified by this Calendar instance is after the
	 * Date specified by the parameter.
	 * 
	 * @param pCalendar
	 *            The Calendar instance to compare.
	 * @return True when this Calendar is after calendar, false otherwise.
	 * @throws IllegalArgumentException
	 *             If the time is not set and the time cannot be computed from
	 *             the current field values.
	 */
	public boolean after(OXCalendar pCalendar) throws IllegalArgumentException;

	/**
	 * Returns whether the Date specified by this Calendar instance is before
	 * the Date specified by the paramter.
	 * 
	 * @param pCalendar
	 *            The Calendar instance to compare.
	 * @return True when this Calendar is before calendar, false otherwise.
	 * @throws IllegalArgumentException
	 *             If the time is not set and the time cannot be computed from
	 *             the current field values.
	 */
	public boolean before(OXCalendar pCalendar) throws IllegalArgumentException;

	/**
	 * Refreshes the Calendar to the current time
	 * 
	 * @return The refresh OXCalendar-Object itself
	 */
	public OXCalendar refresh();

	/**
	 * Returns a new Calendar with the same properties
	 * 
	 * @return A shallow copy of this Calendar.
	 * @throws CloneNotSupportedException
	 *             If this object's class does not implement the Cloneable
	 *             interface.
	 */
	public Object clone() throws CloneNotSupportedException;

	/**
	 * Getter-Method
	 * 
	 * @return The underlying Calendar-Instace
	 */
	public Calendar getCalendar();

	/**
	 * Force a implemented toString-Method
	 * 
	 * @return A humanily readable representation of this object
	 */
	public String toString();
}
