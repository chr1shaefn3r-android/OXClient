/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.mockinterfaces;

public interface OXPreferences {

	public static final String VERSIONCODE = "versioncode";
	public static final int VERSIONCODE_DEFAULT = 0;
	public static final String LANGUAGE = "language";
	public static final String LANGUAGE_DEFAULT = "default";
	public static final String DEFAULTLANGUAGE = "default_language";
	public static final String DEFAULTLANGUAGE_DEFAULT = "";
	public static final String FIRST_EMAIL_MAPPING = "first_email_mapping";
	public static final String SECOND_EMAIL_MAPPING = "second_email_mapping";
	public static final String THIRD_EMAIL_MAPPING = "third_email_mapping";
	public static final String LAST_SYNC = "last_sync";
	public static final long LAST_SYNC_DEFAULT = 0;
	public static final String SYNC_FREQUENCY = "sync_frequency";
	public static final String SYNC_FREQUENCY_DEFAULT = "900";
	public static final String CONSTANT_SYNC_FREQUENCY = "constanty_sync_frequency";
	public static final boolean CONSTANT_SYNC_FREQUENCY_DEFAULT = true;
	public static final String MAINTIME_DAYS = "variable_sync_frequency_maintime_days";
	public static final String MAINTIME_DAYS_DEFAULT = "2;3;4;5;6;7;1";
	public static final String MAINTIME_BEGIN = "variable_sync_frequency_maintime_begin";
	public static final String MAINTIME_BEGIN_DEFAULT = "08:00";
	public static final String MAINTIME_END = "variable_sync_frequency_maintime_end";
	public static final String MAINTIME_END_DEFAULT = "20:00";
	public static final String MAINTIME_SYNC_FREQUENCY = "variable_sync_frequency_maintime";
	public static final String MAINTIME_SYNC_FREQUENCY_DEFAULT = "900";
	public static final String SECONDARYTIME_SYNC_FREQUENCY = "variable_sync_frequency_secondarytime";
	public static final String SECONDARYTIME_SYNC_FREQUENCY_DEFAULT = "7200";
	public static final String INFORMATIONDIALOG_SEEN = "informationendialog_seen";
	public static final boolean INFORMATIONDIALOG_SEEN_DEFAULT = false;
	public static final String SYNC_PICTURES_ONLY_ON_WIFI = "sync_pictures_on_wifi";
	public static final boolean SYNC_PICTURES_ONLY_ON_WIFI_DEFAULT = true;

	/**
	 * Returns the last saved versioncode
	 * 
	 * @return Saved Versioncode
	 */
	public int getVersionCode();

	/**
	 * Set the versioncode to a specific value
	 * 
	 * @param pVersionCode
	 *            The versioncode to be set
	 */
	public void setVersionCode(int pVersionCode);

	/**
	 * Returns the last saved language
	 * 
	 * @return Saved language
	 */
	public String getLanguage();

	/**
	 * Returns the saved default language
	 * 
	 * @return Saved default language
	 */
	public String getDefaultLanguage();

	/**
	 * Set the default language to a specific value
	 * 
	 * @param pDefaultLanguage
	 *            The default language to be set
	 */
	public void setDefaultLanguage(String pDefaultLanguage);

	/**
	 * Returns the first email-mapping
	 * 
	 * @return Saved first email-mapping
	 */
	public String getFirstEmailMapping();

	/**
	 * Set the first email-mapping to a specific value
	 * 
	 * @param pFirstEmailMapping
	 *            The first email-mapping
	 */
	public void setFirstEmailMapping(String pFirstEmailMapping);

	/**
	 * Returns the second email-mapping
	 * 
	 * @return Saved second email-mapping
	 */
	public String getSecondEmailMapping();

	/**
	 * Set the first second-mapping to a specific value
	 * 
	 * @param pSecondEmailMapping
	 *            The first second-mapping
	 */
	public void setSecondEmailMapping(String pSecondEmailMapping);

	/**
	 * Returns the third email-mapping
	 * 
	 * @return Saved third email-mapping
	 */
	public String getThirdEmailMapping();

	/**
	 * Set the first third-mapping to a specific value
	 * 
	 * @param pThirdEmailMapping
	 *            The first third-mapping
	 */
	public void setThirdEmailMapping(String pThirdEmailMapping);

	/**
	 * Returns the saved last sync
	 * 
	 * @return Saved last sync
	 */
	public long getLastSync();

	/**
	 * Set the last sync to a specific value
	 * 
	 * @param pLastSync
	 *            The last sync to be set
	 */
	public void setLastSync(long pLastSync);

	/**
	 * Returns the flag "ConstantSyncFrequency"
	 * 
	 * @return Saved flag
	 */
	public boolean getIsConstantSyncFrequency();

	/**
	 * Returns the saved days when maintime shouold be
	 * 
	 * @return Saved days of maintime
	 */
	public String[] getMaintimeDays();

	/**
	 * Returns the saved time when maintime should begin
	 * 
	 * @return Saved begin of maintime
	 */
	public String getMaintimeBegin();

	/**
	 * Returns the saved time when maintime should end
	 * 
	 * @return Saved end of maintime
	 */
	public String getMaintimeEnd();

	/**
	 * Returns the saved sync frequency in maintime
	 * 
	 * @return Saved sync frequency
	 */
	public String getMaintimeSyncFrequency();

	/**
	 * Returns the saved sync frequency in secondarytime
	 * 
	 * @return Saved sync frequency
	 */
	public String getSecondarytimeSyncFrequency();

	/**
	 * Returns the saved sync frequency
	 * 
	 * @return Saved sync frequency
	 */
	public String getSyncFrequency();

	/**
	 * Returns the flag "InformationDialogSeen"
	 * 
	 * @return Saved flag
	 */
	public boolean getInformationenDialogSeen();

	/**
	 * Set the flag "InformationDialogSeen"
	 * 
	 * @param pInformationenDialogSeen
	 *            The flag to be set
	 */
	public void setInformationenDialogSeen(boolean pInformationenDialogSeen);

	/**
	 * Returns the flag "SyncPicturesOnlyOnWifi"
	 * 
	 * @return Saved flag
	 */
	public boolean getSyncPicturesOnlyOnWifi();

	/**
	 * Set the flag "SyncPicturesOnlyOnWifi"
	 * 
	 * @param pSyncPicturesOnlyOnWifi
	 *            The flag to be set
	 */
	public void setSyncPicturesOnlyOnWifi(boolean pSyncPicturesOnlyOnWifi);
}
