/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient;

import java.util.ArrayList;
import java.util.Locale;

import org.acra.ACRA;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TabHost;

import com.alien.oxclient.activities.FolderList;
import com.alien.oxclient.activities.Preferences;
import com.alien.oxclient.dialog.ChangelogDialogFragment;
import com.alien.oxclient.dialog.ChangelogDialogFragment.OnChangelogDialogInterface;
import com.alien.oxclient.dialog.InformationDialogFragment;
import com.alien.oxclient.dialog.InformationDialogFragment.OnInformationDialogInterface;
import com.alien.oxclient.dialog.StartDialogFragment;
import com.alien.oxclient.dialog.StartDialogFragment.OnStartDialogInterface;
import com.alien.oxclient.fragments.HomeScreenCalendarFragment;
import com.alien.oxclient.fragments.HomeScreenConfigurationFragment;
import com.alien.oxclient.fragments.HomeScreenContactsFragment;
import com.alien.oxclient.fragments.HomeScreenInfostoreFragment;
import com.alien.oxclient.fragments.HomeScreenMailFragment;
import com.alien.oxclient.fragments.HomeScreenTasksFragment;
import com.alien.oxclient.mockinterfaces.OXPreferences;
import com.alien.oxclient.ui.Tab;
import com.alien.oxclient.ui.TabsAdapter;

/**
 * Main activity
 */
public class OXClient extends FragmentActivity implements
		OnStartDialogInterface, OnChangelogDialogInterface,
		OnInformationDialogInterface {
	private static final String TAG = "OXClient";
	private static final String CHANGELOGDIALOG = "changelog";
	private static final String INFORMATIONDIALOG = "information";
	private static final String STARTDIALOG = "start";

	private int mVersionCode = 0;
	private TabHost mTabHost;
	private ViewPager mViewPager;
	private TabsAdapter mTabsAdapter;

	@Override
	public void onCreate(Bundle pSavedInstanceState) {
		super.onCreate(pSavedInstanceState);

		OXPreferences prefs = OXApplication.getPrefs();
		String languageToLoad = prefs.getLanguage();
		Locale locale = null;
		if(languageToLoad.compareTo("default") != 0) {
			locale = new Locale(languageToLoad);
		} else {
			if(!TextUtils.isEmpty(prefs.getDefaultLanguage())) locale = new Locale(
					prefs.getDefaultLanguage());
		}
		if(locale != null) {
			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			Resources r = getBaseContext().getResources();
			r.updateConfiguration(config, r.getDisplayMetrics());
		}

		setContentView(R.layout.oxclient);

		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();

		mViewPager = (ViewPager) findViewById(R.id.oxclient_pager);

		mTabsAdapter = new TabsAdapter(this, mTabHost, mViewPager);

		// Liste aller Tabs anlegen
		ArrayList<Tab> tabList = new ArrayList<Tab>();
		tabList.add(new Tab(this, getString(R.string.oxclient_tab_contact),
				HomeScreenContactsFragment.class, R.drawable.ic_contacts, null));
		tabList.add(new Tab(this, getString(R.string.oxclient_tab_mail),
				HomeScreenMailFragment.class, R.drawable.ic_mail, null));
		tabList.add(new Tab(this, getString(R.string.oxclient_tab_tasks),
				HomeScreenTasksFragment.class, R.drawable.ic_tasks, null));
		tabList.add(new Tab(this, getString(R.string.oxclient_tab_calendar),
				HomeScreenCalendarFragment.class, R.drawable.ic_calendar, null));
		tabList.add(new Tab(this,
				getString(R.string.oxclient_tab_configuration),
				HomeScreenConfigurationFragment.class,
				R.drawable.ic_configuration, null));
		tabList.add(new Tab(this, getString(R.string.oxclient_tab_infostore),
				HomeScreenInfostoreFragment.class, R.drawable.ic_infostore,
				null));
		// Liste der Tabs dem TabHost bekanntmachen

		for(Tab tab : tabList) {
			tab.addTab(mTabsAdapter, mTabHost);
		}
	}

	@Override
	protected void onResume() {
		OXPreferences prefs = OXApplication.getPrefs();
		if(OXApplication.getUtils().hasOXAccount()) {
			// read current version information about this package
			PackageManager manager = getPackageManager();
			PackageInfo info = new PackageInfo();
			try {
				info = manager.getPackageInfo(getPackageName(), 0);
				mVersionCode = info.versionCode;
			} catch (NameNotFoundException e) {
				// Interesting to know why this part failed
				ACRA.getErrorReporter().handleSilentException(e);
				Log.e(TAG,
						"[OXClient->onResume] NameNotFoundException: "
								+ e.getMessage());
				e.printStackTrace();
			}
			if(mVersionCode != 0) {
				if(mVersionCode == prefs.getVersionCode()) {
					// Nothing changed => nothing to do
				} else if(mVersionCode > prefs.getVersionCode()) {
					// The queried version is greater then the saved version
					// => Show Changelog with "OK"-Button
					if(getSupportFragmentManager().findFragmentByTag(
							CHANGELOGDIALOG) == null) {
						// Show ChangelogDialog only when he is not already
						// there
						DialogFragment newFragment = ChangelogDialogFragment
								.newInstance(info.versionName);
						try {
							newFragment.show(getSupportFragmentManager(),
									CHANGELOGDIALOG);
						} catch (IllegalStateException e) {
							// Workaround
							Intent thisActivity = new Intent(this,
									OXClient.class);
							startActivity(thisActivity);
							finish();
						}
					}
				} else {
					// The queried version is less then the saved version
					// => Downgrade => update saved version
					prefs.setVersionCode(mVersionCode);
				}
			}
		} else {
			// Query DefaultLanguage and save
			prefs.setDefaultLanguage(Locale.getDefault().toString()
					.substring(0, 2));
			// StartDialog: Will the user create an account or quit?
			if(getSupportFragmentManager().findFragmentByTag(STARTDIALOG) == null) {
				// Show Startdialog only when he is not already there
				DialogFragment newFragment = StartDialogFragment.newInstance();
				try {
					newFragment.show(getSupportFragmentManager(), STARTDIALOG);
				} catch (IllegalStateException e) {
					// Workaround
					Intent thisActivity = new Intent(this, OXClient.class);
					startActivity(thisActivity);
					finish();
				}
			}
		}
		super.onResume();
	}

	/*****************************************************************************************************************************/
	/* --> .:DialogCallbacks:. <-- */
	/*****************************************************************************************************************************/
	public void onChangelogDialogCancel() {
		OXApplication.getPrefs().setVersionCode(mVersionCode);
		if(OXApplication.getPrefs().getInformationenDialogSeen()) {
			// Workaround
			Intent thisActivity = new Intent(this, OXClient.class);
			startActivity(thisActivity);
			finish();
		} else {
			DialogFragment newFragment = InformationDialogFragment
					.newInstance(false);
			newFragment.show(getSupportFragmentManager(), INFORMATIONDIALOG);
		}
	}

	public void onStartDialogNegativClick() {
		finish();
	}

	public void onStartDialogPositivClick() {
		Intent createAccount = new Intent(Settings.ACTION_ADD_ACCOUNT);
		DialogFragment dialogFragment = (DialogFragment) getSupportFragmentManager()
				.findFragmentByTag(STARTDIALOG);
		dialogFragment.dismiss();
		startActivity(createAccount);
	}

	public void onStartDialogCancel() {
		finish();
	}

	public void onInformationDialogDismissed() {
		// Workaround
		Intent thisActivity = new Intent(this, OXClient.class);
		startActivity(thisActivity);
		finish();
	}

	/*****************************************************************************************************************************/
	/* --> .:Menu-Methoden:. <-- */
	/*****************************************************************************************************************************/
	@Override
	public boolean onCreateOptionsMenu(Menu pMenu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.oxclient, pMenu);
		return super.onCreateOptionsMenu(pMenu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem pMenuItem) {
		switch (pMenuItem.getItemId()) {
		case R.id.oxclient_menu_folderlist:
			Intent folderIntent = new Intent(OXClient.this, FolderList.class);
			startActivity(folderIntent);
			break;
		case R.id.oxclient_menu_infodialog:
			DialogFragment newFragment = InformationDialogFragment
					.newInstance(false);
			newFragment.show(getSupportFragmentManager(), INFORMATIONDIALOG);
			break;
		case R.id.oxclient_menu_preferences:
			Intent settingsIntent = new Intent(OXClient.this, Preferences.class);
			startActivity(settingsIntent);
			break;
		case R.id.oxclient_menu_quit:
			finish();
			break;
		}
		return super.onOptionsItemSelected(pMenuItem);
	}
}
