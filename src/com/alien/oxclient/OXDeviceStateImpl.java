/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.alien.oxclient.mockinterfaces.OXDeviceState;
import com.alien.oxclient.mockinterfaces.OXDeviceStateObserver;

public class OXDeviceStateImpl implements OXDeviceStateObserver, OXDeviceState {

	private Context mContext;
	private ConnectivityManager mConnectivityManager;
	private boolean mIsConnected;
	private boolean mOnWifi;

	public OXDeviceStateImpl(Context pContext) {
		this.mContext = pContext;
		mConnectivityManager = (ConnectivityManager) mContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		updateConnectivity();
	}

	public boolean isOnWifi() {
		return mOnWifi;
	}

	public void shutdownAllReceivers() {
		// TODO: Shut down Low-Battery- and (Un)Plugged-Receiver
	}

	public void startAllReceivers() {
		// TODO: (Re)Start Low-Battery- and (Un)Plugged-Receiver
	}

	public void updateConnectivity() {
		NetworkInfo activeNW = mConnectivityManager.getActiveNetworkInfo();
		mIsConnected = (activeNW != null && activeNW.isConnectedOrConnecting());
		if(mIsConnected) {
			mOnWifi = activeNW.getType() == ConnectivityManager.TYPE_WIFI;
		} else {
			mOnWifi = false;
			shutdownAllReceivers();
		}
	}

	public void connectionChanged() {
		updateConnectivity();
	}
}
