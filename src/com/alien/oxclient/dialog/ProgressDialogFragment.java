/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.alien.oxclient.R;

public class ProgressDialogFragment extends DialogFragment {

	// Container Activity must implement this interface
	public interface OnProgressDialogInterface {
		public void onProgressDialogCancel();
	}

	private OnProgressDialogInterface mListener;

	public static ProgressDialogFragment newInstance(Context pContext) {
		ProgressDialogFragment frag = new ProgressDialogFragment();
		return frag;
	}

	@Override
	public void onAttach(Activity pActivity) {
		super.onAttach(pActivity);
		try {
			mListener = (OnProgressDialogInterface) pActivity;
		} catch (ClassCastException e) {
			throw new ClassCastException(pActivity.toString()
					+ " must implement OnArticleSelectedListener");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle pSavedInstanceState) {
		ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "",
				getString(R.string.progressdialogfragment_loading_please_wait),
				true, true, null);
		return progressDialog;
	}

	@Override
	public void onCancel(DialogInterface pDialog) {
		mListener.onProgressDialogCancel();
	}
}