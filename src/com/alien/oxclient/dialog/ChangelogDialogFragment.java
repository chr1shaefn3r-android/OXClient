/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.alien.oxclient.R;

public class ChangelogDialogFragment extends DialogFragment {

	// Container Activity must implement this interface
	public interface OnChangelogDialogInterface {
		public void onChangelogDialogCancel();
	}

	private static OnChangelogDialogInterface mListener;
	private ChangelogDialog mChangelog;

	public static ChangelogDialogFragment newInstance(String pVersion) {
		ChangelogDialogFragment frag = new ChangelogDialogFragment();
		Bundle args = new Bundle();
		args.putString("version", pVersion);
		frag.setArguments(args);
		return frag;
	}

	@Override
	public void onAttach(Activity pActivity) {
		super.onAttach(pActivity);
		try {
			mListener = (OnChangelogDialogInterface) pActivity;
		} catch (ClassCastException e) {
			throw new ClassCastException(pActivity.toString()
					+ " must implement OnChangelogDialogInterface");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String version = getArguments().getString("version");

		mChangelog = new ChangelogDialog(getActivity());

		mChangelog.setCurrentVersion(version);
		mChangelog.addChange(
				getString(R.string.changelogdialog_version_preAlpha),
				getString(R.string.changelogdialog_version_preAlpha_changes));
		mChangelog.addChange(getString(R.string.changelogdialog_version_Alpha),
				getString(R.string.changelogdialog_version_Alpha_changes));
		mChangelog.addChange(getString(R.string.changelogdialog_version_001),
				getString(R.string.changelogdialog_version_001_changes));
		mChangelog.show();

		return mChangelog;
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		mListener.onChangelogDialogCancel();
		super.onCancel(dialog);
	}
}
