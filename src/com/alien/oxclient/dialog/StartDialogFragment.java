/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.alien.oxclient.R;

public class StartDialogFragment extends DialogFragment {

	// Container Activity must implement this interface
	public interface OnStartDialogInterface {
		public void onStartDialogNegativClick();

		public void onStartDialogPositivClick();

		public void onStartDialogCancel();
	}

	private static OnStartDialogInterface mListener;

	public static StartDialogFragment newInstance() {
		StartDialogFragment frag = new StartDialogFragment();
		return frag;
	}

	@Override
	public void onAttach(Activity pActivity) {
		super.onAttach(pActivity);
		try {
			mListener = (OnStartDialogInterface) pActivity;
		} catch (ClassCastException e) {
			throw new ClassCastException(pActivity.toString()
					+ " must implement OnStartDialogInterface");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return new AlertDialog.Builder(getActivity())
				.setTitle(R.string.oxclient_createAccountTitle)
				.setMessage(R.string.oxclient_createAccountMessage)
				.setCancelable(false)
				.setPositiveButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								mListener.onStartDialogPositivClick();
							}
						})
				.setNegativeButton(android.R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								mListener.onStartDialogNegativClick();
							}
						}).create();
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		mListener.onStartDialogCancel();
		super.onCancel(dialog);
	}
}
