/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.alien.oxclient.R;

public class ChangelogDialog extends Dialog {
	private static final String TAG = "OXClient";

	private String mChangelogContent;
	private TextView mTextViewCurrentVersion;
	private TextView mTextViewChangelog;
	private Button mButtonOk;
	private String mCurrentVersionText;
	private Context mContext;

	public ChangelogDialog(Context pContext) {
		super(pContext);
		this.mContext = pContext;
	}

	@Override
	protected void onCreate(Bundle pSavedInstanceState) {
		Log.d(TAG, "[=>][Changelog(Bundle)] called");
		super.onCreate(pSavedInstanceState);
		setContentView(R.layout.changelogdialog);
		setTitle(R.string.changelogdialog_title);

		mTextViewCurrentVersion = (TextView) findViewById(R.id.changelogdialog_textView_current);
		mTextViewCurrentVersion.setText(Html.fromHtml(mCurrentVersionText));

		mTextViewChangelog = (TextView) findViewById(R.id.changelogdialog_textView_changes);
		mTextViewChangelog.setText(Html.fromHtml("<b>Changelog:</b>"
				+ mChangelogContent));

		mButtonOk = (Button) findViewById(R.id.changelogdialog_button_ok);
		mButtonOk.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				cancel();
			}
		});
		Log.d(TAG, "[<=][Changelog(Bundle)] finished");
	}

	public void setCurrentVersion(String pVersionName) {
		mCurrentVersionText = mContext
				.getString(R.string.changelogdialog_current)
				+ "<br>	<b>"
				+ pVersionName + "</b>";
	}

	public void addChange(String pVersion, String pChanges) {
		mChangelogContent = "<br><br><b>" + pVersion + ":</b><br>" + pChanges
				+ mChangelogContent;
	}
}
