/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.alien.oxclient.R;

public class AlertDialogFragment extends DialogFragment {

	// Container Activity must implement this interface
	public interface OnAlertDialogInterface {
		public void onAlertDialogCancel();

		public void onAlertDialogPositiveClick();

		public void onAlertDialogNegativClick();
	}

	private OnAlertDialogInterface mListener;

	public static AlertDialogFragment newInstance(String pMessage) {
		AlertDialogFragment frag = new AlertDialogFragment();
		Bundle args = new Bundle();
		args.putString("message", pMessage);
		frag.setArguments(args);
		return frag;
	}

	@Override
	public void onAttach(Activity pActivity) {
		super.onAttach(pActivity);
		try {
			mListener = (OnAlertDialogInterface) pActivity;
		} catch (ClassCastException e) {
			throw new ClassCastException(pActivity.toString()
					+ " must implement OnArticleSelectedListener");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String message = getArguments().getString("message");
		return new AlertDialog.Builder(getActivity())
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setTitle(getString(R.string.error_title))
				.setCancelable(true)
				.setMessage(message)
				.setPositiveButton(R.string.retry,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								mListener.onAlertDialogPositiveClick();
							}
						})
				.setNegativeButton(android.R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								mListener.onAlertDialogNegativClick();
							}
						}).create();
	}

	@Override
	public void onCancel(DialogInterface pDialog) {
		mListener.onAlertDialogCancel();
	}
}
