/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.R;
import com.alien.oxclient.activities.Preferences;

public class InformationDialog extends Dialog {
	private static final String TAG = "OXClient";

	private Context mContext;
	private boolean mCancelable;
	private Button mButtonOk;
	private Button mButtonPrefs;

	public InformationDialog(Context pContext, boolean pCancelable) {
		super(pContext);
		this.mContext = pContext;
		this.mCancelable = pCancelable;
	}

	@Override
	protected void onCreate(Bundle pSavedInstanceState) {
		Log.d(TAG, "[=>][InformationDialog(Bundle)] called");
		super.onCreate(pSavedInstanceState);
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.informationdialog);
		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,
				android.R.drawable.ic_dialog_info);
		setTitle(R.string.informationdialog_title);

		setCancelable(mCancelable); // die Informationen sind zu wichtig um sie
									// einfach wegdruecken zu koennen
		getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT,
				WindowManager.LayoutParams.WRAP_CONTENT);

		mButtonOk = (Button) findViewById(R.id.informationdialog_button_ok);
		mButtonPrefs = (Button) findViewById(R.id.informationdialog_button_prefs);

		mButtonOk.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				OXApplication.getPrefs().setInformationenDialogSeen(true);
				dismiss();
			}
		});
		mButtonPrefs.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent settingsIntent = new Intent(mContext, Preferences.class);
				mContext.startActivity(settingsIntent);
			}
		});
		Log.d(TAG, "[<=][InformationDialog(Bundle)] finished");
	}
}
