package com.alien.oxclient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.util.Log;

import com.alien.oxclient.mockinterfaces.OXCalendar;

public class OXTimeManager {

	private static final String TAG = "OXClient";
	/** http://oxpedia.org/wiki/index.php?title=HTTP_API#Date_and_time */
	private static final String OXTIMEZONE = "UTC";

	private OXCalendar mBegin;
	private OXCalendar mEnd;
	private boolean mMaintime;

	public OXTimeManager() {
		SimpleDateFormat sdf = new SimpleDateFormat("kk:mm");
		Date maintimeBegin, maintimeEnd;
		try {
			maintimeBegin = sdf.parse(OXApplication.getPrefs()
					.getMaintimeBegin());
			maintimeEnd = sdf.parse(OXApplication.getPrefs().getMaintimeEnd());
		} catch (ParseException e) {
			throw new RuntimeException(e.getClass().getSimpleName() + ": "
					+ e.getLocalizedMessage());
		}
		try {
			mBegin = (OXCalendar) OXApplication.getCalendar().clone();
			mBegin.setHourAndMinute(maintimeBegin);
			mEnd = (OXCalendar) OXApplication.getCalendar().clone();
			mEnd.setHourAndMinute(maintimeEnd);
			mMaintime = inMaintime();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e.getClass().getSimpleName() + ": "
					+ e.getMessage());
		}
	}

	private boolean inMaintime() throws CloneNotSupportedException {
		OXCalendar now = (OXCalendar) OXApplication.getCalendar().clone();
		String today = String.valueOf(now.get(Calendar.DAY_OF_WEEK));
		List<String> days = Arrays.asList(OXApplication.getPrefs()
				.getMaintimeDays());
		mMaintime = days.contains(today) && now.after(mBegin)
				&& now.before(mEnd);
		return mMaintime;
	}

	public long differenceInSeconds() {
		try {
			OXCalendar now = (OXCalendar) OXApplication.getCalendar().clone();
			OXCalendar until;
			long syncFrequencyNormal;
			if(mMaintime) {
				until = mEnd;
				syncFrequencyNormal = Long.parseLong(OXApplication.getPrefs()
						.getMaintimeSyncFrequency());

			} else {
				until = mBegin;
				syncFrequencyNormal = Long.parseLong(OXApplication.getPrefs()
						.getSecondarytimeSyncFrequency());
			}
			long strictSyncFrequencyTillEnd = Math
					.abs((now.getTimeInMillis() - until.getTimeInMillis()) / 1000);
			if(strictSyncFrequencyTillEnd < syncFrequencyNormal) {
				// Add 2% just to make sure we are really in the maintime after
				// the following sync
				return (long) (strictSyncFrequencyTillEnd * 1.02);
			} else {
				return syncFrequencyNormal;
			}
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e.getClass().getSimpleName() + ": "
					+ e.getMessage());
		}
	}

	public String convertOXTime2Android(String pMillisecondsSince1970) {
		try {
			Date datum = new Date(Long.valueOf(pMillisecondsSince1970));
			SimpleDateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			TimeZone tZ = TimeZone.getTimeZone(OXTIMEZONE);
			dfm.setTimeZone(tZ);
			return dfm.format(datum);
		} catch (NumberFormatException e) {
			Log.e(TAG,
					"[OXUtils->convertOXTime2Android] NumberFormatException: "
							+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public String convertAndroidTime2OX(String pDatum) {
		try {
			SimpleDateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			TimeZone tZ = TimeZone.getTimeZone(OXTIMEZONE);
			dfm.setTimeZone(tZ);
			Date datum = dfm.parse(pDatum);
			return String.valueOf(datum.getTime());
		} catch (ParseException e) {
			Log.e(TAG,
					"[OXUtils->convertTime2OX] ParseException: "
							+ e.getMessage());
			e.printStackTrace();
		} catch (NullPointerException e) {
			// Expected excepetion as it always happens if somebody passes null,
			// therefore only verbose logoutput
			Log.v(TAG,
					"[OXUtils->convertTime2OX] NullPointerException: "
							+ e.getMessage());
		}
		return null;
	}

	public boolean isInMaintime() {
		return mMaintime;
	}
}
