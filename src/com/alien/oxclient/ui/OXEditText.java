package com.alien.oxclient.ui;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

import com.alien.oxclient.model.OXContact;

public class OXEditText extends EditText implements Updateable {

	private String mKey;
	private OXContact mContact;

	/**
	 * This Constructor only exists to be used by tools!
	 */
	public OXEditText(Context context, AttributeSet attrSet, int defStyle) {
		super(context, attrSet, defStyle);
	}

	public OXEditText(Context context, String pKey, OXContact pContact,
			int pInputType, int pId, Object pTag) {
		super(context);
		this.updateUi(context, pKey, pContact, pInputType, false, pId, pTag,
				"", "");
	}

	public void updateUi(Context pContext, String pKey, OXContact pContact,
			int pInputType, boolean pCalendarViewShown, int pId, Object pTag,
			String pContentDescription, String pHint) {
		this.mKey = pKey;
		this.mContact = pContact;

		this.setId(pId);
		this.setTag(pTag);
		this.setInputType(pInputType);
		this.setContentDescription(pContentDescription);
		this.setHint(pHint);

		final String value = mContact.getContactInformation(mKey);
		if(value != null) this.setText(value);
		else this.setText("");

		// afterTextChanged is called with an empty Editable (not null) just
		// when the View appears on screen. This not-user-made call has to be
		// filtered out, otherwise the dirty-flag of the model is set without
		// the user intentionally changing something
		this.addTextChangedListener(new TextWatcher() {
			private String currentValue = null;

			public void afterTextChanged(Editable s) {
				String newValue = s.toString();
				if(TextUtils.isEmpty(newValue)) {
					if(currentValue != value) mContact
							.removeContactInformation(mKey);
				} else {
					mContact.putContactInformation(mKey, newValue);
					currentValue = newValue;
				}
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
		});
	}
}
