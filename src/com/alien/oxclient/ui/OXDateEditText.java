/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.ui;

import java.text.DecimalFormat;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextUtils.SimpleStringSplitter;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.alien.oxclient.R;
import com.alien.oxclient.model.OXContact;

// TODO: If this works out: remove OXDatePicker
public class OXDateEditText extends EditText implements OnClickListener,
		OnDateSetListener, Updateable {

	private static final int DEFAULT_YEAR = 1900;
	private static final int DEFAULT_MONTH = 0;
	private static final int DEFAULT_DAY = 1;
	private String mKey;
	private OXContact mContact;
	private int mYear, mMonth, mDay;
	private Context mContext;

	public OXDateEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public OXDateEditText(Context pContext, String pKey, OXContact pContact,
			int pId, Object pTag) {
		super(pContext);
		this.mContext = pContext;
		this.mContact = pContact;
		this.mKey = pKey;
		this.setOnClickListener(this);
		this.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {
				Toast.makeText(mContext, R.string.oxdateedittext_hint,
						Toast.LENGTH_SHORT).show();
				return false;
			}
		});
		updateUi(pContext, pKey, pContact, InputType.TYPE_CLASS_DATETIME,
				false, pId, pTag, "", "");
	}

	public void onClick(View v) {
		readDateFromContact();
		DatePickerDialog dpd = new DatePickerDialog(mContext, this, mYear,
				mMonth, mDay);
		dpd.show();
	}

	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		DecimalFormat dFormatter = new DecimalFormat("00");
		String date = new StringBuilder().append(year).append("-")
				.append(dFormatter.format(monthOfYear + 1)).append("-")
				.append(dFormatter.format(dayOfMonth)).toString();
		mContact.putContactInformation(mKey, date);
		this.setText(date);
	}

	private void setDefaults() {
		// Default values:
		mYear = DEFAULT_YEAR;
		mMonth = DEFAULT_MONTH;
		mDay = DEFAULT_DAY;
	}

	public void updateUi(Context pContext, String pKey, OXContact pContact,
			int pInputType, boolean pCalendarViewShown, int pId, Object pTag,
			String pContentDescription, String pHint) {
		this.setId(pId);
		// this.setInputType(pInputType);
		this.setTag(pTag);
		this.setContentDescription(pContentDescription);
		this.setHint(pHint);

		readDateFromContact();
	}

	private void readDateFromContact() {
		String value = mContact.getContactInformation(mKey);
		if(!TextUtils.isEmpty(value)) {
			SimpleStringSplitter simpleStringSplitter = new SimpleStringSplitter(
					'-');
			simpleStringSplitter.setString(value);
			try {
				mYear = Integer.parseInt(simpleStringSplitter.next());
				mMonth = Integer.parseInt(simpleStringSplitter.next()) - 1;
				mDay = Integer.parseInt(simpleStringSplitter.next());
			} catch (NumberFormatException e) {
				setDefaults();
			}
		} else {
			setDefaults();
		}
	}
}
