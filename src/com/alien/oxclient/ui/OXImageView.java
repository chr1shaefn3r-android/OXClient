/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.alien.oxclient.R;
import com.alien.oxclient.model.OXContact;

public class OXImageView extends ImageView implements Updateable {

	private OXContact mContact;
	private BitmapDrawable mBitmap;

	/**
	 * This Constructor only exists to be used by tools!
	 */
	public OXImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public OXImageView(Context pContext, OXContact pContact, int pId,
			Object pTag) {
		super(pContext);
		this.updateUi(pContext, null, pContact, 0, false, pId, pTag, "", "");
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onDraw(Canvas canvas) {
		if(mBitmap != null) {
			this.setBackgroundDrawable(mBitmap);
		} else {
			this.setBackgroundResource(R.drawable.ic_oxcontact_picture);
		}
		super.onDraw(canvas);
	}

	@Override
	public void invalidate() {
		loadBitmap();
		super.invalidate();
	}

	public void updateUi(Context pContext, String pKey, OXContact pContact,
			int pInputType, boolean pCalendarViewShown, int pId, Object pTag,
			String pContentDescription, String pHint) {
		this.mContact = pContact;

		this.setId(pId);
		this.setTag(pTag);
		this.setContentDescription(pContentDescription);

		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lp.gravity = Gravity.CENTER_HORIZONTAL;
		this.setLayoutParams(lp);
		loadBitmap();
	}

	private void loadBitmap() {
		if(mContact.getPicture() != null) mBitmap = new BitmapDrawable(
				getResources(), mContact.getPicture());
		else mBitmap = null;
	}
}
