package com.alien.oxclient.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class TabInfo {
	private final String tag;
	protected final Class<?> clss;
	protected final Bundle args;
	protected Fragment fragment;

	public TabInfo(String _tag, Class<?> _class, Bundle _args) {
		tag = _tag;
		clss = _class;
		args = _args;
	}

	public String getTag() {
		return tag;
	}

	public Fragment getFragment() {
		return fragment;
	}
}
