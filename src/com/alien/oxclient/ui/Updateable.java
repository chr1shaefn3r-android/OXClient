/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.ui;

import android.content.Context;
import android.view.View;

import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.provider.OXMapping;

public interface Updateable {

	/**
	 * updateUi - Tell the {@link View} to update
	 * 
	 * @param pContext
	 *            The {@link Context}
	 * @param pKey
	 *            The key of the information based on the {@link OXMapping}
	 * @param pContact
	 *            The {@link OXContact} to be changed
	 * @param pInputType
	 *            The InputType for a {@link OXEditText}
	 * @param pCalendarViewShown
	 *            Whether the CalendarView should be shown or not
	 * @param pId
	 *            The id this {@link View} should get
	 * @param pTag
	 *            The tag this {@link View} should get
	 * @param pContentDescription
	 *            The contentDescription to be set to this {@link View}
	 * @param pHint
	 *            The hint to be set for a {@link OXEditText}
	 */
	public void updateUi(Context pContext, String pKey, OXContact pContact,
			int pInputType, boolean pCalendarViewShown, int pId, Object pTag,
			String pContentDescription, String pHint);
}
