package com.alien.oxclient.ui;

import java.text.DecimalFormat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils.SimpleStringSplitter;
import android.util.AttributeSet;
import android.view.View;
import android.widget.DatePicker;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.model.OXContact;

public class OXDatePicker extends DatePicker implements Updateable {

	private static final int DATEPICKER_ACTIVATED_COLOR = 0xFF000000;
	private static final int DATEPICKER_DEACTIVATED_COLOR = 0xFFff0000;
	private static final int DEFAULT_YEAR = 1900;
	private static final int DEFAULT_MONTH = 0;
	private static final int DEFAULT_DAY = 1;
	private String mKey;
	private OXContact mContact;
	private int mYear, mMonth, mDay;

	/**
	 * This Constructor only exists to be used by tools!
	 */
	public OXDatePicker(Context context, AttributeSet attrSet, int defStyle) {
		super(context, attrSet, defStyle);
	}

	public OXDatePicker(Context pContext, String pKey, OXContact pContact,
			boolean pCalendarViewShown, int pId, Object pTag) {
		super(pContext);
		this.updateUi(pContext, pKey, pContact, 0, pCalendarViewShown, pId,
				pTag, "", "");
		this.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(isBackgroundActivated()) setBackgroundDeactivated();
				else setBackgroundActivated();
				save();
			}
		});
	}

	@SuppressLint("NewApi")
	public void updateUi(Context pContext, String pKey, OXContact pContact,
			int pInputType, boolean pCalendarViewShown, int pId, Object pTag,
			String pContentDescription, String pHint) {
		this.mKey = pKey;
		this.mContact = pContact;

		this.setId(pId);
		this.setTag(pTag);
		this.setContentDescription(pContentDescription);

		this.setId(pId);
		this.setTag(pTag);
		this.setContentDescription(pContentDescription);

		if(OXApplication.getUtils().isHoneyCombOrBetter()) this
				.setCalendarViewShown(pCalendarViewShown);

		String value = mContact.getContactInformation(mKey);
		if(value != null) {
			SimpleStringSplitter simpleStringSplitter = new SimpleStringSplitter(
					'-');
			simpleStringSplitter.setString(value);
			try {
				mYear = Integer.parseInt(simpleStringSplitter.next());
				mMonth = Integer.parseInt(simpleStringSplitter.next()) - 1;
				mDay = Integer.parseInt(simpleStringSplitter.next());
				setBackgroundActivated();
			} catch (NumberFormatException e) {
				setDefaults();
			}
		} else {
			setDefaults();
			setBackgroundDeactivated();
		}
		this.init(mYear, mMonth, mDay, new OnDateChangedListener() {
			public void onDateChanged(DatePicker view, int year,
					int monthOfYear, int dayOfMonth) {
				// As soon as somebody changes the date he most likely also
				// wants to save it ...
				setBackgroundActivated();
				save();
			}
		});
	}

	private void save() {
		if(isBackgroundDeactivated()) {
			mContact.removeContactInformation(mKey);
		} else {
			DecimalFormat dFormatter = new DecimalFormat("00");
			String date = new StringBuilder().append(this.getYear())
					.append("-").append(dFormatter.format(this.getMonth() + 1))
					.append("-")
					.append(dFormatter.format(this.getDayOfMonth())).toString();
			mContact.putContactInformation(mKey, date);
		}
	}

	private void setDefaults() {
		// Default values:
		mYear = DEFAULT_YEAR;
		mMonth = DEFAULT_MONTH;
		mDay = DEFAULT_DAY;
	}

	private boolean isBackgroundDeactivated() {
		return(this.getDrawingCacheBackgroundColor() == DATEPICKER_DEACTIVATED_COLOR);
	}

	private boolean isBackgroundActivated() {
		return(this.getDrawingCacheBackgroundColor() == DATEPICKER_ACTIVATED_COLOR);
	}

	private void setBackgroundDeactivated() {
		this.setBackgroundColor(DATEPICKER_DEACTIVATED_COLOR);
		this.setDrawingCacheBackgroundColor(DATEPICKER_DEACTIVATED_COLOR);
	}

	private void setBackgroundActivated() {
		this.setBackgroundColor(DATEPICKER_ACTIVATED_COLOR);
		this.setDrawingCacheBackgroundColor(DATEPICKER_ACTIVATED_COLOR);
	}

	@Override
	public String toString() {
		return "OXDatePicker [Year=" + this.getYear() + ", Month="
				+ (this.getMonth() + 1) + ", Day=" + this.getDayOfMonth() + "]";
	}
}
