/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.ui;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.TabHost;

/**
 * Class to hold all informationen needed for one tab
 */
public class Tab {
	private Context mContext;
	private String mLabel;
	private Class<?> mClass;
	private int mIconResId;
	private Bundle mArgs;

	public Tab(Context pContext, String pLabel, Class<?> pClass,
			int pIconResId, Bundle pArgs) {
		this.mContext = pContext;
		this.mLabel = pLabel;
		this.mClass = pClass;
		this.mIconResId = pIconResId;
		this.mArgs = pArgs;
	}

	public void addTab(TabsAdapter pTabsAdapter, TabHost pTabHost) {
		if(((mContext.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE)
				&& ((mContext.getResources().getConfiguration().orientation) == Configuration.ORIENTATION_LANDSCAPE)) {
			pTabsAdapter.addTab(
					pTabHost.newTabSpec(mLabel).setIndicator(mLabel,
							mContext.getResources().getDrawable(mIconResId)),
					mClass, mArgs);
		} else {
			pTabsAdapter.addTab(
					pTabHost.newTabSpec(mLabel).setIndicator("",
							mContext.getResources().getDrawable(mIconResId)),
					mClass, mArgs);
		}
	}

	public void addFragment(FragmentManager pFragmentManager, int pLinearLayout) {
		try {
			FragmentTransaction ft = pFragmentManager.beginTransaction();
			Fragment fragment = (Fragment) mClass.newInstance();
			fragment.setArguments(mArgs);
			ft.add(pLinearLayout, fragment, mLabel);
			ft.commit();
		} catch (Exception e) {
			throw new RuntimeException(e.getClass().getSimpleName() + ": "
					+ e.getLocalizedMessage());
		}
	}
}
