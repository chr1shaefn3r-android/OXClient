/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class FoldersDatabaseHelper extends DatabaseHelper {

	private static final String DATABASE_NAME = "folders.db";
	private static int DATABASE_VERSION = 1;

	public FoldersDatabaseHelper(Context pContext) {
		super(pContext, DATABASE_NAME, null, DATABASE_VERSION);
		this.mContext = pContext;
	}

	@Override
	public void onCreate(SQLiteDatabase pDatabase) {
		pDatabase.execSQL("create table " + Folders.TABLE + " (" + Folders._ID
				+ " integer primary key, " + Folders.TITLE + " text not null,"
				+ Folders.FOLDER_ID + " integer not null," + Folders.SUBFOLDERS
				+ " integer not null);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + Folders.TABLE);
		onCreate(db);
	}
}
