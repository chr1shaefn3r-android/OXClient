/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class FoldersContentProvider extends ContentProvider {

	public static final String AUTHORITY = "com.alien.oxclient.provider.FoldersContentProvider";
	private static final int MATCH = 1;

	private static final UriMatcher mUriMatcher;
	static {
		mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		mUriMatcher.addURI(AUTHORITY, Folders.TABLE, MATCH);
	}

	private FoldersDatabaseHelper mFoldersDBHelper;

	@Override
	public boolean onCreate() {
		mFoldersDBHelper = new FoldersDatabaseHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri pUri, String[] pProjection, String pSelection,
			String[] pSelectionArgs, String pSortOrder) {
		switch (mUriMatcher.match(pUri)) {
		case MATCH:
			SQLiteDatabase db = mFoldersDBHelper.getReadableDatabase();
			Cursor cursor = db.query(Folders.TABLE, pProjection, pSelection,
					pSelectionArgs, null, null, pSortOrder);
			cursor.setNotificationUri(getContext().getContentResolver(), pUri);
			return cursor;
		default:
			throw new IllegalArgumentException("Unknown URI " + pUri);
		}
	}

	@Override
	public int delete(Uri pUri, String pSelection, String[] pSelectionArgs) {
		SQLiteDatabase db = mFoldersDBHelper.getWritableDatabase();
		int count;
		switch (mUriMatcher.match(pUri)) {
		case MATCH:
			count = db.delete(Folders.TABLE, pSelection, pSelectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + pUri);
		}

		getContext().getContentResolver().notifyChange(pUri, null);
		return count;
	}

	@Override
	public Uri insert(Uri pUri, ContentValues pContentValues) {
		if(mUriMatcher.match(pUri) != MATCH) throw new IllegalArgumentException(
				"Unknown URI " + pUri);

		SQLiteDatabase db = mFoldersDBHelper.getWritableDatabase();
		long rowId = db
				.insert(Folders.TABLE, Folders.FOLDER_ID, pContentValues);
		if(rowId > 0) {
			Uri noteUri = ContentUris
					.withAppendedId(Folders.CONTENT_URI, rowId);
			getContext().getContentResolver().notifyChange(noteUri, null);
			return noteUri;
		}

		throw new SQLException("Failed to insert row into " + pUri);
	}

	@Override
	public int update(Uri pUri, ContentValues pContentvalues,
			String pSelection, String[] pSelectionArgs) {
		if(mUriMatcher.match(pUri) != MATCH) throw new IllegalArgumentException(
				"Unknown URI " + pUri);

		SQLiteDatabase db = mFoldersDBHelper.getWritableDatabase();
		int count = db.update(Folders.TABLE, pContentvalues, pSelection,
				pSelectionArgs);
		getContext().getContentResolver().notifyChange(pUri, null);
		return count;
	}

	@Override
	public String getType(Uri pUri) {
		switch (mUriMatcher.match(pUri)) {
		case MATCH:
			return Folders.CONTENT_TYPE;
		default:
			throw new IllegalArgumentException("Unknown URI " + pUri);
		}
	}

	public FoldersDatabaseHelper getDatabaseHelper() {
		return mFoldersDBHelper;
	}
}
