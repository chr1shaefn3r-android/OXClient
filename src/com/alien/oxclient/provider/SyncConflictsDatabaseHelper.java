/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.provider;

import java.util.HashMap;

import android.app.SearchManager;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.provider.BaseColumns;

public class SyncConflictsDatabaseHelper extends DatabaseHelper {

	private static final String DATABASE_NAME = "syncconflicts.db";
	private static int DATABASE_VERSION = 1;
	private static final HashMap<String, String> mColumnMap = buildColumnMap();

	/**
	 * Builds a map for all columns that may be requested, which will be given
	 * to the SQLiteQueryBuilder. This is a good way to define aliases for
	 * column names, but must include all columns, even if the value is the key.
	 * This allows the ContentProvider to request columns w/o the need to know
	 * real column names and create the alias itself.
	 */
	private static HashMap<String, String> buildColumnMap() {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(SyncConflicts.OXID, SyncConflicts.OXID);
		map.put(SyncConflicts.USERNAME, SyncConflicts.USERNAME);
		map.put(SyncConflicts.TIMESTAMP, SyncConflicts.TIMESTAMP);
		map.put(BaseColumns._ID, "rowid AS " + BaseColumns._ID);
		map.put(SearchManager.SUGGEST_COLUMN_INTENT_DATA, "rowid AS "
				+ SearchManager.SUGGEST_COLUMN_INTENT_DATA);
		map.put(SearchManager.SUGGEST_COLUMN_TEXT_1, SyncConflicts.USERNAME
				+ " AS " + SearchManager.SUGGEST_COLUMN_TEXT_1);
		map.put(SearchManager.SUGGEST_COLUMN_TEXT_2, SyncConflicts.TIMESTAMP
				+ " AS " + SearchManager.SUGGEST_COLUMN_TEXT_2);
		return map;
	}

	public SyncConflictsDatabaseHelper(Context pContext) {
		super(pContext, DATABASE_NAME, null, DATABASE_VERSION);
		this.mContext = pContext;
	}

	@Override
	public void onCreate(SQLiteDatabase pDatabase) {
		pDatabase.execSQL("create table " + SyncConflicts.TABLE + " ("
				+ SyncConflicts._ID + " integer primary key autoincrement, "
				+ SyncConflicts.OXID + " text not null,"
				+ SyncConflicts.USERNAME + " text not null,"
				+ SyncConflicts.TIMESTAMP + " text not null);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase pDatabase, int pOldVersion,
			int pNewVersion) {
		pDatabase.execSQL("DROP TABLE IF EXISTS " + SyncConflicts.TABLE);
		onCreate(pDatabase);
	}

	public Cursor query(String[] pProjection, String pSelection,
			String[] pSelectionArgs, String pSortOrder) {
		SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
		builder.setTables(SyncConflicts.TABLE);
		builder.setProjectionMap(mColumnMap);
		Cursor cursor = builder.query(getReadableDatabase(), pProjection,
				pSelection, pSelectionArgs, null, null, pSortOrder);
		return cursor;
	}
}
