/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.provider;

import android.provider.BaseColumns;

public class OXMapping implements BaseColumns {

	public static final String TABLE = "oxmapping";

	public static final String OXMAPPING_KEY = "key";
	public static final String OX_NAME = "name";
	public static final String OX_POSITION = "position";
	public static final String OX_NUMBER = "number";
	public static final String ANDROID_MIMETYPE = "mimetype";
	public static final String ANDROID_TYPE = "type";
	public static final String ANDROID_COLUMN = "column";
	public static final String ALTERCONTACT_INFORMATIONTYPE = "informationType";
	public static final String ALTERCONTACT_INPUTTYPE = "inputType";
	public static final String ALTERCONTACT_TRANSLATION = "translation";
	public static final String ALTERCONTACT_GROUP = "groupCategory";
}
