/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.provider;

import android.net.Uri;
import android.provider.BaseColumns;

public class Folders implements BaseColumns {

	public static final Uri CONTENT_URI = Uri.parse("content://"
			+ FoldersContentProvider.AUTHORITY + "/folders");
	public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.alien.folders";

	// id wird in _id gespeichert!
	// public static final String ID = "id"; // 1; Object ID
	public static final String FOLDER_ID = "folder_id"; // 20; Object ID of the
														// parent folder.
	public static final String TITLE = "title"; // 300; Name of this folder.
	public static final String SUBFOLDERS = "subfolders"; // 304; true if this
															// folder has
															// subfolders.
	public static final String TABLE = "folders";
	// boolean value = cursor.getInt(boolean_column_index)>0;

	// Beispiel eines joins aus ContactProvider2 aus dem Androidquelltext
	// http://www.google.com/codesearch/p?hl=en#cbQwy62oRIQ/src/com/android/providers/contacts/ContactsProvider
	// public static final String TABLE = "data "
	// + "JOIN raw_contacts ON (data.raw_contact_id = raw_contacts._id) "
	// + "JOIN contacts ON (raw_contacts.contact_id = contacts._id)";
	// db.query(TABLE, projection, selection, selectionArgs, null, null,
	// sortOrder);
}
