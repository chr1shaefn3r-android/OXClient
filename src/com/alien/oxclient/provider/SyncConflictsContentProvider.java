/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.provider;

import java.util.Locale;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class SyncConflictsContentProvider extends ContentProvider {

	public static final String AUTHORITY = "com.alien.oxclient.provider.SyncConflictsContentProvider";
	private static final int MATCH = 1;
	private static final int SEARCH_SUGGEST = 2;

	private static final UriMatcher mUriMatcher;
	static {
		mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		mUriMatcher.addURI(AUTHORITY, SyncConflicts.TABLE, MATCH);
		mUriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY,
				SEARCH_SUGGEST);
		mUriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY
				+ "/*", SEARCH_SUGGEST);
	}

	private SyncConflictsDatabaseHelper mSyncConflictsDBHelper;

	@Override
	public boolean onCreate() {
		mSyncConflictsDBHelper = new SyncConflictsDatabaseHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri pUri, String[] pProjection, String pSelection,
			String[] pSelectionArgs, String pSortOrder) {
		switch (mUriMatcher.match(pUri)) {
		case MATCH:
			Cursor cursor_match = mSyncConflictsDBHelper.query(pProjection,
					pSelection, pSelectionArgs, pSortOrder);
			cursor_match.setNotificationUri(getContext().getContentResolver(),
					pUri);
			return cursor_match;
		case SEARCH_SUGGEST:
			if(pSelectionArgs == null) throw new IllegalArgumentException(
					"electionArgs must be prvoided for the Uri: " + pUri);
			Cursor cursor_searchsuggest = mSyncConflictsDBHelper.query(
					pProjection,
					SearchManager.SUGGEST_COLUMN_TEXT_1 + " LIKE ?",
					new String[] { "%"
							+ pSelectionArgs[0].toLowerCase(Locale.US) + "%" },
					SearchManager.SUGGEST_COLUMN_TEXT_1);
			/*
			 * Just want to do ASCII replacement, so Locale.US will do the trick
			 * otherwise one should use Locale.getDefault()
			 */
			return cursor_searchsuggest;
		default:
			throw new IllegalArgumentException("Unknown URI " + pUri);
		}
	}

	@Override
	public int delete(Uri pUri, String pSelection, String[] pSelectionArgs) {
		SQLiteDatabase db = mSyncConflictsDBHelper.getWritableDatabase();
		int count;
		switch (mUriMatcher.match(pUri)) {
		case MATCH:
			count = db.delete(SyncConflicts.TABLE, pSelection, pSelectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + pUri);
		}

		getContext().getContentResolver().notifyChange(pUri, null);
		return count;
	}

	@Override
	public Uri insert(Uri pUri, ContentValues pContentValues) {
		if(mUriMatcher.match(pUri) != MATCH) throw new IllegalArgumentException(
				"Unknown URI " + pUri);

		SQLiteDatabase db = mSyncConflictsDBHelper.getWritableDatabase();
		long rowId = db.insert(SyncConflicts.TABLE, SyncConflicts.OXID,
				pContentValues);
		if(rowId > 0) {
			Uri noteUri = ContentUris.withAppendedId(SyncConflicts.CONTENT_URI,
					rowId);
			getContext().getContentResolver().notifyChange(noteUri, null);
			return noteUri;
		}

		throw new SQLException("Failed to insert row into " + pUri);
	}

	@Override
	public int update(Uri pUri, ContentValues pContentvalues,
			String pSelection, String[] pSelectionArgs) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getType(Uri pUri) {
		switch (mUriMatcher.match(pUri)) {
		case MATCH:
			return SyncConflicts.CONTENT_TYPE;
		case SEARCH_SUGGEST:
			return SearchManager.SUGGEST_MIME_TYPE;
		default:
			throw new IllegalArgumentException("Unknown URI " + pUri);
		}
	}

	public SyncConflictsDatabaseHelper getDatabaseHelper() {
		return mSyncConflictsDBHelper;
	}
}
