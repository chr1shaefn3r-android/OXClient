/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.provider;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Event;
import android.provider.ContactsContract.CommonDataKinds.Im;
import android.provider.ContactsContract.CommonDataKinds.Nickname;
import android.provider.ContactsContract.CommonDataKinds.Note;
import android.provider.ContactsContract.CommonDataKinds.Organization;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.ContactsContract.CommonDataKinds.Relation;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.CommonDataKinds.StructuredPostal;
import android.provider.ContactsContract.CommonDataKinds.Website;
import android.text.InputType;

import com.alien.oxclient.OXConstants;
import com.alien.oxclient.ui.InformationTypes;

public class OXMappingDatabaseHelper extends DatabaseHelper {

	private static final String DATABASE_NAME = "oxmapping.db";
	private static int DATABASE_VERSION = 6;
	private ArrayList<Cursor> mCursors = new ArrayList<Cursor>();

	public OXMappingDatabaseHelper(Context pContext) {
		super(pContext, DATABASE_NAME, null, DATABASE_VERSION);
		this.mContext = pContext;
	}

	@Override
	public void onCreate(SQLiteDatabase pDatabase) {
		pDatabase.execSQL("create table " + OXMapping.TABLE + " ("
				+ OXMapping._ID + " integer primary key autoincrement, "
				+ OXMapping.OXMAPPING_KEY + " text not null,"
				+ OXMapping.OX_POSITION + " integer not null, "
				+ OXMapping.OX_NUMBER + " text not null, " + OXMapping.OX_NAME
				+ " text not null, " + OXMapping.ANDROID_COLUMN
				+ " text not null, " + OXMapping.ANDROID_MIMETYPE
				+ " text not null, " + OXMapping.ANDROID_TYPE
				+ " integer not null, "
				+ OXMapping.ALTERCONTACT_INFORMATIONTYPE
				+ " integer not null, " + OXMapping.ALTERCONTACT_TRANSLATION
				+ " integer not null, " + OXMapping.ALTERCONTACT_INPUTTYPE
				+ " integer not null, " + OXMapping.ALTERCONTACT_GROUP
				+ " integer not null);");
		addEntry(pDatabase, "mID", 0, "1", "id", OXConstants.MIMETYPE_OXID, 0,
				"data1", InformationTypes.NOT.ordinal(), "0", 0,
				OXConstants.ALTERCONTACT_GROUP_NO);
		// Namen:
		addEntry(pDatabase, "mNameDisplayName", 1, "500", "display_name",
				StructuredName.CONTENT_ITEM_TYPE, 0, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mNameDisplayName",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_PERSON_NAME,
				OXConstants.ALTERCONTACT_GROUP_NO);
		addEntry(pDatabase, "mNameGivenName", 2, "501", "first_name",
				StructuredName.CONTENT_ITEM_TYPE, 0, "data2",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mNameGivenName",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_PERSON_NAME,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mNameFamilyName", 3, "502", "last_name",
				StructuredName.CONTENT_ITEM_TYPE, 0, "data3",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mNameFamilyName",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_PERSON_NAME,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mNameMiddleName", 4, "503", "second_name",
				StructuredName.CONTENT_ITEM_TYPE, 0, "data5",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mNameMiddleName",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_PERSON_NAME,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mNameSuffix", 5, "504", "suffix",
				StructuredName.CONTENT_ITEM_TYPE, 0, "data6",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mNameSuffix",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_PERSON_NAME,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mNamePrefix", 6, "505", "title",
				StructuredName.CONTENT_ITEM_TYPE, 0, "data4",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mNamePrefix",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_PERSON_NAME,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mNicknameTypeDefault", 7, "515", "nickname",
				Nickname.CONTENT_ITEM_TYPE, Nickname.TYPE_DEFAULT, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mNicknameTypeDefault",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_PERSON_NAME,
				OXConstants.ALTERCONTACT_GROUP_HOME);
		// Privat-Adresse:
		addEntry(pDatabase, "mPostalTypeHomeStreet", 8, "506", "street_home",
				StructuredPostal.CONTENT_ITEM_TYPE, StructuredPostal.TYPE_HOME,
				"data4", InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPostalTypeHomeStreet",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_HOME);
		addEntry(pDatabase, "mPostalTypeHomePostalCode", 9, "507",
				"postal_code_home", StructuredPostal.CONTENT_ITEM_TYPE,
				StructuredPostal.TYPE_HOME,
				"data9",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPostalTypeHomePostalCode",
				// Not TYPE_CLASS_NUMBER because then you would not be able to
				// wright something like 'D-48176'!
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_HOME);
		addEntry(pDatabase, "mPostalTypeHomeCity", 10, "508", "city_home",
				StructuredPostal.CONTENT_ITEM_TYPE, StructuredPostal.TYPE_HOME,
				"data7", InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPostalTypeHomeCity",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_HOME);
		addEntry(pDatabase, "mPostalTypeHomeState", 11, "509", "state_home",
				StructuredPostal.CONTENT_ITEM_TYPE, StructuredPostal.TYPE_HOME,
				"data8", InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPostalTypeHomeState",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_HOME);
		addEntry(pDatabase, "mPostalTypeHomeCountry", 12, "510",
				"country_home", StructuredPostal.CONTENT_ITEM_TYPE,
				StructuredPostal.TYPE_HOME, "data10",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPostalTypeHomeCountry",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_HOME);
		// Events:
		addEntry(pDatabase, "mEventTypeBirthday", 13, "511", "birthday",
				Event.CONTENT_ITEM_TYPE, Event.TYPE_BIRTHDAY, "data1",
				InformationTypes.DATE.ordinal(),
				"altercontact_translation_mEventTypeBirthday",
				InputType.TYPE_CLASS_DATETIME,
				OXConstants.ALTERCONTACT_GROUP_HOME);
		addEntry(pDatabase, "mEventTypeAnniversary", 14, "517", "anniversary",
				Event.CONTENT_ITEM_TYPE, Event.TYPE_ANNIVERSARY, "data1",
				InformationTypes.DATE.ordinal(),
				"altercontact_translation_mEventTypeAnniversary",
				InputType.TYPE_CLASS_DATETIME,
				OXConstants.ALTERCONTACT_GROUP_HOME);
		// Relations:
		addEntry(pDatabase, "mRelationTypeSpouse", 15, "516", "spouse_name",
				Relation.CONTENT_ITEM_TYPE, Relation.TYPE_SPOUSE, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mRelationTypeSpouse",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_PERSON_NAME,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		addEntry(pDatabase, "mRelationTypeAssistant", 16, "536",
				"assistant_name", Relation.CONTENT_ITEM_TYPE,
				Relation.TYPE_ASSISTANT, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mRelationTypeAssistant",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_PERSON_NAME,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		addEntry(pDatabase, "mRelationTypeManager", 17, "537", "manager_name",
				Relation.CONTENT_ITEM_TYPE, Relation.TYPE_MANAGER, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mRelationTypeManager",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_PERSON_NAME,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		// Notiz:
		addEntry(pDatabase, "mNote", 18, "518", "note", Note.CONTENT_ITEM_TYPE,
				0, "data1", InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mNote", InputType.TYPE_CLASS_TEXT
						| InputType.TYPE_TEXT_FLAG_MULTI_LINE
						| InputType.TYPE_TEXT_VARIATION_LONG_MESSAGE,
				OXConstants.ALTERCONTACT_GROUP_HOME);
		// Arbeitsstelle:
		addEntry(pDatabase, "mOrganizationTypeWorkDepartment", 19, "519",
				"department", Organization.CONTENT_ITEM_TYPE,
				Organization.TYPE_WORK, "data5",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mOrganizationTypeWorkDepartment",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mOrganizationTypeWorkTitle", 20, "520",
				"position", Organization.CONTENT_ITEM_TYPE,
				Organization.TYPE_WORK, "data4",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mOrganizationTypeWorkTitle",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mOrganizationTypeWorkJobDescription", 21, "521",
				"employee_type", Organization.CONTENT_ITEM_TYPE,
				Organization.TYPE_WORK, "data6",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mOrganizationTypeWorkJobDescription",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mOrganizationTypeWorkOfficeLocation", 22, "522",
				"room_number", Organization.CONTENT_ITEM_TYPE,
				Organization.TYPE_WORK, "data9",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mOrganizationTypeWorkOfficeLocation",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mOrganizationTypeWorkCompany", 23, "569",
				"company", Organization.CONTENT_ITEM_TYPE,
				Organization.TYPE_WORK, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mOrganizationTypeWorkCompany",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		// Arbeitsadresse
		addEntry(pDatabase, "mPostalTypeWorkStreet", 24, "523",
				"street_business", StructuredPostal.CONTENT_ITEM_TYPE,
				StructuredPostal.TYPE_WORK, "data4",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPostalTypeWorkStreet",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mPostalTypeWorkPostalCode", 25, "525",
				"postal_code_business", StructuredPostal.CONTENT_ITEM_TYPE,
				StructuredPostal.TYPE_WORK,
				"data9",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPostalTypeWorkPostalCode",
				// Not TYPE_CLASS_NUMBER because then you would not be able to
				// wright something like 'D-48176'!
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mPostalTypeWorkCity", 26, "526", "city_business",
				StructuredPostal.CONTENT_ITEM_TYPE, StructuredPostal.TYPE_WORK,
				"data7", InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPostalTypeWorkCity",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mPostalTypeWorkState", 27, "527",
				"state_business", StructuredPostal.CONTENT_ITEM_TYPE,
				StructuredPostal.TYPE_WORK, "data8",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPostalTypeWorkState",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mPostalTypeWorkCountry", 28, "528",
				"country_business", StructuredPostal.CONTENT_ITEM_TYPE,
				StructuredPostal.TYPE_WORK, "data10",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPostalTypeWorkCountry",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		// Andere Adresse:
		addEntry(pDatabase, "mPostalTypeOtherStreet", 29, "538",
				"street_other", StructuredPostal.CONTENT_ITEM_TYPE,
				StructuredPostal.TYPE_OTHER, "data4",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPostalTypeOtherStreet",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		addEntry(pDatabase, "mPostalTypeOtherCity", 30, "539", "city_other",
				StructuredPostal.CONTENT_ITEM_TYPE,
				StructuredPostal.TYPE_OTHER, "data7",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPostalTypeOtherCity",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		addEntry(pDatabase, "mPostalTypeOtherPostalCode", 31, "540",
				"postal_code_other", StructuredPostal.CONTENT_ITEM_TYPE,
				StructuredPostal.TYPE_OTHER,
				"data9",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPostalTypeOtherPostalCode",
				// Not TYPE_CLASS_NUMBER because then you would not be able to
				// wright something like 'D-48176'!
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		addEntry(pDatabase, "mPostalTypeOtherState", 32, "598", "state_other",
				StructuredPostal.CONTENT_ITEM_TYPE,
				StructuredPostal.TYPE_OTHER, "data8",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPostalTypeOtherState",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		addEntry(pDatabase, "mPostalTypeOtherCountry", 33, "541",
				"country_other", StructuredPostal.CONTENT_ITEM_TYPE,
				StructuredPostal.TYPE_OTHER, "data10",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPostalTypeOtherCountry",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS
						| InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		// Telephonadressen:
		addEntry(pDatabase, "mPhoneTypeWork1", 34, "542",
				"telephone_business1", Phone.CONTENT_ITEM_TYPE,
				Phone.TYPE_WORK, "data1", InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeWork1",
				InputType.TYPE_CLASS_PHONE, OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mPhoneTypeWork2", 35, "543",
				"telephone_business2", Phone.CONTENT_ITEM_TYPE,
				Phone.TYPE_WORK, "data1", InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeWork2",
				InputType.TYPE_CLASS_PHONE, OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mPhoneTypeFaxWork", 36, "544", "fax_business",
				Phone.CONTENT_ITEM_TYPE, Phone.TYPE_FAX_WORK, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeFaxWork",
				InputType.TYPE_CLASS_PHONE, OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mPhoneTypeCallback", 37, "545",
				"telephone_callback", Phone.CONTENT_ITEM_TYPE,
				Phone.TYPE_CALLBACK, "data1", InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeCallback",
				InputType.TYPE_CLASS_PHONE,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		addEntry(pDatabase, "mPhoneTypeCar", 38, "546", "telephone_car",
				Phone.CONTENT_ITEM_TYPE, Phone.TYPE_CAR, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeCar",
				InputType.TYPE_CLASS_PHONE,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		addEntry(pDatabase, "mPhoneTypeCompanyMain", 39, "547",
				"telephone_company", Phone.CONTENT_ITEM_TYPE,
				Phone.TYPE_COMPANY_MAIN, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeCompanyMain",
				InputType.TYPE_CLASS_PHONE, OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mPhoneTypeAssistant", 40, "568",
				"telephone_assistant", Phone.CONTENT_ITEM_TYPE,
				Phone.TYPE_ASSISTANT, "data1", InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeAssistant",
				InputType.TYPE_CLASS_PHONE,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		addEntry(pDatabase, "mPhoneTypeTelex", 41, "563", "telephone_telex",
				Phone.CONTENT_ITEM_TYPE, Phone.TYPE_TELEX, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeTelex",
				InputType.TYPE_CLASS_PHONE,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		addEntry(pDatabase, "mPhoneTypeHome1", 42, "548", "telephone_home1",
				Phone.CONTENT_ITEM_TYPE, Phone.TYPE_HOME, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeHome1",
				InputType.TYPE_CLASS_PHONE, OXConstants.ALTERCONTACT_GROUP_HOME);
		addEntry(pDatabase, "mPhoneTypeHome2", 43, "549", "telephone_home2",
				Phone.CONTENT_ITEM_TYPE, Phone.TYPE_HOME, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeHome2",
				InputType.TYPE_CLASS_PHONE, OXConstants.ALTERCONTACT_GROUP_HOME);
		addEntry(pDatabase, "mPhoneTypeFaxHome", 44, "550", "fax_home",
				Phone.CONTENT_ITEM_TYPE, Phone.TYPE_FAX_HOME, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeFaxHome",
				InputType.TYPE_CLASS_PHONE, OXConstants.ALTERCONTACT_GROUP_HOME);
		addEntry(pDatabase, "mPhoneTypeRadio", 45, "562", "telephone_radio",
				Phone.CONTENT_ITEM_TYPE, Phone.TYPE_RADIO, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeRadio",
				InputType.TYPE_CLASS_PHONE,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		addEntry(pDatabase, "mPhoneTypeWorkMobile", 46, "551",
				"cellular_telephone1", Phone.CONTENT_ITEM_TYPE,
				Phone.TYPE_WORK_MOBILE, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeWorkMobile",
				InputType.TYPE_CLASS_PHONE, OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mPhoneTypeMobile", 47, "552",
				"cellular_telephone2", Phone.CONTENT_ITEM_TYPE,
				Phone.TYPE_MOBILE, "data1", InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeMobile",
				InputType.TYPE_CLASS_PHONE, OXConstants.ALTERCONTACT_GROUP_HOME);
		addEntry(pDatabase, "mPhoneTypeOther", 48, "553", "telephone_other",
				Phone.CONTENT_ITEM_TYPE, Phone.TYPE_OTHER, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeOther",
				InputType.TYPE_CLASS_PHONE,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		addEntry(pDatabase, "mPhoneTypeOtherFax", 49, "554", "fax_other",
				Phone.CONTENT_ITEM_TYPE, Phone.TYPE_OTHER_FAX, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeOtherFax",
				InputType.TYPE_CLASS_PHONE,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		addEntry(pDatabase, "mPhoneTypeISDN", 50, "559", "telephone_isdn",
				Phone.CONTENT_ITEM_TYPE, Phone.TYPE_ISDN, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeISDN",
				InputType.TYPE_CLASS_PHONE,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		addEntry(pDatabase, "mPhoneTypePager", 51, "560", "telephone_pager",
				Phone.CONTENT_ITEM_TYPE, Phone.TYPE_PAGER, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypePager",
				InputType.TYPE_CLASS_PHONE, OXConstants.ALTERCONTACT_GROUP_HOME);
		addEntry(pDatabase, "mPhoneTypeMain", 52, "561", "telephone_primary",
				Phone.CONTENT_ITEM_TYPE, Phone.TYPE_MAIN, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeMain",
				InputType.TYPE_CLASS_PHONE, OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mPhoneTypeTTYTDD", 53, "564", "telephone_ttytdd",
				Phone.CONTENT_ITEM_TYPE, Phone.TYPE_TTY_TDD, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mPhoneTypeTTYTDD",
				InputType.TYPE_CLASS_PHONE,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		// Emails:
		addEntry(pDatabase, "mEmailTypeWork", 54, "555", "email1",
				Email.CONTENT_ITEM_TYPE, Email.TYPE_WORK, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mEmailTypeWork",
				InputType.TYPE_CLASS_TEXT
						| InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mEmailTypeHome", 55, "556", "email2",
				Email.CONTENT_ITEM_TYPE, Email.TYPE_HOME, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mEmailTypeHome",
				InputType.TYPE_CLASS_TEXT
						| InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_HOME);
		addEntry(pDatabase, "mEmailTypeOther", 56, "557", "email3",
				Email.CONTENT_ITEM_TYPE, Email.TYPE_OTHER, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mEmailTypeOther",
				InputType.TYPE_CLASS_TEXT
						| InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS,
				OXConstants.ALTERCONTACT_GROUP_OTHER);
		// Website/URL;
		addEntry(pDatabase, "mWebsiteURL", 57, "558", "url",
				Website.CONTENT_ITEM_TYPE, 0, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mWebsiteURL",
				InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_URI,
				OXConstants.ALTERCONTACT_GROUP_WORK);
		// InstantMessenger:
		addEntry(pDatabase, "mImTypeWorkIm1", 58, "565", "instant_messenger1",
				Im.CONTENT_ITEM_TYPE, Im.TYPE_WORK, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mImTypeWorkIm1",
				InputType.TYPE_CLASS_TEXT, OXConstants.ALTERCONTACT_GROUP_WORK);
		addEntry(pDatabase, "mImTypeHomeIm1", 59, "566", "instant_messenger2",
				Im.CONTENT_ITEM_TYPE, Im.TYPE_HOME, "data1",
				InformationTypes.TEXT.ordinal(),
				"altercontact_translation_mImTypeHomeIm1",
				InputType.TYPE_CLASS_TEXT, OXConstants.ALTERCONTACT_GROUP_HOME);
		// DistributionList:
		addEntry(pDatabase, "mDistributionList", 60, "592",
				"distribution_list", "", 0, "", InformationTypes.NOT.ordinal(),
				"0", 0, OXConstants.ALTERCONTACT_GROUP_NO);
		// Picture:
		addEntry(pDatabase, "mPicture", 61, "570", "image1",
				Photo.CONTENT_ITEM_TYPE, 0, Photo.PHOTO,
				InformationTypes.PIC.ordinal(),
				"altercontact_translation_mPicture", 0,
				OXConstants.ALTERCONTACT_GROUP_PIC);
	}

	@Override
	public void onUpgrade(SQLiteDatabase pDatabase, int pOldVersion,
			int pNewVersion) {
		pDatabase.execSQL("DROP TABLE IF EXISTS " + OXMapping.TABLE);
		onCreate(pDatabase);
	}

	private void addEntry(SQLiteDatabase pDatabase, String pOXMappingKey,
			int pOXPosition, String pOXNumber, String pOXName,
			String pAndroidMimeType, int pAndroidType, String pAndroidColumn,
			int pInformationType, String pTranslation, int pInputType,
			int pGroup) {
		ContentValues values = new ContentValues();
		values.put(OXMapping.OXMAPPING_KEY, pOXMappingKey);
		values.put(OXMapping.OX_NAME, pOXName);
		values.put(OXMapping.OX_POSITION, pOXPosition);
		values.put(OXMapping.OX_NUMBER, pOXNumber);
		values.put(OXMapping.ANDROID_MIMETYPE, pAndroidMimeType);
		values.put(OXMapping.ANDROID_TYPE, pAndroidType);
		values.put(OXMapping.ANDROID_COLUMN, pAndroidColumn);
		values.put(OXMapping.ALTERCONTACT_INFORMATIONTYPE, pInformationType);
		values.put(OXMapping.ALTERCONTACT_TRANSLATION, pTranslation);
		values.put(OXMapping.ALTERCONTACT_INPUTTYPE, pInputType);
		values.put(OXMapping.ALTERCONTACT_GROUP, pGroup);
		pDatabase.insert(OXMapping.TABLE, null, values);
		return;
	}

	public Cursor query(String[] pProjection, String pSelection,
			String[] pSelectionArgs, String pSortOrder) {
		return query(pProjection, pSelection, pSelectionArgs, pSortOrder, false);
	}

	/**
	 * 
	 * @param pProjection
	 *            A list of which columns to return. Passing null will return
	 *            all columns, which is inefficient.
	 * @param pSelection
	 *            A filter declaring which rows to return, formatted as an SQL
	 *            WHERE clause (excluding the WHERE itself). Passing null will
	 *            return all rows.
	 * @param pSelectionArgs
	 *            You may include ?s in selection, which will be replaced by the
	 *            values from selectionArgs, in the order that they appear in
	 *            the selection. The values will be bound as Strings.
	 * @param pSortOrder
	 *            How to order the rows, formatted as an SQL ORDER BY clause
	 *            (excluding the ORDER BY itself). Passing null will use the
	 *            default sort order, which may be unordered.
	 * @param pWithPicGroup
	 *            Whether you want the query to containt the
	 *            group-picture-information
	 * @return
	 */
	public Cursor query(String[] pProjection, String pSelection,
			String[] pSelectionArgs, String pSortOrder, boolean pWithPicGroup) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor;
		if(pWithPicGroup) {
			cursor = db.query(OXMapping.TABLE, pProjection, pSelection,
					pSelectionArgs, null, null, pSortOrder);
		} else {
			// OXConstants.EDITCONTACT_PIC ausschließen
			StringBuilder selection = new StringBuilder();
			if(pSelection != null) selection.append(pSelection + " AND ");
			selection.append(OXMapping.ALTERCONTACT_GROUP + " != "
					+ OXConstants.ALTERCONTACT_GROUP_PIC);
			cursor = db.query(OXMapping.TABLE, pProjection,
					selection.toString(), pSelectionArgs, null, null,
					pSortOrder);
		}
		mCursors.add(cursor);
		return cursor;
	}

	// TODO: Clean this whole "close"-thing
	// Close shouldn't be handled by the DatabaseHelper at least not by saving
	// all the cursors and override close to close them :/
	@Override
	public synchronized void close() {
		for(Cursor cursor : mCursors) {
			cursor.close();
		}
		try {
			this.getReadableDatabase().close();
		} catch (IllegalStateException e) {
			// Database was not even opened -> never mind
		}
		super.close();
	}
}
