/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient;

import java.util.ArrayList;

import android.accounts.Account;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.Bundle;
import android.os.RemoteException;

import com.alien.oxclient.mockinterfaces.OXContentResolver;

public class OXContentResolverImpl implements OXContentResolver {

	private Context mContext;

	public OXContentResolverImpl(Context pContext) {
		this.mContext = pContext;
	}

	public void addPeriodicSync(Account pAccount, String pAuthority,
			Bundle pExtrasBundle, long pFrequency) {
		ContentResolver.addPeriodicSync(pAccount, pAuthority, pExtrasBundle,
				pFrequency);
	}

	public ContentProviderResult[] applyBatch(String pAuthority,
			ArrayList<ContentProviderOperation> pOperations)
			throws OperationApplicationException, RemoteException {
		if(pOperations.size() > 0) return mContext.getContentResolver()
				.applyBatch(pAuthority, pOperations);
		else return null;
	}
}
