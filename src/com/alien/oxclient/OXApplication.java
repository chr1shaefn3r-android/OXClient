/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import android.app.Application;

import com.alien.oxclient.action.OXActionDependencyManager;
import com.alien.oxclient.action.OXActionDependencyManagerImpl;
import com.alien.oxclient.client.OXNetworkImpl;
import com.alien.oxclient.mockinterfaces.OXAccountManager;
import com.alien.oxclient.mockinterfaces.OXCalendar;
import com.alien.oxclient.mockinterfaces.OXContentResolver;
import com.alien.oxclient.mockinterfaces.OXDecision;
import com.alien.oxclient.mockinterfaces.OXDeviceState;
import com.alien.oxclient.mockinterfaces.OXDeviceStateObserver;
import com.alien.oxclient.mockinterfaces.OXNetwork;
import com.alien.oxclient.mockinterfaces.OXNotificationManager;
import com.alien.oxclient.mockinterfaces.OXPreferences;
import com.alien.oxclient.mockinterfaces.OXUtils;
import com.alien.oxclient.preferences.OXPreferencesImpl;

@ReportsCrashes(formUri = "http://www.bugsense.com/api/acra?api_key=e2794335", formKey = "", mode = ReportingInteractionMode.NOTIFICATION, resNotifTickerText = R.string.crash_notif_ticker_text, resNotifTitle = R.string.crash_notif_title, resNotifText = R.string.crash_notif_text, resNotifIcon = android.R.drawable.stat_notify_error, resDialogText = R.string.crash_dialog_text, resDialogIcon = android.R.drawable.ic_dialog_alert, resDialogTitle = R.string.crash_dialog_title, resDialogCommentPrompt = R.string.crash_dialog_comment_prompt, resDialogOkToast = R.string.crash_dialog_ok_toast)
public class OXApplication extends Application {

	private static OXAccountManager mAccountManager;
	private static OXUtils mUtils;
	private static OXPreferences mPrefs;
	private static OXContentResolver mContentResolver;
	private static OXNotificationManager mNotificationManager;
	private static OXCalendar mCalendar;
	private static OXNetwork mNetwork;
	private static OXActionDependencyManager mActionDepdencyManager;
	private static OXDecision mDecision;
	private static OXDeviceState mDeviceState;
	private static OXDeviceStateObserver mDeviceStateObserver;

	@Override
	public void onCreate() {
		// The following line triggers the initialization of ACRA
		try {
			ACRA.init(this);
		} catch (IllegalStateException e) {
			// Thrown when ACRA.init has already been called
			// silently ignore it ...
		}

		if(mUtils == null) mUtils = new OXUtilsImpl(getBaseContext());
		if(mAccountManager == null) mAccountManager = new OXAccountManagerImpl(
				getBaseContext());
		if(mPrefs == null) mPrefs = new OXPreferencesImpl(getBaseContext());
		if(mContentResolver == null) mContentResolver = new OXContentResolverImpl(
				getBaseContext());
		if(mNotificationManager == null) mNotificationManager = new OXNotificationManagerImpl(
				getBaseContext());
		if(mCalendar == null) mCalendar = new OXCalendarImpl();
		if(mNetwork == null) mNetwork = new OXNetworkImpl();
		if(mActionDepdencyManager == null) mActionDepdencyManager = new OXActionDependencyManagerImpl(
				getBaseContext());
		OXDeviceStateImpl deviceStateImpl = new OXDeviceStateImpl(
				getBaseContext());
		if(mDecision == null) mDecision = new OXDecisionImpl(getBaseContext());
		if(mDeviceState == null) mDeviceState = deviceStateImpl;
		if(mDeviceStateObserver == null) mDeviceStateObserver = deviceStateImpl;

		super.onCreate();
	}

	public static OXAccountManager getAccountManager() {
		return mAccountManager;
	}

	public static void setAccountManager(OXAccountManager pAccountManager) {
		OXApplication.mAccountManager = pAccountManager;
	}

	public static OXUtils getUtils() {
		return mUtils;
	}

	public static void setUtils(OXUtils pUtils) {
		OXApplication.mUtils = pUtils;
	}

	public static OXPreferences getPrefs() {
		return mPrefs;
	}

	public static void setPrefs(OXPreferences pPrefs) {
		OXApplication.mPrefs = pPrefs;
	}

	public static OXContentResolver getOXContentResolver() {
		return mContentResolver;
	}

	public static void setOXContentResolver(OXContentResolver pContextResolver) {
		OXApplication.mContentResolver = pContextResolver;
	}

	public static OXNotificationManager getNotificationManager() {
		return mNotificationManager;
	}

	public static void setNotificationManager(
			OXNotificationManager pNotificationManager) {
		OXApplication.mNotificationManager = pNotificationManager;
	}

	public static OXCalendar getCalendar() {
		return mCalendar.refresh();
	}

	public static void setCalendar(OXCalendar pCalendar) {
		OXApplication.mCalendar = pCalendar;
	}

	public static OXNetwork getOXNetwork() {
		return mNetwork;
	}

	public static void setNetwork(OXNetwork pNetwork) {
		OXApplication.mNetwork = pNetwork;
	}

	public static OXActionDependencyManager getActionDependencyManager() {
		return mActionDepdencyManager;
	}

	public static void setActionDependencyManager(
			OXActionDependencyManager pActionDependencyManager) {
		OXApplication.mActionDepdencyManager = pActionDependencyManager;
	}

	public static OXDecision getDecision() {
		return mDecision;
	}

	public static void setDecision(OXDecision pDecision) {
		OXApplication.mDecision = pDecision;
	}

	public static void setDeviceStateObserver(OXDeviceStateObserver pObserver) {
		OXApplication.mDeviceStateObserver = pObserver;
	}

	public static OXDeviceStateObserver getDeviceStateObserver() {
		return mDeviceStateObserver;
	}

	public static void setDeviceState(OXDeviceState pDeviceState) {
		OXApplication.mDeviceState = pDeviceState;
	}

	public static OXDeviceState getDeviceState() {
		return mDeviceState;
	}
}
