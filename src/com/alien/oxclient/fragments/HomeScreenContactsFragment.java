/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.fragments;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.R;
import com.alien.oxclient.activities.AddContact;
import com.alien.oxclient.activities.CoverFlowExample;
import com.alien.oxclient.activities.SolveConflicts;
import com.alien.oxclient.activities.UploadContacts;

public class HomeScreenContactsFragment extends Fragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view;
		// Grab the versionCode
		PackageInfo info = new PackageInfo();
		try {
			info = getActivity().getPackageManager().getPackageInfo(
					getActivity().getPackageName(), 0);
		} catch (NameNotFoundException e) {
			// This should never happen ;)
		}
		// Inflate the layout for this fragment, depending on whether the
		// OXAccount already exists
		// due to bug http://code.google.com/p/android/issues/detail?id=18529
		if(OXApplication.getPrefs().getInformationenDialogSeen()
				&& info.versionCode <= OXApplication.getPrefs()
						.getVersionCode()) view = inflater.inflate(
				R.layout.menufragment_contacts, container, false);
		else {
			// Because of the bug mentioned before we start with a layout
			// without fragments
			view = inflater.inflate(
					R.layout.menufragment_contacts_without_fragments,
					container, false);
		}

		// Setup onClickListener
		view.findViewById(R.id.menufragment_contacts_button_add_contact)
				.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						Intent addContactIntent = new Intent(getActivity(),
								AddContact.class);
						startActivity(addContactIntent);
					}
				});
		view.findViewById(R.id.menufragment_contacts_button_uploadcontacts)
				.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						Intent uploadContactsIntent = new Intent(getActivity(),
								UploadContacts.class);
						startActivity(uploadContactsIntent);
					}
				});
		view.findViewById(R.id.menufragment_contacts_button_contactsflow)
				.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						Intent coverflowExampleIntent = new Intent(
								getActivity(), CoverFlowExample.class);
						startActivity(coverflowExampleIntent);
					}
				});
		view.findViewById(R.id.menufragment_contacts_button_solveconflicts)
				.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						Intent editAccountIntent = new Intent(getActivity(),
								SolveConflicts.class);
						startActivity(editAccountIntent);
					}
				});
		return view;
	}
}
