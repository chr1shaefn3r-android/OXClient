/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 *
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 *
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.fragments;

import java.util.Date;

import org.json.JSONException;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.RawContacts;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.QuickContactBadge;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.alien.oxclient.ContactPhotoLoader;
import com.alien.oxclient.OXApplication;
import com.alien.oxclient.R;
import com.alien.oxclient.action.OXAction;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.model.OXContactOXIdBuilder;
import com.alien.oxclient.provider.OXMapping;
import com.alien.oxclient.provider.OXMappingDatabaseHelper;
import com.alien.oxclient.provider.SyncConflicts;

public class ConflictFragment extends Fragment {

	private GetContactInformationsTask mTask;
	private ContactPhotoLoader mPhotoLoader;
	private boolean mHasConflicts = false;
	private String mOXid;
	private String mName;
	private String mTimestamp;
	private OXContact mServerContact;
	private OXContact mLokalContact;
	private boolean mServerContactAvailable = mServerContact != null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Create new PhotoLoader
		mPhotoLoader = new ContactPhotoLoader(getActivity(),
				R.drawable.ic_oxcontact_picture);
		// Declare hasOptionsMenu
		this.setHasOptionsMenu(true);
		// Declare retained instance
		this.setRetainInstance(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.conflictfragment, container);
		view.findViewById(R.id.solveconflict_fragment_button_toserver)
				.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						mTask = new PushContactTask();
						mTask.execute(false, false);
					}
				});
		view.findViewById(R.id.solveconflict_fragment_button_tomobile)
				.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						mTask = new PullContactTask();
						mTask.execute(false, false);
					}
				});
		return view;
	}

	public void updateContent(String pOXId, String pName, String pTimestamp)
			throws NumberFormatException, JSONException {
		this.mOXid = pOXId;
		this.mName = pName;
		this.mTimestamp = pTimestamp;
		if(!TextUtils.isEmpty(mName)) {
			mHasConflicts = true;
			mServerContactAvailable = mServerContact != null;
			TextView username = (TextView) getView().findViewById(
					R.id.solveconflict_fragment_name);
			QuickContactBadge contactPicture = (QuickContactBadge) getView()
					.findViewById(R.id.solveconflict_fragment_badge);
			TextView timestamp = (TextView) getView().findViewById(
					R.id.solveconflict_fragment_timestamp);
			TableLayout tableLayout = (TableLayout) getView().findViewById(
					R.id.solveconflict_fragment_tablelayout);
			username.setText(pName);
			timestamp.setText(pTimestamp);
			// Set up the QuickContactBadge
			ContentResolver ctr = getActivity().getContentResolver();
			String rawContactId = OXContact.getRawContactId(pOXId, ctr);
			Cursor c = ctr.query(ContactsContract.Data.CONTENT_URI,
					new String[] { RawContacts.Entity.RAW_CONTACT_ID,
							Contacts._ID, Contacts.LOOKUP_KEY,
							Contacts.PHOTO_ID },
					RawContacts.Entity.RAW_CONTACT_ID + " = ?",
					new String[] { rawContactId }, null);
			try {
				c.moveToFirst();
				long photoId = 0;
				if(!c.isNull(c.getColumnIndex(Contacts.PHOTO_ID))) {
					photoId = c.getLong(c.getColumnIndex(Contacts.PHOTO_ID));
				}
				final long contactId = c
						.getLong(c.getColumnIndex(Contacts._ID));
				final String lookupKey = c.getString(c
						.getColumnIndex(Contacts.LOOKUP_KEY));
				contactPicture.assignContactUri(Contacts.getLookupUri(
						contactId, lookupKey));
				mPhotoLoader.loadPhoto(contactPicture, photoId);
			} finally {
				c.close();
			}
			// String-Ressourcen cachen
			String equals = getActivity().getString(
					R.string.solveconflict_fragment_equals);
			String toLeft = getActivity().getString(
					R.string.solveconflict_fragment_to_left);
			String toRight = getActivity().getString(
					R.string.solveconflict_fragment_to_right);
			String unknown = getActivity().getString(
					R.string.solveconflict_fragment_unknown);
			String empty = unknown;
			// OXContact erstellen
			mLokalContact = new OXContactOXIdBuilder()
					.addContext(getActivity()).addTimestamp(new Date())
					.addFromOXId(pOXId).getOXObject();
			if(mServerContactAvailable) {
				empty = "";
			}
			// Setup TableLayout
			TableRow.LayoutParams lp = new TableRow.LayoutParams(
					TableRow.LayoutParams.WRAP_CONTENT,
					TableRow.LayoutParams.WRAP_CONTENT, 1f);
			String[] projection = new String[] { OXMapping.OXMAPPING_KEY };
			OXMappingDatabaseHelper oxMappingDBHelper = new OXMappingDatabaseHelper(
					getActivity());
			Cursor cursor = oxMappingDBHelper.query(projection, null, null,
					null);
			try {
				int index = 2;
				while (cursor.moveToNext()) {
					String key = cursor.getString(cursor
							.getColumnIndex(OXMapping.OXMAPPING_KEY));
					if(key.equals("mID")) continue;
					String contactValue = mLokalContact
							.getContactInformation(key);
					String serverValue = null;
					if(mServerContactAvailable) serverValue = mServerContact
							.getContactInformation(key);
					if(TextUtils.isEmpty(contactValue)
							&& TextUtils.isEmpty(serverValue)) continue;
					View child = getChild(tableLayout, key);
					if(child != null) {
						TableRow tableRow = (TableRow) child;
						changeTextInformation(true,
								contactValue == null ? empty : contactValue,
								tableRow);
						if(TextUtils.equals(contactValue, serverValue)) {
							changeSeperator(equals, tableRow);
						} else if(TextUtils.isEmpty(contactValue)) {
							changeSeperator(toLeft, tableRow);
						} else if(TextUtils.isEmpty(serverValue)) {
							changeSeperator(toRight, tableRow);
						} else {
							changeSeperator(unknown, tableRow);
						}
						changeTextInformation(false,
								serverValue == null ? empty : serverValue,
								tableRow);
					} else {
						TableRow tableRow = new TableRow(getActivity());
						tableRow.setTag(key);
						addTextInformation(true, contactValue == null ? empty
								: contactValue, tableRow, lp);
						if(TextUtils.equals(contactValue, serverValue)) {
							addSeperator(equals, tableRow);
						} else if(TextUtils.isEmpty(contactValue)) {
							addSeperator(toLeft, tableRow);
						} else if(TextUtils.isEmpty(serverValue)) {
							addSeperator(toRight, tableRow);
						} else {
							addSeperator(unknown, tableRow);
						}
						addTextInformation(false, serverValue == null ? empty
								: serverValue, tableRow, lp);
						tableLayout.addView(tableRow, index);
						index++;
					}
				}
			} finally {
				cursor.close();
				oxMappingDBHelper.close();
			}
		} else {
			getView().findViewById(R.id.solveconflict_fragment_scrollview)
					.setVisibility(View.INVISIBLE);
			getView().findViewById(R.id.solveconflict_fragment_button_toserver)
					.setVisibility(View.INVISIBLE);
			getView().findViewById(R.id.solveconflict_fragment_button_tomobile)
					.setVisibility(View.INVISIBLE);
			getView().findViewById(R.id.solveconflict_fragment_badge)
					.setVisibility(View.INVISIBLE);
		}
	}

	private void changeTextInformation(boolean pRightOrLeft, String pText,
			TableRow pTableRow) {
		int index = pRightOrLeft == true ? 0 : 2;
		((TextView) pTableRow.getChildAt(index)).setText(pText);
	}

	private void changeSeperator(String pText, TableRow pTableRow) {
		changeTextView(pText, (TextView) pTableRow.getChildAt(1));
	}

	private void addTextInformation(boolean pRightOrLeft, String pText,
			TableRow pTableRow, TableRow.LayoutParams pLayoutParams) {
		TextView text = new TextView(getActivity());
		text.setText(pText);
		text.setGravity(pRightOrLeft != true ? Gravity.RIGHT : Gravity.LEFT);
		text.setLayoutParams(pLayoutParams);
		text.setSingleLine();
		pTableRow.addView(text);
	}

	private void addSeperator(String pText, TableRow pTableRow) {
		TextView equals = new TextView(getActivity());
		changeTextView(pText, equals);
		pTableRow.addView(equals);
	}

	private void changeTextView(String pText, TextView pTextView) {
		Activity activity = getActivity();
		pTextView.setText(pText);
		pTextView.setGravity(Gravity.CENTER);
		if(pText.equals(activity
				.getString(R.string.solveconflict_fragment_equals))) {
			if(mServerContactAvailable) pTextView.setTextColor(0xFF00FF00); // GREEN
		} else if(pText.equals(activity
				.getString(R.string.solveconflict_fragment_to_left))
				|| pText.equals(activity
						.getString(R.string.solveconflict_fragment_to_right))) {
			if(mServerContactAvailable) pTextView.setTextColor(0xFFFFAA33); // ORANGE
		} else {
			if(mServerContactAvailable) pTextView.setTextColor(0xFFFF0000); // RED
		}
	}

	@Override
	public void onDestroy() {
		mPhotoLoader.stop();
		cancelTask();
		super.onDestroy();
	}

	@Override
	public void onResume() {
		mPhotoLoader.resume();
		super.onResume();
	}

	/*****************************************************************************************************************************/
	/* --> .:Menu-Methoden:. <-- */
	/*****************************************************************************************************************************/
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.solveconflict_fragment, menu);
		menu.findItem(R.id.menu_refresh).setEnabled(mHasConflicts);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem pMenuItem) {
		switch (pMenuItem.getItemId()) {
		case R.id.menu_refresh:
			if(mHasConflicts) {
				if(mTask == null
						|| (mTask != null && mTask.getStatus() != AsyncTask.Status.RUNNING)) {
					mTask = new GetContactInformationsTask();
					mTask.execute(false, false);
				}
			}
			break;
		}
		return super.onOptionsItemSelected(pMenuItem);
	}

	/*****************************************************************************************************************************/
	/* --> .:Gesturelistener:. <-- */
	/*****************************************************************************************************************************/
	class MyGestureDetector extends SimpleOnGestureListener {
		private int SWIPE_MAX_OFF_PATH;
		private int SWIPE_MIN_DISTANCE;
		private int SWIPE_THRESHOLD_VELOCITY;
		private View mView;

		public MyGestureDetector(Context pContext, View pView) {
			final ViewConfiguration vc = ViewConfiguration.get(pContext);
			SWIPE_MIN_DISTANCE = vc.getScaledTouchSlop();
			SWIPE_THRESHOLD_VELOCITY = vc.getScaledMinimumFlingVelocity();
			SWIPE_MAX_OFF_PATH = vc.getScaledMaximumFlingVelocity();
			this.mView = pView;
		}

		@Override
		public boolean onDown(MotionEvent e) {
			return true;
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			try {
				if(Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH) return false;
				if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					animateViewLeftOut(mView);
				} else if(e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					animateViewRightOut(mView);
				}
			} catch (Exception e) {
				// This should never happen ;)
			}
			return false;
		}
	}

	/*****************************************************************************************************************************/
	/* --> .:GetContactInformationsTask:. <-- */
	/*****************************************************************************************************************************/
	private class GetContactInformationsTask extends
			AsyncTask<Boolean, Integer, OXAction<?>> {
		@Override
		protected OXAction<?> doInBackground(Boolean... pForces) {
			OXAction<?> action = OXApplication.getActionDependencyManager()
					.getOXActionGetContact(mOXid);
			action.perform();
			return action;
		}

		@Override
		protected void onPreExecute() {
			// TODO: Fix for new setup
			// ((ActionBarActivity) getActivity()).getActionBarHelper()
			// .setRefreshActionItemState(true);
		}

		@Override
		protected void onPostExecute(OXAction<?> pResult) {
			// TODO: Fix for new setup
			// ((ActionBarActivity) getActivity()).getActionBarHelper()
			// .setRefreshActionItemState(false);
			try {
				if(pResult.hasResult()) {
					if(pResult.hasWarningMessage()) showWarning(pResult
							.getWarningMessage());
					mServerContact = (OXContact) pResult.getResult();
					mServerContact.setContext(getActivity());
					mServerContact.setTimestamp(new Date());
					updateContent(mOXid, mName, mTimestamp);
				} else {
					if(pResult.hasErrorMessage()) showError(pResult
							.getErrorMessage());
					else showError(getString(R.string.error_without_errormessage));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		protected void showSuccess(String pText) {
			setupMessage(pText, getResources()
					.getColor(R.color.message_success), false);
		}

		protected void showWarning(String pText) {
			setupMessage(pText, getResources()
					.getColor(R.color.message_warning), false);
		}

		protected void showError(String pText) {
			setupMessage(pText, getResources().getColor(R.color.message_error),
					true);
		}

		protected void setupMessage(String pText, int pBackgroundColor,
				boolean warning) {
			final RelativeLayout message = (RelativeLayout) getView()
					.findViewById(R.id.solveconflict_fragment_message);
			// Gesture detection
			final GestureDetector gestureDetector = new GestureDetector(
					new MyGestureDetector(getActivity(), message));
			View.OnTouchListener gestureListener = new View.OnTouchListener() {
				public boolean onTouch(View v, MotionEvent event) {
					return gestureDetector.onTouchEvent(event);
				}
			};
			message.setOnTouchListener(gestureListener);
			TextView text = (TextView) message
					.findViewById(R.id.solveconflict_fragment_message_text);
			ImageButton close = (ImageButton) message
					.findViewById(R.id.solveconflict_fragment_message_button_close);
			close.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					animateViewTopOut(message);
				}
			});
			if(warning) {
				ImageButton retry = (ImageButton) message
						.findViewById(R.id.solveconflict_fragment_message_button_retry);
				retry.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						animateViewTopOut(message);
						mTask = new GetContactInformationsTask();
						mTask.execute(true, true);
					}
				});
				retry.setVisibility(View.VISIBLE);
			}
			text.setText(pText);
			message.setBackgroundColor(pBackgroundColor);
			animateViewIn(message);
		}
	}

	/**
	 * Cancel Task
	 */
	private void cancelTask() {
		if(mTask != null && mTask.getStatus() == AsyncTask.Status.RUNNING) {
			mTask.cancel(false);
		}
		return;
	}

	/*****************************************************************************************************************************/
	/* --> .:PullContactTask:. <-- */
	/*****************************************************************************************************************************/
	private class PullContactTask extends GetContactInformationsTask {
		@Override
		protected OXAction<?> doInBackground(Boolean... pForces) {
			OXAction<?> action = null;
			if(mServerContact == null) {
				action = OXApplication.getActionDependencyManager()
						.getOXActionGetContact(mOXid);
				action.perform();
			}
			return action;
		}

		@Override
		protected void onPostExecute(OXAction<?> pResult) {
			// TODO: Fix for new setup
			// ((ActionBarActivity) getActivity()).getActionBarHelper()
			// .setRefreshActionItemState(false);
			if(pResult != null) {
				if(pResult.hasResult()) {
					if(pResult.hasWarningMessage()) showWarning(pResult
							.getWarningMessage());
					mServerContact = (OXContact) pResult.getResult();
					mServerContact.setContext(getActivity());
					mServerContact.setTimestamp(new Date());
				} else {
					if(pResult.hasErrorMessage()) showError(pResult
							.getErrorMessage());
					else showError(getString(R.string.error_without_errormessage));
				}
			}
			try {
				ContentResolver ctr = getActivity().getContentResolver();
				ctr.applyBatch(ContactsContract.AUTHORITY,
						mServerContact.updateContact());
				ctr.applyBatch(ContactsContract.AUTHORITY,
						mServerContact.unmarkAsConflict());
				ctr.delete(SyncConflicts.CONTENT_URI, SyncConflicts.OXID
						+ "= ?", new String[] { mLokalContact.getId() });
				showSuccess(getString(R.string.solveconflict_fragment_message_success));
			} catch (Exception e) {
				showError(e.getClass().getSimpleName() + ": "
						+ e.getLocalizedMessage());
			}
		}
	}

	/*****************************************************************************************************************************/
	/* --> .:PushContactTask:. <-- */
	/*****************************************************************************************************************************/
	private class PushContactTask extends GetContactInformationsTask {
		@Override
		protected OXAction<?> doInBackground(Boolean... pForces) {
			OXAction<?> action = OXApplication.getActionDependencyManager()
					.getOXActionUpdateContact(mLokalContact);
			action.perform();
			return action;
		}

		@Override
		protected void onPostExecute(OXAction<?> pResult) {
			// TODO: Fix for new setup
			// ((ActionBarActivity) getActivity()).getActionBarHelper()
			// .setRefreshActionItemState(false);
			if(pResult.hasResult()) {
				if(pResult.hasWarningMessage()) showWarning(pResult
						.getWarningMessage());
				try {
					ContentResolver ctr = getActivity().getContentResolver();
					ctr.delete(SyncConflicts.CONTENT_URI, SyncConflicts.OXID
							+ "= ?", new String[] { mLokalContact.getId() });
					ctr.applyBatch(ContactsContract.AUTHORITY,
							mLokalContact.unmarkAsConflict());
					showSuccess(getString(R.string.solveconflict_fragment_message_success));
				} catch (Exception e) {
					showError(e.getClass().getSimpleName() + ": "
							+ e.getLocalizedMessage());
				}
			} else {
				if(pResult.hasErrorMessage()) showError(pResult
						.getErrorMessage());
				else showError(getString(R.string.error_without_errormessage));
			}
		}
	}

	/*****************************************************************************************************************************/
	/* --> .:Animation-Methode:. <-- */
	/*****************************************************************************************************************************/
	private void animateViewTopOut(final View pView) {
		animateViewOut(pView, R.anim.push_top_out);
	}

	private void animateViewLeftOut(final View pView) {
		animateViewOut(pView, R.anim.push_left_out);
	}

	private void animateViewRightOut(final View pView) {
		animateViewOut(pView, R.anim.push_right_out);
	}

	private void animateViewOut(final View pView, int pAnimation) {
		Animation pushOut = AnimationUtils.loadAnimation(getActivity(),
				pAnimation);
		pushOut.setAnimationListener(new Animation.AnimationListener() {
			public void onAnimationStart(Animation animation) {
			}

			public void onAnimationRepeat(Animation animation) {
			}

			public void onAnimationEnd(Animation animation) {
				pView.setVisibility(View.GONE);
			}
		});
		pView.startAnimation(pushOut);
	}

	private void animateViewIn(final View pView) {
		Animation pushLeftIn = AnimationUtils.loadAnimation(getActivity(),
				R.anim.push_left_in);
		pView.setVisibility(View.VISIBLE);
		pView.startAnimation(pushLeftIn);
	}

	/*****************************************************************************************************************************/
	/* --> .:TableLayout-Methode:. <-- */
	/*****************************************************************************************************************************/
	private View getChild(TableLayout pTableLayout, String pTag) {
		for(int i = 0; i < pTableLayout.getChildCount(); i++) {
			View view = pTableLayout.getChildAt(i);
			if(pTag.equals(view.getTag())) return view;
		}
		return null;
	}
}
