/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.fragments;

import java.util.Date;
import java.util.List;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncInfo;
import android.content.SyncStatusObserver;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.R;
import com.alien.oxclient.mockinterfaces.OXPreferences;

public class SyncIndicator extends Fragment implements OnClickListener {

	private boolean mIsActive = false;
	private boolean mIsPending = false;
	private boolean mChecked = false;
	private final Handler mHandler = new Handler();
	private Object mStatusChangeListenerHandle;
	private SyncStatusObserver mSyncStatusObserver = new SyncStatusObserver() {
		public void onStatusChanged(int which) {
			mHandler.post(new Runnable() {
				public void run() {
					onSyncStateUpdated();
				}
			});
		}
	};

	@Override
	public void onResume() {
		super.onResume();
		mStatusChangeListenerHandle = ContentResolver.addStatusChangeListener(
				ContentResolver.SYNC_OBSERVER_TYPE_ACTIVE
						| ContentResolver.SYNC_OBSERVER_TYPE_PENDING
						| ContentResolver.SYNC_OBSERVER_TYPE_SETTINGS,
				mSyncStatusObserver);
		onSyncStateUpdated();
	}

	@Override
	public void onPause() {
		super.onPause();
		ContentResolver.removeStatusChangeListener(mStatusChangeListenerHandle);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View parent = inflater
				.inflate(R.layout.syncindicator, container, false);
		parent.findViewById(
				R.id.menufragment_contacts_button_sync_now_image_button)
				.setOnClickListener(this);
		return parent;
	}

	public void onClick(View pView) {
		ContentResolver.requestSync(OXApplication.getAccountManager()
				.getOXAccount(), ContactsContract.AUTHORITY, new Bundle());
		onSyncStateUpdated();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		onSyncStateUpdated();
	}

	@TargetApi(11)
	private void onSyncStateUpdated() {
		String authority = ContactsContract.AUTHORITY;
		Account account = OXApplication.getAccountManager().getOXAccount();
		boolean syncEnabled = ContentResolver.getSyncAutomatically(account,
				authority);
		boolean authorityIsPending = ContentResolver.isSyncPending(account,
				authority);
		boolean activelySyncing;
		if(OXApplication.getUtils().isHoneyCombOrBetter()) {
			activelySyncing = isSyncing(ContentResolver.getCurrentSyncs(),
					account, authority);
		} else {
			activelySyncing = ContentResolver.isSyncActive(account, authority);
		}

		int syncState = ContentResolver.getIsSyncable(account, authority);

		mIsActive = activelySyncing && (syncState >= 0);
		mIsPending = authorityIsPending && (syncState >= 0);

		ConnectivityManager connManager = (ConnectivityManager) getActivity()
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		final boolean masterSyncAutomatically = ContentResolver
				.getMasterSyncAutomatically();
		final boolean backgroundDataEnabled = connManager
				.getBackgroundDataSetting();
		final boolean oneTimeSyncMode = !masterSyncAutomatically
				|| !backgroundDataEnabled;
		mChecked = (oneTimeSyncMode || syncEnabled);
		if(getView() != null) updateView();
	}

	private void updateView() {
		final boolean activeVisible = mIsActive || mIsPending;
		if(activeVisible) {
			getView().findViewById(
					R.id.menufragment_contacts_button_sync_now_image_button)
					.setVisibility(View.GONE);
			if(mIsActive) {
				getView()
						.findViewById(
								R.id.menufragment_contacts_button_sync_now_progress_bar_pending)
						.setVisibility(View.GONE);
				ProgressBar progressBar = (ProgressBar) getView()
						.findViewById(
								R.id.menufragment_contacts_button_sync_now_progress_bar_active);
				progressBar.setVisibility(View.VISIBLE);
				progressBar.setIndeterminate(true);
			} else {
				getView()
						.findViewById(
								R.id.menufragment_contacts_button_sync_now_progress_bar_active)
						.setVisibility(View.GONE);
				ProgressBar progressBar = (ProgressBar) getView()
						.findViewById(
								R.id.menufragment_contacts_button_sync_now_progress_bar_pending);
				progressBar.setVisibility(View.VISIBLE);
				progressBar.setIndeterminate(false);
				progressBar.setMax(100);
				progressBar.setProgress(50);
			}
		} else {
			getView()
					.findViewById(
							R.id.menufragment_contacts_button_sync_now_progress_bar_active)
					.setVisibility(View.GONE);
			getView()
					.findViewById(
							R.id.menufragment_contacts_button_sync_now_progress_bar_pending)
					.setVisibility(View.GONE);
			getView().findViewById(
					R.id.menufragment_contacts_button_sync_now_image_button)
					.setVisibility(View.VISIBLE);
			getView().findViewById(
					R.id.menufragment_contacts_button_sync_now_image_button)
					.setEnabled(mChecked);
			getView().findViewById(
					R.id.menufragment_contacts_button_sync_now_image_button)
					.setClickable(mChecked);
			Context context = getActivity();
			if(context != null) {
				TextView lastSynced = ((TextView) getView().findViewById(
						R.id.menufragment_contacts_button_last_synced));
				OXPreferences prefs = OXApplication.getPrefs();
				if(prefs.getLastSync() == 0) {
					lastSynced
							.setText(context
									.getString(R.string.syncindicator_last_synced)
									+ ": "
									+ context
											.getString(R.string.syncindicator_not_synced));
				} else {
					Date date = new Date(prefs.getLastSync());
					lastSynced.setText(context
							.getString(R.string.syncindicator_last_synced)
							+ ": "
							+ DateFormat.getDateFormat(context).format(date)
							+ " "
							+ DateFormat.getTimeFormat(context).format(date));
				}
			}
		}
	}

	private boolean isSyncing(List<SyncInfo> currentSyncs, Account account,
			String authority) {
		for(SyncInfo syncInfo : currentSyncs) {
			if(syncInfo.account.equals(account)
					&& syncInfo.authority.equals(authority)) { return true; }
		}
		return false;
	}
}
