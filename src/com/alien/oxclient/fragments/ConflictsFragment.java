/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 *
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 *
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SearchViewCompat;
import android.support.v4.widget.SearchViewCompat.OnQueryTextListenerCompat;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.alien.oxclient.R;
import com.alien.oxclient.provider.SyncConflicts;

public class ConflictsFragment extends ListFragment implements
		LoaderManager.LoaderCallbacks<Cursor> {

	private OnItemSelectedListener mListener;
	private OnSearchInformationAvailable mSearchListener;
	private int mCurPosition = 0;
	private SimpleCursorAdapter mAdapter;
	private boolean mDualPane = false;
	private String mCurFilter;

	/**
	 * Container Activity must implement those interfaces and we ensure that it
	 * does during the onAttach() callback
	 */
	public interface OnItemSelectedListener {
		public void onItemSelected(int pPosition, String pOxid, String pName,
				String pTimestamp);
	}

	public interface OnSearchInformationAvailable {
		public String onSearchInformationAvailable();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Declare hasOptionsMenu
		this.setHasOptionsMenu(true);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// Current position should survive screen rotations.
		if(savedInstanceState != null) {
			mCurPosition = savedInstanceState.getInt("listPosition");
		}

		setEmptyText(getActivity().getString(
				R.string.solveconflict_fragment_no_conflict));

		// Create an empty adapter we will use to display the loaded data
		mAdapter = new SimpleCursorAdapter(getActivity(),
				android.R.layout.simple_list_item_2, null, new String[] {
						SyncConflicts.USERNAME, SyncConflicts.TIMESTAMP },
				new int[] { android.R.id.text1, android.R.id.text2 }, 0);
		setListAdapter(mAdapter);

		// Start out with a progress indicator.
		setListShown(false);

		ListView lv = getListView();
		lv.setCacheColorHint(Color.TRANSPARENT); // Improves scrolling
													// performance

		ConflictFragment frag = (ConflictFragment) getFragmentManager()
				.findFragmentById(R.id.solveconflicts_resolve_frag);
		if(frag != null) mDualPane = true;

		// If showing both fragments
		if(mDualPane) {
			// Highlight the currently selected item
			lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			// select the appropriate list item by default
			selectPosition(mCurPosition);
		}

		mCurFilter = mSearchListener.onSearchInformationAvailable();

		// Prepare the loader. Either re-connect with an existing one,
		// or start a new one.
		getLoaderManager().initLoader(0, null, this);
	}

	@Override
	public void onAttach(Activity pActivity) {
		super.onAttach(pActivity);
		// Check that the container activity has implemented the callback
		// interface
		try {
			mListener = (OnItemSelectedListener) pActivity;
			mSearchListener = (OnSearchInformationAvailable) pActivity;
		} catch (ClassCastException e) {
			throw new ClassCastException(
					pActivity.toString()
							+ " must implement OnItemSelectedListener and OnSearchInformationAvailable.");
		}

	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// Send the event to the host activity via OnItemSelectedListener
		// callback
		Cursor c = (Cursor) mAdapter.getItem(position);
		mListener.onItemSelected(position,
				c.getString(c.getColumnIndex(SyncConflicts.OXID)),
				c.getString(c.getColumnIndex(SyncConflicts.USERNAME)),
				c.getString(c.getColumnIndex(SyncConflicts.TIMESTAMP)));
		mCurPosition = position;
	}

	/** Called to select an item from the listview */
	public void selectPosition(int position) {
		// Only if we're showing both fragments should the item be "highlighted"
		if(mDualPane) {
			ListView lv = getListView();
			lv.setItemChecked(position, true);
		}
		// Calls the parent activity's implementation of the
		// OnItemSelectedListener
		// so the activity can pass the event to the sibling fragment as
		// appropriate
		Cursor c = (Cursor) mAdapter.getItem(position);
		if(c != null) mListener.onItemSelected(position,
				c.getString(c.getColumnIndex(SyncConflicts.OXID)),
				c.getString(c.getColumnIndex(SyncConflicts.USERNAME)),
				c.getString(c.getColumnIndex(SyncConflicts.TIMESTAMP)));
		else mListener.onItemSelected(position, null, null, null);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("listPosition", mCurPosition);
	}

	public Loader<Cursor> onCreateLoader(int pId, Bundle pArgs) {
		// This is called when a new Loader needs to be created. We
		// only have one Loader, so we don't care about the ID.
		Uri baseUri = SyncConflicts.CONTENT_URI;
		String[] projection = new String[] { SyncConflicts._ID,
				SyncConflicts.OXID, SyncConflicts.USERNAME,
				SyncConflicts.TIMESTAMP };
		String[] selectionArgs;
		String selection;
		if(mCurFilter == null) {
			selectionArgs = null;
			selection = null;
		} else {
			selectionArgs = new String[] { "%" + mCurFilter.replace('*', '%')
					+ "%" };
			selection = SyncConflicts.USERNAME + " LIKE ?";
		}
		return new CursorLoader(getActivity(), baseUri, projection, selection,
				selectionArgs, SyncConflicts.TIMESTAMP);

	}

	public void onLoadFinished(Loader<Cursor> pLoader, Cursor pData) {
		// Swap the new cursor in. (The framework will take care of closing the
		// old cursor once we return.)
		mAdapter.swapCursor(pData);

		// The list should now be shown.
		if(isResumed()) {
			setListShown(true);
		} else {
			setListShownNoAnimation(true);
		}

	}

	public void onLoaderReset(Loader<Cursor> pLoader) {
		// This is called when the last Cursor provided to onLoadFinished()
		// above is about to be closed. We need to make sure we are no
		// longer using it.
		mAdapter.swapCursor(null);
	}

	/*****************************************************************************************************************************/
	/* --> .:Menu-Methoden:. <-- */
	/*****************************************************************************************************************************/
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.solveconflicts_fragment, menu);
		MenuItem item = menu.findItem(R.id.menu_search);
		View searchView = SearchViewCompat.newSearchView(getActivity());
		if(searchView != null) {
			SearchViewCompat.setOnQueryTextListener(searchView,
					new OnQueryTextListenerCompat() {
						@Override
						public boolean onQueryTextChange(String newText) {
							// Called when the action bar search text has
							// changed. Update
							// the search filter, and restart the loader to do a
							// new query
							// with this filter.
							mCurFilter = !TextUtils.isEmpty(newText) ? newText
									: null;
							getLoaderManager().restartLoader(0, null,
									ConflictsFragment.this);
							return true;
						}
					});
			MenuItemCompat.setActionView(item, searchView);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem pMenuItem) {
		switch (pMenuItem.getItemId()) {
		case R.id.menu_search:
			getActivity().onSearchRequested();
			break;
		}
		return super.onOptionsItemSelected(pMenuItem);
	}
}
