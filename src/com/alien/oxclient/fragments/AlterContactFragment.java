/*
 * Copyright (C) 2012  Christoph Haefner <ChristophHaefner@web.de>
 * 
 * OXClient is a AndroidApp to sync your phone with your
 * openXchange-Server at home.
 * 
 * This file is part of OXClient.
 *
 *  OXClient is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OXClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OXClient.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.alien.oxclient.fragments;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.ContactsContract.DisplayPhoto;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alien.oxclient.OXApplication;
import com.alien.oxclient.OXConstants;
import com.alien.oxclient.R;
import com.alien.oxclient.model.OXContact;
import com.alien.oxclient.provider.OXMapping;
import com.alien.oxclient.provider.OXMappingDatabaseHelper;
import com.alien.oxclient.ui.InformationTypes;
import com.alien.oxclient.ui.OXDateEditText;
import com.alien.oxclient.ui.OXEditText;
import com.alien.oxclient.ui.OXImageView;
import com.alien.oxclient.ui.Updateable;

public class AlterContactFragment extends Fragment {
	// Container Activity must implement this interface
	public interface OnAlterContactInterface {
		public OXContact getContact();

		public void forbidWriteAndSync();

		public void allowWriteAndSync();
	}

	private static int mPhotoDim;
	private OnAlterContactInterface mListener;
	private ListView mListView;
	private OXMappingDatabaseHelper mOxMappingDBHelper;
	private static final int ACTIVITY_TAKE_PICTURE = 23;
	private static final int ACTIVITY_CROP_PICTURE = 32;
	private static final int ACTIVITY_SELECT_PICTURE = 42;
	private static final String[] mProjection = new String[] {
			OXMapping.OXMAPPING_KEY, /* 0 */
			OXMapping.ALTERCONTACT_INFORMATIONTYPE, /* 1 */
			OXMapping.ALTERCONTACT_INPUTTYPE, /* 2 */
			OXMapping.ALTERCONTACT_TRANSLATION, /* 3 */
			OXMapping._ID };

	@Override
	public void onAttach(Activity pActivity) {
		super.onAttach(pActivity);
		try {
			mListener = (OnAlterContactInterface) pActivity;
		} catch (ClassCastException e) {
			throw new ClassCastException(pActivity.toString()
					+ " must implement OnAlterContactInterface");
		}
		mOxMappingDBHelper = new OXMappingDatabaseHelper(getActivity());
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPhotoDim = getPhotoPickSize();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mOxMappingDBHelper.close();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Bundle args = getArguments();
		int group = args.getInt(OXConstants.ALTERCONTACT_ARGS_KEY_GROUP);
		View parent = inflater.inflate(R.layout.altercontact_fragment, null);
		mListView = (ListView) parent
				.findViewById(R.id.altercontact_fragment_listview);
		mListView.setItemsCanFocus(true);
		TextView textViewHeader = (TextView) parent
				.findViewById(R.id.altercontact_fragment_header);
		textViewHeader.setText(args
				.getString(OXConstants.ALTERCONTACT_ARGS_LABEL));
		Cursor c = mOxMappingDBHelper.query(
				mProjection,
				OXMapping.ALTERCONTACT_INFORMATIONTYPE + " != ? AND "
						+ OXMapping.ALTERCONTACT_GROUP + " = ?",
				new String[] { String.valueOf(InformationTypes.NOT),
						String.valueOf(group) }, OXMapping.OX_POSITION, true);
		CursorAdapter cursorAdapter = new ContactModelAdapter(getActivity(),
				args, c, false);
		mListView.setAdapter(cursorAdapter);
		return parent;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		mListener.allowWriteAndSync();
		if(resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
			case ACTIVITY_SELECT_PICTURE:
				final Bitmap bitmap = data.getParcelableExtra("data");
				mListener.getContact().setPicture(bitmap);
				break;
			case ACTIVITY_TAKE_PICTURE:
				mListener.getContact().setPicture(
						(Bitmap) data.getExtras().get("data"));
				break;
			case ACTIVITY_CROP_PICTURE:
				mListener.getContact().setPicture(
						(Bitmap) data.getExtras().get("data"));
				break;
			}
			mListView.invalidateViews();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void startTake() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		startActivity(intent, ACTIVITY_TAKE_PICTURE);
	}

	private void startSelect() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT, null);
		intent.setType("image/*");
		addCropExtras(intent);
		startActivity(intent, ACTIVITY_SELECT_PICTURE);
	}

	private void startCrop() {
		if(mListener.getContact().getPicture() != null) {
			Intent intent = new Intent("com.android.camera.action.CROP");
			intent.setType("image/*");
			intent.putExtra("data", mListener.getContact().getPicture());
			addCropExtras(intent);
			startActivity(intent, ACTIVITY_CROP_PICTURE);
		}
	}

	private void startActivity(Intent pIntent, int pRequestCode) {
		mListener.forbidWriteAndSync();
		try {
			startActivityForResult(pIntent, pRequestCode);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(getActivity(),
					R.string.error_no_activity_for_intent, Toast.LENGTH_LONG)
					.show();
		}
	}

	private void addCropExtras(Intent pIntent) {
		pIntent.putExtra("crop", "true");
		pIntent.putExtra("scale", true);
		pIntent.putExtra("scaleUpIfNeeded", true);
		pIntent.putExtra("aspectX", 1);
		pIntent.putExtra("aspectY", 1);
		pIntent.putExtra("outputX", mPhotoDim);
		pIntent.putExtra("outputY", mPhotoDim);
		pIntent.putExtra("return-data", true);
	}

	private int getPhotoPickSize() {
		// Load the photo dimension to request.
		if(OXApplication.getUtils().isIcsOrBetter()) {
			Cursor c = getActivity().getContentResolver().query(
					DisplayPhoto.CONTENT_MAX_DIMENSIONS_URI,
					new String[] { DisplayPhoto.DISPLAY_MAX_DIM }, null, null,
					null);
			try {
				c.moveToFirst();
				return c.getInt(0);
			} finally {
				c.close();
			}
		} else {
			return 96;
		}
	}

	private class ContactModelAdapter extends CursorAdapter {

		private Bundle mArgs;

		public ContactModelAdapter(Context context, Bundle pArgs, Cursor c,
				boolean autoRequery) {
			super(context, c, autoRequery);
			this.mArgs = pArgs;
		}

		@Override
		public int getItemViewType(int position) {
			Cursor cursor = (Cursor) getItem(position);
			return cursor.getInt(1);
		}

		@Override
		public int getViewTypeCount() {
			return InformationTypes.values().length;
		}

		@Override
		public void bindView(View convertView, Context pContext, Cursor pCursor) {
			String key = pCursor.getString(0); // OXMapping.OXMAPPING_KEY
			String value = mListener.getContact().getContactInformation(key);
			String translation = OXApplication.getUtils().getStringFromName(
					pContext, pCursor.getString(3)); // OXMapping.ALTERCONTACT_TRANSLATION
			String contentDescription = getString(R.string.altercontact_row_contentdescription)
					+ " " + translation;

			TextView textView = (TextView) convertView
					.findViewById(android.R.id.text1);
			// TODO: Hardcodierte Textgroesse entfernen
			if(TextUtils.isEmpty(value)) textView.setTextSize(10.0f);
			textView.setText(translation);
			textView.setHint(translation);
			textView.setContentDescription(contentDescription);
			LinearLayout rootLayout = (LinearLayout) convertView
					.findViewById(android.R.id.content);
			((Updateable) rootLayout.getChildAt(1)).updateUi(pContext, key,
					mListener.getContact(), pCursor.getInt(2),
					mArgs.getBoolean(
							OXConstants.ALTERCONTACT_ARGS_SHOW_CALENDARVIEW,
							true), pCursor.getInt(4), key, contentDescription,
					translation);
		}

		@Override
		public View newView(Context pContext, Cursor pCursor, ViewGroup pParent) {
			String key = pCursor.getString(0); // OXMapping.OXMAPPING_KEY
			int id = pCursor.getInt(4);
			LinearLayout rootLayout = new LinearLayout(pContext);
			rootLayout.setId(android.R.id.content);
			rootLayout.setOrientation(LinearLayout.VERTICAL);
			TextView textView = new TextView(pContext);
			textView.setId(android.R.id.text1);
			rootLayout.addView(textView);
			int type = getItemViewType(pCursor.getPosition());
			if(type == InformationTypes.TEXT.ordinal()) {
				OXEditText addInfo = new OXEditText(pContext, key,
						mListener.getContact(), pCursor.getInt(2), id, key);
				rootLayout.addView(addInfo);
			} else if(type == InformationTypes.DATE.ordinal()) {
				// OXDatePicker addInfo = new OXDatePicker(
				// pContext,
				// key,
				// mListener.getContact(),
				// mArgs.getBoolean(
				// OXConstants.ALTERCONTACT_ARGS_SHOW_CALENDARVIEW,
				// true), id, key);
				OXDateEditText addInfo = new OXDateEditText(pContext, key,
						mListener.getContact(), id, key);
				rootLayout.addView(addInfo);
			} else if(type == InformationTypes.PIC.ordinal()) {
				buildPicture(rootLayout, pContext, key, id);
			}
			return rootLayout;
		}

		private void buildPicture(LinearLayout pRootLayout, Context pContext,
				String pKey, int pId) {
			OXImageView addInfo = new OXImageView(getActivity(),
					mListener.getContact(), pId, pKey);
			pRootLayout.addView(addInfo);
			Button buttonTakePicture = new Button(pContext);
			buttonTakePicture.setText(R.string.altercontact_takepicture);
			buttonTakePicture.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					startTake();
				}
			});
			pRootLayout.addView(buttonTakePicture);
			Button buttonSelectPicture = new Button(pContext);
			buttonSelectPicture.setText(R.string.altercontact_selectpicture);
			buttonSelectPicture.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					startSelect();
				}
			});
			pRootLayout.addView(buttonSelectPicture);
			Button buttonCropPicture = new Button(pContext);
			buttonCropPicture.setText(R.string.altercontact_croppicture);
			buttonCropPicture.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					startCrop();
				}
			});
			pRootLayout.addView(buttonCropPicture);
		}
	}
}
